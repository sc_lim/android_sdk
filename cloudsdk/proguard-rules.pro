# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
-keepattributes *Annotation*, SourceFile,LineNumberTable, Signature, Exceptions
#-dontoptimize
#-dontshrink
-keep class com.altimedia.cloud.comp.ui.IAnimationView{ *;}
-keep class com.altimedia.cloud.ApexCloudDoc{ *;}
-keep class com.altimedia.cloud.ApexCloudEventListener{ *;}
-keep class com.altimedia.cloud.ApexCloudManager{ *;}
-keep class com.altimedia.cloud.ApexCloudSession{ *;}
-keep class com.altimedia.cloud.ApexMessage{ *;}
-keep class com.altimedia.cloud.ApexMessageController{ *;}
-keep class com.altimedia.cloud.ApexMessageHandler{ *;}

-keep class * extends com.google.gson.TypeAdapter
-keep class * implements com.google.gson.TypeAdapterFactory
-keep class * implements com.google.gson.JsonSerializer
-keep class * implements com.google.gson.JsonDeserializer

-keep class com.altimedia.cloud.constant.SourceTypeConstant{ *;}
-keep class com.altimedia.cloud.system.server.parser.obj.ComponentType{ *;}
-keep class com.altimedia.cloud.system.server.parser.obj.ProtocolPackageType{ *;}
-keep class com.altimedia.cloud.system.server.parser.obj.ProtocolType{ *;}
-keepclassmembers,allowobfuscation class * {
  @com.google.gson.annotations.SerializedName <fields>;
}
#-keep class com.altimedia.cloud.system.server.parser.obj.animation.request.**{*;}
# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile
