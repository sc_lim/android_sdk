/*
 *
 *  * Copyright (C) $year. Altimedia Corporation. All Rights Reserved.
 *  *
 *  * This software is the confidential and proprietary information
 *  * of Altimedia Corporation. You may not use or distribute
 *  * this software except in compliance with the terms and conditions
 *  * of any applicable license agreement in writing between Altimedia
 *  * Corporation and you.
 *
 */

package com.altimedia.cloud;

public interface ApexMessageHandler {
    /**
     * notify WebApp message via ApexMessage object.
     * @param @ApexMessage object that is include WebApp message
     * @return true if message is consumed, otherwise should return false
     */
    boolean notifyWebAppMessage(ApexMessage message);
}
