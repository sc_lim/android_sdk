/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.constant;

/**
 * Created by IntelliJ IDEA.
 * User: gh.baek
 * Date: 2018. 05. 16
 * To change this template use File | Settings | File Templates.
 */
public interface ProtocolConstant {

    /**
     * input type id : client to server
     */
    public static final int
            input_type_key = 0x0001,
            input_type_mouse_button = 0x0002,
            input_type_mouse_absolute = 0x0004,
            input_type_mouse_relative = 0x0008,
            input_type_cqos_decoding_fps = 0x0010,
            input_type_mouse_scroll = 0x0020,
            input_type_mouse_wheel = 0x0800,
            input_type_mouse_resolution = 0x0400,
            input_type_mouse =
                    input_type_mouse_button |
                            input_type_mouse_absolute |
                            input_type_mouse_relative |
                            input_type_mouse_wheel;

    public static final int
            MESSAGE_TYPE_IMG = 0x11,
            MESSAGE_TYPE_KEY = 0x21,
            MESSAGE_TYPE_MSG = 0x22,
            MESSAGE_TYPE_DATA = 0x23,
            MESSAGE_TYPE_NOTI = 0x31,
            MESSAGE_TYPE_STR = 0x81;


    /**
     * command id : client to server
     */
    public static final int
            COMMAND_CREATE_SESSION = 0x0200,
            COMMAND_CLIENT_INIT = 0x0010,
            COMMAND_REQUEST_URL = 0x0020,
            COMMAND_REQUEST_FRAME = 0x0030,
            COMMAND_KEY_EVENT = 0x0040,
            COMMAND_MOUSE_EVENT = 0x0050,
            COMMAND_PING = 0x0060,
            COMMAND_MESSAGE_TO_WEB = 0x0070,
            COMMAND_QUIT = 0x0080;

    /**
     * command id : server to client
     */
    public static final int
            COMMAND_RESPONSE_STATUS = 0X0110,
            COMMAND_RESPONSE_PAGE_INFO = 0X0120,
            COMMAND_UPDATE_FRAME = 0X0011,
            COMMAND_RESPONSE_PING = 0X0160,
            COMMAND_RESPONSE_WEB_MESSAGE = 0X0022,
            COMMAND_RESPONSE_NOTIFY = 0X0031;

    /**
     * element id : client to server
     */
    public static final int
            REQUEST_ENCODING_TYPE = 0x0011,
            WINDOW_SIZE = 0x0012,
            MAC_ADDRESS = 0x0013,
            REQUEST_URL = 0x0021,
            INCREMENTAL_UPDATE_FLAG = 0x0031,
            UPDATE_RECT = 0x0032,
            EVENT_TYPE = 0x0041,
            KEY_CODE = 0x0042,
            KEY_CHAR = 0x0043,
            BUTTON_FLAG = 0x0051,
            MOUSE_POSITION = 0x0052,
            REQUEST_MESSAGE = 0x0071;

    /**
     * element id : server to client
     */
    public static final int
            STATUS_CODE = 0x0111,
            STATUS_MESSAGE = 0x0112,
            TITLE = 0x0121,
            URL = 0x0122,
            CONTENT_SIZE = 0x0123,
            SEQUENCE = 0x0131,
            RESPONSE_ENCODING_TYPE = 0x0132,
            RECT = 0x0133,
            IMAGE_DATA_SIZE = 0x0134,
            IMAGE_DATA = 0x0135,
            RESPONSE_MESSAGE = 0x0171,
            NOTIFY_CODE = 0x0191,
            NOTIFY_MESSAGE = 0x0192;

    /**
     * notify code
     */
    public static final int NOTIFYING_MESSAGE_PAGE_ERROR = 0xEE;
    public static final int NOTIFYING_MESSAGE_DISCONNECT = 0xFF;
    public static final int NOTIFYING_MESSAGE_CONNECT = 0xFE;

}
