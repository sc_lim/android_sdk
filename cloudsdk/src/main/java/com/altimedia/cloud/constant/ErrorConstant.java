/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.constant;

import com.altimedia.cloud.ApexCloudSession;

/**
 * Created by IntelliJ IDEA.
 * User: gh.baek
 * Date: 2018. 5. 31
 * To change this template use File | Settings | File Templates.
 */
public interface ErrorConstant {

    /**
     * cloud server 연결 실패
     * </br> {@link ApexCloudSession#start()} 에서 수행 중 발생
     */
    public static final int CONNECT_SERVER_ERROR = 1002;


    /**
     * 서버에서 응답을 받지 못했을 때 (Ping/Pong)
     */
    public static final int ERR_DECTECTED_DISCONNECTION_RECV = 1003;


    /**
     * Connect failed - Bad Request
     */
    public static final int ERR_SERVER_BAD_REQUEST = 1101;

    /**
     * Connect failed - Session Full (Not acceptable session)
     */
    public static final int ERR_SERVER_SESSION_FULL = 1102;

    /**
     * Send Error
     */
    public static final int ERR_DECTECTED_DISCONNECTION_SEND = 1201;

    /**
     * 이미지 수신 스레드 생성 Error
     */
    public static final int ERR_RECV_THREAD_INVALID = 1202;

    /**
     * Image Decode Error
     */
    public static final int ERR_IMAGE_DECODE = 1203;
}
