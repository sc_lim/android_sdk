/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.constant;

/**
 * Created by IntelliJ IDEA.
 * User: gh.baek
 * Date: 2018. 5. 29
 * To change this template use File | Settings | File Templates.
 */
public interface SourceTypeConstant {

    /**
     * encoding type
     */
    public static final int FRAME_ENCODING_BMP = 0x01;
    public static final int FRAME_ENCODING_JPG = 0x02;
    public static final int FRAME_ENCODING_PNG = 0x04;
    public static final int FRAME_ENCODING_WEBP = 0x05;
    public static final int FRAME_ENCODING_NONE = 0x99; // Just Update
}
