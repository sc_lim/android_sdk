/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.constant;

/**
 * Created by IntelliJ IDEA.
 * User: gh.baek
 * Date: 2018. 5. 14
 * To change this template use File | Settings | File Templates.
 */
public interface LogModeConstant {
    public static final int DEBUG = 0;
    public static final int INFO = 1;
    public static final int ERROR = 2;
    public static final int NONE = 3;
}
