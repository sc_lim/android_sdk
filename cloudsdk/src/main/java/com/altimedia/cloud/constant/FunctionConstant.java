/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.constant;

/**
 * Created by IntelliJ IDEA.
 * User: gh.baek
 * Date: 2018. 5. 24
 * To change this template use File | Settings | File Templates.
 */
public interface FunctionConstant {
    //key event type
    public static final int SEND_KEY_PRESS = 0X01;
    public static final int SEND_KEY_RELEASE = 0x02;
    public static final int SEND_KEY_MOVE = 0X03;
    public static final int SEND_KEY_WHEEL = 0X04;
}
