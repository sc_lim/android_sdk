/*
 *
 *  * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *  *
 *  * This software is the confidential and proprietary information
 *  * of Altimedia Corporation. You may not use or distribute
 *  * this software except in compliance with the terms and conditions
 *  * of any applicable license agreement in writing between Altimedia
 *  * Corporation and you.
 *
 */

package com.altimedia.cloud;


import com.altimedia.cloud.constant.SourceTypeConstant;
import com.altimedia.cloud.system.util.apexLogger;

/**
 * Created by IntelliJ IDEA.
 * User: gh.baek
 * Date: 2018. 5. 14
 * To change this template use File | Settings | File Templates.
 */

public final class ApexCloudDoc {
    int encodingType = SourceTypeConstant.FRAME_ENCODING_PNG;

    private String cloudIP;
    private int cloudPort = 9006;
    private String initialURL;

    private int xPos = 0;
    private int yPos = 0;
    private int width = 1280;
    private int height = 720;

    private String deviceID;
    private int priority;
    private int serverConnectTimeOut = 15 * 1000;
    private int pingPeriod = 10 * 1000;
    private int zOrder = 0;
    // add by ghbaek 2018.10.02
    private String Optional;

    /**
     * ping 주기 </br> 기본값은 10초
     *
     * @return millisecond
     */
    public int getPingPeriod() {
        return pingPeriod;
    }

    /**
     * sdk 내부에서 자동으로 ping을 수행하는데 이때의 ping 주기를 설정한다. </br>
     * 일정 시간(서버에서 설정)동안 ping이 클라우드 서버로 전달이 안될 경우, 서버는 해당 연결을 끊는다.
     * </br> 해당 값의 변경을 원할 경우는, 반드시 클라우드 서버 담당자와 협의 후 변경해야 한다.
     * </br> default 10초
     *
     * @param pingPeriod millisecond
     */
    public void setPingPeriod(int pingPeriod) {
        this.pingPeriod = pingPeriod;
    }

    /**
     * 이미지 인코딩 타입 </br>
     *
     * @return {@link SourceTypeConstant}
     * @see SourceTypeConstant
     */
    public int getEncodingType() {
        return encodingType;
    }

    /**
     * Image encoding tyoe
     * @param encodingType
     */
    public void setEncodingType(int encodingType) {
        this.encodingType = encodingType;
    }

    /**
     * stb device id
     *
     * @return string
     */
    public String getDeviceID() {
        return deviceID;
    }

    /**
     * stb의 고유 id
     */
    public void setDeviceID(String stbID) {
        this.deviceID = stbID;
    }

    public int getXPos() {
        return xPos;
    }

    public int getYPos() {
        return yPos;
    }

    public int getWidth() {
        return width;
    }

    /**
     * 화면 사이즈 </br>default 1280
     *
     * @param w
     */
    public void setWidth(int w) {
        width = w;
    }

    public int getHeight() {
        return height;
    }

    /**
     * 화면 사이즈 </br>default 720
     *
     * @param h
     */
    public void setHeight(int h) {
        height = h;
    }

    /**
     * 서비스 대상 URL
     *
     * @return string
     */
    public String getURL() {
        return initialURL;
    }

    /**
     * 서비스 대상 URL
     *
     * @param path
     */
    public void setURL(String path) {
        initialURL = null;
        initialURL = path;
    }

    /**
     * cloud server ip
     *
     * @return string
     */
    public String getCloudIP() {
        return cloudIP;
    }

    /**
     * cloud server ip
     *
     * @param ip
     */
    public void setCloudIP(String ip) {
        cloudIP = null;
        cloudIP = ip;
    }

    /**
     * cloud server port </br> default 9006
     *
     * @return int
     */
    public int getCloudPort() {
        return cloudPort;
    }

    /**
     * cloud server port </br> default 9006
     *
     * @param port
     */
    public void setCloudPort(int port) {
        cloudPort = port;
    }

    /**
     * cloud parameter option
     *
     * @return string
     */
    public String getOption() {
        return Optional;
    }

    /**
     * set optional parameter to be sent for the server
     * @param option
     */
    public void setOption(String option) {
        Optional = null;
        Optional = option;
    }

    public int getServerConnectTimeOut() {
        return serverConnectTimeOut;
    }

    /**
     * 클라우드 서버와의 연결 socket에 대한 sotimeout 값을 설정한다.</br>
     * default 10 * 1000
     *
     * @param milliseconds
     */
    public void setServerConnectTimeOut(int milliseconds) {
        serverConnectTimeOut = milliseconds;
    }


    /**
     * clean values as default
     */
    public void cleanup() {
        cloudIP = null;
        initialURL = null;
        deviceID = null;
        if (apexLogger.INFO) apexLogger.out("[9] ApexCloudDoc.cleanup ----------- END !!!");
    }

    /**
     * priority for screen layer
     * @return
     */
    public int getPriority() {
        return priority;
    }

    /**
     * priority for screen layer
     * @param priority
     */
    public void setPriority(int priority) {
        this.priority = priority;
    }

//    @Override
//    public String toString() {
//        return "ApexCloudDoc{" +
//                "encodingType=" + encodingType +
//                ", cloudIP='" + cloudIP + '\'' +
//                ", cloudPort=" + cloudPort +
//                ", initialURL='" + initialURL + '\'' +
//                ", xPos=" + xPos +
//                ", yPos=" + yPos +
//                ", width=" + width +
//                ", height=" + height +
//                ", deviceID='" + deviceID + '\'' +
//                ", serverConnectTimeOut=" + serverConnectTimeOut +
//                ", pingPeriod=" + pingPeriod +
//                ", zOrder=" + zOrder +
//                ", Optional='" + Optional + '\'' +
//                '}';
//    }
}
