/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.comp;

import android.content.Context;
import android.util.Log;

import com.altimedia.cloud.system.util.apexLogger;
import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.cache.ExternalPreferredCacheDiskCacheFactory;
import com.bumptech.glide.load.engine.cache.LruResourceCache;
import com.bumptech.glide.module.AppGlideModule;
import com.bumptech.glide.request.RequestOptions;

@GlideModule
public
class ComponentGlideModule extends AppGlideModule {
    private static final ComponentGlideModule instance = new ComponentGlideModule();

    private int memoryCacheSizeBytes;
    private int diskCacheSizeBytes;

    public static ComponentGlideModule getInstance() {
        return instance;
    }

    public ComponentGlideModule() {
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
        final int cacheSize = maxMemory / 4;
        int diskCacheSizeBytes = 1024 * 1024 * 100; // 100 MB

        init(cacheSize, diskCacheSizeBytes);
    }

    public void init(final int memoryCacheSizeBytes, final int diskCacheSizeBytes) {
        this.memoryCacheSizeBytes = memoryCacheSizeBytes;
        this.diskCacheSizeBytes = diskCacheSizeBytes;
    }

    @Override
    public void applyOptions(Context context, GlideBuilder builder) {
        builder.setDefaultRequestOptions(new RequestOptions().format(DecodeFormat.PREFER_ARGB_8888));

        builder.setMemoryCache(new LruResourceCache(memoryCacheSizeBytes));
//        builder.setBitmapPool(new LruBitmapPool(memoryCacheSizeBytes)); // TODO
        builder.setDiskCache(new ExternalPreferredCacheDiskCacheFactory(context, "componentCacheFolder", diskCacheSizeBytes));

        builder.setLogLevel((apexLogger.DEBUG || apexLogger.CACHE) ? Log.DEBUG : Log.ERROR);
    }

    @Override
    public boolean isManifestParsingEnabled() {
        return false;
    }
}
