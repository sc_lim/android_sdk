/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */
package com.altimedia.cloud.comp;

import android.graphics.Color;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.concurrent.TimeUnit;

public class ComponentConfig {
    public static final long DEFAULT_FPS = 24;
    public static final long FRAME_DELAY = TimeUnit.SECONDS.toMillis(1) / DEFAULT_FPS;

    public static boolean showBounds = false;
    public static boolean showCacheBounds = false;
    public static boolean showComponentCacheBounds = false;
    public static boolean showComponentPreloadBounds = false;
    public static final boolean IS_USE_COMPONENT_RESULT_CALLBACK = false;

    public static final int COLOR_REPAINT_REGION = Color.YELLOW;
    public static final int COLOR_BOUNDS = Color.BLUE;
    public static final int COLOR_CACHE_NONE = Color.TRANSPARENT;
    public static final int COLOR_CACHE_ICS_BOUNDS = Color.DKGRAY;
    public static final int COLOR_CACHE_COMPONENT_BOUNDS = Color.GRAY;
    public static final int COLOR_PRELOAD_COMPONENT_BOUNDS = Color.GREEN;
    public static final int COLOR_CACHE_DISK_COMPONENT_BOUNDS = Color.CYAN;

    public static final DiskCacheStrategy DISK_CACHE_STRATEGY = DiskCacheStrategy.RESOURCE;

    public static int getCacheColor(DataSource dataSource) {
        int color = COLOR_CACHE_NONE;
        switch (dataSource) {
            case REMOTE:
                color = COLOR_CACHE_NONE;
                break;
            case MEMORY_CACHE:
                color = COLOR_CACHE_COMPONENT_BOUNDS;
                break;
            case LOCAL:
            case DATA_DISK_CACHE:
            case RESOURCE_DISK_CACHE:
                color = COLOR_CACHE_DISK_COMPONENT_BOUNDS;
                break;
        }
        return color;
    }
}