/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.comp.ic;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.SurfaceTexture;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.TextureView;
import android.view.View;

import androidx.annotation.NonNull;

import com.altimedia.cloud.system.server.parser.obj.CsFrame;
import com.altimedia.cloud.system.util.apexLogger;

/**
 * Created by mc.kim on 08,04,2020
 */
public class CsTextureView extends TextureView implements TextureView.SurfaceTextureListener, CsCloudView {
    private final String TAG = CsTextureView.class.getSimpleName();
    private OnWindowCallback mOnWindowCallback = null;
    public final ApexCloudView cloudView;
    private Context mContext = null;

    public CsTextureView(@NonNull Context context, @NonNull OnWindowCallback onWindowCallback) {
        super(context);
        this.mContext = context;
        this.mOnWindowCallback = onWindowCallback;
        this.cloudView = new ApexCloudView(context, this);
        setSurfaceTextureListener(this);

        setLayerType(View.LAYER_TYPE_HARDWARE, null);
    }

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
//        this.cloudView.drawPlaceHolder();
        if (apexLogger.DEBUG) {
            Log.d(TAG, "onSurfaceTextureAvailable");
        }
        this.cloudView.onPrepared(true);

    }


    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {

    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
        this.cloudView.onPrepared(false);
        return false;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {

    }

    @Override
    public Canvas lockCanvas(Rect dirty) {
        Canvas canvas = super.lockCanvas(dirty);

        return canvas;
    }

    @Override
    public void unlockCanvasAndPost(Canvas canvas) {
        super.unlockCanvasAndPost(canvas);
    }

    @Override
    public View getPlayerView() {
        return this;
    }

    @Override
    public void release() {
        this.cloudView.clear(getContext());
    }

    @Override
    public void updateFragment(CsFrame csFrame) {
        if (apexLogger.DEBUG) {
            Log.d(TAG, "updateFragment : " + System.currentTimeMillis());
            Log.d(TAG, "updateFragment csFrame : " + csFrame);
        }
        cloudView.updateFragment(csFrame);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.mOnWindowCallback.onAttachedToWindow();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.mOnWindowCallback.onDetachedFromWindow();
    }

    @Override
    public void notifyStartApp(String appName) {
        if (apexLogger.DEBUG) {
            Log.d(TAG, "notifyStartApp : " + appName + " | time : " + System.currentTimeMillis());
        }
//        cloudView.setCacheEnable(appName, is720Window(mContext));
        cloudView.setCacheEnable(appName, true);
    }

    private boolean is720Window(Context context) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return metrics.heightPixels == 720;
//        return metrics.heightPixels == 1080;
    }
}
