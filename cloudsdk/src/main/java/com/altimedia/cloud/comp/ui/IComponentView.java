/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.comp.ui;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.BounceInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

import com.altimedia.cloud.ApexCloudSession;
import com.alticast.cloud.cloudsdk.R;
import com.altimedia.cloud.comp.CacheUtil;
import com.altimedia.cloud.comp.ComponentConfig;
import com.altimedia.cloud.comp.ImageUtil;
import com.altimedia.cloud.comp.ic.OnWindowCallback;
import com.altimedia.cloud.system.server.parser.obj.BaseRequest;
import com.altimedia.cloud.system.server.parser.obj.BaseResponse;
import com.altimedia.cloud.system.server.parser.obj.ComponentType;
import com.altimedia.cloud.system.server.parser.obj.ProtocolType;
import com.altimedia.cloud.system.server.parser.obj.animation.request.animation.Action;
import com.altimedia.cloud.system.server.parser.obj.animation.request.animation.InterpolatorType;
import com.altimedia.cloud.system.server.parser.obj.animation.request.animation.close.AnimationCloseData;
import com.altimedia.cloud.system.server.parser.obj.animation.request.animation.create.ActionItem;
import com.altimedia.cloud.system.server.parser.obj.animation.request.animation.create.AnimationCreateData;
import com.altimedia.cloud.system.server.parser.obj.animation.request.animation.create.Param;
import com.altimedia.cloud.system.server.parser.obj.animation.request.animation.finish.AnimationFinishData;
import com.altimedia.cloud.system.server.parser.obj.animation.request.component.attach.ComponentAttachData;
import com.altimedia.cloud.system.server.parser.obj.animation.request.component.attach.DestComponent;
import com.altimedia.cloud.system.server.parser.obj.animation.request.component.attach.SrcComponent;
import com.altimedia.cloud.system.server.parser.obj.animation.request.component.create.ComponentData;
import com.altimedia.cloud.system.server.parser.obj.animation.request.component.create.CreateComponentData;
import com.altimedia.cloud.system.server.parser.obj.animation.request.component.create.CreateSourceData;
import com.altimedia.cloud.system.server.parser.obj.animation.request.component.create.RectData;
import com.altimedia.cloud.system.server.parser.obj.animation.request.component.delete.ComponentDeleteData;
import com.altimedia.cloud.system.server.parser.obj.animation.request.component.detach.ComponentDetachData;
import com.altimedia.cloud.system.server.parser.obj.animation.request.component.rect.FillRectData;
import com.altimedia.cloud.system.server.parser.obj.animation.request.component.visible.ComponentVisibleData;
import com.altimedia.cloud.system.server.parser.obj.animation.response.CreateAnimationResponse;
import com.altimedia.cloud.system.server.parser.obj.animation.response.CreateDataResponse;
import com.altimedia.cloud.system.util.ElapsedTime;
import com.altimedia.cloud.system.util.apexLogger;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.transition.Transition;
import com.bumptech.glide.signature.ObjectKey;
import com.google.gson.Gson;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import static com.altimedia.cloud.comp.ComponentConfig.DISK_CACHE_STRATEGY;
import static com.altimedia.cloud.comp.ComponentConfig.IS_USE_COMPONENT_RESULT_CALLBACK;

/**
 * Created by mc.kim on 10,04,2020
 */
public class IComponentView extends RelativeLayout implements IAnimation {
    private final String TAG = IComponentView.class.getSimpleName();
    private final OnWindowCallback mOnWindowCallback;
    private final int ROOT_ID = 100000;
    private final int BACK_ID = 99;
    private HashMap<Integer, View> mCacheView = new HashMap<>();
    private HashMap<String, AnimatorSet> mCacheAnimation = new HashMap<>();
    private HashMap<Integer, Integer> mCacheAnimationViewId = new HashMap<>();
    private final ApexCloudSession mApexCloudSession;
    private ImageUtil mImageUtil = null;

    public IComponentView(Context context, ApexCloudSession apexCloudSession, OnWindowCallback windowCallback) {
        super(context);
        mImageUtil = new ImageUtil(context);
        this.mApexCloudSession = apexCloudSession;
        this.mOnWindowCallback = windowCallback;
        setId(ROOT_ID);
        setTag(ROOT_ID);
        idMap.add(ROOT_ID);
    }

    private boolean clearAnimation(final int id) {
        if (mCacheAnimationViewId.containsKey(id)) {
            int requestId = mCacheAnimationViewId.get(id);
            AnimatorSet set = mCacheAnimation.get(requestId + ":" + id);
            if (set != null) {
                set.end();
                return true;
            }
        }
        return false;
    }

    private boolean isSupportedSpriteFunction(View view) {
        return view.getTag(R.id.key_srcImageList) != null;
    }

    private void createAnimation(final AnimationCreateData animationCreateData) {
        HashMap<Integer, List<ActionItem>> propertyValuesHolders = generatePropertyHolderMap(animationCreateData.getData().getActionItem());
        Iterator<Integer> ids = propertyValuesHolders.keySet().iterator();
        try {
            setAnimatorScale(getContext(), 1f);
        } catch (Exception e) {
            e.printStackTrace();
        }
        while (ids.hasNext()) {
            final int id = ids.next();
            boolean isCanceledItem = clearAnimation(id);
            List<ActionItem> holderList = propertyValuesHolders.get(id);
            final View targetView = findViewById(id);
            if (targetView == null) {
                continue;
            }
            ArrayList<Animator> animatorList = buildAnimator(isCanceledItem, holderList, targetView);
            final AnimatorSet animatorSet = new AnimatorSet();
            animatorSet.setTarget(targetView);
            animatorSet.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    mCacheAnimation.remove(animationCreateData.getRequestId() + ":" + id);
                    mCacheAnimationViewId.remove(targetView.getId());
                    CreateAnimationResponse response = new CreateAnimationResponse(animationCreateData.getRequestId(), animationCreateData.getProtocolType());
                    response.setResult(true);
                    response.setResultData(3);
                    sendResult(response);

                }

                @Override
                public void onAnimationStart(Animator animation) {
                    super.onAnimationStart(animation);
                    mCacheAnimation.put(animationCreateData.getRequestId() + ":" + targetView.getId(), animatorSet);
                    mCacheAnimationViewId.put(targetView.getId(), animationCreateData.getRequestId());
                    CreateAnimationResponse response = new CreateAnimationResponse(animationCreateData.getRequestId(), animationCreateData.getProtocolType());
                    response.setResult(true);
                    response.setResultData(1);
                    sendResult(response);
                }
            });
            animatorSet.playTogether(animatorList);
            animatorSet.start();
        }
    }

    private Method scaleMethod = null;

    private void setAnimatorScale(Context context, float value) throws InvocationTargetException, IllegalAccessException, ClassNotFoundException {
        float durationScale = Settings.Global.getFloat(context.getContentResolver(),
                Settings.Global.ANIMATOR_DURATION_SCALE, 0);
        if (durationScale != value) {
            Object[] args = new Object[]{value};
            if (scaleMethod == null) {
                String methodName = "setDurationScale";
                Class c = Class.forName("android.animation.ValueAnimator");
                Method[] methods = c.getMethods();
                for (Method m : methods) {
                    if (methodName.equals(m.getName())) {
                        // for static methods we can use null as instance of class
                        scaleMethod = m;
                        break;
                    }
                }
            }
            if (scaleMethod != null) {
                scaleMethod.invoke(null, args);
            }
        }
    }


    private void cancelAnimation(AnimationCloseData animationCloseData) {
        boolean isSuccess = false;
        int requestID = animationCloseData.getData().getAnimateId();
        if (mCacheAnimation.containsKey(requestID + ":" + getViewIdByAnimationRequestId(requestID))) {
            AnimatorSet set = mCacheAnimation.get(requestID + ":" + getViewIdByAnimationRequestId(requestID));
            set.cancel();
            isSuccess = true;
        }
        BaseResponse animationResponse = new BaseResponse(animationCloseData.getRequestId(), animationCloseData.getProtocolType());
        animationResponse.setResult(isSuccess);
        sendResult(animationResponse);
    }

    private int getViewIdByAnimationRequestId(int requestID) {
        Iterator<Integer> viewIdList = mCacheAnimationViewId.keySet().iterator();
        while (viewIdList.hasNext()) {
            int viewId = viewIdList.next();
            if (mCacheAnimationViewId.get(viewId) == requestID) {
                return viewId;
            }
        }
        return -1;
    }

    private void finishAnimation(AnimationFinishData animationFinishData) {
        boolean isSuccess = false;
        int requestID = animationFinishData.getData().getAnimateId();
        if (mCacheAnimation.containsKey(requestID + ":" + getViewIdByAnimationRequestId(requestID))) {
            AnimatorSet set = mCacheAnimation.get(requestID + ":" + getViewIdByAnimationRequestId(requestID));
            set.end();
            isSuccess = true;
        }
        BaseResponse animationResponse = new BaseResponse(animationFinishData.getRequestId(), animationFinishData.getProtocolType());
        animationResponse.setResult(isSuccess);
        sendResult(animationResponse);
    }


    private HashMap<Integer, List<ActionItem>> generatePropertyHolderMap(List<ActionItem> items) {
        HashMap<Integer, List<ActionItem>> resultMap = new HashMap<>();

        for (ActionItem item : items) {
            Integer objectId = item.getObjectid();
            List<ActionItem> holders = null;
            if (resultMap.containsKey(objectId)) {
                holders = resultMap.get(objectId);
            } else {
                holders = new ArrayList<>();
            }
            holders.add(item);
            resultMap.put(objectId, holders);
        }
        return resultMap;
    }

    private ArrayList<Animator> buildAnimator(boolean isCanceledItem, List<ActionItem> actionItemList, final View targetView) {
        ArrayList<Animator> animatorList = new ArrayList<>();

        for (ActionItem actionItem : actionItemList) {
            Action action = actionItem.getAction();
            Param param = actionItem.getParam();
            switch (action) {
                case sprite: {
                    if (!isSupportedSpriteFunction(targetView)) {
                        if (apexLogger.DEBUG) {
                            Log.d(TAG, "sprite continue");
                        }

                        continue;
                    }
                    Bitmap[] bitmaps = (Bitmap[]) targetView.getTag(R.id.key_srcImageList);
                    long duration = actionItem.getDuration();
                    int perDuration = (int) (duration / bitmaps.length);
                    AnimationDrawable animationDrawable = new AnimationDrawable();
                    for (Bitmap bitmap : bitmaps) {
                        int startIndex = param.getStartIndex();
                        int repeat = param.getRepeat();
                        Log.d(TAG, "sprite bitmap");
                        Drawable drawable = new BitmapDrawable(bitmap);
                        animationDrawable.addFrame(drawable, perDuration);
                        animationDrawable.setOneShot(repeat == 1);
                    }
                    ((ImageView) targetView).setBackground(animationDrawable);
                    ((AnimationDrawable)targetView.getBackground()).start();
                    break;
                }

                case fade: {

                    ObjectAnimator fadeAnimator = ObjectAnimator.ofFloat(targetView, "alpha", param.getExtraAlphaStart() / 100f, param.getExtraAlphaEnd() / 100f);
                    fadeAnimator.setInterpolator(generateInterpolator(actionItem.getInterpolatorType()));
                    fadeAnimator.setDuration(actionItem.getDuration());
                    animatorList.add(fadeAnimator);
                }
                break;
                case scale:
                case bounds: {

                    ObjectAnimator translationX = ObjectAnimator.ofFloat(targetView, "translationX", targetView.getTranslationX() + param.getTranslationX());
                    translationX.setInterpolator(generateInterpolator(actionItem.getInterpolatorType()));
                    translationX.setDuration(actionItem.getDuration());

                    ObjectAnimator translationY = ObjectAnimator.ofFloat(targetView, "translationY", targetView.getTranslationY() + param.getTranslationY());
                    translationY.setInterpolator(generateInterpolator(actionItem.getInterpolatorType()));
                    translationY.setDuration(actionItem.getDuration());
                    ObjectAnimator scaleX = ObjectAnimator.ofFloat(targetView, "scaleX", param.getScaleX() * targetView.getScaleX());
                    scaleX.setInterpolator(generateInterpolator(actionItem.getInterpolatorType()));
                    scaleX.setDuration(actionItem.getDuration());
                    ObjectAnimator scaleY = ObjectAnimator.ofFloat(targetView, "scaleY", param.getScaleY() * targetView.getScaleY());
                    scaleY.setInterpolator(generateInterpolator(actionItem.getInterpolatorType()));
                    scaleY.setDuration(actionItem.getDuration());

                    animatorList.add(translationX);
                    animatorList.add(translationY);
                    animatorList.add(scaleX);
                    animatorList.add(scaleY);
                }
                break;
            }
        }


        return animatorList;
    }


    private Interpolator generateInterpolator(InterpolatorType interpolatorType) {
        Log.d(TAG, "generateInterpolator interpolatorType: " + interpolatorType);
        switch (interpolatorType) {
            case linear:
                return new LinearInterpolator();
            case bounce:
                return new BounceInterpolator();
            case elastic:
                return new AccelerateInterpolator();
        }
        return null;
    }

    private void sendResult(Object o) {
        String result = new Gson().toJson(o);
        if (apexLogger.DEBUG) {
            Log.d(TAG, "sendResult to web : " + result);
        }
        try {
            mApexCloudSession.sendWeb(result);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private LayoutParams initContainer(CreateSourceData source) {
        int sourceWidth = source.getWidth();
        int sourceHeight = source.getHeight();
        final int webWidth = 1280;
        final int webHeight = 720;

        boolean isOverWidth = sourceWidth >= webWidth;
        boolean isOverHeight = sourceHeight >= webHeight;
        LayoutParams params = new LayoutParams(isOverWidth ? ViewGroup.LayoutParams.MATCH_PARENT : sourceWidth,
                isOverHeight ? ViewGroup.LayoutParams.MATCH_PARENT : source.getHeight());

        if (isOverWidth || isOverHeight) {
            if (isOverWidth) {
                params.rightMargin = webWidth - source.getWidth();
                params.leftMargin = webWidth - source.getWidth();
            }
            if (isOverHeight) {
                params.topMargin = webHeight - source.getHeight();
                params.bottomMargin = webHeight - source.getHeight();
            }
        }

        return params;
    }

    private LayoutParams initContainerRect(RectData source) {
        int sourceWidth = source.getWidth();
        int sourceHeight = source.getHeight();
        final int webWidth = 1280;
        final int webHeight = 720;

        boolean isOverWidth = sourceWidth >= webWidth;
        boolean isOverHeight = sourceHeight >= webHeight;
        LayoutParams params = new LayoutParams(isOverWidth ? ViewGroup.LayoutParams.MATCH_PARENT : sourceWidth,
                isOverHeight ? ViewGroup.LayoutParams.MATCH_PARENT : source.getHeight());

        if (isOverWidth || isOverHeight) {
            if (isOverWidth) {
                params.rightMargin = webWidth - source.getWidth();
                params.leftMargin = webWidth - source.getWidth();
            }
            if (isOverHeight) {
                params.topMargin = webHeight - source.getHeight();
                params.bottomMargin = webHeight - source.getHeight();
            }
        }

        return params;
    }


    private void createComponent(CreateComponentData createComponentData) {
        ArrayList<ComponentData> componentDatas = createComponentData.getData();

        LoadComponentResultCallback loadComponentResultCallback = new LoadComponentResultCallback(this, getContext(), mApexCloudSession);
        loadComponentResultCallback.setComponentBaseInfo(createComponentData.getRequestId(), createComponentData.getProtocolType(), componentDatas.size());

        if (componentDatas != null) {
            boolean isSuccess = true;
            for (ComponentData componentData : componentDatas) {
                ComponentType type = componentData.getType();
                CreateSourceData sourceData = componentData.getSource();
               final ResourceItem item = new ResourceItem(type == ComponentType.root ? ROOT_ID : generatedItemId(), componentData);

                if (apexLogger.DEBUG) {
                    Log.d(TAG, "createComponent create components ResourceItem : " + item);
                }

                switch (type) {
                    case root:
                        mCacheView.put(item.getId(), this);
                        loadComponentResultCallback.onLoadStart(item.getId(), getHeight(), getHeight());
                        loadComponentResultCallback.onLoaded(true, item.getId(), getWidth(), getHeight());
                        break;
                    case container:
                        RelativeLayout itemView = new RelativeLayout(getContext());
                        itemView.setLayoutParams(initContainer(sourceData));
                        itemView.setTag(item.getId());
                        itemView.setId(item.getId());
                        itemView.setVisibility(componentData.isVisible() ? View.VISIBLE : View.INVISIBLE);
                        mCacheView.put(item.getId(), itemView);

                        loadComponentResultCallback.onLoadStart(item.getId(), itemView.getWidth(), itemView.getHeight());
                        loadComponentResultCallback.onLoaded(true, item.getId(), itemView.getWidth(), itemView.getHeight());
                        break;
                    case raw:
                        if (apexLogger.DEBUG) {
                            Log.d(TAG, "createComponent raw: not support yet");
                        }
                        loadComponentResultCallback.onLoadStart(item.getId(), 0, 0);
                        loadComponentResultCallback.onLoaded(true, item.getId(), 0, 0);
                        break;
                    case url:
                        loadImage(item, loadComponentResultCallback);
                        break;
                    case file:
                        if (apexLogger.DEBUG) {
                            Log.d(TAG, "createComponent file: not support yet");
                        }
                        loadImageFile(item, loadComponentResultCallback);
                        break;
                    case binary:
                        loadBinaryImage(item, loadComponentResultCallback);
                        break;
                    case sprite:
                        loadSpriteImage(item, loadComponentResultCallback);
                        break;
                    default:
                        break;
                }
            }
            // Optional
            loadComponentResultCallback.onResult(isSuccess);
        }

    }

    private boolean detachComponent(
            com.altimedia.cloud.system.server.parser.obj.animation.request.component.detach.Data componentDetachData) {
        boolean isSuccess = false;
        int viewId = componentDetachData.getSrcComponent().getObjectid();
        final View targetView = findViewById(viewId);
        if (targetView != null) {
            clearAnimation(targetView.getId());
            ((ViewGroup) targetView.getParent()).removeView(targetView);
            isSuccess = true;
        }
        return isSuccess;
    }

    private void detachComponents(ComponentDetachData componentDetachData) {
        boolean isSuccess = true;
        boolean isDetached = false;
        List<com.altimedia.cloud.system.server.parser.obj.animation.request.component.detach.Data> detachList =
                componentDetachData.getData();
        for (com.altimedia.cloud.system.server.parser.obj.animation.request.component.detach.Data data : detachList) {
            isDetached = detachComponent(data);
            if (!isDetached) {
                isSuccess = false;
            }
        }

        BaseResponse animationResponse = new BaseResponse(componentDetachData.getRequestId(), componentDetachData.getProtocolType());
        animationResponse.setResult(isSuccess);
        sendResult(animationResponse);
    }


    private boolean attachComponent(
            com.altimedia.cloud.system.server.parser.obj.animation.request.component.attach.Data componentAttachData) {

        if (apexLogger.DEBUG) {
            Log.d(TAG, "thlee attachComponent componentAttachData : " + componentAttachData);
        }

        boolean isSuccess = false;
        DestComponent desComponent = componentAttachData.getDestComponent();
        SrcComponent srcComponent = componentAttachData.getSrcComponent();
        ViewGroup destComponent = null;
        if (isAttachedView(desComponent.getObjectid())) {
            destComponent = findViewById(desComponent.getObjectid());
        } else {
            destComponent = (ViewGroup) mCacheView.get(desComponent.getObjectid());
        }
        if (destComponent != null) {
            View targetView = mCacheView.get(srcComponent.getObjectid());

            if (targetView != null) {
                int left = desComponent.getLeft();
                int top = desComponent.getTop();
                ViewGroup.LayoutParams layoutParams = targetView.getLayoutParams();
                layoutParams.width = desComponent.getWidth();
                layoutParams.height = desComponent.getHeight();
                if (layoutParams instanceof RelativeLayout.LayoutParams) {
                    ((LayoutParams) layoutParams).topMargin = top;
                    ((LayoutParams) layoutParams).leftMargin = left;
                }

                targetView.setTag(R.id.key_zOrder, desComponent.getzOrder());
                targetView.setLayoutParams(layoutParams);
                addViewByZOrder(destComponent, targetView);
                isSuccess = true;
            }
        }
        return isSuccess;
    }

    private void attachComponents(ComponentAttachData componentAttachData){
        boolean isSuccess = true;
        boolean isAttached = false;
        List<com.altimedia.cloud.system.server.parser.obj.animation.request.component.attach.Data> attachList =
                componentAttachData.getData();
        for (com.altimedia.cloud.system.server.parser.obj.animation.request.component.attach.Data data : attachList) {
            isAttached = attachComponent(data);
            if (!isAttached) {
                isSuccess = false;
            }
        }
        BaseResponse animationResponse = new BaseResponse(componentAttachData.getRequestId(), componentAttachData.getProtocolType());
        animationResponse.setResult(isSuccess);
        sendResult(animationResponse);
    }


    private void addViewByZOrder(ViewGroup desView, View targetView) {
        int targetZOrder = (Integer) targetView.getTag(R.id.key_zOrder);

        int childCount = desView.getChildCount();
        if (childCount == 0) {
            desView.addView(targetView);
        } else if (childCount == 1) {
            View checkView = desView.getChildAt(0);
            int checkZOrder = (Integer) checkView.getTag(R.id.key_zOrder);
            if (targetZOrder < checkZOrder) {
                desView.addView(targetView, 0);
            } else {
                desView.addView(targetView, 1);
            }
        } else {
            int index = -1;
            for (int i = 0; i < childCount - 1; i++) {

                View nCheckView = desView.getChildAt(i + 1);
                int nCheckZOrder = (Integer) nCheckView.getTag(R.id.key_zOrder);

                if (targetZOrder < nCheckZOrder) {
                    index = i;
                    break;
                }
            }
            if (index == -1) {
                index = childCount;
            }
            desView.addView(targetView, index);
        }
//        initTranslate(targetView);
    }

    private void initTranslate(View targetView) {
        if (targetView instanceof ViewGroup) {
            ViewGroup mViewGroup = (ViewGroup) targetView;
            int childCount = mViewGroup.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View view = mViewGroup.getChildAt(i);
                initTranslate(view);
            }
        } else {
            float translateX = targetView.getTranslationX();
            float translateY = targetView.getTranslationY();
            targetView.setTranslationX(0);
            targetView.setTranslationY(0);
        }
    }

    private boolean isAttachedView(int id) {
        return findViewById(id) != null;
    }


    private void clearChildViewCache(View view) {
        clearChildViewCache(view, true);
    }

    private void clearChildViewCache(View view, boolean isRemoveChild) {
        if (view instanceof ViewGroup) {
            ViewGroup parentView = (ViewGroup) view;
            int viewSize = parentView.getChildCount();
            for (int i = 0; i < viewSize; i++) {
                View childView = ((ViewGroup) view).getChildAt(i);
                clearChildViewCache(childView);
            }
        }
        mCacheView.remove(view.getId());
    }

    private void deleteComponent(
            com.altimedia.cloud.system.server.parser.obj.animation.request.component.delete.Data componentDeleteData) {
        int targetId = componentDeleteData.getObjectid();
        if (isAttachedView(targetId)) {
            View targetView = findViewById(componentDeleteData.getObjectid());
            clearChildViewCache(targetView, componentDeleteData.isDeleteTree());
        }
        if (isAttachedView(targetId)) {
            clearAnimation(targetId);
            View targetView = findViewById(targetId);
            ((ViewGroup) targetView.getParent()).removeView(targetView);
        }
    }

    private void deleteComponents(ComponentDeleteData componentDeleteData) {
        List<com.altimedia.cloud.system.server.parser.obj.animation.request.component.delete.Data>  delList =
                componentDeleteData.getData();
        for (com.altimedia.cloud.system.server.parser.obj.animation.request.component.delete.Data data : delList) {
            deleteComponent(data);
        }
        BaseResponse animationResponse = new BaseResponse(componentDeleteData.getRequestId(), componentDeleteData.getProtocolType());
        animationResponse.setResult(true);
        sendResult(animationResponse);
    }

    private void setVisibilityComponent(ComponentVisibleData componentVisibleData) {
        List<com.altimedia.cloud.system.server.parser.obj.animation.request.component.visible.ActionItem> actionItems =
                componentVisibleData.getData().getActionItem();
        for (com.altimedia.cloud.system.server.parser.obj.animation.request.component.visible.ActionItem item : actionItems) {
            if (!isAttachedView(item.getObjectid())) {
                if (mCacheView.containsKey(item.getObjectid())) {
                    mCacheView.get(item.getObjectid()).setVisibility(item.isVisible() ? View.VISIBLE : View.INVISIBLE);
                }
            } else {
                View view = findViewById(item.getObjectid());
                view.setVisibility(item.isVisible() ? View.VISIBLE : View.INVISIBLE);
            }

        }
        BaseResponse animationResponse = new BaseResponse(componentVisibleData.getRequestId(), componentVisibleData.getProtocolType());
        animationResponse.setResult(true);
        sendResult(animationResponse);
    }


    private void fillRectComponent(FillRectData fillRectData) {
        int viewId = fillRectData.getData().getObjectid();
        com.altimedia.cloud.system.server.parser.obj.animation.request.component.rect.Data fillRectInfo = fillRectData.getData();
        boolean isSuccess = false;
        if (isAttachedView(viewId)) {
            View targetView = findViewById(viewId);
            targetView.setBackgroundColor(fillRectInfo.getColorInt());
            isSuccess = true;
        }

        BaseResponse animationResponse = new BaseResponse(fillRectData.getRequestId(), fillRectData.getProtocolType());
        animationResponse.setResult(isSuccess);
        sendResult(animationResponse);
    }


    private void loadBinaryImage(final ResourceItem item, @NonNull ComponentLoadCallback componentLoadCallback) {
        final ComponentData componentData = item.getComponentData();
        if (componentData.getSource() == null || componentData.getSource().getByteArray() == null) {
            if (componentLoadCallback != null) {
                componentLoadCallback.onLoadStart(item.getId(), 0, 0);
                componentLoadCallback.onLoaded(false, item.getId(), 0, 0);
            }
            return;
        }
        String source = componentData.getSource().getByteArray();
        String imageType = componentData.getSource().getImageType();
        CreateSourceData createSourceData = componentData.getSource();


        byte[] decodedString = Base64.decode(source, Base64.DEFAULT);

        if (!IS_USE_COMPONENT_RESULT_CALLBACK) {
            if (componentLoadCallback != null) {
                componentLoadCallback.onLoadStart(item.getId(), createSourceData.getWidth(), createSourceData.getHeight());
                componentLoadCallback.onLoaded(true, item.getId(), createSourceData.getWidth(), createSourceData.getHeight());
                componentLoadCallback = null;
            }
        }

        final ComponentLoadTarget mImageLoadTarget = new ComponentLoadTarget(getContext(), source, item, componentLoadCallback, mCacheView, this);

        Glide.with(getContext())
                .load(decodedString)
                .override(createSourceData.getWidth(), createSourceData.getHeight())
                .diskCacheStrategy(componentData.isCache_file() ? DISK_CACHE_STRATEGY : DiskCacheStrategy.NONE)
                .skipMemoryCache(!componentData.isCache_mem())
//                .signature(new ObjectKey(??))
                .into(mImageLoadTarget);
    }

    // TODO
    private void loadImageFile(final ResourceItem item, @NonNull ComponentLoadCallback componentLoadCallback) {
        final ComponentData componentData = item.getComponentData();
        if (componentData.getSource() == null || componentData.getSource().getPath() == null) {
            if (componentLoadCallback != null) {
                componentLoadCallback.onLoadStart(item.getId(), 0, 0);
                componentLoadCallback.onLoaded(false, item.getId(), 0, 0);
            }
            // Fail case
            return;
        }
        String source = componentData.getSource().getPath();

//        Bitmap cacheBitmap = CacheUtil.getInstance().getImage(source);
//        if (apexLogger.DEBUG) {
//            Log.d(TAG, "loadImageFile cacheBitmap: " + cacheBitmap);
//        }
//
//
//        if (cacheBitmap != null) {
//            ImageView imageView = new ImageView(getContext());
//            LayoutParams params = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
//                    ViewGroup.LayoutParams.WRAP_CONTENT);
//            imageView.setLayoutParams(params);
//
//            imageView.setVisibility(componentData.isVisible() ? View.VISIBLE : View.INVISIBLE);
//            imageView.setTag(R.id.key_srcUrl, source);
//            imageView.setTag(item.getId());
//            imageView.setId(item.getId());
//            imageView.setImageBitmap(cacheBitmap);
//            mCacheView.put(item.getId(), imageView);
        if (componentLoadCallback != null) {
            componentLoadCallback.onLoadStart(item.getId(), 0, 0);
            componentLoadCallback.onLoaded(true, item.getId(), 0, 0);
            componentLoadCallback = null;
        }
//            return;
//        }
    }

    private void loadImage(final ResourceItem item, @NonNull ComponentLoadCallback componentLoadCallback) {
        final ComponentData componentData = item.getComponentData();
        if (componentData.getSource() == null || componentData.getSource().getPath() == null) {
            if (componentLoadCallback != null) {
                componentLoadCallback.onLoadStart(item.getId(), 0, 0);
                componentLoadCallback.onLoaded(false, item.getId(), 0, 0);
            }
            return;
        }

        CreateSourceData createSourceData = componentData.getSource();
        String source = componentData.getSource().getPath();

        RequestListener<Drawable> drawableRequestListener = new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                if (apexLogger.CACHE) {
                    Log.d(TAG, "loadImage onResourceReady : " + dataSource);
                }

                if (ComponentConfig.showComponentCacheBounds) {
                    Bitmap bitmap = ((BitmapDrawable) resource).getBitmap();
                    if (bitmap != null) {
                        ImageUtil.setOverlayRectToBitmap(bitmap, ComponentConfig.getCacheColor(dataSource));
                    }
                }

                return false;
            }
        };

        if (!IS_USE_COMPONENT_RESULT_CALLBACK) {
            if (componentLoadCallback != null) {
                componentLoadCallback.onLoadStart(item.getId(), createSourceData.getWidth(), createSourceData.getHeight());
                componentLoadCallback.onLoaded(true, item.getId(), createSourceData.getWidth(), createSourceData.getHeight());
                componentLoadCallback = null;
            }
        }

        final ComponentLoadTarget mImageLoadTarget = new ComponentLoadTarget(getContext(), source, item, componentLoadCallback, mCacheView, this);

        Glide.with(getContext()).load(source)
                .diskCacheStrategy(componentData.isCache_file() ? DISK_CACHE_STRATEGY : DiskCacheStrategy.NONE)
                .skipMemoryCache(!componentData.isCache_mem())
                .signature(new ObjectKey(source))
                .addListener(drawableRequestListener)
                .into(mImageLoadTarget);
    }

    private void loadSpriteImage(final ResourceItem item, @NonNull ComponentLoadCallback componentLoadCallback) {
        final ComponentData componentData = item.getComponentData();
        if (componentData.getSource() == null || componentData.getSource().getPath() == null) {
            if (componentLoadCallback != null) {
                componentLoadCallback.onLoadStart(item.getId(), 0, 0);
                componentLoadCallback.onLoaded(false, item.getId(), 0, 0);
            }
            return;
        }

        CreateSourceData createSourceData = componentData.getSource();
        String source = createSourceData.getPath();
        int spriteWidth = createSourceData.getSpriteWidth() * createSourceData.getSpriteCount();
        int spriteHeight = createSourceData.getSpriteHeight();

        if (!IS_USE_COMPONENT_RESULT_CALLBACK) {
            if (componentLoadCallback != null) {
                componentLoadCallback.onLoadStart(item.getId(), spriteWidth, spriteHeight);
                componentLoadCallback.onLoaded(true, item.getId(), spriteWidth, spriteHeight);
                componentLoadCallback = null;
            }
        }

        final ComponentLoadTarget mImageLoadTarget = new ComponentLoadTarget(getContext(), source, item, componentLoadCallback, mCacheView, this);

        Glide.with(getContext()).load(source)
                .override(spriteWidth, spriteHeight)
                .diskCacheStrategy(componentData.isCache_file() ? DISK_CACHE_STRATEGY : DiskCacheStrategy.NONE)
                .skipMemoryCache(!componentData.isCache_mem())
                .signature(new ObjectKey(source))
                .into(mImageLoadTarget);
    }

    private ArrayList<Integer> idMap = new ArrayList<>();
    private int generatedItemId() {
        long seed = System.currentTimeMillis();
        Random rand = new Random(seed);
        int randomId = 0;
        do {
            randomId = Math.abs(rand.nextInt());
        } while (idMap.contains(randomId) || randomId <= 0);
//        Log.d(TAG, "randomValue : " + randomValue);
        idMap.add(randomId);
        return randomId;
    }

    @Override
    public void draw(Canvas canvas) {
        ElapsedTime.getInstance().addSubSession(TAG + " draw start");

        super.draw(canvas);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        ElapsedTime.getInstance().addSubSession(TAG + " onDraw end");
    }

    private class ComponentLoadTarget extends SimpleTarget<Drawable> {
        private final String TAG = ComponentLoadTarget.class.getSimpleName();
        final ResourceItem item;
        final ComponentLoadCallback componentLoadCallback;
        final Context mContext;
        final HashMap<Integer, View> mCacheMap;
        final View mParentsView;
        final String mTargetUrl;

        public ComponentLoadTarget(Context context, String url, ResourceItem item,
                                   ComponentLoadCallback componentLoadCallback, HashMap cacheMap, View parentsView) {
            this.item = item;
            this.mTargetUrl = url;
            this.mContext = context;
            this.componentLoadCallback = componentLoadCallback;
            this.mParentsView = parentsView;
            this.mCacheMap = cacheMap;
        }

        @Override
        public void onLoadStarted(@Nullable Drawable placeholder) {
            super.onLoadStarted(placeholder);
            ComponentData componentData = item.getComponentData();

            if (apexLogger.DEBUG) {
                Log.d(TAG, "onLoadStarted componentData " + componentData);
            }

            CreateSourceData createSourceData = componentData.getSource();

            ImageView imageView = new ImageView(mContext);
            LayoutParams params = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            imageView.setLayoutParams(params);
            imageView.setTag(R.id.key_srcUrl, mTargetUrl);
            imageView.setTag(item.getId());
            imageView.setId(item.getId());

            if (componentData != null) {
                imageView.setVisibility(componentData.isVisible() ? View.VISIBLE : View.INVISIBLE);
                mCacheMap.put(item.getId(), imageView);
            }

            if (componentLoadCallback != null) {
                componentLoadCallback.onLoadStart(item.getId(), imageView.getWidth(), imageView.getHeight());
            }
        }

        @Override
        public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
            if (apexLogger.DEBUG) {
                Log.d(TAG, "onResourceReady id : " + item.getId());
            }
            ComponentData componentData = item.getComponentData();

            View view = mParentsView.findViewById(item.getId());
            if (view == null) {
                view = mCacheMap.get(item.getId());
            }
            if (view instanceof ImageView) {
                Bitmap bitmap = ((BitmapDrawable) resource).getBitmap();

                if (componentData.getType() == ComponentType.sprite) {
                    CreateSourceData createSourceData = componentData.getSource();
                    Bitmap[] bitmaps = mImageUtil.makeCropBitmap(bitmap, new Rect(0, 0, createSourceData.getSpriteWidth(), createSourceData.getSpriteHeight()));
                    view.setTag(R.id.key_srcImageList, bitmaps);
                } else {
                    ((ImageView) view).setImageBitmap(bitmap);
                }

                if (componentLoadCallback != null) {
                    componentLoadCallback.onLoaded(true, item.getId(), view.getWidth(), view.getHeight());
                }

                if (bitmap != null) {
                    CreateSourceData createSourceData = componentData.getSource();
                    if (createSourceData != null) {
                        final String source = createSourceData.getPath();
                        if (source != null) {
                            if (componentData.isPreload_file()) {
                                CacheUtil.addPreloadImageSource(source);
                            } else {
                                CacheUtil.removePreloadImageSource(source);
                            }
                        }
                    }
                }
            } else {
                if (componentLoadCallback != null) {
                    componentLoadCallback.onLoaded(false, item.getId(), 0, 0);
                }
            }

        }

        @Override
        public void onLoadCleared(@Nullable Drawable placeholder) {
            if (apexLogger.ERROR) {
                Log.d(TAG, "onLoadCleared id : " + item.getId());
            }
        }

        @Override
        public void onLoadFailed(@Nullable Drawable errorDrawable) {
            super.onLoadFailed(errorDrawable);
            if (apexLogger.ERROR) {
                Log.d(TAG, "onLoadFailed ");
            }
            if (componentLoadCallback != null) {
                componentLoadCallback.onLoaded(false, item.getId(), 0, 0);
            }
        }
    }


    public void notifyComponentMessage(BaseRequest animationRequest) {
        if (apexLogger.DEBUG) {
            Log.d(TAG, "notifyComponentMessage animationRequest: " + animationRequest);
        }

        if (animationRequest instanceof AnimationCreateData) {
            createAnimation((AnimationCreateData) animationRequest);
        } else if (animationRequest instanceof AnimationCloseData) {
            cancelAnimation((AnimationCloseData) animationRequest);
        } else if (animationRequest instanceof AnimationFinishData) {
            finishAnimation((AnimationFinishData) animationRequest);
        } else if (animationRequest instanceof CreateComponentData) {
            createComponent((CreateComponentData) animationRequest);
        } else if (animationRequest instanceof ComponentAttachData) {
            attachComponents((ComponentAttachData) animationRequest);
        } else if (animationRequest instanceof ComponentDetachData) {
            detachComponents((ComponentDetachData) animationRequest);
        } else if (animationRequest instanceof ComponentDeleteData) {
            deleteComponents((ComponentDeleteData) animationRequest);
        } else if (animationRequest instanceof ComponentVisibleData) {
            setVisibilityComponent(((ComponentVisibleData) animationRequest));
        } else if (animationRequest instanceof FillRectData) {
            fillRectComponent((FillRectData) animationRequest);
        }
    }

    private class LoadComponentResultCallback implements ComponentLoadCallback {
        private final String TAG = LoadComponentResultCallback.class.getSimpleName();
        private final Context context;
        private final ApexCloudSession apexCloudSession;
        private CreateDataResponse mCreateDataResponse = null;
        private final View mRootView;
        private int resultSize;

        public LoadComponentResultCallback(View rootView, Context context, ApexCloudSession apexCloudSession) {
            this.mRootView = rootView;
            this.context = context;
            this.apexCloudSession = apexCloudSession;
        }

        public void setComponentBaseInfo(int requestId, ProtocolType protocolType, int resultSize) {
            mCreateDataResponse = new CreateDataResponse(requestId, protocolType);
            this.resultSize = resultSize;
        }

        @Override
        public void onLoadStart(int objectId, int width, int height) {
            if (apexLogger.DEBUG) {
                Log.d(TAG, "onLoadStart : " + objectId);
            }
            mCreateDataResponse.putData(objectId, width, height);

            if (apexLogger.DEBUG) {
                Log.d(TAG, "onLoadStart data : " + mCreateDataResponse.getDataSize() + " |  : " + resultSize);
            }
        }

        @Override
        public void onLoaded(boolean success, int objectId, int width, int height) {
            if (apexLogger.DEBUG) {
                Log.d(TAG, "onLoaded : " + objectId + " | success : " + success);
            }

            mCreateDataResponse.setResult(success); // TODO
            if (IS_USE_COMPONENT_RESULT_CALLBACK) {
                if (resultSize == mCreateDataResponse.getDataSize()) {
                    onResult(true);
                }
            }
        }

        @Override
        public void onResult(boolean success) {
            if (apexLogger.DEBUG) {
                Log.d(TAG, "onResult success : " + success);
            }

            mCreateDataResponse.setResult(success);
            sendResult(mCreateDataResponse);
        }

        private void sendResult(Object o) {
            String result = new Gson().toJson(o);
            if (apexLogger.DEBUG) {
                Log.d(TAG, "sendResult to web : " + result);
            }
            try {
                apexCloudSession.sendWeb(result);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private interface ComponentLoadCallback {
        void onLoadStart(int id, int width, int height);

        void onLoaded(boolean success, int resourceId, int width, int height);

        void onResult(boolean success);
    }

    public void release() {
        idMap.clear();
        mCacheView.clear();
        mCacheAnimation.clear();
        mCacheAnimationViewId.clear();

        removeAllViews();
    }

    @VisibleForTesting
    public void readyRect() {
        ImageView relativeLayout = new ImageView(getContext());
        relativeLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        relativeLayout.setTag(R.id.key_zOrder, 0);
        relativeLayout.setBackgroundColor(Color.BLACK);
        relativeLayout.setTag(BACK_ID);
        relativeLayout.setId(BACK_ID);
        addView(relativeLayout, 0);
    }


    @VisibleForTesting
    public void notifyFrameUpdate() {
    }


    @VisibleForTesting
    public void updateFragment(byte[] source, int x, int y, int w, int h) {
    }


}
