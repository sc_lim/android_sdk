/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.comp.ui;

import com.altimedia.cloud.system.server.parser.obj.animation.request.component.create.ComponentData;

/**
 * Created by mc.kim on 08,04,2020
 */
public class ResourceItem {
    private final int mComponentId;
    private final ComponentData mComponentData;

    public ResourceItem(int id) {
        this(id, null);
    }

    public ResourceItem(int id, ComponentData componentData) {
        mComponentId = id;
        mComponentData = componentData;
    }

    public int getId() {
        return mComponentId;
    }

    public ComponentData getComponentData() {
        return mComponentData;
    }

    @Override
    public String toString() {
        return "ResourceItem{" +
                "mComponentId=" + mComponentId +
                ", mComponentData=" + mComponentData +
                '}';
    }
}
