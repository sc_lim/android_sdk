/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.comp.ic;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.Shader;
import android.util.DisplayMetrics;
import android.util.Log;

import androidx.annotation.NonNull;

import com.altimedia.cloud.comp.CacheUtil;
import com.altimedia.cloud.system.server.parser.obj.CsFrame;
import com.altimedia.cloud.system.util.ElapsedTime;
import com.altimedia.cloud.system.util.apexLogger;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by gh.baek on 2018. 5. 14.
 * - Updated by as.choi  2020.03.17 :  Remove Page Tearing.
 * - use Double Buffering, Apex SDK 내에서 이미지 디코딩 하여 디코딩 완료된 것을 전달해 주는 방식 그대로. (원래 에뮬레이터의 방식이었음.)
 */

class ApexCloudView {
    private final String TAG = ApexCloudView.class.getSimpleName();
    private Paint clearPaint;
    private Bitmap backBitmap;
    private Canvas backCanvas;
    private ExecutorService FrameUpdateExecutorService = Executors.newSingleThreadExecutor();
    private final CsCloudView mPanel;
    private String mCurrentCloudAppName = null;

    private BitmapShader bitmapShader;
    private Paint bitmapPaint = new Paint(/*Paint.ANTI_ALIAS_FLAG*/);
    private Rect changedRect = new Rect();

    private boolean isPrepared = false;
    private boolean useCache = false;

    public ApexCloudView(Context context, CsCloudView panel) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        mPanel = panel;
        clearPaint = new Paint();
        clearPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
        backBitmap = Bitmap.createBitmap(metrics.widthPixels, metrics.heightPixels, Bitmap.Config.ARGB_8888);
        backCanvas = new Canvas(backBitmap);

        bitmapPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC));
        bitmapPaint.setAntiAlias(false);
        bitmapPaint.setFilterBitmap(false);
    }

    public void onPrepared(boolean ready) {
        Log.d(TAG, "onPrepared : " + ready);
        this.isPrepared = ready;
    }

    public void draw(@NonNull Canvas canvas) {
        if (canvas != null && bitmapPaint != null) {
            // re-draw background
            canvas.drawRect(0, 0, backBitmap.getWidth(), backBitmap.getHeight(), bitmapPaint);
        }
    }

    private void drawCanvas(Rect rect) {
        ElapsedTime.getInstance().addSubSession("frame_draw : " + rect);

        Canvas canvas = null;
        try {
            canvas = mPanel.lockCanvas(rect);      // obtain canvas
            // null if user presses the home button
            if (canvas != null) {
                // re-draw background
                canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
                // calculate elapsed time
//                canvas.drawBitmap(getFrontImg(), 0, 0, null);
                canvas.drawRect(0, 0, backBitmap.getWidth(), backBitmap.getHeight(), bitmapPaint);
                // apply the change
                // refresh lastDrawnTime
            }
        } finally {
            mPanel.unlockCanvasAndPost(canvas);
//            System.gc(); //test code
        }

//        if (apexLogger.DEBUG) {
        ElapsedTime.getInstance().addSubSession("draw canvas", "decoding end");
    }

    public void updateFragment(CsFrame csFrame) {
        if (apexLogger.DEBUG) {
            Log.d(TAG, "updateFragment : " + csFrame);
        }
        if (csFrame != null && csFrame.getSource() != null) {
            frameTotalLength += csFrame.getSource().length;
        }

        FrameUpdateExecutorService.submit(new FrameUpdater(csFrame));
    }

    public Bitmap getFrontImg() {
        return backBitmap;
    }

    public void clear(Context context) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        if (backCanvas != null) {
            backCanvas.drawRect(0, 0, metrics.widthPixels, metrics.heightPixels, clearPaint);
        }
    }

    public void destroy() {
        clearPaint = null;
        if (backBitmap != null) {
            backBitmap.recycle();
        }
        backBitmap = null;
        backCanvas = null;
        FrameUpdateExecutorService.shutdown();
    }

    void setCacheEnable(String appName, boolean useCache) {
        if (apexLogger.DEBUG) {
            Log.d(TAG, "notifyStartApp : " + appName);
        }
        this.mCurrentCloudAppName = appName;
        this.useCache = useCache;
    }

    float frameTotalLength;
    boolean isDecodingStarted = false;

    private class FrameUpdater implements Runnable {
        CsFrame csFrame;

        public FrameUpdater() {
            if (apexLogger.DEBUG) {
                Log.d(TAG, "FrameUpdater changeRect : " + changedRect);
            }
        }

        public FrameUpdater(CsFrame csFrame) {
            this.csFrame = csFrame;

            if (apexLogger.DEBUG) {
                Log.d(TAG, "FrameUpdater csFrame : " + csFrame);
            }
        }

        @Override
        public void run() {
            if (!isPrepared) {
                if (apexLogger.DEBUG) {
                    Log.d(TAG, "isPrepared : not yet");
                }
                return;
            }

            if (csFrame == null) {
                if (apexLogger.DEBUG) {
                    Log.d(TAG, "csFrame is null");
                }
                return;
            }

            if (!isDecodingStarted) {
                isDecodingStarted = true;
                ElapsedTime.getInstance().addSubSession("decoding start");
            }

            if (csFrame.getSource() != null) {
                // draw frags into back buffer
                Rect rect = csFrame.getBounds();
                byte[] source = csFrame.getSource();
                int hashCode = csFrame.getHashcode();

                Bitmap drawBitmap = CacheUtil.getInstance().getFragmentCacheBitmap(mCurrentCloudAppName, csFrame);
                if (apexLogger.CACHE) {
                    Log.d(TAG, "userCached fragment : " + drawBitmap);
                }
                if (drawBitmap == null) {
                    if (apexLogger.CACHE) {
                        Log.d(TAG, "put fragment source : " + source);
                    }

                    drawBitmap = csFrame.getBitmap();
                    CacheUtil.getInstance().addFragmentCache(mCurrentCloudAppName, csFrame);
                }
                backCanvas.save();
                backCanvas.clipRect(rect);
                backCanvas.drawRect(rect, clearPaint);
                backCanvas.drawBitmap(drawBitmap, rect.left, rect.top, null);
                changedRect.union(rect);
                backCanvas.restore();

                if (apexLogger.DEBUG) {
                    Log.d(TAG, "fragment updated : " + rect + " | changedRect : " + changedRect);
                }
            }

            if (csFrame.isUpdate()) { // frame update
                if (changedRect.left == 0 && changedRect.top == 0 && changedRect.right == 0 && changedRect.bottom == 0) {
                    if (apexLogger.DEBUG) {
                        Log.d(TAG, "skip draw. no changed rect.");
                    }
                    return;
                }

                ElapsedTime.getInstance().addSubSession("decoding end", "decoding start");
                isDecodingStarted = false;

                bitmapShader = new BitmapShader(backBitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
                bitmapPaint.setShader(bitmapShader);

                drawCanvas(new Rect(changedRect));

                if (apexLogger.DEBUG) {
                    Log.d(TAG, "frame updated : " + changedRect);
                }

                changedRect.set(0, 0, 0, 0);

                ElapsedTime.getInstance().addSubSession("received image byte : " + (frameTotalLength/1024) + "kb");
                Log.d(TAG, "drawCanvas ElapsedTime : " + ElapsedTime.getInstance().toString());
                frameTotalLength = 0;
            }
        }
    }
}
