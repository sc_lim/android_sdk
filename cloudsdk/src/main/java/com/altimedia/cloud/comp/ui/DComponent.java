/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.comp.ui;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;

import com.alticast.cloud.cloudsdk.R;
import com.altimedia.cloud.comp.ComponentConfig;
import com.altimedia.cloud.system.util.apexLogger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class DComponent {
    private static final String TAG = DComponent.class.getSimpleName();

    public static final int NO_ID = -1;
    private View parentView;
    private DComponent dComponents[];
    protected Paint nPaint = new Paint(/*Paint.ANTI_ALIAS_FLAG*/);
    private float mAlpha = 1f;

    protected ResourceItem resourceItem;

    private Rect clipRect = null;
    private Rect bounds = new Rect(0, 0, 0, 0);
    private int id = -1;
    private int zOrder = 0;
    private String srcUrl;
    private boolean visible = true;
    private boolean isSkippedRepaint = false;
    private long lastRequestedRepaintMillis = 0L;

    private int backgroundColor = -1;

    DComponent() {
        if (apexLogger.DEBUG) {
            Log.d(TAG, "DComponent constructor : " + getClass().getSimpleName());
        }
    }

//    public DComponent(ResourceItem resourceItem) {
//        this(resourceItem, null);
//    }
//
//    public DComponent(View parent) {
//        this(null, parent);
//    }

    public DComponent(ResourceItem resourceItem, View parent) {
        this();
        if (apexLogger.DEBUG) {
            Log.d(TAG, "DComponent constructor resourceItem : " + resourceItem);
        }

        this.resourceItem = resourceItem;
        this.parentView = parent;
        if(resourceItem != null) {
            setId(resourceItem.getId());
        }
    }

    public DComponent[] getDComponents() {
        return dComponents;
    }

    public ArrayList<DComponent> getDComponentArrayList() {
        ArrayList<DComponent> dComponentArrayList = new ArrayList<DComponent>();

        if (this.dComponents != null) {
            for (DComponent comp : this.dComponents) {
                dComponentArrayList.add(comp);
            }
        }

        return dComponentArrayList;
    }

    public void addComponent(DComponent dComponent) {
        if (apexLogger.DEBUG) {
            Log.d(TAG, "addComponent dComponent: " + dComponent);
        }
        ArrayList<DComponent> dComponentArrayList = getDComponentArrayList();
        dComponentArrayList.add(dComponent);

        Collections.sort(dComponentArrayList, zOrderComparator);
        this.dComponents = dComponentArrayList.toArray(new DComponent[dComponentArrayList.size()]);

        repaint();
    }

    public void removeComponent(DComponent dComponent) {
        if (apexLogger.DEBUG) {
            Log.d(TAG, "removeComponent dComponent: " + dComponent);
        }
        ArrayList<DComponent> dComponentArrayList = getDComponentArrayList();

        if (dComponentArrayList.contains(dComponent)) {
            dComponentArrayList.remove(dComponent);
        }

        this.dComponents = dComponentArrayList.toArray(new DComponent[dComponentArrayList.size()]);

        repaint();
    }

    public void removeAllComponent() {
        if (apexLogger.DEBUG) {
            Log.d(TAG, "removeAllComponent nComponent: " + getClass().getName());
        }
        if (dComponents != null) {
            for (DComponent dComponent : dComponents) {
                if (dComponent.getDComponents() != null && dComponent.getDComponents().length > 0) {
                    dComponent.removeAllComponent();
                }

                removeComponent(dComponent);
            }
        }
        repaint();
    }

    public void repaint(int left, int top, int right, int bottom) {
        if (apexLogger.REPAINT) {
            Log.d(TAG, "repaint bounds : " + new Rect(left, top, right, bottom));
        }

        if (parentView != null) {
//            parentView.postInvalidate(left, top, right, bottom);
            parentView.invalidate();
//            parentView.postInvalidate();
        }
    }

    public void repaint() {
        if (apexLogger.REPAINT) {
            Log.d(TAG, "repaint parentView: " + parentView);
        }
        repaint(bounds.left, bounds.top, bounds.right, bounds.bottom);
    }

    public DComponent getComponent(int id) {
        if (dComponents != null) {
            for (DComponent dComponent : dComponents) {
                if (dComponent != null) {
                    if (dComponent.getId() == id) {
                        return dComponent;
                    }
                }
            }
        }
        return null;
    }
    public boolean containComponent(int id) {
        return getComponent(id) != null;
    }


    public boolean containComponent(DComponent dComponent) {
        if (apexLogger.DEBUG) {
            Log.d(TAG, "containComponent dComponent: " + dComponent);
        }
        if (dComponent == null) {
            return false;
        }

        ArrayList<DComponent> dComponentArrayList = getDComponentArrayList();
        return dComponentArrayList.contains(dComponent);
    }

    public final int getWidth() {
        return bounds.right - bounds.left;
    }

    public final int getHeight() {
        return bounds.bottom - bounds.top;
    }

    public Rect getBounds() {
        return bounds;
    }

    public void setBounds(int left, int top, int right, int bottom) {
        setBounds(new Rect(left, top, right, bottom));
    }

    public void setBounds(Rect bounds) {
        Rect repaintBounds = new Rect(this.bounds);
        this.bounds = new Rect(bounds);
        repaintBounds.union(bounds);
        repaint(repaintBounds.left, repaintBounds.top, repaintBounds.right, repaintBounds.bottom);
    }

    public float getAlpha() {
        return mAlpha;
    }

    public int getX() {
        return bounds.left;
    }

    public int getY() {
        return bounds.top;
    }


    public void setAlpha(float alpha) {
        if (apexLogger.DEBUG) {
            Log.d(TAG, "setAlpha: " + alpha);
        }
        this.mAlpha = alpha;
        repaint();
    }


    public void draw(@NonNull Canvas canvas) {
        if (visible) {
            if (dComponents != null) {
                for (DComponent comp : dComponents) {

                    Rect bounds = comp.getBounds();

//                    Log.d(TAG, "draw bounds: " + bounds + canvas.quickReject(bounds.left, bounds.top, bounds.right, bounds.bottom, Canvas.EdgeType.BW));

//                    if(!canvas.quickReject(bounds.left, bounds.top, bounds.right, bounds.bottom, Canvas.EdgeType.BW)) {
                    canvas.save();

                    int cx = bounds.left;
                    int cy = bounds.top;

                    canvas.translate(cx, cy);
                    canvas.clipRect(0, 0, bounds.width(), bounds.height());

                    try {
                        nPaint.setAlpha(floatToByte(comp.getAlpha()));

                        if (apexLogger.REPAINT) {
                            Log.d(TAG + getClass(), "draw comp: " + comp.getClass().getSimpleName() + nPaint.getAlpha());
                        }

                        comp.draw(canvas);
                        drawBounds(canvas, getBounds());
                    } finally {
                        nPaint.reset();
                        canvas.translate(-cx, -cy);
                    }

                    drawBounds(canvas, bounds);
                    canvas.restore();
//                    }
                }
            }

            if (backgroundColor >= 0) {
                nPaint.setAlpha(floatToByte(getAlpha()));
                nPaint.setColor(backgroundColor);
                nPaint.setStyle(Paint.Style.STROKE);
                nPaint.setStrokeWidth(2f);
                canvas.drawRect(bounds, nPaint);
                nPaint.reset();
            }
            drawBounds(canvas,getBounds());
        }
    }

    protected void drawBounds(Canvas canvas, Rect bounds) {
        if (ComponentConfig.showBounds) {
            Paint paint = new Paint();
            paint.setColor(ComponentConfig.COLOR_BOUNDS);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeWidth(2f);
            canvas.drawRect(bounds, paint);
        }
    }

    public void setParentView(View parentView) {
        this.parentView = parentView;
    }

    public View getParentView() {
        return parentView;
    }

    public ResourceItem getResourceItem() {
        return resourceItem;
    }

    public void setResourceItem(ResourceItem resourceItem) {
        this.resourceItem = resourceItem;
    }

    public int getzOrder() {
        return zOrder;
    }

    public void setzOrder(int zOrder) {
        if (apexLogger.DEBUG) {
            Log.d(TAG, "setzOrder zOrder: " + zOrder);
        }

        this.zOrder = zOrder;
        repaint();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSrcUrl() {
        return srcUrl;
    }

    public void setSrcUrl(String srcUrl) {
        this.srcUrl = srcUrl;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
        repaint();
    }

    public int getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(@ColorInt int backgroundColor) {
        this.backgroundColor = backgroundColor;
        repaint();
    }

    public static String getURLForResource(int resId) {
        return Uri.parse("android.resource://" + R.class.getPackage().getName() + "/" + resId).toString();
    }

    public static Bitmap viewToBitmap(View view) {
        Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        if (view instanceof SurfaceView) {
            SurfaceView surfaceView = (SurfaceView) view;
            surfaceView.setZOrderOnTop(true);
            surfaceView.draw(canvas);
            surfaceView.setZOrderOnTop(false);
            return bitmap;
        } else {
            //For ViewGroup & View
            view.draw(canvas);
            return bitmap;
        }
    }

    public Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
//        float scaleWidth = ((float) newWidth) / width;
//        float scaleHeight = ((float) newHeight) / height; // CREATE A MATRIX FOR THE MANIPULATION
//        Matrix matrix = new Matrix(); // RESIZE THE BIT MAP
//        matrix.postScale(scaleWidth, scaleHeight); // "RECREATE" THE NEW BITMAP
//        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
        Bitmap resizedBitmap = Bitmap.createScaledBitmap(bm, width, height, false);
        bm.recycle();
        return resizedBitmap;
    }

    public static Bitmap drawableToBitmap(Drawable drawable) {
        Bitmap bitmap = null;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if (bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if (drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    public static byte floatToByte(float f) {
//        return (int) max(0, min(255, (int)floor(f * 256.0)));
        return (byte) (f * 255 % 256);
    }

    public static int adjustAlpha(@ColorInt int color, float factor) {
        int alpha = Math.round(Color.alpha(color) * factor);
        int red = Color.red(color);
        int green = Color.green(color);
        int blue = Color.blue(color);
        return Color.argb(alpha, red, green, blue);
    }

    Comparator<DComponent> zOrderComparator = new Comparator<DComponent>() {
        @Override
        public int compare(DComponent o1, DComponent o2) {
            return o1.getzOrder() - o2.getzOrder();
        }
    };
}
