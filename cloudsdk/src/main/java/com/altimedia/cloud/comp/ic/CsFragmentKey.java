/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.comp.ic;

import android.graphics.Rect;

public class CsFragmentKey {
    private String appName;
    private Rect rect;
    private int hashCode;

    public CsFragmentKey(String appName, int hashCode, Rect rect) {
        this.appName = appName;
        this.rect = rect;
        this.hashCode = hashCode;
    }

    public int getHashCode() {
        return hashCode;
    }

    public Rect getRect() {
        return rect;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CsFragmentKey csFragmentKey = (CsFragmentKey) o;

        if (hashCode == csFragmentKey.hashCode) {
            if (appName != null && appName.equalsIgnoreCase(csFragmentKey.appName)) {
                if (rect != null && rect.equals(csFragmentKey.rect)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        int result = hashCode;
        result = 31 * result + ( appName != null ? appName.hashCode() : 0);
        result = 31 * result + (rect != null ? rect.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "CsFragmentKey{" +
                "rect=" + rect +
                ", hashCode=" + hashCode +
                '}';
    }
}