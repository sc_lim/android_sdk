/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.comp;

import android.graphics.Bitmap;
import android.graphics.Rect;

import androidx.annotation.NonNull;

/**
 * Created by mc.kim on 19,05,2020
 */
public class ImageProvider {
    final String imageFilePath;
    final Rect shellRect;
    final ResultCallback mResultCallback;

    public ImageProvider(@NonNull String imageFilePath,
                         @NonNull Rect shellRect, @NonNull ResultCallback resultCallback) {
        this.imageFilePath = imageFilePath;
        this.shellRect = shellRect;
        this.mResultCallback = resultCallback;
    }

    public String getImageFilePath() {
        return imageFilePath;
    }

    public Rect getShellRect() {
        return shellRect;
    }

    public ResultCallback getResultCallback() {
        return mResultCallback;
    }

    public static class ResultCallback {
        public void onImageArrayLoaded(Bitmap originalBitmap, Bitmap[] bitmaps) {
        }
    }

    public void reset() {
    }
}
