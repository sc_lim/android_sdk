/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.comp;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.altimedia.cloud.system.util.apexLogger;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;

import java.util.ArrayList;

/**
 * Created by mc.kim on 19,05,2020
 */
public class ImageUtil {
    private static final String TAG = ImageUtil.class.getSimpleName();
    private final Context mContext;

    public ImageUtil(Context context) {
        this.mContext = context;
    }

    public Bitmap[] makeCropBitmap(Bitmap bitmap, Rect shellRect) {
        Rect originalRect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        ArrayList<Rect> shellRectArray = calculateRectArray(originalRect, shellRect);
        int imageLength = shellRectArray.size();
        Log.d(TAG, "makeCropBitmap : onResourceReady | " + "shellRect : " + shellRect.toString() + ","
                + "originalRect : " + originalRect.toString() + ", imageLength : " + imageLength);
        Bitmap[] mBitmapArray = new Bitmap[imageLength];
        for (int i = 0; i < imageLength; i++) {
            Rect rect = shellRectArray.get(i);
            Bitmap croppedImage = cropBitmap(bitmap, rect);
            mBitmapArray[i] = croppedImage;
        }
        return mBitmapArray;
    }

    private static Bitmap cropBitmap(Bitmap original, Rect rect) {
        Log.d(TAG, "cropBitmap : " + rect.toString());
        Bitmap result = Bitmap.createBitmap(original
                , rect.left
                , rect.top
                , rect.width()
                , rect.height());
//        if (result != original) {
//            original.recycle();
//        }
        return result;
    }

    private ArrayList<Rect> calculateRectArray(Rect original, Rect spriteRect) {
        Rect shellRect = new Rect(spriteRect);
        ArrayList<Rect> rectList = new ArrayList<>();
        boolean isContains = original.contains(shellRect);
        if (!isContains) {
            return null;
        }
        int shellRow = 0;
        int shellColumn = 0;
        do {
            Rect saveRect = new Rect(shellRect);
            rectList.add(saveRect);
            shellColumn = shellColumn + 1;

            shellRect.offsetTo((spriteRect.width() * shellColumn), shellRow * shellRect.height());
            isContains = original.contains(shellRect);
            if (!isContains) {
                int nextShellRow = shellRow + 1;
                Rect checkNextRowFirstRect = new Rect(shellRect);
                checkNextRowFirstRect.offsetTo(0, nextShellRow * checkNextRowFirstRect.height());
                boolean needCheckNextRow = original.contains(checkNextRowFirstRect);
                if (needCheckNextRow) {
                    shellColumn = 0;
                    shellRow = nextShellRow;
                    shellRect = checkNextRowFirstRect;
                    isContains = true;
                }
            }
        } while (isContains);

        return rectList;
    }

    // for develop
    public static void setOverlayRectToBitmap(Bitmap bitmap, int color) {
        Bitmap bmOverlay = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), bitmap.getConfig());
        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint();
        paint.setColor(color);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(2f);

        canvas.drawBitmap(bmOverlay, 0, 0, paint);
        canvas.drawRect(0, 0, bitmap.getWidth(), bitmap.getHeight(), paint);
    }
}
