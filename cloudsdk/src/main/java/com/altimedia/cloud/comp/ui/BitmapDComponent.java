/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.comp.ui;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Shader;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;

import com.altimedia.cloud.system.util.apexLogger;

class BitmapDComponent extends DComponent {
    private static final String TAG = BitmapDComponent.class.getSimpleName();
    private Paint bitmapPaint = new Paint(/*Paint.ANTI_ALIAS_FLAG*/);
    private Bitmap bitmap;
    private BitmapShader bitmapShader;

    public BitmapDComponent(ResourceItem resourceItem, View parent, Bitmap bitmap, Rect bounds) {
        super(resourceItem, parent);
        setBitmap(bitmap);
        setBounds(bounds);

        // TODO 차후 옵션 변경.
        bitmapPaint.setAntiAlias(false);
        bitmapPaint.setFilterBitmap(false);
    }

    public BitmapDComponent(ResourceItem resourceItem, View parent, Bitmap bitmap, int width, int height) {
        this(resourceItem, parent, bitmap, new Rect(0, 0, width, height));
    }

    @Override
    public void draw(@NonNull Canvas canvas) {
//        super.draw(canvas); // not necessary
        if (apexLogger.REPAINT) {
            Log.d(TAG, "draw: " + canvas.getClipBounds());
        }

        if (isVisible()) {
            if (bitmap != null) {
                bitmapPaint.setAlpha(floatToByte(getAlpha()));
                canvas.drawRect(0, 0, getWidth(), getHeight(), bitmapPaint);
            }
        }
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    @Override
    public void setBounds(Rect bounds) {
        super.setBounds(bounds);
        setFitBounds(bitmap);
    }

    public void setFitBounds(Bitmap bitmap) {
        if (bitmap != null) {
            float scaleX = (float) getWidth() / bitmap.getWidth();
            float scaleY = (float) getHeight() / bitmap.getHeight();

            if (bitmapShader != null) {
                Matrix matrix = new Matrix();
                matrix.postScale(scaleX, scaleY);
                bitmapShader.setLocalMatrix(matrix);
            }
        }
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;

        if (bitmap != null) {
            bitmap.prepareToDraw(); // // BitmapShader를 사용할때만 활성화.

            bitmapShader = new BitmapShader(bitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
            bitmapPaint.setShader(bitmapShader);

            // TODO
            if (getWidth() == 0 || getHeight() == 0) {
                setBounds(0, 0, bitmap.getWidth(), bitmap.getHeight());
            } else {
                setFitBounds(bitmap);
            }
        }
        repaint();
    }
}
