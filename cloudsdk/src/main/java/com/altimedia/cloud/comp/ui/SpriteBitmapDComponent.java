/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.comp.ui;

import android.graphics.Bitmap;
import android.graphics.Rect;
import android.util.Log;
import android.view.View;

class SpriteBitmapDComponent extends BitmapDComponent {
    private static final String TAG = SpriteBitmapDComponent.class.getSimpleName();
    private int bitmapIndex;
    private int spriteCount;
    private Bitmap[] bitmaps;

    public SpriteBitmapDComponent(ResourceItem resourceItem, View parent, Rect bounds, int firstIndex, int spriteCount) {
        super(resourceItem, parent, null, bounds);
        this.bitmapIndex = firstIndex;
        this.spriteCount = spriteCount;
    }

    public SpriteBitmapDComponent(ResourceItem resourceItem, View parent, Bitmap[] bitmaps, int firstIndex, int spriteCount) {
        super(resourceItem, parent, bitmaps[firstIndex], bitmaps[firstIndex].getWidth(), bitmaps[firstIndex].getHeight());
        bitmapIndex = firstIndex;
        this.spriteCount = spriteCount;
        this.bitmaps = bitmaps;
    }

    public Bitmap[] getBitmaps() {
        return bitmaps;
    }

    public int getSpriteCount() {
        if (bitmaps != null) {
            return Math.min(spriteCount, bitmaps.length);
        }

        return spriteCount;
    }

    public int getBitmapIndex() {
        return bitmapIndex;
    }

    public void setBitmapIndex(int index) {
        Log.d(TAG, "setBitmapIndex index: " + index);

        this.bitmapIndex = index;

        if (bitmaps != null && bitmapIndex < getSpriteCount()) {
            setBitmap(bitmaps[bitmapIndex]);
        }
    }


    public void setBitmaps(Bitmap[] bitmaps) {
        this.bitmaps = bitmaps;

        if (bitmaps != null && bitmapIndex < bitmaps.length) {
            setBitmap(bitmaps[bitmapIndex]);
        }
    }
}
