/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.comp.ic;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

import androidx.annotation.NonNull;

import com.altimedia.cloud.comp.ui.IAnimationView;
import com.altimedia.cloud.system.server.parser.obj.CsFrame;
import com.altimedia.cloud.system.util.apexLogger;

/**
 * Created by mc.kim on 18,04,2020
 * @deprecated
 */
public class CsSurfaceView extends SurfaceView implements SurfaceHolder.Callback, CsCloudView {
    private final String TAG = CsSurfaceView.class.getSimpleName();
    private OnWindowCallback mOnWindowCallback = null;
    public final ApexCloudView cloudView;
    private Context mContext = null;
    private SurfaceHolder mSurfaceHolder;

    RenderingThread mRThread;

    public CsSurfaceView(@NonNull Context context, @NonNull OnWindowCallback onWindowCallback) {
        super(context);
        this.mContext = context;
        this.mOnWindowCallback = onWindowCallback;
        this.cloudView = new ApexCloudView(context, this);
        this.mSurfaceHolder = getHolder();

        mSurfaceHolder.addCallback(this);
//        mRThread = new RenderingThread();
        setLayerType(View.LAYER_TYPE_HARDWARE, null);
    }

    @Override
    public View getPlayerView() {
        return this;
    }

    @Override
    public void release() {
        this.cloudView.clear(getContext());
    }

    @Override
    public Canvas lockCanvas(Rect rect) {
        Log.d(TAG, "lockCanvas rect : " + rect);
        if (mSurfaceHolder != null) {
            return mSurfaceHolder.lockCanvas(rect);
        } else {
            return null;
        }
    }

    @Override
    public void unlockCanvasAndPost(Canvas canvas) {
        Log.d(TAG, "unlockCanvasAndPost canvas : " + canvas);
        if (mSurfaceHolder != null) {
            mSurfaceHolder.unlockCanvasAndPost(canvas);
        }
    }

//    @Override
//    public void onDrawForeground(Canvas canvas) {
//        super.onDrawForeground(canvas);
//        Log.d(TAG, "onDrawForeground");
//    }

//    @Override
//    public void draw(Canvas canvas) {
//        super.draw(canvas);
//
//        Log.d(TAG, "draw");
//
//        if (iAnimationView != null) {
//            iAnimationView.draw(canvas);
//        }
//    }
//
//    @Override
//    protected void onDraw(Canvas canvas) {
//        super.onDraw(canvas);
//        Log.d(TAG, "onDraw");
//    }


    @Override
    public void updateFragment(CsFrame csFrame) {
        if (apexLogger.DEBUG) {
            Log.d(TAG, "updateFragment : " + System.currentTimeMillis());
            Log.d(TAG, "updateFragment csFrame : " + csFrame);
        }
        cloudView.updateFragment(csFrame);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.mOnWindowCallback.onAttachedToWindow();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.mOnWindowCallback.onDetachedFromWindow();
    }

    @Override
    public void notifyStartApp(String appName) {
        Log.d(TAG, "notifyStartApp appName : " + appName);
        cloudView.setCacheEnable(appName, true);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        Log.d(TAG, "surfaceCreated holder: " + holder);

        this.cloudView.onPrepared(true);

//        mRThread.start();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        Log.d(TAG, "surfaceChanged holder: " + holder);
        mSurfaceHolder = holder;
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        Log.d(TAG, "surfaceDestroyed holder: " + holder);
        mSurfaceHolder = holder;
        this.cloudView.onPrepared(false);
    }


    class RenderingThread extends Thread {
        public RenderingThread() {
            Log.d("RenderingThread", "RenderingThread()");
        }

        public void run() {
            Log.d("RenderingThread", "run()");
            Canvas canvas = null;
            while (true) {
                canvas = mSurfaceHolder.lockCanvas();
                if (canvas != null) {
                    try {
                        synchronized (mSurfaceHolder) {
                            draw(canvas);
                        }
                    } finally {
                        mSurfaceHolder.unlockCanvasAndPost(canvas);
                    }
                }
            }

        }
    }
}
