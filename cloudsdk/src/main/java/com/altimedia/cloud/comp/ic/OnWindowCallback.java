/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.comp.ic;

/**
 * Created by mc.kim on 10,04,2020
 */
public interface OnWindowCallback {
    void onAttachedToWindow();

    void onDetachedFromWindow();
}
