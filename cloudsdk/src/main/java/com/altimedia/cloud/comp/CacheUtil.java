/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.comp;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.util.Log;
import android.util.LruCache;

import androidx.annotation.Nullable;

import com.altimedia.cloud.system.server.parser.obj.CsFrame;
import com.altimedia.cloud.system.util.apexLogger;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.signature.ObjectKey;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by mc.kim on 12,05,2020
 */
public class CacheUtil {
    private static final String TAG = CacheUtil.class.getSimpleName();
    private static final CacheUtil ourInstance = new CacheUtil();
    public static final String DOC_FILENAME = "ImageMap.properties";
    public static final String IMAGE_FOLDER_NAME = "assetFolder";
    public static final String SOURCE_FOLDER_NAME = "sourceFolder";
    public static final String DOC_FOLDER_NAME = "docFolder";
    public static final String IMAGE_PRELOAD_FILENAME = "preloadImages";

    public final String KEY_LEFT = "left";
    public final String KEY_TOP = "top";
    public final String KEY_RIGHT = "right";
    public final String KEY_BOTTOM = "bottom";
    public boolean mIsUsed = false;

    private LruCache<Integer, Bitmap> mFragmentCache = null;
    // TODO 동기화 이슈 수정.
    private static ArrayList<String> preloadImagelList = new ArrayList<>();

    private static TimerTask writePreloadImageTimerTask;
    private static Timer writePreloadImageTimer;
    private static long WRITE_PRELOAD_IMAGE_TIMER_DELAY = 5000L;

    public static CacheUtil getInstance() {
        return ourInstance;
    }

    private CacheUtil() {
//        if (apexLogger.CACHE) {
//            Log.d(TAG, "Memory Total : " + (Runtime.getRuntime().totalMemory() / 1024));
//            Log.d(TAG, "Memory Max : " + (Runtime.getRuntime().maxMemory() / 1024));
//            Log.d(TAG, "Memory Free : " + (Runtime.getRuntime().freeMemory() / 1024));
//        }

        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
        final int cacheSize = maxMemory / 8;
        int diskCacheSizeBytes = 1024 * 1024 * 100; // 100 MB

        init(cacheSize, diskCacheSizeBytes);
    }

    public void init(final int memoryCacheSizeBytes, final int diskCacheSizeBytes) {
        ComponentGlideModule.getInstance().init(memoryCacheSizeBytes / 2, diskCacheSizeBytes);

        mFragmentCache = new LruCache<Integer, Bitmap>(memoryCacheSizeBytes / 2) {
            @Override
            protected int sizeOf(Integer key, Bitmap bitmap) {
                int size = 0;
                if (bitmap != null) {
                    size = bitmap.getByteCount() / 1024;
                }
                return size;
            }
        };

        if (apexLogger.CACHE) {
            Log.d(TAG, "init ics cacheSize : " + mFragmentCache.maxSize());
        }
    }

    public boolean addFragmentCache(String appName, final CsFrame csFrame) {
        if (apexLogger.CACHE) {
            Log.d(TAG, "addFragmentCache : " + appName + " | " + csFrame);
        }

        if (mFragmentCache.get(csFrame.getHashcode()) == null) {
            Bitmap bitmap = csFrame.getBitmap();

            if (ComponentConfig.showCacheBounds) {
                if (bitmap != null) {
                    Bitmap bmOverlay = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), bitmap.getConfig());
                    Canvas canvas = new Canvas(bmOverlay);
                    Paint paint = new Paint();
                    paint.setColor(ComponentConfig.COLOR_CACHE_ICS_BOUNDS);
                    paint.setStyle(Paint.Style.STROKE);
                    paint.setStrokeWidth(2f);

                    canvas.drawBitmap(bitmap, 0, 0, paint);
                    canvas.drawRect(0, 0, bitmap.getWidth(), bitmap.getHeight(), paint);

                    bitmap = bmOverlay;
                }
            }

            mFragmentCache.put(csFrame.getHashcode(), bitmap);
        }

        if (apexLogger.CACHE) {
            Log.d(TAG, "addFragmentCache putCount | usage  : " + mFragmentCache.putCount() + " | " + (mFragmentCache.putCount() / mFragmentCache.evictionCount()));
        }


        return true;
    }

    public Bitmap getFragmentCacheBitmap(String appName, CsFrame csFrame) {
        Bitmap bitmap = mFragmentCache.get(csFrame.getHashcode());

        if (apexLogger.CACHE) {
            Log.d(TAG, "getFragmentCacheBitmap : " + appName + " | " + csFrame + " | " + bitmap);
        }

        return bitmap;
    }

    public static boolean addPreloadImageSource(String source) {
        boolean result = false;
        if (!isContainPreloadImageSource(source)) {
            result = preloadImagelList.add(source);

            if (apexLogger.CACHE) {
                Log.d(TAG, "addPreloadImageSource : " + source + "|" + result);
            }
        }

        if (result) {
            writePreloadImageSource();
        }

        return result;
    }

    public static boolean removePreloadImageSource(String source) {
        boolean result = false;
        if (isContainPreloadImageSource(source)) {
            result = preloadImagelList.remove(source);

            if (apexLogger.CACHE) {
                Log.d(TAG, "removePreloadImageSource : " + source + "|" + result);
            }
        }
        if (result) {
            writePreloadImageSource();
        }

        return result;
    }

    public static boolean isContainPreloadImageSource(String source) {
        return preloadImagelList.contains(source);
    }

    public static void loadPreloadImageSource(Context context, OnLoadResult onLoadResult) {
        File dir = new File(Environment.getExternalStorageDirectory() + File.separator + DOC_FOLDER_NAME);
        if (dir.exists()) {
            File imageFolder = new File(Environment.getExternalStorageDirectory() + File.separator + DOC_FOLDER_NAME, IMAGE_PRELOAD_FILENAME);

            try {
                FileInputStream fileIn = new FileInputStream(imageFolder);
                ObjectInputStream ois = new ObjectInputStream(fileIn);

                preloadImagelList = (ArrayList<String>) ois.readObject();

                if (apexLogger.CACHE) {
                    Log.d(TAG, "loadPreloadImageSource : " + Arrays.asList(preloadImagelList).toString());
                }

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }

        for (final String source : preloadImagelList) {
            if (apexLogger.CACHE) {
                Log.d(TAG, "loadPreloadImageSource : " + source);
            }

            RequestListener<Drawable> drawableRequestListener = new RequestListener<Drawable>() {
                @Override
                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                    return false;
                }

                @Override
                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                    if (apexLogger.CACHE) {
                        Log.d(TAG, "loadPreloadImageSource onResourceReady : " + source);
                    }

                    if (ComponentConfig.showComponentPreloadBounds) {
                        Bitmap bitmap = ((BitmapDrawable) resource).getBitmap();
                        if (bitmap != null) {
                            ImageUtil.setOverlayRectToBitmap(bitmap, ComponentConfig.COLOR_PRELOAD_COMPONENT_BOUNDS);
                        }
                    }

                    return false;
                }
            };

            Glide.with(context)
                    .load(source)
                    .signature(new ObjectKey(source))
                    .onlyRetrieveFromCache(true)
                    .addListener(drawableRequestListener)
                    .preload();
        }

        if (onLoadResult != null) {
            onLoadResult.onLoaded();
        }
    }

    public static void writePreloadImageSource() {
        if (writePreloadImageTimer != null) {
            writePreloadImageTimer.cancel();
            writePreloadImageTimer = null;
        }
        if (writePreloadImageTimerTask != null) {
            writePreloadImageTimerTask.cancel();
            writePreloadImageTimerTask = null;
        }

        writePreloadImageTimer = new Timer();
        writePreloadImageTimerTask = new TimerTask() {
            @Override
            public void run() {
                File dir = new File(Environment.getExternalStorageDirectory() + File.separator + DOC_FOLDER_NAME);
                boolean doSave = true;
                if (!dir.exists()) {
                    doSave = dir.mkdirs();
                }

                if (doSave) {
                    File imageFolder = new File(Environment.getExternalStorageDirectory() + File.separator + DOC_FOLDER_NAME, IMAGE_PRELOAD_FILENAME);
                    ObjectOutput out = null;

                    try {
                        out = new ObjectOutputStream(new FileOutputStream(imageFolder));
                        out.writeObject(preloadImagelList);
                        out.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                if (apexLogger.CACHE) {
                    Log.d(TAG, "writePreloadImageSource : " + Arrays.asList(preloadImagelList).toString());
                }
            }
        };
        writePreloadImageTimer.schedule(writePreloadImageTimerTask, WRITE_PRELOAD_IMAGE_TIMER_DELAY);
    }

    public static void clearPreloadImageSource(Context context) {
        if (apexLogger.CACHE) {
            Log.d(TAG, "clearPreloadImageSource : " + preloadImagelList.size());
        }
        Glide.get(context).clearDiskCache();
        preloadImagelList.clear();
        writePreloadImageSource();
    }

    public interface OnLoadResult {
        void onLoaded();
    }

}
