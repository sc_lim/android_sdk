/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.comp.ic;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Shader;
import android.graphics.SurfaceTexture;
import android.util.Log;
import android.util.LruCache;
import android.view.TextureView;
import android.view.View;

import androidx.annotation.NonNull;

import com.altimedia.cloud.system.server.parser.obj.CsFrame;
import com.altimedia.cloud.system.util.apexLogger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

/**
 * Created by mc.kim on 08,04,2020
 */
class CsTextureView1 extends TextureView implements TextureView.SurfaceTextureListener, CsCloudView {
    private final String TAG = CsTextureView1.class.getSimpleName();
    private OnWindowCallback mOnWindowCallback = null;
    private Context mContext = null;
    private Bitmap icsBitmap;
    private BitmapShader icsBitmapShader;
    private HashMap<CsFragmentKey, BitmapShader> fragmentHashMap = new HashMap<>();
    private LruCache<Integer, Bitmap> mFrameCacheMap = null;
    private ArrayList<CsFrame> frameList = new ArrayList<CsFrame>();


    public CsTextureView1(@NonNull Context context, @NonNull OnWindowCallback onWindowCallback) {
        super(context);
        this.mContext = context;
        this.mOnWindowCallback = onWindowCallback;
        setSurfaceTextureListener(this);

        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
        final int cacheSize = maxMemory / 8;
        mFrameCacheMap = new LruCache<Integer, Bitmap>(cacheSize){
            @Override
            protected int sizeOf(Integer key, Bitmap bitmap) {
                return bitmap.getByteCount() / 1024;
            }
        };
    }

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
//        this.cloudView.drawPlaceHolder();
        if (apexLogger.DEBUG) {
            Log.d(TAG, "onSurfaceTextureAvailable");
        }
    }


    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {

    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
        return false;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {

    }

    @Override
    public Canvas lockCanvas(Rect dirty) {
        return super.lockCanvas(dirty);
    }

    @Override
    public void unlockCanvasAndPost(Canvas canvas) {
        super.unlockCanvasAndPost(canvas);
    }

    @Override
    public View getPlayerView() {
        return this;
    }

    @Override
    public void release() {
    }

    @Override
    public void updateFragment(CsFrame csFrame) {
        if (apexLogger.DEBUG) {
            Log.d(TAG, "updateFragment : " + System.currentTimeMillis());
            Log.d(TAG, "updateFragment csFrame : " + csFrame);
        }
    }

    //    @Override
    public void updateFragment(ArrayList<CsFrame> csFrames) {
        Log.d(TAG, "updateFragment size: " + csFrames.size());

        frameList.clear();
        for (CsFrame csFrame : csFrames) {
            int hashCode = csFrame.getHashcode();

            Bitmap bitmap = mFrameCacheMap.get(hashCode);
            if (bitmap != null) {
                csFrame.setBitmap(bitmap);
            } else {
//                Bitmap bitmap = BitmapFactory.decodeByteArray(source, 0, source.length);
//                BitmapShader icsBitmapShader = new BitmapShader(bitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
                mFrameCacheMap.put(hashCode, csFrame.getBitmap());
            }
            Log.d(TAG, "updateFragment csFrame: " + csFrame);

            frameList.add(csFrame);
        }
        rendering();
    }

    public void rendering() {
        Log.d("RenderingThread", "run()");
        Canvas canvas = null;
        canvas = lockCanvas();
        if (canvas != null) {
            try {
                for (CsFrame csFrame : frameList) {
                    Log.d(TAG, "rendering :  "  +csFrame);

                    Rect bounds = csFrame.getBounds();
//                    Paint paint = new Paint();
//                    paint.setShader(frameList.get(key));
//                    canvas.drawRect(bounds.left, bounds.top, bounds.right, bounds.bottom, paint);
                    canvas.translate(bounds.left, bounds.top);
                    canvas.drawBitmap(csFrame.getBitmap(), 0, 0, null);
                    canvas.translate(-bounds.left, -bounds.top);
                }
            } finally {
                unlockCanvasAndPost(canvas);
                frameList.clear();
            }
        }
    }

    class RenderingThread extends Thread {
        private final byte[] source;
        Rect bounds;
        public RenderingThread(byte[] source, int x, int y, int w, int h) {
            Log.d("RenderingThread", "RenderingThread()");
            this.source = source;
            bounds = new Rect(x, y, x + w, y + h);
        }

        public void run() {
            Log.d("RenderingThread", "run()");
            int hashCode = Arrays.hashCode(source);
            Bitmap bitmap = BitmapFactory.decodeByteArray(source, 0, source.length);
            BitmapShader icsBitmapShader = new BitmapShader(bitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
            CsFragmentKey csFragmentKey = new CsFragmentKey(null, hashCode, bounds);

            fragmentHashMap.put(csFragmentKey, icsBitmapShader);

            Canvas canvas = null;
            canvas = lockCanvas();
            if (canvas != null) {
                try {
                    for (CsFragmentKey key : fragmentHashMap.keySet()) {
                        Rect bounds = key.getRect();
                        Paint paint = new Paint();
                        paint.setShader(fragmentHashMap.get(key));
                        canvas.drawRect(bounds.left, bounds.top, bounds.right, bounds.bottom, paint);
                    }
                } finally {
                    unlockCanvasAndPost(canvas);
                }
            }

        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.mOnWindowCallback.onAttachedToWindow();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.mOnWindowCallback.onDetachedFromWindow();
    }

    @Override
    public void notifyStartApp(String appName) {
        if (apexLogger.DEBUG) {
            Log.d(TAG, "notifyStartApp : " + appName + " | time : " + System.currentTimeMillis());
        }
    }
}
