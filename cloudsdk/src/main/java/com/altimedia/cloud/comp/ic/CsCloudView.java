/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.comp.ic;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.view.View;
import android.view.ViewGroup;

import com.altimedia.cloud.system.server.parser.obj.CsFrame;

/**
 * Created by mc.kim on 18,04,2020
 */
public interface CsCloudView {

    View getPlayerView();

    void release();

    void updateFragment(CsFrame csFrame);

    void notifyStartApp(String appName);

    int getWidth();

    int getHeight();

    Canvas lockCanvas(Rect rect);

    void unlockCanvasAndPost(Canvas canvas);

    void setLayoutParams(ViewGroup.LayoutParams layoutParams);
}
