/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.comp.ui;

import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.provider.Settings;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.BounceInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.altimedia.cloud.ApexCloudSession;
import com.altimedia.cloud.comp.CacheUtil;
import com.altimedia.cloud.comp.ComponentConfig;
import com.altimedia.cloud.comp.ImageUtil;
import com.altimedia.cloud.system.server.parser.obj.BaseRequest;
import com.altimedia.cloud.system.server.parser.obj.BaseResponse;
import com.altimedia.cloud.system.server.parser.obj.ComponentType;
import com.altimedia.cloud.system.server.parser.obj.ProtocolType;
import com.altimedia.cloud.system.server.parser.obj.animation.request.animation.Action;
import com.altimedia.cloud.system.server.parser.obj.animation.request.animation.InterpolatorType;
import com.altimedia.cloud.system.server.parser.obj.animation.request.animation.close.AnimationCloseData;
import com.altimedia.cloud.system.server.parser.obj.animation.request.animation.create.ActionItem;
import com.altimedia.cloud.system.server.parser.obj.animation.request.animation.create.AnimationCreateData;
import com.altimedia.cloud.system.server.parser.obj.animation.request.animation.create.Param;
import com.altimedia.cloud.system.server.parser.obj.animation.request.animation.finish.AnimationFinishData;
import com.altimedia.cloud.system.server.parser.obj.animation.request.component.attach.ComponentAttachData;
import com.altimedia.cloud.system.server.parser.obj.animation.request.component.attach.DestComponent;
import com.altimedia.cloud.system.server.parser.obj.animation.request.component.attach.SrcComponent;
import com.altimedia.cloud.system.server.parser.obj.animation.request.component.create.ComponentData;
import com.altimedia.cloud.system.server.parser.obj.animation.request.component.create.CreateComponentData;
import com.altimedia.cloud.system.server.parser.obj.animation.request.component.create.CreateSourceData;
import com.altimedia.cloud.system.server.parser.obj.animation.request.component.delete.ComponentDeleteData;
import com.altimedia.cloud.system.server.parser.obj.animation.request.component.detach.ComponentDetachData;
import com.altimedia.cloud.system.server.parser.obj.animation.request.component.rect.FillRectData;
import com.altimedia.cloud.system.server.parser.obj.animation.request.component.visible.ComponentVisibleData;
import com.altimedia.cloud.system.server.parser.obj.animation.response.CreateAnimationResponse;
import com.altimedia.cloud.system.server.parser.obj.animation.response.CreateDataResponse;
import com.altimedia.cloud.system.util.ElapsedTime;
import com.altimedia.cloud.system.util.apexLogger;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.transition.Transition;
import com.bumptech.glide.signature.ObjectKey;
import com.google.gson.Gson;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import static com.altimedia.cloud.comp.ComponentConfig.DISK_CACHE_STRATEGY;
import static com.altimedia.cloud.comp.ComponentConfig.FRAME_DELAY;
import static com.altimedia.cloud.comp.ComponentConfig.IS_USE_COMPONENT_RESULT_CALLBACK;

public class IAnimationView extends View implements IAnimation {
    private final String TAG = IAnimationView.class.getSimpleName();

    private final int ROOT_ID = 100000;
    private final int BACK_ID = 99;
    private HashMap<Integer, DComponent> mCacheView = new HashMap<>();
    private HashMap<String, AnimatorSet> mCacheAnimation = new HashMap<>();
    private HashMap<Integer, Integer> mCacheAnimationViewId = new HashMap<>();
    private ApexCloudSession mApexCloudSession;
    private ImageUtil mImageUtil = null;
    private Context mContext;
    private DComponent rootDComponent;

    public IAnimationView(Context context, ApexCloudSession apexCloudSession) {
        super(context);
        mImageUtil = new ImageUtil(context);
        this.mApexCloudSession = apexCloudSession;
        this.mContext = context;

        final DisplayMetrics metrics = mContext.getResources().getDisplayMetrics();
        rootDComponent = new DComponent(new ResourceItem(ROOT_ID),this);
        rootDComponent.setBounds(0, 0, metrics.widthPixels, metrics.heightPixels);
    }

    private boolean isSupportedSpriteFunction(DComponent dComponent) {
        return dComponent instanceof SpriteBitmapDComponent && ((SpriteBitmapDComponent) dComponent).getBitmaps() != null;
    }

    private void createAnimation(final AnimationCreateData animationCreateData) {
        HashMap<Integer, List<ActionItem>> propertyValuesHolders = generatePropertyHolderMap(animationCreateData.getData().getActionItem());
        Iterator<Integer> ids = propertyValuesHolders.keySet().iterator();
        try {
            setAnimatorScale(getContext(), 1f);
        } catch (Exception e) {
            e.printStackTrace();
        }
        while (ids.hasNext()) {
            final int id = ids.next();
            boolean isCanceledItem = clearAnimation(id);
            List<ActionItem> holderList = propertyValuesHolders.get(id);
            final DComponent targetDComponent = findComponentById(id);
            if (targetDComponent == null) {
                continue;
            }
            ArrayList<android.animation.Animator> animatorList = buildAnimator(isCanceledItem, holderList, targetDComponent);
            final AnimatorSet animatorSet = new AnimatorSet();
            animatorSet.setTarget(targetDComponent);
            animatorSet.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(android.animation.Animator animation) {
                    super.onAnimationEnd(animation);
                    setLayerType(View.LAYER_TYPE_NONE, null);

                    mCacheAnimation.remove(animationCreateData.getRequestId() + ":" + id);
                    mCacheAnimationViewId.remove(targetDComponent.getId());
                    CreateAnimationResponse response = new CreateAnimationResponse(animationCreateData.getRequestId(), animationCreateData.getProtocolType());
                    response.setResult(true);
                    response.setResultData(3);
                    sendResult(response);

                }

                @Override
                public void onAnimationStart(android.animation.Animator animation) {
                    super.onAnimationStart(animation);
                    setLayerType(View.LAYER_TYPE_HARDWARE, null);

                    mCacheAnimation.put(animationCreateData.getRequestId() + ":" + targetDComponent.getId(), animatorSet);
                    mCacheAnimationViewId.put(targetDComponent.getId(), animationCreateData.getRequestId());
                    CreateAnimationResponse response = new CreateAnimationResponse(animationCreateData.getRequestId(), animationCreateData.getProtocolType());
                    response.setResult(true);
                    response.setResultData(1);
                    sendResult(response);
                }
            });
            animatorSet.playTogether(animatorList);
            animatorSet.start();
        }
    }

    private Method scaleMethod = null;

    private void setAnimatorScale(Context context, float value) throws InvocationTargetException, IllegalAccessException, ClassNotFoundException {
        float durationScale = Settings.Global.getFloat(context.getContentResolver(),
                Settings.Global.ANIMATOR_DURATION_SCALE, 0);
        if (durationScale != value) {
            Object[] args = new Object[]{value};
            if (scaleMethod == null) {
                String methodName = "setDurationScale";
                Class c = Class.forName("android.animation.ValueAnimator");
                Method[] methods = c.getMethods();
                for (Method m : methods) {
                    if (methodName.equals(m.getName())) {
                        // for static methods we can use null as instance of class
                        scaleMethod = m;
                        break;
                    }
                }
            }
            if (scaleMethod != null) {
                scaleMethod.invoke(null, args);
            }
        }
    }

    private void cancelAnimation(AnimationCloseData animationCloseData) {
        boolean isSuccess = false;
        int requestID = animationCloseData.getData().getAnimateId();
        if (mCacheAnimation.containsKey(requestID + ":" + getViewIdByAnimationRequestId(requestID))) {
            AnimatorSet set = mCacheAnimation.get(requestID + ":" + getViewIdByAnimationRequestId(requestID));
            set.cancel();
            isSuccess = true;
        }
        BaseResponse animationResponse = new BaseResponse(animationCloseData.getRequestId(), animationCloseData.getProtocolType());
        animationResponse.setResult(isSuccess);
        sendResult(animationResponse);
    }

    private boolean clearAnimation(final int id) {
        if (mCacheAnimationViewId.containsKey(id)) {
            int requestId = mCacheAnimationViewId.get(id);
            AnimatorSet set = mCacheAnimation.get(requestId + ":" + id);
            if (set != null) {
                set.end();
                return true;
            }
        }
        return false;
    }

    private int getViewIdByAnimationRequestId(int requestID) {
        Iterator<Integer> viewIdList = mCacheAnimationViewId.keySet().iterator();
        while (viewIdList.hasNext()) {
            int viewId = viewIdList.next();
            if (mCacheAnimationViewId.get(viewId) == requestID) {
                return viewId;
            }
        }
        return -1;
    }

    private void finishAnimation(AnimationFinishData animationFinishData) {
        boolean isSuccess = false;
        int requestID = animationFinishData.getData().getAnimateId();
        if (mCacheAnimation.containsKey(requestID + ":" + getViewIdByAnimationRequestId(requestID))) {
            AnimatorSet set = mCacheAnimation.get(requestID + ":" + getViewIdByAnimationRequestId(requestID));
            set.end();
            isSuccess = true;
        }
        BaseResponse animationResponse = new BaseResponse(animationFinishData.getRequestId(), animationFinishData.getProtocolType());
        animationResponse.setResult(isSuccess);
        sendResult(animationResponse);
    }


    private HashMap<Integer, List<ActionItem>> generatePropertyHolderMap(List<ActionItem> items) {
        HashMap<Integer, List<ActionItem>> resultMap = new HashMap<>();
        for (ActionItem item : items) {
            Integer objectId = item.getObjectid();
            List<ActionItem> holders = null;
            if (resultMap.containsKey(objectId)) {
                holders = resultMap.get(objectId);
            } else {
                holders = new ArrayList<>();
            }
            holders.add(item);
            resultMap.put(objectId, holders);
        }
        return resultMap;
    }

    private ArrayList<android.animation.Animator> buildAnimator(boolean isCanceledItem, List<ActionItem> actionItemList, final DComponent dComponent) {
        ArrayList<android.animation.Animator> animatorList = new ArrayList<>();

        for (ActionItem actionItem : actionItemList) {
            Action action = actionItem.getAction();
            Param param = actionItem.getParam();
            switch (action) {
                case sprite: {
                    if (!isSupportedSpriteFunction(dComponent)) {
                        if (apexLogger.DEBUG) {
                            Log.d(TAG, "sprite continue");
                        }
                        continue;
                    }
                    final SpriteBitmapDComponent spriteBitmapDComponent = (SpriteBitmapDComponent) dComponent;
                    Bitmap[] bitmaps = spriteBitmapDComponent.getBitmaps();
                    int repeat = param.getRepeat();
                    long duration = actionItem.getDuration();
                    int spriteCount = spriteBitmapDComponent.getSpriteCount();

                    ValueAnimator valueAnimator = ValueAnimator.ofInt(0, spriteCount);
                    valueAnimator.setInterpolator(generateInterpolator(actionItem.getInterpolatorType()));
                    valueAnimator.setFrameDelay(FRAME_DELAY);
                    valueAnimator.setDuration(duration);
                    valueAnimator.setRepeatCount(repeat);
                    valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                        public void onAnimationUpdate(ValueAnimator animation) {
                            if (apexLogger.DEBUG) {
                                Log.d(TAG, "onAnimationUpdate sprite animation: " + animation.getAnimatedValue() + "|" + dComponent);
                            }
                            spriteBitmapDComponent.setBitmapIndex(Math.abs((Integer) animation.getAnimatedValue()));
                        }
                    });
                    animatorList.add(valueAnimator);

                    break;
                }

                case fade: {
                    ValueAnimator fadeAnimator = ValueAnimator.ofFloat(0f, 1f);
                    fadeAnimator.setInterpolator(generateInterpolator(actionItem.getInterpolatorType()));
                    fadeAnimator.setFrameDelay(FRAME_DELAY);
                    fadeAnimator.setDuration(actionItem.getDuration());
                    fadeAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                        public void onAnimationUpdate(ValueAnimator animation) {
                            if (apexLogger.DEBUG) {
                                Log.d(TAG, "onAnimationUpdate fade animation: " + animation.getAnimatedValue() + "|" + dComponent);
                            }
                            dComponent.setAlpha((Float) animation.getAnimatedValue());
                        }
                    });
                    animatorList.add(fadeAnimator);
                }
                break;
                case scale:
                case bounds: {
                    Rect startBounds = new Rect((int) param.getsLeft(), (int) param.getsTop(), (int) param.getsRight(), (int) param.getsBottom());
                    Rect destBounds = new Rect((int) param.getdLeft(), (int) param.getdTop(), (int) param.getdRight(), (int) param.getdBottom());

                    ValueAnimator translation = ValueAnimator.ofObject(new RectEvaluator(), startBounds, destBounds);
                    translation.setInterpolator(generateInterpolator(actionItem.getInterpolatorType()));
                    translation.setFrameDelay(FRAME_DELAY);
                    translation.setDuration(actionItem.getDuration());
                    translation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                        public void onAnimationUpdate(ValueAnimator animation) {
                            if (apexLogger.DEBUG) {
                                Log.d(TAG, "onAnimationUpdate bounds animation: " + animation.getAnimatedValue() + "|" + dComponent);
                            }
                            dComponent.setBounds((Rect) animation.getAnimatedValue());
                        }
                    });

                    animatorList.add(translation);
                }
                break;
            }
        }

        return animatorList;
    }


    private Interpolator generateInterpolator(InterpolatorType interpolatorType) {
        if (apexLogger.DEBUG) {

            Log.d(TAG, "generateInterpolator interpolatorType: " + interpolatorType);
        }
        switch (interpolatorType) {
            case linear:
                return new LinearInterpolator();
            case bounce:
                return new BounceInterpolator();
            case elastic:
                return new AccelerateInterpolator();
        }
        return null;
    }

    private void sendResult(Object o) {
        String result = new Gson().toJson(o);
        if (apexLogger.DEBUG) {
            Log.d(TAG, "sendResult to web : " + result);
        }
        try {
            mApexCloudSession.sendWeb(result);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Rect initContainerRect(CreateSourceData source) {
        DisplayMetrics metrics = mContext.getResources().getDisplayMetrics();

        int sourceWidth = source.getWidth();
        int sourceHeight = source.getHeight();
        final int webWidth = metrics.widthPixels;
        final int webHeight = metrics.heightPixels;

        boolean isOverWidth = sourceWidth >= webWidth;
        boolean isOverHeight = sourceHeight >= webHeight;

//        if (isOverWidth || isOverHeight) {
//            if (isOverWidth) {
//                params.rightMargin = webWidth - source.getWidth();
//                params.leftMargin = webWidth - source.getWidth();
//            }
//            if (isOverHeight) {
//                params.topMargin = webHeight - source.getHeight();
//                params.bottomMargin = webHeight - source.getHeight();
//            }
//        }

        return new Rect(0, 0, isOverWidth ? webWidth : sourceWidth,
                isOverHeight ? webHeight : source.getHeight());
    }


    private void createComponent(CreateComponentData createComponentData) {
        ArrayList<ComponentData> componentDatas = createComponentData.getData();

        if (componentDatas != null) {
            LoadComponentResultCallback loadComponentResultCallback = new LoadComponentResultCallback(this, getContext(), mApexCloudSession);
            loadComponentResultCallback.setComponentBaseInfo(createComponentData.getRequestId(), createComponentData.getProtocolType(), componentDatas.size());

            boolean isSuccess = true;
            int i =0;
            for (ComponentData componentData : componentDatas) {
                ComponentType type = componentData.getType();
                CreateSourceData sourceData = componentData.getSource();
                final ResourceItem item = new ResourceItem(type == ComponentType.root ? ROOT_ID : generatedItemId(), componentData);

                if (apexLogger.DEBUG) {
                    Log.d(TAG, "createComponent create components ResourceItem : [" + ++i + "]" + item);
                }

                switch (type) {
                    case root:
                        mCacheView.put(item.getId(), rootDComponent);
                        loadComponentResultCallback.onLoadStart(item.getId(), getHeight(), getHeight());
                        loadComponentResultCallback.onLoaded(true, item.getId(), getWidth(), getHeight());
                        break;
                    case container:
                        DComponent dComponent = new DComponent(item, this);
                        dComponent.setBounds(initContainerRect(sourceData));
                        dComponent.setVisible(componentData.isVisible());
                        mCacheView.put(item.getId(), dComponent);

                        loadComponentResultCallback.onLoadStart(item.getId(), dComponent.getWidth(), dComponent.getHeight());
                        loadComponentResultCallback.onLoaded(true, item.getId(), dComponent.getWidth(), dComponent.getHeight());
                        break;
                    case raw:
                        if (apexLogger.DEBUG) {
                            Log.d(TAG, "createComponent raw: not support yet");
                        }
                        loadComponentResultCallback.onLoadStart(item.getId(), 0, 0);
                        loadComponentResultCallback.onLoaded(true, item.getId(), 0, 0);
                        break;
                    case url:
                        loadImage(item, loadComponentResultCallback);
                        break;
                    case file:
                        if (apexLogger.DEBUG) {
                            Log.d(TAG, "createComponent file: not support yet");
                        }
                        loadImageFile(item, loadComponentResultCallback);
                        break;
                    case binary:
                        loadBinaryImage(item, loadComponentResultCallback);
                        break;
                    case sprite:
                        loadSpriteImage(item, loadComponentResultCallback);
                        break;
                    default:
                        if (apexLogger.DEBUG) {
                            Log.d(TAG, "createComponent ??");
                        }
                        break;
                }
            }
            if (!IS_USE_COMPONENT_RESULT_CALLBACK) {
                loadComponentResultCallback.onResult(isSuccess);
            }
        }
    }


    private void detachComponents(ComponentDetachData componentDetachData) {
        boolean isSuccess = true;
        boolean isDetached = false;

        List<com.altimedia.cloud.system.server.parser.obj.animation.request.component.detach.Data> detachList =
                componentDetachData.getData();
        for (com.altimedia.cloud.system.server.parser.obj.animation.request.component.detach.Data data : detachList) {
            isDetached = detachComponent(data);
            if (!isDetached) {
                isSuccess = false;
            }
        }
        BaseResponse animationResponse = new BaseResponse(componentDetachData.getRequestId(), componentDetachData.getProtocolType());
        animationResponse.setResult(isSuccess);
        sendResult(animationResponse);
    }

    private boolean detachComponent(
            com.altimedia.cloud.system.server.parser.obj.animation.request.component.detach.Data componentDetachData) {

        if (apexLogger.DEBUG) {
            Log.d(TAG, "attachComponent componentDetachData : " + componentDetachData);
        }

        boolean isSuccess = false;
        int viewId = componentDetachData.getSrcComponent().getObjectid();
        final DComponent dComponent = findComponentById(viewId);
        if (dComponent != null) {
            clearAnimation(dComponent.getId());
            rootDComponent.removeComponent(dComponent);
            isSuccess = true;
        }
        return isSuccess;
    }

    private void attachComponents(ComponentAttachData componentAttachData){
        if (apexLogger.DEBUG) {
            Log.d(TAG, "attachComponent componentAttachData : " + componentAttachData);
        }

        boolean isSuccess = true;
        boolean isAttached = false;
        List<com.altimedia.cloud.system.server.parser.obj.animation.request.component.attach.Data> attachList =
                componentAttachData.getData();
        for (com.altimedia.cloud.system.server.parser.obj.animation.request.component.attach.Data data : attachList) {
            isAttached = attachComponent(data);
            if (!isAttached) {
                isSuccess = false;
            }
        }
        BaseResponse animationResponse = new BaseResponse(componentAttachData.getRequestId(), componentAttachData.getProtocolType());
        animationResponse.setResult(isSuccess);
        sendResult(animationResponse);
    }

    private boolean attachComponent(
            com.altimedia.cloud.system.server.parser.obj.animation.request.component.attach.Data componentAttachData) {
        boolean isSuccess = false;
        DestComponent desComponent = componentAttachData.getDestComponent();
        SrcComponent srcComponent = componentAttachData.getSrcComponent();
        DComponent desDComponent = null;
        DComponent srcDComponent = null;
        if (apexLogger.DEBUG) {
            Log.d(TAG, "attachComponent desComponent: " + desComponent);
            Log.d(TAG, "attachComponent srcComponent: " + srcComponent);
        }

        desDComponent = findComponentById(desComponent.getObjectid());
        if (desDComponent != null) {
            if (apexLogger.DEBUG) {
                Log.d(TAG, "attachComponent desDComponent: " + desDComponent.getId());
            }
            srcDComponent = findComponentById(srcComponent.getObjectid());
            if (srcDComponent != null) {
                if (apexLogger.DEBUG) {
                    Log.d(TAG, "attachComponent srcDComponent: " + srcDComponent.getId());
                }

                srcDComponent.setBounds(desComponent.getLeft(), desComponent.getTop(), desComponent.getRight(), desComponent.getBottom());
                srcDComponent.setzOrder(desComponent.getzOrder());
                desDComponent.addComponent(srcDComponent);

                isSuccess = true;
            }
        }

        if (apexLogger.DEBUG) {
            Log.d(TAG, "attachComponent isSuccess: " + isSuccess);
        }
        return  isSuccess;
    }

    private boolean isAttachedComponent(int id) {
        if (id == ROOT_ID) {
            return true;
        }
        
        return rootDComponent.containComponent(id);
    }

    private DComponent findComponentById(int id) {
        if (id == ROOT_ID) {
            return rootDComponent;
        }
        return mCacheView.get(id);
    }


    private void clearChildViewCache(DComponent dComponent) {
        clearChildViewCache(dComponent, true);
    }

    private void clearChildViewCache(DComponent dComponent, boolean isRemoveChild) {
        DComponent[] dComponents = dComponent.getDComponents();
        if (dComponents != null) {
            for (DComponent childDComponent : dComponents) {
                if (apexLogger.DEBUG) {
                    Log.d(TAG, "clearChildViewCache childDComponent: " + childDComponent.getId());
                }
                dComponent.removeComponent(childDComponent);
                clearChildViewCache(childDComponent);
                mCacheView.remove(childDComponent.getId());
            }
        }
    }

    private void deleteComponents(ComponentDeleteData componentDeleteData) {
        List<com.altimedia.cloud.system.server.parser.obj.animation.request.component.delete.Data>  delList =
                componentDeleteData.getData();
        for (com.altimedia.cloud.system.server.parser.obj.animation.request.component.delete.Data data : delList) {
            deleteComponent(data);
        }
        BaseResponse animationResponse = new BaseResponse(componentDeleteData.getRequestId(), componentDeleteData.getProtocolType());
        animationResponse.setResult(true);
        sendResult(animationResponse);
    }

    private void deleteComponent(
            com.altimedia.cloud.system.server.parser.obj.animation.request.component.delete.Data componentDeleteData) {
        int targetId = componentDeleteData.getObjectid();
        DComponent dComponent = findComponentById(componentDeleteData.getObjectid());

        if (isAttachedComponent(targetId)) {
            if (componentDeleteData.isDeleteTree()) {
                clearChildViewCache(dComponent);
            }
            rootDComponent.removeComponent(dComponent);
        }
        clearAnimation(targetId);
        if (dComponent.getId() != ROOT_ID) {
            mCacheView.remove(dComponent.getId());
        }
    }

    private void setVisibilityComponent(ComponentVisibleData componentVisibleData) {
        List<com.altimedia.cloud.system.server.parser.obj.animation.request.component.visible.ActionItem> actionItems =
                componentVisibleData.getData().getActionItem();
        for (com.altimedia.cloud.system.server.parser.obj.animation.request.component.visible.ActionItem item : actionItems) {
            DComponent dComponent = null;
            if (!isAttachedComponent(item.getObjectid())) {
                if (mCacheView.containsKey(item.getObjectid())) {
                    dComponent = mCacheView.get(item.getObjectid());
                }
            } else {
                dComponent = findComponentById(item.getObjectid());
            }
            if (dComponent != null) {
                dComponent.setVisible(item.isVisible());
            }

        }
        BaseResponse animationResponse = new BaseResponse(componentVisibleData.getRequestId(), componentVisibleData.getProtocolType());
        animationResponse.setResult(true);
        sendResult(animationResponse);
    }


    private void fillRectComponent(FillRectData fillRectData) {
        int viewId = fillRectData.getData().getObjectid();
        com.altimedia.cloud.system.server.parser.obj.animation.request.component.rect.Data fillRectInfo = fillRectData.getData();
        boolean isSuccess = false;
        if (isAttachedComponent(viewId)) {
            DComponent dComponent = findComponentById(viewId);
            dComponent.setBackgroundColor(fillRectInfo.getColorInt());
            isSuccess = true;
        }

        BaseResponse animationResponse = new BaseResponse(fillRectData.getRequestId(), fillRectData.getProtocolType());
        animationResponse.setResult(isSuccess);
        sendResult(animationResponse);
    }


    private void loadBinaryImage(final ResourceItem item, @NonNull ComponentLoadCallback componentLoadCallback) {
        final ComponentData componentData = item.getComponentData();
        if (componentData.getSource() == null || componentData.getSource().getByteArray() == null) {
            if (componentLoadCallback != null) {
                componentLoadCallback.onLoadStart(item.getId(), 0, 0);
                componentLoadCallback.onLoaded(false, item.getId(), 0, 0);
            }
            return;
        }
        String source = componentData.getSource().getByteArray();
        String imageType = componentData.getSource().getImageType();
        CreateSourceData createSourceData = componentData.getSource();

        byte[] decodedString = Base64.decode(source, Base64.DEFAULT);

        if (!IS_USE_COMPONENT_RESULT_CALLBACK) {
            if (componentLoadCallback != null) {
                componentLoadCallback.onLoadStart(item.getId(), createSourceData.getWidth(), createSourceData.getHeight());
                componentLoadCallback.onLoaded(true, item.getId(), createSourceData.getWidth(), createSourceData.getHeight());
                componentLoadCallback = null;
            }
        }

        final ComponentLoadTarget mImageLoadTarget = new ComponentLoadTarget(getContext(), source, item, componentLoadCallback, mCacheView, rootDComponent);

        Glide.with(getContext())
                .load(decodedString)
                .override(createSourceData.getWidth(), createSourceData.getHeight())
                .diskCacheStrategy(componentData.isCache_file() ? DISK_CACHE_STRATEGY : DiskCacheStrategy.NONE)
                .skipMemoryCache(!componentData.isCache_mem())
//                .signature(new ObjectKey(??))
                .into(mImageLoadTarget);

    }

    // TODO
    private void loadImageFile(final ResourceItem item, @NonNull ComponentLoadCallback componentLoadCallback) {
        final ComponentData componentData = item.getComponentData();

        if (componentData.getSource() == null || componentData.getSource().getPath() == null) {
            if (componentLoadCallback != null) {
                componentLoadCallback.onLoadStart(item.getId(), 0, 0);
                componentLoadCallback.onLoaded(false, item.getId(), 0, 0);
            }
            // Fail case
            return;
        }
//        String source = componentData.getSource().getPath();
//
//        Bitmap cacheBitmap = CacheUtil.getInstance().getImage(source);
//        if (apexLogger.DEBUG) {
//            Log.d(TAG, "loadImageFile cacheBitmap: " + cacheBitmap);
//        }
//
//        if (cacheBitmap != null) {
//            BitmapDComponent dComponent = new BitmapDComponent(item, this, cacheBitmap);
//            dComponent.setVisible(componentData.isVisible());
//            mCacheView.put(item.getId(), dComponent);
//
//            if (componentLoadCallback != null) {
//                componentLoadCallback.onLoadStart(item.getId(), cacheBitmap.getWidth(), cacheBitmap.getHeight());
//                componentLoadCallback.onLoaded(true, item.getId(), cacheBitmap.getWidth(), cacheBitmap.getHeight());
//            }
//        } else {
            if (componentLoadCallback != null) {
                componentLoadCallback.onLoadStart(item.getId(), 0, 0);
                componentLoadCallback.onLoaded(true, item.getId(), 0, 0);
                componentLoadCallback = null;
            }
//        }
    }

    private void loadImage(final ResourceItem item, @NonNull ComponentLoadCallback componentLoadCallback) {
        final ComponentData componentData = item.getComponentData();
        if (componentData.getSource() == null || componentData.getSource().getPath() == null) {
            if (componentLoadCallback != null) {
                componentLoadCallback.onLoadStart(item.getId(), 0, 0);
                componentLoadCallback.onLoaded(false, item.getId(), 0, 0);
            }
            return;
        }

        CreateSourceData createSourceData = componentData.getSource();
        String source = componentData.getSource().getPath();

        RequestListener<Drawable> drawableRequestListener = new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                if (apexLogger.CACHE) {
                    Log.d(TAG, "loadImage onResourceReady : " + dataSource);
                }

                if (ComponentConfig.showComponentCacheBounds) {
                    Bitmap bitmap = ((BitmapDrawable) resource).getBitmap();
                    if (bitmap != null) {
                        ImageUtil.setOverlayRectToBitmap(bitmap, ComponentConfig.getCacheColor(dataSource));
                    }
                }

                return false;
            }
        };

        if (!IS_USE_COMPONENT_RESULT_CALLBACK) {
            if (componentLoadCallback != null) {
                componentLoadCallback.onLoadStart(item.getId(), createSourceData.getWidth(), createSourceData.getHeight());
                componentLoadCallback.onLoaded(true, item.getId(), createSourceData.getWidth(), createSourceData.getHeight());
                componentLoadCallback = null;
            }
        }

        final ComponentLoadTarget mImageLoadTarget = new ComponentLoadTarget(getContext(), source, item, componentLoadCallback, mCacheView, rootDComponent);

        Glide.with(getContext()).load(source)
//                .override(createSourceData.getWidth(), createSourceData.getHeight())
                .diskCacheStrategy(componentData.isCache_file() ? DISK_CACHE_STRATEGY : DiskCacheStrategy.NONE)
                .skipMemoryCache(!componentData.isCache_mem())
                .signature(new ObjectKey(source))
                .addListener(drawableRequestListener)
                .into(mImageLoadTarget);

    }

    private void loadSpriteImage(final ResourceItem item, @NonNull ComponentLoadCallback componentLoadCallback) {
        final ComponentData componentData = item.getComponentData();
        if (componentData.getSource() == null || componentData.getSource().getPath() == null) {
            if (componentLoadCallback != null) {
                componentLoadCallback.onLoadStart(item.getId(), 0, 0);
                componentLoadCallback.onLoaded(false, item.getId(), 0, 0);
            }
            return;
        }

        CreateSourceData createSourceData = componentData.getSource();
        String source = createSourceData.getPath();
        int spriteWidth = createSourceData.getSpriteWidth() * createSourceData.getSpriteCount();
        int spriteHeight = createSourceData.getSpriteHeight();

        if (!IS_USE_COMPONENT_RESULT_CALLBACK) {
            if (componentLoadCallback != null) {
                componentLoadCallback.onLoadStart(item.getId(), spriteWidth, spriteHeight);
                componentLoadCallback.onLoaded(true, item.getId(), spriteWidth, spriteHeight);
                componentLoadCallback = null;
            }
        }

        final ComponentLoadTarget mImageLoadTarget = new ComponentLoadTarget(getContext(), source, item, componentLoadCallback, mCacheView, rootDComponent);

        Glide.with(getContext()).load(source)
                .override(spriteWidth, spriteHeight)
                .diskCacheStrategy(componentData.isCache_file() ? DISK_CACHE_STRATEGY : DiskCacheStrategy.NONE)
                .skipMemoryCache(!componentData.isCache_mem())
                .signature(new ObjectKey(source))
                .into(mImageLoadTarget);
    }


    private ArrayList<Integer> idMap = new ArrayList<>();

    private int generatedItemId() {
        long seed = System.currentTimeMillis();
        Random rand = new Random(seed);
        int randomValue = 0;
        do {
            randomValue = rand.nextInt();
        } while (idMap.contains(randomValue) || randomValue <= 0);
//        Log.d(TAG, "randomValue : " + randomValue);
        idMap.add(randomValue);
        return randomValue;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        ElapsedTime.getInstance().addSubSession(TAG + " onDraw start");
//        canvas.drawBitmap(bufBitmap, 0, 0, null);
        if (rootDComponent != null) {
            rootDComponent.draw(canvas);
        }
        ElapsedTime.getInstance().addSubSession(TAG + " onDraw start end");
    }

    private class ComponentLoadTarget extends CustomTarget<Drawable> {
        private final String TAG = this.getClass().getSimpleName();
        final ResourceItem item;
        final ComponentLoadCallback componentLoadCallback;
        final Context mContext;
        final HashMap<Integer, BitmapDComponent> mCacheMap;
        final DComponent mParentsDComponent;
        final String mTargetUrl;

        public ComponentLoadTarget(Context context, String url, ResourceItem item, ComponentLoadCallback ComponentLoadCallback,
                               HashMap cacheMap, DComponent mParentsDComponent) {
            this.item = item;
            this.mTargetUrl = url;
            this.mContext = context;
            this.componentLoadCallback = ComponentLoadCallback;
            this.mParentsDComponent = mParentsDComponent;
            this.mCacheMap = cacheMap;
        }

        @Override
        public void onLoadStarted(@Nullable Drawable placeholder) {
            super.onLoadStarted(placeholder);
            ComponentData componentData = item.getComponentData();

            if (apexLogger.DEBUG) {
                Log.d(TAG, "onLoadStarted mComponentData " + componentData);
            }

            BitmapDComponent bitmapDComponent = null;
            CreateSourceData createSourceData = componentData.getSource();

            if (componentData.getType() == ComponentType.sprite) {
                final String source = createSourceData.getPath();

                int spriteWidth = createSourceData.getSpriteWidth();
                int spriteHeight = createSourceData.getSpriteHeight();
                int spriteCount = createSourceData.getSpriteCount();
                final int firstIndex = createSourceData.getFirstVisibleIndex();
                Rect shellRect = new Rect(0, 0, spriteWidth, spriteHeight);
                bitmapDComponent = new SpriteBitmapDComponent(item, mParentsDComponent.getParentView(), shellRect, firstIndex, spriteCount);
            } else {
                bitmapDComponent = new BitmapDComponent(item, mParentsDComponent.getParentView(), null, createSourceData.getWidth(), createSourceData.getHeight());
            }

            bitmapDComponent.setSrcUrl(mTargetUrl);
            if (componentData != null) {
                bitmapDComponent.setVisible(componentData.isVisible());
                mCacheMap.put(item.getId(), bitmapDComponent);
            }

            if (componentLoadCallback != null) {
                componentLoadCallback.onLoadStart(item.getId(), bitmapDComponent.getWidth(), bitmapDComponent.getHeight());
            }
        }

        @Override
        public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
            if (apexLogger.DEBUG) {
                Log.d(TAG, "onResourceReady id : " + item.getId());
            }
            ComponentData componentData = item.getComponentData();

            BitmapDComponent bitmapDComponent = (BitmapDComponent) mParentsDComponent.getComponent(item.getId());
            if (bitmapDComponent == null) {
                bitmapDComponent = mCacheMap.get(item.getId());
            }


            if (bitmapDComponent != null) {
                Bitmap bitmap = ((BitmapDrawable) resource).getBitmap();

                if (apexLogger.DEBUG) {
                    Log.d(TAG, "onResourceReady bitmap : " + bitmap);
                    Log.d(TAG, "onResourceReady bitmapDComponent : " + bitmapDComponent);
                }
                if (bitmapDComponent instanceof SpriteBitmapDComponent) {
                    CreateSourceData createSourceData = componentData.getSource();
                    Bitmap[] bitmaps = mImageUtil.makeCropBitmap(bitmap, new Rect(0, 0, createSourceData.getSpriteWidth(), createSourceData.getSpriteHeight()));
                    ((SpriteBitmapDComponent) bitmapDComponent).setBitmaps(bitmaps);
                } else {
                    bitmapDComponent.setBitmap(bitmap);
                }

                if (componentLoadCallback != null) {
                    componentLoadCallback.onLoaded(true, item.getId(), bitmapDComponent.getWidth(), bitmapDComponent.getHeight());
                }

                if (bitmap != null) {
                    CreateSourceData createSourceData = componentData.getSource();
                    if (createSourceData != null) {
                        final String source = createSourceData.getPath();
                        if (source != null) {
                            if (componentData.isPreload_file()) {
                                CacheUtil.addPreloadImageSource(source);
                            } else {
                                CacheUtil.removePreloadImageSource(source);
                            }
                        }
                    }
                }

            } else {
                if (componentLoadCallback != null) {
                    componentLoadCallback.onLoaded(false, item.getId(), 0, 0);
                }
            }
        }

        @Override
        public void onLoadCleared(@Nullable Drawable placeholder) {
            if (apexLogger.ERROR) {
                Log.d(TAG, "onLoadCleared id : " + item.getId());
            }
        }

        @Override
        public void onLoadFailed(@Nullable Drawable errorDrawable) {
            super.onLoadFailed(errorDrawable);
            if (apexLogger.ERROR) {
                Log.d(TAG, "onLoadFailed id : " + item.getId());
            }
            if (componentLoadCallback != null) {
                componentLoadCallback.onLoaded(false, item.getId(), 0, 0);
            }
        }
    }

    public void notifyComponentMessage(BaseRequest animationRequest) {
        if (apexLogger.DEBUG) {
            Log.d(TAG, "notifyComponentMessage animationRequest: " + animationRequest);
        }
        if (animationRequest instanceof AnimationCreateData) {
            createAnimation((AnimationCreateData) animationRequest);
        } else if (animationRequest instanceof AnimationCloseData) {
            cancelAnimation((AnimationCloseData) animationRequest);
        } else if (animationRequest instanceof AnimationFinishData) {
            finishAnimation((AnimationFinishData) animationRequest);
        } else if (animationRequest instanceof CreateComponentData) {
            createComponent((CreateComponentData) animationRequest);
        } else if (animationRequest instanceof ComponentAttachData) {
            attachComponents((ComponentAttachData) animationRequest);
        } else if (animationRequest instanceof ComponentDetachData) {
            detachComponents((ComponentDetachData) animationRequest);
        } else if (animationRequest instanceof ComponentDeleteData) {
            deleteComponents((ComponentDeleteData) animationRequest);
        } else if (animationRequest instanceof ComponentVisibleData) {
            setVisibilityComponent(((ComponentVisibleData) animationRequest));
        } else if (animationRequest instanceof FillRectData) {
            fillRectComponent((FillRectData) animationRequest);
        }
    }

    private class LoadComponentResultCallback implements ComponentLoadCallback {
        private final String TAG = LoadComponentResultCallback.class.getSimpleName();
        private final Context context;
        private final ApexCloudSession apexCloudSession;
        private CreateDataResponse mCreateDataResponse = null;
        private final View mRootView;
        private int resultSize;

        public LoadComponentResultCallback(View rootView, Context context, ApexCloudSession apexCloudSession) {
            this.mRootView = rootView;
            this.context = context;
            this.apexCloudSession = apexCloudSession;
        }

        public void setComponentBaseInfo(int requestId, ProtocolType protocolType, int resultSize) {
            mCreateDataResponse = new CreateDataResponse(requestId, protocolType);
            this.resultSize = resultSize;
        }

        @Override
        public void onLoadStart(int objectId, int width, int height) {
            if (apexLogger.DEBUG) {
                Log.d(TAG, "onLoadStart : " + objectId);
            }
            mCreateDataResponse.putData(objectId, width, height);

            if (apexLogger.DEBUG) {
                Log.d(TAG, "onLoadStart data : " + mCreateDataResponse.getDataSize() + " |  : " + resultSize);
            }
        }

        @Override
        public void onLoaded(boolean success, int objectId, int width, int height) {
            if (apexLogger.DEBUG) {
                Log.d(TAG, "onLoaded : " + objectId + " | success : " + success);
            }

            mCreateDataResponse.setResult(success); // TODO
            if (IS_USE_COMPONENT_RESULT_CALLBACK) {
                if (resultSize == mCreateDataResponse.getDataSize()) {
                    onResult(true);
                }
            }
        }

        @Override
        public void onResult(boolean success) {
            if (apexLogger.DEBUG) {
                Log.d(TAG, "onResult success : " + success);
            }

            mCreateDataResponse.setResult(success);
            sendResult(mCreateDataResponse);
        }

        private void sendResult(Object o) {
            String result = new Gson().toJson(o);
            if (apexLogger.DEBUG) {
                Log.d(TAG, "sendResult to web : " + result);
            }
            try {
                apexCloudSession.sendWeb(result);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private interface ComponentLoadCallback {
        void onLoadStart(int id, int width, int height);

        void onLoaded(boolean success, int resourceId, int width, int height);

        void onResult(boolean success);
    }

    @Override
    public void release() {
        idMap.clear();
        mCacheView.clear();
        mCacheAnimation.clear();
        mCacheAnimationViewId.clear();

        rootDComponent.removeAllComponent();
    }
}
