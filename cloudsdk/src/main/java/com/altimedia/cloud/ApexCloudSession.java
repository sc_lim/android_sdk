/*
 *
 *  * Copyright (C) $year. Altimedia Corporation. All Rights Reserved.
 *  *
 *  * This software is the confidential and proprietary information
 *  * of Altimedia Corporation. You may not use or distribute
 *  * this software except in compliance with the terms and conditions
 *  * of any applicable license agreement in writing between Altimedia
 *  * Corporation and you.
 *
 */

package com.altimedia.cloud;

import android.view.View;

import java.io.IOException;

public abstract class ApexCloudSession {

    /**
     * Get doc for cloud settings
     * @return ApexCloudDoc
     */
    public abstract ApexCloudDoc getApexCloudDoc();

    /**
     * set doc for cloud settings
     * @param anyDoc ApexCloudDoc
     */
    public abstract void setApexCloudDoc(ApexCloudDoc anyDoc);

    /**
     * Add cloud event listener
     * It allowed only one listener per one manager instance
     */
    public abstract void addApexCloudListener(ApexCloudEventListener listener);

    /**
     * Remove cloud event listener
     */
    public abstract void removeApexCloudListener();

    /**
     * cloud service start
     */
    public abstract void start();

    /**
     * Send text message to web page
     * @param text message to be sent
     * @throws Exception
     */
    public abstract void sendWeb(String text) throws Exception;

    /**
     * Send binary data to web page
     * @param buffer binary buffer to be sent
     * @throws Exception
     */
    public abstract void sendBinary(byte[] buffer) throws Exception;

    /**
     * Stop cloud service
     * When stop() is called, graphics of web page will disappear
     * And also session will be disconnected
     * @see #start()
     * @throws Exception
     */
    public abstract void stop() throws IOException;

    /**
     * Pause cloud service
     * When pause() is called, graphics of web page will NOT disappear
     * And also session will be disconnected
     * @see #resume()
     * @throws Exception
     */
    public abstract void pause() throws IOException;

    /**
     * Resume cloud service
     * @param param option parameter when resume is called
     * @see #pause()
     */
    public abstract void resume(String param);

    /**
     * released mouse event to web page
     * @param x
     * @param y
     */
    public abstract void mouseReleaseEvent(int x, int y);

    /**
     * pressed mouse event to web page
     * @param x
     * @param y
     */
    public abstract void mousePressEvent(int x, int y);

    /**
     * key event to web page
     * @param type 0: pressed / 1: released (KeyEvent.ACTION_DOWN or KeyEvent.ACTION_UP)
     * @param code key code
     * @return true if event is reserved.
     *         false if event is not reserved.
     */
    public abstract boolean sendKeyEvent(int type, short code);

    /**
     * Remove all graphics on screen
     */
    public abstract void flush();

    /**
     * Interface to check of websocket connection state that is provided by ping/pong checking
     * When network has trouble, it returns after ping period + ping timeout(3 sec)
     * @return isConnecting status (true/false)
     */
    public abstract boolean isConnecting();

    /**
     * Interface to check pause state
     * @return pause status (true/false)
     */
    public abstract boolean isPaused();


    /**
     * application id
     * @return appid
     */
    public abstract String getAppID();

    /**
     * set ViewGroup for drawing Image Cloud response
     * @param view
     */
    public abstract void setCloudView(View view);
}
