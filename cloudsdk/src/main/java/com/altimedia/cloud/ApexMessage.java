/*
 *
 *  * Copyright (C) $year. Altimedia Corporation. All Rights Reserved.
 *  *
 *  * This software is the confidential and proprietary information
 *  * of Altimedia Corporation. You may not use or distribute
 *  * this software except in compliance with the terms and conditions
 *  * of any applicable license agreement in writing between Altimedia
 *  * Corporation and you.
 *
 */

package com.altimedia.cloud;

/**
 *
 */
public abstract class ApexMessage {

    /**
     * get data message
     * @return data String
     */
    public abstract String getData();

    /**
     * get protocolMethod
     * @return protocol method string
     */
    public abstract String getProtocolMethod();

    /**
     * get protocolPackage
     * @return protocol package string
     */
    public abstract String getProtocolPackage();

    /**
     * get AppID
     * AppID have unique value for session
     * @return AppID String
     */
    public abstract String getAppID();

}
