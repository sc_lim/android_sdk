/*
 *
 *  * Copyright (C) $year. Altimedia Corporation. All Rights Reserved.
 *  *
 *  * This software is the confidential and proprietary information
 *  * of Altimedia Corporation. You may not use or distribute
 *  * this software except in compliance with the terms and conditions
 *  * of any applicable license agreement in writing between Altimedia
 *  * Corporation and you.
 *
 */

package com.altimedia.cloud;

import com.altimedia.cloud.impl.ApexMessageControllerImpl;

/**
 *
 */
public abstract class ApexMessageController {

    /**
     * ApexMessageController instance
     * @return ApexMessageController instance
     */
    public static ApexMessageController getInstance() {
        return ApexMessageControllerImpl.getInstance();
    }

    /**
     * add ApexMessageHandler with protocol package.
     * It means that message will be delivered if its protocol package is same.
     *  @param handler ApexMessageHandler instance
     *  @param protocolPackage protocolPackage
     */
    public abstract void addApexMessageHandler(ApexMessageHandler handler, String protocolPackage);

    /**
     *  remove ApexMessageHandler
     *  @param handler ApexMessageHandler instance
     */
    public abstract void removeApexMessageHandler(ApexMessageHandler handler);

    /**
     * send reply message to web app with data
     * @param messageObject  ApexMessage object
     * @param data  string message data
     */
    public abstract void replyMessage(ApexMessage messageObject, String data);

    /**
     * send message to specific webapp with data
     * @param appID  webapp id
     * @param protocolPackage  protocolPackage
     * @param protocolMethod  protocol method api
     * @param data  string message data
     */
    public abstract void sendMessage(String appID, String protocolPackage, String protocolMethod, String data);
}
