/*
 *
 *  * Copyright (C) $year. Altimedia Corporation. All Rights Reserved.
 *  *
 *  * This software is the confidential and proprietary information
 *  * of Altimedia Corporation. You may not use or distribute
 *  * this software except in compliance with the terms and conditions
 *  * of any applicable license agreement in writing between Altimedia
 *  * Corporation and you.
 *
 */

package com.altimedia.cloud;

import com.altimedia.cloud.impl.ApexCloudManagerImpl;

import java.util.ArrayList;

public abstract class ApexCloudManager {
    /**
     * check version
     */
    public abstract void printProtocolVersion();

    /**
     * ApexCloudManager instance
     * @return ApexCloudManager instance
     */
    public static ApexCloudManager getInstance() {
        return ApexCloudManagerImpl.getInstance();
    }

    /**
     * Enable cache preload feature
     * default value is enable true
     * @param enable
     * @return true/false
     */
    //public abstract boolean enableCachePreLoad(boolean enable);

    /**
     * Enable cache feature
     * default value is enable true
     * @param enable
     * @return true/false
     */
    //public abstract boolean enableCache(boolean enable);

    /**
     * release cache data
     * default value is enable true
     */
    //public abstract void releaseCache();

    /**
     * store cached data to flash disk
     */
    //public abstract void storeCache();

    /**
     * Create New ApexCloudSession for specific application id
     * @param appID application id to create session
     * @return ApexCloudSession object
     */
    public abstract ApexCloudSession create(String appID);

    /**
     * Obtain ApexCloudSession object for specific application id
     * @param appID application id to get session
     * @return ApexCloudSession object
     */
    public abstract ApexCloudSession get(String appID);

    /**
     * Destroy all of ApexCloudSession
     */
    public abstract void destroyAll();

    /**
     * Obtain all of ApexCloudSession
     * @return ApexCloudSession ArrayList
     */
    public abstract ArrayList getSessions();

    /**
     * Obtain number of all of ApexCloudSession
     * @return ApexCloudSession counts integer
     */
    public abstract int getSessionCount();

    /**
     * Destroy ApexCloudSession for specific application id
     * @param appID application id to destroy session
     */
    public abstract void destroy(String appID);
}
