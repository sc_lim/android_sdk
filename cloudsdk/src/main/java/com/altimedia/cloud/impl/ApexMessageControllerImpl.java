/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.impl;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.altimedia.cloud.ApexCloudSession;
import com.altimedia.cloud.ApexMessage;
import com.altimedia.cloud.ApexMessageController;
import com.altimedia.cloud.ApexMessageHandler;
import com.altimedia.cloud.system.util.apexLogger;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class ApexMessageControllerImpl extends ApexMessageController {
    private HashMap<String, ArrayList<ApexMessageHandler> > mListeners;

    /*
     * ApexMessages for replying
     */
    private static ArrayList itemList = new ArrayList();

    private static ApexMessageControllerImpl instance = new ApexMessageControllerImpl();

    private ApexMessageControllerImpl(){
    }

    public static ApexMessageControllerImpl getInstance(){
        return instance;
    }

    @Override
    public void addApexMessageHandler(ApexMessageHandler handler, String protocolPackage) {
        if (mListeners == null) {
            mListeners = new HashMap<String, ArrayList<ApexMessageHandler> >();
        }
        if (!mListeners.containsKey(protocolPackage)) {
            mListeners.put(protocolPackage, new ArrayList<ApexMessageHandler>());
        }
        ArrayList arr = (ArrayList)mListeners.get(protocolPackage);
        arr.add(handler);
    }

    @Override
    public void removeApexMessageHandler(ApexMessageHandler handler) {
        for (ArrayList<ApexMessageHandler> value : mListeners.values()) {
            value.remove(handler);
        }
    }

    @Override
    public void replyMessage(ApexMessage message, String data) {
        if (apexLogger.DEBUG) {
            apexLogger.out("ApexMessageControllerImpl replyMessage data : " + data);
        }
        ApexCloudSession session = null;
        ApexMessageQueueItem item = null;
        ApexMessageImpl cachedMessage = null;
        Iterator iter = itemList.iterator();
        while( iter.hasNext() ) {
            item = (ApexMessageQueueItem)iter.next();
            cachedMessage = (ApexMessageImpl)item.getApexMessage();
            if ( ((ApexMessageImpl) message).getRequestId() == cachedMessage.getRequestId() ) {
                session = ApexCloudManagerImpl.getInstance().get(item.getAppId());
                cachedMessage.setData(data);
                break;
            }
        }
        if (session != null) {
            boolean isConnected = session.isConnecting();
            if (isConnected) {
                try {
                    String response = new Gson().toJson(cachedMessage);
                    session.sendWeb(response);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            itemList.remove(item);
        }
    }

    @Override
    public void sendMessage(String appId, String protocolPackage, String protocolMethod, String data) {
        if (apexLogger.DEBUG) {
            apexLogger.out("ApexMessageControllerImpl sendMessage appId : " + appId + " " + protocolPackage + "." + protocolMethod);
        }
        ApexCloudSession session = ApexCloudManagerImpl.getInstance().get(appId);
        if (session != null) {
            boolean isConnected = session.isConnecting();
            if (isConnected) {
                try {
                    ApexMessageImpl apexMessage = new ApexMessageImpl(0, protocolPackage +"."+protocolMethod, data);
                    String message = new Gson().toJson(apexMessage);
                    session.sendWeb(message);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                if (apexLogger.DEBUG) {
                    apexLogger.out("ApexMessageControllerImpl appId isConnected : " + isConnected);
                }
            }
        }
    }

    private void processWebMessage(ApexMessageQueueItem item){
        ApexMessage apexMessage = item.getApexMessage();
        ArrayList<ApexMessageHandler> handler = null;
        boolean notifyResult = false;
        if (apexMessage != null) {
            if (apexLogger.DEBUG) {
                apexLogger.out("ApexMessageControllerImpl processWebMessage apexMessage : " + apexMessage);
            }
            handler = mListeners.get(apexMessage.getProtocolPackage());
        }

        if (handler != null && handler.size() > 0) {
            Object[] listeners = handler.toArray();
            int len = listeners.length;
            for (int i = len - 1; i >= 0; i--) {
                ApexMessageHandler handler1 = (ApexMessageHandler) listeners[i];
                if (handler1.notifyWebAppMessage(apexMessage)) {
                    notifyResult = true;
                    if (apexMessage instanceof ApexMessageImpl) {
                        itemList.add(item);
                    }
                    break;
                }
            }
        }

        if (apexLogger.DEBUG) {
            apexLogger.out("deliverMessage handler : " + handler + " notifyResult : " + notifyResult);
        }

        if (!notifyResult) {
            //TODO: send fail to Web App
            //실패 결과를 전송해야 하나?
            itemList.remove(item);
        }
    }

    //WebResponseParser -> dispatch pool
    public void notifyWebMessage(final String appId, final ApexMessage apexMessage) {
        if (apexLogger.DEBUG) {
            apexLogger.out("ApexMessageControllerImpl notifyWebMessage apexMessage : " + apexMessage);
        }
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                ApexMessageQueueItem item = new ApexMessageQueueItem(appId, apexMessage);
                processWebMessage(item);
            }
        });
    }

    private class ApexMessageQueueItem {
        ApexMessage apexMessage = null;
        String appId = null;
        // 혹시 처리 안된 msg 들이 쌓이면?
        long arrivalTime;

        public ApexMessageQueueItem(String appId, ApexMessage apexMessage) {
            this.apexMessage = apexMessage;
            this.appId = appId;
            this.arrivalTime = System.currentTimeMillis();
        }

        public ApexMessage getApexMessage() {
            return apexMessage;
        }

        public String getAppId() {
            return appId;
        }
    }
}
