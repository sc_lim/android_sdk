/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.impl;

import com.altimedia.cloud.ApexMessage;

public class ApexMessageImpl extends ApexMessage {

    private int requestId = 0;
    private String protocolType = "";
    private String data = "";
    private String appID = null;

    public ApexMessageImpl(int requestId, String protocolType, String data){
        this.requestId = requestId;
        this.protocolType = protocolType;
        this.data = data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public void setRequestId(int requestId) {
        this.requestId = requestId;
    }

    public int getRequestId() {
        return requestId;
    }

    @Override
    public String getData() {
        return data;
    }

    @Override
    public String getProtocolMethod() {
        String protocolMethod = null;
        if (protocolType != null) {
            int index = protocolType.lastIndexOf(".");
            if (index > -1) {
                protocolMethod = protocolType.substring(index + 1);
            }
        }
        return protocolMethod;
    }

    @Override
    public String getProtocolPackage() {
        String protocolPackage = null;
        if (protocolType != null) {
            int index = protocolType.lastIndexOf(".");
            if (index > -1) {
                protocolPackage = protocolType.substring(0, index);
            }
        }
        return protocolPackage;
    }

    public void setAppID(String id){
        this.appID = id;
    }

    @Override
    public String getAppID() {
        return appID;
    }
}
