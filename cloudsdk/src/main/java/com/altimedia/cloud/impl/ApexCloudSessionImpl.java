/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.impl;

import android.net.Uri;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;

import com.altimedia.cloud.ApexCloudDoc;
import com.altimedia.cloud.ApexCloudEventListener;
import com.altimedia.cloud.ApexCloudSession;
import com.altimedia.cloud.constant.ErrorConstant;
import com.altimedia.cloud.constant.FunctionConstant;
import com.altimedia.cloud.constant.LogModeConstant;
import com.altimedia.cloud.system.server.ServerConnector;
import com.altimedia.cloud.system.server.dispatch.EventCommand;
import com.altimedia.cloud.system.server.dispatch.EventDispatcher;
import com.altimedia.cloud.system.server.dispatch.command.ErrorEventCommand;
import com.altimedia.cloud.system.util.CheckResponse;
import com.altimedia.cloud.system.util.ElapsedTime;
import com.altimedia.cloud.system.util.apexLogger;

import java.io.IOException;

public class ApexCloudSessionImpl extends ApexCloudSession {
    private static final String TAG = "ApexCloudSessionImpl";

    private ServerConnector serverConnector;
    private EventDispatcher eventDispatcher;

    private ApexCloudDoc apexCloudDoc;
    private boolean isPaused = false;
    private ApexCloudPlayer apexCloudPlayer;
    private String appId;

    public ApexCloudSessionImpl() {
        isPaused = false;
        readyConnector();
    }

    /**
     * get App id of ApexCloudManager instance and it have unique value
     * @return String App ID
     */
    public String getAppID(){
        return appId;
    }

    /**
     * set App id
     * @param appId
     */
    protected void setAppId(String appId){
        this.appId = appId;
    }

    public void setCloudView(View view){
        apexCloudPlayer = new ApexCloudPlayer(view, this);
    };

    private void readyConnector() {
        if (eventDispatcher == null) {
            eventDispatcher = new EventDispatcher();
        }
        if (serverConnector == null) {
            serverConnector = new ServerConnector(appId);
        }
    }

    /**
     * @return ApexCloudDoc
     */
    public ApexCloudDoc getApexCloudDoc() {
        return apexCloudDoc;
    }

    /**
     * cloud 서비스 설정파일을 set
     *
     * @param anyDoc ApexCloudDoc
     */
    public void setApexCloudDoc(ApexCloudDoc anyDoc) {
        this.apexCloudDoc = anyDoc;
        if (serverConnector == null) {
            return;
        }
        serverConnector.setApexCloudDoc(anyDoc);
    }


    /**
     * cloud event 수신을 위한 리스너 </br> 하나의 리스너만 등록 가능하다
     *
     * @param listener ApexCloudEventListener
     */
    public void addApexCloudListener(ApexCloudEventListener listener) {
        if (eventDispatcher == null) {
            return;
        }
        eventDispatcher.addApexCloudEventListener(listener);
    }

    /**
     * cloud event 리스너 제거
     */
    public void removeApexCloudListener() {
        eventDispatcher.removeApexCloudEventListener();
    }


    /**
     * cloud service start
     */
    public void start() {
        start(null);
    }

    private void start(final String param){
        String docUrl = apexCloudDoc.getURL();
        if (docUrl != null) {
            Uri uri = Uri.parse(docUrl);
            int port = (uri.getPort() == -1) ? 80 : uri.getPort();

            String resultUrl = uri.getScheme() + "://" + uri.getHost() + ":" + port + uri.getPath();
            if (param != null) {
                resultUrl += "?param=" + param;
            }

            Log.d(TAG, "start resultUrl : " + docUrl);

            apexCloudDoc.setURL(docUrl);
        }

        if (apexLogger.DEBUG) {
            apexLogger.out("start url : " + apexCloudDoc.getURL());
        }
        readyConnector();
        setApexCloudDoc(apexCloudDoc);
        ignite();
    }

    public void resume(final String param) {
        if (!isPaused) {
            return;
        }
        start(param);
    }


    private boolean ignite(){
        if (eventDispatcher == null) {
            return false;
        }
        if (apexLogger.INFO) apexLogger.out(" apexManager.start !!!! ");
        isPaused = false;

        eventDispatcher.startDispatcher();
        apexCloudPlayer.notifyOnStart();
        return connectServer();
    }


    /**
     * cloud module의 로그 수준
     *
     * @param mode
     * @see LogModeConstant
     */
    protected void setLogMode(int mode) {
        apexLogger.out(" apexManager.setLogMode -----------> " + mode);

        switch (mode) {
            case LogModeConstant.DEBUG:
                apexLogger.DEBUG = true;
                apexLogger.INFO = true;
                apexLogger.ERROR = true;
                break;

            case LogModeConstant.INFO:
                apexLogger.DEBUG = false;
                apexLogger.INFO = true;
                apexLogger.ERROR = true;
                break;

            case LogModeConstant.ERROR:
                apexLogger.DEBUG = false;
                apexLogger.INFO = false;
                apexLogger.ERROR = true;
                break;

            case LogModeConstant.NONE:
                apexLogger.DEBUG = false;
                apexLogger.INFO = false;
                apexLogger.ERROR = false;
                break;
        }
    }

    private boolean connectServer() {

        String ip = apexCloudDoc.getCloudIP();
        int port = apexCloudDoc.getCloudPort();

        String url = apexCloudDoc.getURL();
        int pingPeriod = apexCloudDoc.getPingPeriod();
        int connectTimeout = apexCloudDoc.getServerConnectTimeOut();
        boolean result = false;

        try {
            if (apexLogger.INFO)
                apexLogger.out(" apexManager.connectServer  - ip : " + ip + "   / port : " + port);
            ElapsedTime.getInstance().addSubSession("ICS connect");
            result = serverConnector.connect(eventDispatcher);
            ElapsedTime.getInstance().addSubSession("ICS connected");
//            result = serverConnector.connect(eventDispatcher, ip, port);

        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.

            notifyError(ErrorConstant.CONNECT_SERVER_ERROR, e.getMessage());
            return false;
        }

        try {

            //if (apexLogger.INFO) apexLogger.out(" apexManager.connectServer  - requestURL : " + url);

            if (result && pingPeriod > 0)
                serverConnector.startPing(eventDispatcher, pingPeriod, connectTimeout);

        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.

            notifyError(ErrorConstant.CONNECT_SERVER_ERROR, e.getMessage());
        }

        return result;
    }


    /**
     * web page로 메세지 전송
     *
     * @param text 전송할 메세지
     * @throws Exception
     */
    public void sendWeb(String text) throws Exception {
        if (apexLogger.DEBUG) {
            apexLogger.out("msg is | sendWeb : " + text);
        }
        serverConnector.sendWeb(text);
    }

    /**
     * web page로 데이터 전송
     *
     * @param buffer 전송할 메세지
     * @throws Exception
     */
    public void sendBinary(byte[] buffer) throws Exception {
        serverConnector.sendBinary(buffer);
    }

    /**
     * cloud service 종료</br>
     *
     * @see #stop()
     */
    public void stop() throws IOException {
        if (apexLogger.INFO) apexLogger.out(" [1] apexManager.stop !!!! ");

        destroyEventDispatcher();

        disconnectServer();
    }

    public void pause() throws IOException {
        isPaused = true;
        destroyEventDispatcher();
        disconnectServer();
    }

    public boolean isPaused() {
        return isPaused;
    }

    private void destroyEventDispatcher() {
        if (eventDispatcher != null) {
            eventDispatcher.destroy();
            eventDispatcher = null;
        }
    }

    private void disconnectServer() throws IOException {
        if (serverConnector != null) {
            serverConnector.disconnect();
        }
    }

    /**
     * released mouse event -> web page
     *
     * @param x
     * @param y
     */
    public void mouseReleaseEvent(int x, int y) {
        if (apexLogger.DEBUG) {
            apexLogger.out(" apexManager.mouseReleaseEvent ---- is server alive ? " + serverConnector.isAlive());
        }
        int button = 0x01; //imsi
        mouseEvent(FunctionConstant.SEND_KEY_RELEASE, button, x, y);
    }

    /**
     * pressed mouse event -> web page
     *
     * @param x
     * @param y
     */
    public void mousePressEvent(int x, int y) {
        if (apexLogger.DEBUG) {
            apexLogger.out(" apexManager.mousePressEvent ---- is server alive ? " + serverConnector.isAlive());
        }
        int button = 0x01;
        mouseEvent(FunctionConstant.SEND_KEY_PRESS, button, x, y);
    }

    private void mouseEvent(int type, int button, int x, int y) {
        if (!serverConnector.isAlive()) return;

        try {
            serverConnector.sendMouseEvent(type, button, x, y);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private void sendDisconnect() {
        if (!serverConnector.isAlive()) return;

        try {
            serverConnector.sendDisconnect();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    /**
     * key event -> web page
     *
     * @param type      KeyEvent.ACTION_DOWN or KeyEvent.ACTION_UP
     * @param code      key code
     */
    public boolean sendKeyEvent(int type, short code) {
        if (apexLogger.DEBUG) {
            apexLogger.out(" apexManager.onEvent ---- type: " + type);
            apexLogger.out(" apexManager.onEvent ---- code: " + code);
            apexLogger.out(" apexManager.onEvent ---- is server alive ? " + serverConnector.isAlive());
        }

        if (!serverConnector.isAlive()) {
            notifyError(ErrorConstant.ERR_DECTECTED_DISCONNECTION_RECV, "detected disconnection");
            return false;
        }

        /**
         * android keycode -> virtual keycode
         */
        short vk_code = code;
        try {
            switch (type) {
                case KeyEvent.ACTION_DOWN:
                    serverConnector.sendKeyEvent(vk_code, true);
                    break;

                case KeyEvent.ACTION_UP:
                    ElapsedTime.getInstance().addSubSession("Key Up");
                    serverConnector.sendKeyEvent(vk_code, false);
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return true;
    }

    private void notifyError(int code, String msg) {
        ErrorEventCommand errorEventCommand = new ErrorEventCommand();
        errorEventCommand.setError(code, msg);

        eventDispatcher.putEvent(errorEventCommand);
    }

    public void notifyFrameUpdate(final int x, final int y, final int w, final int h) {
        eventDispatcher.putEvent(new EventCommand() {
            @Override
            public void doBitCloudCommand(ApexCloudEventListener listener) {
                listener.notifyFrameUpdate(x, y, w, h);
            }
        });
    }

    /**
     * destroy instance
     */

    protected void destroy() throws IOException {
        apexCloudPlayer.releaseView();
        destroyEventDispatcher();
        closeServer();
        cleanupAnyDoc();
        if (apexLogger.INFO) apexLogger.out("[10] apexManager.destroy ----------- END !!!");
    }


    private void closeServer() throws IOException {
        if (serverConnector != null) {
            serverConnector.close();
            serverConnector = null;
        }
    }

    private void cleanupAnyDoc() {
        if (apexCloudDoc != null) {
            apexCloudDoc.cleanup();
            apexCloudDoc = null;
        }
    }

    /**
     * Web socket의 Ping/Pong 체크로 인한 정상 연결 여부를 App에서 확인할 수 있는 Interface.
     * 네트워크 장애 발생시 최대 ping period + ping timeout(3초) 후에 변경된 값을 리턴할 수 있음.
     *
     * @return isConnecting status (true/false)
     */
    public boolean isConnecting() {
        return CheckResponse.getInstance().getPingCheck();
    }


    /**
     * Remove all graphics on screen
     */
    public void flush(){}
}
