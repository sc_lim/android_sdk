/*
 *
 *  * Copyright (C) $year. Altimedia Corporation. All Rights Reserved.
 *  *
 *  * This software is the confidential and proprietary information
 *  * of Altimedia Corporation. You may not use or distribute
 *  * this software except in compliance with the terms and conditions
 *  * of any applicable license agreement in writing between Altimedia
 *  * Corporation and you.
 *
 */

package com.altimedia.cloud.impl;

import android.content.Context;
import android.graphics.Rect;
import android.os.Debug;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.RelativeLayout;

import androidx.core.util.Pair;

import com.altimedia.cloud.ApexCloudSession;
import com.altimedia.cloud.ApexMessage;
import com.altimedia.cloud.ApexMessageController;
import com.altimedia.cloud.ApexMessageHandler;
import com.altimedia.cloud.comp.ic.CsCloudView;
import com.altimedia.cloud.comp.ic.CsLayoutView;
import com.altimedia.cloud.comp.ic.CsTextureView;
import com.altimedia.cloud.comp.ic.OnWindowCallback;
import com.altimedia.cloud.comp.ui.IAnimation;
import com.altimedia.cloud.comp.ui.IAnimationView;
import com.altimedia.cloud.comp.ui.IComponentView;
import com.altimedia.cloud.system.server.parser.obj.BaseRequest;
import com.altimedia.cloud.system.server.parser.obj.BaseResponse;
import com.altimedia.cloud.system.server.parser.obj.CsFrame;
import com.altimedia.cloud.system.server.parser.obj.CsFrameApexMessage;
import com.altimedia.cloud.system.server.parser.obj.InnerApexMessage;
import com.altimedia.cloud.system.server.parser.obj.application.request.GetHistoryRequest;
import com.altimedia.cloud.system.server.parser.obj.application.request.SetHistoryRequest;
import com.altimedia.cloud.system.server.parser.obj.application.response.GetHistoryResponse;
import com.altimedia.cloud.system.util.apexLogger;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

import static com.altimedia.cloud.system.server.parser.obj.ProtocolPackageType.AnimationManager;
import static com.altimedia.cloud.system.server.parser.obj.ProtocolPackageType.ApexCsFrame;
import static com.altimedia.cloud.system.server.parser.obj.ProtocolPackageType.ApplicationManager;

final class ApexCloudPlayer implements OnWindowCallback, ApexMessageHandler {
    public enum PlayerMode {
        DrawingComponentTexture, ComponentTexture, View
    }

    PlayerMode mode = PlayerMode.DrawingComponentTexture;

    private final String TAG = ApexCloudPlayer.class.getSimpleName();
    private final Context mContext;
    private CsCloudView mPlayerView;
    private IAnimation mIAnimation;
    private ApexCloudSession apexCloudSession;

    public ApexCloudPlayer(View view, ApexCloudSession apexCloudSession) {

        this.mContext = view.getContext();
        this.apexCloudSession = apexCloudSession;
        if (mode == PlayerMode.View) {
            mPlayerView = new CsLayoutView(this.mContext, this);
        } else {
            mPlayerView = new CsTextureView(this.mContext, this);
        }
        mPlayerView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        addView(view, mPlayerView.getPlayerView());

        if (mode == PlayerMode.DrawingComponentTexture) {
            IAnimationView mIAnimationView = new IAnimationView(this.mContext, apexCloudSession);
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
            mIAnimationView.setLayoutParams(params);

            mIAnimation = mIAnimationView;
        } else {
            IComponentView mIComponentView = new IComponentView(this.mContext, apexCloudSession,  this);
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
            mIComponentView.setLayoutParams(params);

            mIAnimation = mIComponentView;
        }
        addView(view, (View) mIAnimation);

        ApexMessageController.getInstance().addApexMessageHandler(this, AnimationManager.getName());
        ApexMessageController.getInstance().addApexMessageHandler(this, ApplicationManager.getName());
        ApexMessageController.getInstance().addApexMessageHandler(this, ApexCsFrame.getName());
    }

    private void addView(View parentsView, View targetView) {
        if (parentsView instanceof ViewGroup) {
            ((ViewGroup) parentsView).addView(targetView);
        } else {
            ViewParent parent = parentsView.getParent();
            if (parent instanceof ViewGroup) {
                ((ViewGroup) parent).addView(targetView);
            }
        }
    }

    public void notifyOnStart(){
        if (mPlayerView != null) {
            mPlayerView.notifyStartApp(apexCloudSession.getAppID());
        }
    }

    public void releaseView() {
        mPlayerView.release();
        mIAnimation.release();
        ApexMessageController.getInstance().removeApexMessageHandler(this);
    }

    @Override
    public void onAttachedToWindow() {

    }

    @Override
    public void onDetachedFromWindow() {
        apexLogger.out("ApexCloudPlayer onDetachedFromWindow called.. " + apexCloudSession.getAppID());
    }

    @Override
    public boolean notifyWebAppMessage(ApexMessage message) {
        if (message instanceof InnerApexMessage) {
            final BaseRequest mRequest = ((InnerApexMessage) message).getBaseRequest();
            switch (mRequest.getProtocolPackageType()) {
                case AnimationManager:
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            mIAnimation.notifyComponentMessage(mRequest);
                        }
                    });
                    return true;
                case ApplicationManager:
                    return onApplicationManager(mRequest);
            }
        } else if (message instanceof CsFrameApexMessage) {
            CsFrame csFrame = ((CsFrameApexMessage) message).getCsFrame();
            updateFragment(csFrame);

            if (apexCloudSession instanceof ApexCloudSessionImpl) {
                Rect bounds = csFrame.getBounds();
                if (csFrame.isUpdate()) {
                    ((ApexCloudSessionImpl) apexCloudSession).notifyFrameUpdate(bounds.left, bounds.top, bounds.right, bounds.bottom);
                    if (apexLogger.Memory) {
                        Debug.MemoryInfo debugMemInfo = new Debug.MemoryInfo();
                        Debug.getMemoryInfo(debugMemInfo);
                        Log.d(TAG, "memoryUsage Runtime maxMemory  " + Runtime.getRuntime().maxMemory());
                        Log.d(TAG, "memoryUsage Runtime totalMemory " + Runtime.getRuntime().totalMemory());
                        Log.d(TAG, "memoryUsage Runtime freeMemory  " + Runtime.getRuntime().freeMemory());
                        Log.d(TAG, "memoryUsage Debug.MemoryInfo Total PSS " + debugMemInfo.getTotalPss());

                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                            Map<String, String> memStat = debugMemInfo.getMemoryStats();
                            for(Map.Entry<String, String> entry : memStat.entrySet()){
                                Log.d(TAG, "memoryUsage Debug.MemoryInfo " + entry.getKey() + " : " + entry.getValue());
                            }
                        }

                    }
                }
            }

            return true;
        }
        return false;
    }

    private boolean onApplicationManager(BaseRequest baseRequest) {
        switch (baseRequest.getProtocolType()) {
            case applicationManagerGetHistory:
                GetHistoryRequest getHistoryRequest = (GetHistoryRequest) baseRequest;
                String historyKey = getHistoryRequest.getHistoryKey();
                String historyValue = mHistory.get(historyKey);

                GetHistoryResponse response = new GetHistoryResponse(baseRequest.getRequestId(), baseRequest.getProtocolType());
                response.setResult(historyValue != null);
                response.setHistoryData(historyValue);
                sendResponse(response);
                break;
            case applicationManagerSetHistory:
                SetHistoryRequest setHistoryRequest = (SetHistoryRequest) baseRequest;
                Pair<String, String> historyData = setHistoryRequest.getHistory();
                mHistory.put(historyData.first, historyData.second);

                BaseResponse baseResponse = new BaseResponse(baseRequest.getRequestId(), baseRequest.getProtocolType());
                baseResponse.setResult(true);
                sendResponse(baseResponse);
                break;
            default:
                return false;
        }
        return true;
    }

    private void sendResponse(Object o) {
        String result = new Gson().toJson(o);
        if (apexLogger.DEBUG) {
            Log.d(TAG, "sendResult to web : " + result);
        }
        try {
            apexCloudSession.sendWeb(result);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    HashMap<String, String> mHistory = new HashMap<>();

    public void updateFragment(CsFrame csFrame) {
        mPlayerView.updateFragment(csFrame);
    }

    @Override
    public String toString() {
        return "ApexCloudPlayer{" +
                "mode=" + mode +
                " id=" + apexCloudSession.getAppID() +"}";
    }
}
