/*
 *
 *  * Copyright (C) $year. Altimedia Corporation. All Rights Reserved.
 *  *
 *  * This software is the confidential and proprietary information
 *  * of Altimedia Corporation. You may not use or distribute
 *  * this software except in compliance with the terms and conditions
 *  * of any applicable license agreement in writing between Altimedia
 *  * Corporation and you.
 *
 */

package com.altimedia.cloud.impl;

import com.alticast.cloud.cloudsdk.BuildConfig;
import com.altimedia.cloud.ApexCloudManager;
import com.altimedia.cloud.ApexCloudSession;
import com.altimedia.cloud.system.util.apexLogger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

public class ApexCloudManagerImpl extends ApexCloudManager {

    HashMap<String, ApexCloudSession> sessionHashMap = new HashMap<String, ApexCloudSession>();

    private static final ApexCloudManagerImpl instance = new ApexCloudManagerImpl();

    private ApexCloudManagerImpl() {
        printProtocolVersion();
    }

    /**
     * get ApexCloudManager instance
     *
     * @return
     */
    public static ApexCloudManagerImpl getInstance() {
        return instance;
    }

    /**
     * Create New ApexCloudSession for specific application id
     *
     * @param appId
     * @return
     */
    public ApexCloudSession create(String appId) {
        if (get(appId) != null) {
            return (ApexCloudSession) get(appId);
        }
        ApexCloudSession session = new ApexCloudSessionImpl();
        ((ApexCloudSessionImpl) session).setAppId(appId);
        sessionHashMap.put(appId, session);
        return session;
    }

    /**
     * Obtain ApexCloudSession object for specific application id
     *
     * @param appId
     * @return
     */
    public ApexCloudSession get(String appId) {
        return sessionHashMap.get(appId);
    }

    @Override
    public void destroyAll() {
        Collection<ApexCloudSession> sessions = sessionHashMap.values();
        for (ApexCloudSession session : sessions) {
            try {
                session.stop();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        sessionHashMap.clear();
    }

    @Override
    public ArrayList getSessions() {
        Collection<ApexCloudSession> sessions = sessionHashMap.values();
        ArrayList<ApexCloudSession> arrayList = new ArrayList<>(sessions);
        return arrayList;
    }

    @Override
    public int getSessionCount() {
        return sessionHashMap.size();
    }


    /**
     * Destroy ApexCloudSession for specific application id
     *
     * @param appId
     */
    public void destroy(String appId) {
        ApexCloudSession session = get(appId);
        if (session != null) {
            try {
                ((ApexCloudSessionImpl) session).destroy();
            } catch (IOException e) {
                e.printStackTrace();
            }
            sessionHashMap.remove(appId);
        }
    }

    /**
     * check version
     */
    public void printProtocolVersion() {
        if (apexLogger.DEBUG) {
            System.out.println("==========================================================================");
            System.out.println("=");
            System.out.println("=");
            System.out.println("=                        ApexCloud library v " + BuildConfig.VERSION_NAME);
            System.out.println("=");
            System.out.println("=");
            System.out.println("==========================================================================");
        }
    }
}
