/*
 *
 *  * Copyright (C) $year. Altimedia Corporation. All Rights Reserved.
 *  *
 *  * This software is the confidential and proprietary information
 *  * of Altimedia Corporation. You may not use or distribute
 *  * this software except in compliance with the terms and conditions
 *  * of any applicable license agreement in writing between Altimedia
 *  * Corporation and you.
 *
 */

package com.altimedia.cloud;

/**
 * Created by IntelliJ IDEA.
 * User: gh.baek
 * Date: 2018. 5. 31
 * To change this template use File | Settings | File Templates.
 */
public interface ApexCloudEventListener {

    /**
     * 서버 연결 성공
     * Cloud Server와 WebSocket 연결이 성공하였음을 알린다.
     */
    public void notifyConnected();

    /**
     * 서버 연결 종료
     * Cloud Server와 연결이 종료됨을 알린다.
     */
    public void notifyDisconnected();
    /**
     * updated changed frame
     * 기본적으로 전체화면 설정을 권장한다.
     * @param x frame 조각이 그려질 x 좌표
     * @param y frame 조각이 그려질 y 좌표
     * @param w frame 조각의 width
     * @param h frame 조각의 height
     */
    public void notifyFrameUpdate(int x, int y, int w, int h);


    /**
     * 에러가 발생했을 경우..
     * @see com.altimedia.cloud.constant.ErrorConstant
     * @param code 에러 코드
     * @param msg 에러 메시지 전문
     */
    public void notifyErrorEvent(int code, String msg);

}
