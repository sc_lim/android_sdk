/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.server.parser.obj.animation.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.altimedia.cloud.system.server.parser.obj.BaseResponse;
import com.altimedia.cloud.system.server.parser.obj.ProtocolType;
import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CreateDataResponse  extends BaseResponse {
    @Expose
    @SerializedName("data")
    private ArrayList<CreateDataResponse.Data> data;


    public CreateDataResponse(int requestId, ProtocolType protocolType) {
        super(requestId, protocolType);
    }

    protected CreateDataResponse(Parcel in) {
        super(in);

        in.readList(this.data, (CreateDataResponse.Data.class.getClassLoader()));
    }

    public void putData(int objectid, int width, int height) {
        if (data == null) {
            data = new ArrayList<>();
        }
        CreateDataResponse.Data createDataResponseData = new CreateDataResponse.Data(objectid, width, height);
        data.add(createDataResponseData);
    }

    public int getDataSize() {
        if (data == null) {
            return 0;
        }
        return data.size();
    }

    public static class Data implements Parcelable {
        @Expose
        @SerializedName("objectId")
        private int objectId;

        @Expose
        @SerializedName("width")
        private int width;

        @Expose
        @SerializedName("height")
        private int height;

        public Data(int objectid, int width, int height) {
            this.objectId = objectid;
            this.width = width;
            this.height = height;
        }

        protected Data(Parcel in) {
            objectId = in.readInt();
            width = in.readInt();
            height = in.readInt();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(objectId);
            dest.writeInt(width);
            dest.writeInt(height);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<CreateComponentResponse.Data> CREATOR = new Creator<CreateComponentResponse.Data>() {
            @Override
            public CreateComponentResponse.Data createFromParcel(Parcel in) {
                return new CreateComponentResponse.Data(in);
            }

            @Override
            public CreateComponentResponse.Data[] newArray(int size) {
                return new CreateComponentResponse.Data[size];
            }
        };
    }

    public String getJsonString() {
        return new Gson().toJson(this);
    }
}
