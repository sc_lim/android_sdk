/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.server.parser.obj.application.request;

import android.os.Parcel;
import android.os.Parcelable;

import com.altimedia.cloud.system.server.parser.obj.BaseRequest;
import com.altimedia.cloud.system.server.parser.obj.application.def.KeyCode;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mc.kim on 13,05,2020
 */
public class SetKeyArrayRequest extends BaseRequest {
    @Expose
    @SerializedName("data")
    private KeyArrayData data;

    protected SetKeyArrayRequest(Parcel in) {
        super(in);
        data = (KeyArrayData) in.readValue(KeyArrayData.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeValue(data);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SetKeyArrayRequest> CREATOR = new Creator<SetKeyArrayRequest>() {
        @Override
        public SetKeyArrayRequest createFromParcel(Parcel in) {
            return new SetKeyArrayRequest(in);
        }

        @Override
        public SetKeyArrayRequest[] newArray(int size) {
            return new SetKeyArrayRequest[size];
        }
    };

    private static class KeyArrayData implements Parcelable {
        @Expose
        @SerializedName("keys")
        private List<KeyCode> keys;

        @Expose
        @SerializedName("keysLength")
        private int keysLength;

        protected KeyArrayData(Parcel in) {
            in.readList(this.keys, (KeyCode.class.getClassLoader()));
            keysLength = in.readInt();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeList(keys);
            dest.writeInt(keysLength);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<KeyArrayData> CREATOR = new Creator<KeyArrayData>() {
            @Override
            public KeyArrayData createFromParcel(Parcel in) {
                return new KeyArrayData(in);
            }

            @Override
            public KeyArrayData[] newArray(int size) {
                return new KeyArrayData[size];
            }
        };
    }
}
