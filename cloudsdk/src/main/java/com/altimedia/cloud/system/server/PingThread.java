/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.server;

import com.altimedia.cloud.constant.ErrorConstant;
import com.altimedia.cloud.system.server.dispatch.EventDispatcher;
import com.altimedia.cloud.system.util.CheckResponse;
import com.altimedia.cloud.system.util.apexLogger;

import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: gh.baek
 * Date: 2018. 5. 29
 * To change this template use File | Settings | File Templates.
 */
class PingThread extends Thread {
    private boolean isRun = true;
    private long period;
    private long timeout;

    private ServerConnector connection;
    private EventDispatcher dispatcher;

    public PingThread(EventDispatcher eventDispatcher) {

        super("PingThread");
        dispatcher = eventDispatcher;
    }

    public void run() {
        try {

            Thread.sleep(period);
            int sleepTime = 100;
            long pingInterval = period;
            long checktime = 3000;
            if (checktime >= pingInterval)
                pingInterval = checktime + 500;

            long startTime = System.currentTimeMillis();
            while (isRun) {
                //Thread.sleep(period);
                if (!connection.isAlive())
                    stopPing();

                if (System.currentTimeMillis() - startTime >= pingInterval) {
                    CheckResponse.getInstance().setResponse(false);
                    sendPing();
                    startTime = System.currentTimeMillis();
                }

                if (!CheckResponse.getInstance().getResponse()) {
                    if (System.currentTimeMillis() - startTime >= checktime) {
                        //if (dispatcher != null) {
                        //    dispatcher.notifyErrorEvent( ErrorConstant.ERR_DECTECTED_DISCONNECTION_RECV, "[Warning] client didn't get a response from the cloud server." );
                        //}
                        CheckResponse.getInstance().setPingCheck(false);
                        CheckResponse.getInstance().setResponse(true);


                        if(apexLogger.ERROR){
                            apexLogger.out(" Ping -----------> timeout");
                        }


                        // pong 응답을 받지 못하면 ping 주기를 1초로 변경하여 네트웍 연결 상태 체크
                        checktime = 500;
                        pingInterval = checktime + 500;
                    }
                }

                // 응답지연 발생 후 복구되었을 때
                if (CheckResponse.getInstance().getPingCheck() && pingInterval == 1000) {
                    pingInterval = period;
                    checktime = 3000;
                    if (checktime >= pingInterval)
                        pingInterval = checktime + 500;
                }

                Thread.sleep(sleepTime);

            }
        } catch (IOException e) {
            e.printStackTrace();

            if (dispatcher != null) {
                dispatcher.notifyErrorEvent(ErrorConstant.CONNECT_SERVER_ERROR, "client library error : dispatcher is null");
            }

        } catch (Exception e) {
            if (apexLogger.ERROR) apexLogger.out(" PingThread.run -----------> " + e.getMessage());
        } finally {
            stopPing();
        }

        if (apexLogger.INFO) apexLogger.out("[5] PingThread is STOP !!!");
    }

    public void startPing(ServerConnector connection, int period, int timeout) {
        this.connection = connection;
        this.period = period;
        this.timeout = timeout;

        isRun = true;

        start();
    }

    private void sendPing() throws Exception {
        connection.sendPing();
    }

    public void stopPing() {
        isRun = false;

        interrupt();

        dispatcher = null;
        connection = null;
    }
}
