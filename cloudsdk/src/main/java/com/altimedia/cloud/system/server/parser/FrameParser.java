/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.server.parser;

import android.graphics.Bitmap;

import com.altimedia.cloud.ApexMessageController;
import com.altimedia.cloud.ApexCloudEventListener;
import com.altimedia.cloud.constant.ProtocolConstant;
import com.altimedia.cloud.constant.SourceTypeConstant;
import com.altimedia.cloud.impl.ApexMessageControllerImpl;
import com.altimedia.cloud.system.ParserImpl;
import com.altimedia.cloud.system.server.ServerConnector;
import com.altimedia.cloud.system.server.dispatch.EventDispatcher;
import com.altimedia.cloud.system.server.parser.obj.CsFrame;
import com.altimedia.cloud.system.server.parser.obj.CsFrameApexMessage;
import com.altimedia.cloud.system.server.parser.obj.ProtocolPackageType;
import com.altimedia.cloud.system.util.ElapsedTime;
import com.altimedia.cloud.system.util.apexLogger;


/**
 * Created by IntelliJ IDEA.
 * User: gh.baek
 * Date: 2018. 5. 14
 * To change this template use File | Settings | File Templates.
 */
class FrameParser extends ParserImpl {
//    private ApexCloudEventListener listener;
    //private ApexCloudDoc doc;

    private int encoding, sequence;

    private int x;
    private int y;
    private int w;
    private int h;
    private int width;
    private int height;

    private int length;

    private byte[] sourceByte;
    private static Bitmap rectImage;

    public FrameParser(ServerConnector serverConnector) {
        setServerConnector(serverConnector);
    }

    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }

    public int getW() {
        return w;
    }

    public int getH() {
        return h;
    }

    public void initialize() {

        x = 0;
        y = 0;
        w = 0;
        h = 0;
/*
        width = this.doc.getWidth();
        height = this.doc.getHeight();
        apexLogger.out( " initialize  width :"+width+ ", height : "+height );
        */
    }

    public void setServerConnector(ServerConnector serverConnector) {
        super.setServerConnector(serverConnector);

        width = serverConnector.getApexCloudDoc().getWidth();
        height = serverConnector.getApexCloudDoc().getHeight();

        //sourceByte = new byte[width * height * 4];
    }

    protected void deliverMessage(CsFrame csFrame) {
        try {
            final CsFrameApexMessage apexMessage = new CsFrameApexMessage(ProtocolPackageType.ApexCsFrame.getName(), csFrame);
            if (apexLogger.DEBUG) {
                apexLogger.out("deliverMessage csFrame : " + csFrame);
            }

            if (apexMessage != null) {
                if (apexLogger.DEBUG) {
                    apexLogger.out("deliverMessage apexMessage : " + serverConnector);
                }
                ((ApexMessageControllerImpl) ApexMessageController.getInstance())
                        .notifyWebMessage(serverConnector.getAppId(), apexMessage);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    boolean firstFrameReceived = false;

    public void parse(EventDispatcher dispatcher) throws Exception {
/*
        x = width;
        y = height;
        w = 0;
        h = 0;
*/
        boolean btrue = true;
        boolean isUpdate = true;
        int cap = getCapacity();
        int pos = 0;


        //long start = System.currentTimeMillis();
        if (!firstFrameReceived) {
            ElapsedTime.getInstance().addSubSession("receive frames start");
            firstFrameReceived = true;
        }

        do {

            int command = ProtocolConstant.COMMAND_UPDATE_FRAME;
            if (pos > 0)
                command = getByte();
            int imageformat = getByte();
            int left = getShort();
            int top = getShort();
            int right = getShort();
            int bottom = getShort();

            int r_width = (getShort() & 0xffff);
            int r_height = (getShort() & 0xffff);
            int animation = getByte();
            int imagecount = getByte();

            int fieldLength = getShort();
            int imageLength = (getInt() >> 12) & 0x000fffff;

            if (imageLength == 0 && isUpdate) {
                ElapsedTime.getInstance().addSubSession("receive frames end", "receive frames start");
                firstFrameReceived = false;
                deliverMessage(new CsFrame(left, top, r_width, r_height));
            } else {

                byte[] image = new byte[imageLength];
                getArray(image, imageLength);

                if (image == null)
                    if (apexLogger.ERROR) {
                        apexLogger.out("xxxx Error image data is null!!!");
                    }

                if (apexLogger.DEBUG)
                    apexLogger.out(" Reader.readData ==> cmd:" + command + ", format:" + imageformat + ", x:" + left + ", y:" + top + ", w:" + r_width + ", h: " + r_height + ", count:" + imagecount + ", lenght:" + imageLength);


                if (image != null) {
                    // 화면 갱신..
                    deliverMessage(new CsFrame(SourceTypeConstant.FRAME_ENCODING_WEBP, image, left, top, r_width, r_height, false));
                    /*
                    rectImage = BitmapFactory.decodeByteArray( image, 0, imageLength );
                    if(rectImage != null) {
                        int size = rectImage.getByteCount();
                        ByteBuffer dst = ByteBuffer.allocate( size );
                        rectImage.copyPixelsToBuffer( dst );
                        listener.updateFragment( SourceTypeConstant.FRAME_ENCODING_BMP, dst.array(), size, left, top, r_width, r_height );
                    }
                    else
                        listener.notifyErrorEvent( ErrorConstant.ERR_IMAGE_DECODE, "FrameParser : Failed Image decode. Please check image size or format." );
                     */

                }

/*
                GUIManager guiManager = GUIManager.getInstance();

                if (guiManager != null) {
                    isUpdate = guiManager.fireImageSource( imageformat, image, 0, imageLength, left, top, r_width, r_height );

                    if( !isUpdate )
                        listener.notifyErrorEvent( ErrorConstant.ERR_IMAGE_DECODE, "FrameParser : Failed Image decode. Please check image size or format." );
                }

               */

            }

            pos = getPosition();
            //if (apexLogger.DEBUG)
            //    apexLogger.out( "parse buffer capacity: " + cap + ", pos : " + pos );
            if (cap <= pos)
                btrue = false;


        } while (btrue);

        /*
        long end = System.currentTimeMillis();
        if (apexLogger.ERROR)
            apexLogger.out( " parse buffer size : " + cap + ", decode latency: " + (end - start) + " ms");
        */
//        ElapsedTime.getInstance().addSubSession("frame_e");


    }

    protected void cleanup() {
        //To change body of implemented methods use File | Settings | File Templates.
        sourceByte = null;

        x = 0;
        y = 0;
        w = 0;
        h = 0;

        serverConnector = null;
    }

    public void doBitCloudCommand(ApexCloudEventListener listener) {
        //To change body of implemented methods use File | Settings | File Templates.
        if (listener != null) {
            switch (encoding) {
                default:
                    listener.notifyFrameUpdate(0, 0, width, height);
                    break;
            }
        }
    }
}

