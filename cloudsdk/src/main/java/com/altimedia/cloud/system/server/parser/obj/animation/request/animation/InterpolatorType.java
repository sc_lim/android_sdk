/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.server.parser.obj.animation.request.animation;

/**
 * Created by mc.kim on 10,04,2020
 */
public enum InterpolatorType {
    linear("linear"), bounce("bounce"), elastic("elastic"), invalid("invalid");
    private final String name;

    InterpolatorType(String name) {
        this.name = name;
    }

    public static InterpolatorType findByName(String name) {
        InterpolatorType[] types = values();
        for (InterpolatorType type : types) {
            if (type.name.equalsIgnoreCase(name)) {
                return type;
            }
        }
        return invalid;
    }
}
