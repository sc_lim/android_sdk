/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.server.parser.obj.animation.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.altimedia.cloud.system.server.parser.obj.BaseResponse;
import com.altimedia.cloud.system.server.parser.obj.ProtocolType;
import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by mc.kim on 17,04,2020
 */
public class CreateComponentListResponse extends BaseResponse {

    @Expose
    @SerializedName("data")
    private ArrayList<CreateComponentListResponse.Data> dataList;

    public CreateComponentListResponse(int requestId, ProtocolType protocolType) {
        super(requestId, protocolType);
    }

    protected CreateComponentListResponse(Parcel in) {
        super(in);
        in.readList(this.dataList, (CreateComponentListResponse.Data.class.getClassLoader()));
    }

    public void putData(int syncId, int resourceId) {
        if (dataList == null) {
            dataList = new ArrayList<>();
        }
        CreateComponentListResponse.Data data = new Data(syncId, resourceId);
        dataList.add(data);
    }

    public int getDataSize() {
        if (dataList == null) {
            return 0;
        }
        return dataList.size();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeList(dataList);
    }

    @Override
    public int describeContents() {
        return super.describeContents();
    }


    public static class Data implements Parcelable {
        @Expose
        @SerializedName("targetId")
        private int targetId;

        @Expose
        @SerializedName("objectId")
        private int objectId;


        public Data(int targetId, int objectId) {
            this.targetId = targetId;
            this.objectId = objectId;
        }

        protected Data(Parcel in) {
            targetId = in.readInt();
            objectId = in.readInt();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(targetId);
            dest.writeInt(objectId);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<CreateComponentResponse.Data> CREATOR = new Creator<CreateComponentResponse.Data>() {
            @Override
            public CreateComponentResponse.Data createFromParcel(Parcel in) {
                return new CreateComponentResponse.Data(in);
            }

            @Override
            public CreateComponentResponse.Data[] newArray(int size) {
                return new CreateComponentResponse.Data[size];
            }
        };
    }

    public String getJsonString() {
        return new Gson().toJson(this);
    }
}
