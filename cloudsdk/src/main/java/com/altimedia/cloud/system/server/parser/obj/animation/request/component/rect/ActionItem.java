
/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.server.parser.obj.animation.request.component.rect;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ActionItem implements Parcelable
{

    @SerializedName("objectId")
    @Expose
    private int objectId;
    @SerializedName("argb")
    @Expose
    private String argb;
    @SerializedName("x1")
    @Expose
    private String x1;
    @SerializedName("y1")
    @Expose
    private String y1;
    @SerializedName("x2")
    @Expose
    private String x2;
    @SerializedName("y2")
    @Expose
    private String y2;
    public final static Creator<ActionItem> CREATOR = new Creator<ActionItem>() {


        @SuppressWarnings({
            "unchecked"
        })
        public ActionItem createFromParcel(Parcel in) {
            return new ActionItem(in);
        }

        public ActionItem[] newArray(int size) {
            return (new ActionItem[size]);
        }

    }
    ;

    protected ActionItem(Parcel in) {
        this.objectId = in.readInt();
        this.argb = ((String) in.readValue((String.class.getClassLoader())));
        this.x1 = ((String) in.readValue((String.class.getClassLoader())));
        this.y1 = ((String) in.readValue((String.class.getClassLoader())));
        this.x2 = ((String) in.readValue((String.class.getClassLoader())));
        this.y2 = ((String) in.readValue((String.class.getClassLoader())));
    }

    public ActionItem() {
    }

    public int getObjectid() {
        return objectId;
    }


    public String getArgb() {
        return argb;
    }

    public void setArgb(String argb) {
        this.argb = argb;
    }

    public String getX1() {
        return x1;
    }

    public void setX1(String x1) {
        this.x1 = x1;
    }

    public String getY1() {
        return y1;
    }

    public void setY1(String y1) {
        this.y1 = y1;
    }

    public String getX2() {
        return x2;
    }

    public void setX2(String x2) {
        this.x2 = x2;
    }

    public String getY2() {
        return y2;
    }

    public void setY2(String y2) {
        this.y2 = y2;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(objectId);
        dest.writeValue(argb);
        dest.writeValue(x1);
        dest.writeValue(y1);
        dest.writeValue(x2);
        dest.writeValue(y2);
    }

    public int describeContents() {
        return  0;
    }

}
