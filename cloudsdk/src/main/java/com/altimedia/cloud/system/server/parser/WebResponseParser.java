/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.server.parser;

import com.altimedia.cloud.ApexMessageController;
import com.altimedia.cloud.ApexCloudEventListener;
import com.altimedia.cloud.ApexMessage;
import com.altimedia.cloud.constant.ProtocolConstant;
import com.altimedia.cloud.impl.ApexMessageControllerImpl;
import com.altimedia.cloud.impl.ApexMessageImpl;
import com.altimedia.cloud.system.ParserImpl;
import com.altimedia.cloud.system.server.ServerConnector;
import com.altimedia.cloud.system.server.dispatch.EventDispatcher;
import com.altimedia.cloud.system.server.parser.util.JsonUtil;
import com.altimedia.cloud.system.util.apexLogger;

/**
 * User: gh.baek
 * Date: 2018.5.31
 */
public class WebResponseParser extends ParserImpl {
    String msg;
    String appID;

    public WebResponseParser(ServerConnector serverConnector) {
        setServerConnector(serverConnector);
    }

    protected void cleanup() {

        msg = null;
    }

    public void initialize() {

    }

    public void setAppID(String id){
        appID = id;
    }

    @Override
    public void parse(EventDispatcher dispatcher) throws Exception {

        int command = ProtocolConstant.COMMAND_RESPONSE_WEB_MESSAGE;
        int reserved = getByte();
        int Length = getInt();


        byte[] message = new byte[Length];
        getArray(message, Length);
//
        msg = new String(message);
        if (apexLogger.DEBUG)
            apexLogger.out(" WebResponseParser.parser  ---------------- msg is " + msg);

        final ApexMessage apexMessage = JsonUtil.newGson().fromJson(msg, ApexMessage.class);
        if (apexMessage instanceof ApexMessageImpl) {
            ((ApexMessageImpl) apexMessage).setAppID(appID);
        }
        deliverMessage(apexMessage);
    }


    protected void deliverMessage(ApexMessage apexMessage) {
        try {
            if (apexLogger.DEBUG) {
                apexLogger.out("deliverMessage msg : " + msg);
            }

            if (apexMessage != null) {
                if (apexLogger.DEBUG) {
                    apexLogger.out("deliverMessage apexMessage : " + apexMessage);
                }

                ((ApexMessageControllerImpl) ApexMessageController.getInstance())
                        .notifyWebMessage(serverConnector.getAppId(), apexMessage);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    public void doBitCloudCommand(ApexCloudEventListener listener) {
        if (listener == null) return;
        // TODO: 이건 필요한 건지 파악.
        //ApexMessageController.getInstance().deliverMessage(msg);
        //listener.notifyWebMessage(msg);
    }
}
