
/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.server.parser.obj.animation.request.animation.create;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Param implements Parcelable {

    @SerializedName("sx1")
    @Expose
    private int sLeft;
    @SerializedName("sy1")
    @Expose
    private int sTop;
    @SerializedName("sx2")
    @Expose
    private int sRight;
    @SerializedName("sy2")
    @Expose
    private int sBottom;
    @SerializedName("sz")
    @Expose
    private int sz;
    @SerializedName("dx1")
    @Expose
    private int dLeft;
    @SerializedName("dy1")
    @Expose
    private int dTop;
    @SerializedName("dx2")
    @Expose
    private int dRight;
    @SerializedName("dy2")
    @Expose
    private int dBottom;
    @SerializedName("dz")
    @Expose
    private int dz;
    @SerializedName("extraAlphaStart")
    @Expose
    private int extraAlphaStart;
    @SerializedName("extraAlphaEnd")
    @Expose
    private int extraAlphaEnd;

    @SerializedName("startIndex")
    @Expose
    private int startIndex;

    @SerializedName("repeat")
    @Expose
    private int repeat;

    public final static Creator<Param> CREATOR = new Creator<Param>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Param createFromParcel(Parcel in) {
            return new Param(in);
        }

        public Param[] newArray(int size) {
            return (new Param[size]);
        }

    };

    protected Param(Parcel in) {
        this.sLeft = in.readInt();
        this.sTop = in.readInt();
        this.sRight = in.readInt();
        this.sBottom =  in.readInt();
        this.sz =  in.readInt();
        this.dLeft = in.readInt();
        this.dTop =  in.readInt();
        this.dRight = in.readInt();
        this.dBottom =  in.readInt();
        this.dz = in.readInt();
        this.extraAlphaStart =  in.readInt();
        this.extraAlphaEnd =  in.readInt();
        this.startIndex =  in.readInt();
        this.repeat =  in.readInt();
    }

    public Param() {
    }

    public float getsLeft() {
        return sLeft;
    }

    public float getsWidth() {
        return getsRight() - getsLeft();
    }

    public float getsHeight() {
        return getsBottom() - getsTop();
    }

    public float getdWidth() {
        return getdRight() - getdLeft();
    }

    public float getdHeight() {
        return getdBottom() - getdTop();
    }


    public float getsTop() {
        return sTop;
    }

    public float getsRight() {
        return sRight;
    }

    public float getsBottom() {
        return sBottom;
    }


    public int getSz() {
        return sz;
    }


    public float getdLeft() {
        return dLeft;
    }


    public float getdTop() {
        return dTop;
    }


    public float getdRight() {
        return dRight;
    }


    public float getdBottom() {
        return dBottom;
    }

    public int getDz() {
        return dz;
    }

    public int getExtraAlphaStart() {
        return extraAlphaStart;
    }

    public int getExtraAlphaEnd() {
        return extraAlphaEnd;
    }


    public float getTranslationX() {
        return getdLeft() - getsLeft();
    }

    public float getTranslationY() {
        return getdTop() - getsTop();
    }

    public float getScaleX() {
        return getdWidth() / getsWidth();
    }

    public float getScaleY() {
        return getdHeight() / getsHeight();
    }


    public int getStartIndex() {
        return startIndex;
    }

    public int getRepeat() {
        return repeat;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(sLeft);
        dest.writeValue(sTop);
        dest.writeValue(sRight);
        dest.writeValue(sBottom);
        dest.writeValue(sz);
        dest.writeValue(dLeft);
        dest.writeValue(dTop);
        dest.writeValue(dRight);
        dest.writeValue(dBottom);
        dest.writeValue(dz);
        dest.writeValue(extraAlphaStart);
        dest.writeValue(extraAlphaEnd);
        dest.writeValue(startIndex);
        dest.writeValue(repeat);
    }

    public int describeContents() {
        return 0;
    }

    @Override
    public String toString() {
        return "Param{" +
                "sLeft=" + sLeft +
                ", sTop=" + sTop +
                ", sRight=" + sRight +
                ", sBottom=" + sBottom +
                ", sz=" + sz +
                ", dLeft=" + dLeft +
                ", dTop=" + dTop +
                ", dRight=" + dRight +
                ", dBottom=" + dBottom +
                ", dz=" + dz +
                ", extraAlphaStart=" + extraAlphaStart +
                ", extraAlphaEnd=" + extraAlphaEnd +
                ", startIndex=" + startIndex +
                ", repeat=" + repeat +
                '}';
    }
}
