
/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.server.parser.obj.animation.request.animation.create;

import android.os.Parcel;
import android.os.Parcelable;

import com.altimedia.cloud.system.server.parser.obj.animation.request.animation.Action;
import com.altimedia.cloud.system.server.parser.obj.animation.request.animation.InterpolatorType;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ActionItem implements Parcelable {
    @SerializedName("curve")
    @Expose
    private InterpolatorType interpolatorType;
    @SerializedName("duration")
    @Expose
    private long duration;
    @SerializedName("objectId")
    @Expose
    private int objectId;
    @SerializedName("action")
    @Expose
    private Action action;
    @SerializedName("param")
    @Expose
    private Param param;
    public final static Creator<ActionItem> CREATOR = new Creator<ActionItem>() {


        @SuppressWarnings({
                "unchecked"
        })
        public ActionItem createFromParcel(Parcel in) {
            return new ActionItem(in);
        }

        public ActionItem[] newArray(int size) {
            return (new ActionItem[size]);
        }

    };

    protected ActionItem(Parcel in) {
        this.interpolatorType = ((InterpolatorType) in.readValue((InterpolatorType.class.getClassLoader())));
        this.duration = ((Long) in.readValue((Long.class.getClassLoader())));
        this.objectId = in.readInt();
        this.action = ((Action) in.readValue((Action.class.getClassLoader())));
        this.param = ((Param) in.readValue((Param.class.getClassLoader())));
    }

    public ActionItem() {
    }

    public int getObjectid() {
        return objectId;
    }


    public Action getAction() {
        return action;
    }

    public Param getParam() {
        return param;
    }

    public InterpolatorType getInterpolatorType() {
        return interpolatorType;
    }

    public long getDuration() {
        return duration;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(interpolatorType);
        dest.writeValue(duration);
        dest.writeValue(objectId);
        dest.writeValue(action);
        dest.writeValue(param);
    }

    public int describeContents() {
        return 0;
    }

}
