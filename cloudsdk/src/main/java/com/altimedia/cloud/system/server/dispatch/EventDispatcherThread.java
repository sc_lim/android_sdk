/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.server.dispatch;

import com.altimedia.cloud.ApexCloudEventListener;
import com.altimedia.cloud.system.util.apexLogger;

/**
 * Created by IntelliJ IDEA.
 * User: gh.baek
 * Date: 2018. 5. 23
 * To change this template use File | Settings | File Templates.
 */
final class EventDispatcherThread extends Thread {
    private boolean isRun = false;

    private EventDispatcher dispatcher;

    private ApexCloudEventListener apexCloudEventListener;

    EventDispatcherThread() {
        super("EventDispatcherThread");
    }

    void startDispatchThread(EventDispatcher dispatcher) {
        if(isRun) return;

        if(apexLogger.INFO) apexLogger.out(" EventDispatcherThread is START    !!!");

        isRun = true;

        this.dispatcher = dispatcher;

        start();
    }

    void addApexCloudEventListener(ApexCloudEventListener listener) {
        this.apexCloudEventListener = listener;
    }

    ApexCloudEventListener getApexCloudEventListener() {
        return apexCloudEventListener;
    }

    public void run() {
        try {
            while (isRun) {
                EventCommand command = dispatcher.getEvent();

                if(command != null) {
                    command.doBitCloudCommand( apexCloudEventListener );
                }
            }
            dispatcher = null;


        } catch (Exception e) {
            if(apexLogger.ERROR) apexLogger.out(" EventDispatcherThread ==> End Exception : " + e.getMessage());
        }

        if(apexLogger.INFO) apexLogger.out("[3] EventDispatcherThread is STOP !!!");
    }

    void notifyErrorEvent(int code, String msg) {

        if(apexCloudEventListener != null) {
            apexCloudEventListener.notifyErrorEvent(code, msg);
        }
    }


//    public void stopDispatchThread() {
//        dispatcher = null;
//        isRun = false;
//
//        interrupt();
//
//        removeAnyrootListener();
//    }

    public void stopDispatchThread() {
        removeAnyrootListener();

        isRun = false;
    }

    void removeAnyrootListener() {
        apexCloudEventListener = null;
    }
}
