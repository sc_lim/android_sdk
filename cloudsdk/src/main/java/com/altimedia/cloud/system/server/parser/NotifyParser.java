/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.server.parser;

import com.altimedia.cloud.ApexCloudEventListener;
import com.altimedia.cloud.constant.ProtocolConstant;
import com.altimedia.cloud.system.ParserImpl;
import com.altimedia.cloud.system.server.dispatch.EventDispatcher;
import com.altimedia.cloud.system.util.apexLogger;

/**
 * Created by IntelliJ IDEA.
 * User: gh.baek
 * Date: 2018. 5. 31
 * To change this template use File | Settings | File Templates.
 */
class NotifyParser extends ParserImpl {
    private int code;
    private String msg;

    public int getCode() {
        return this.code;
    }

    public void initialize() {

    }

    public void parse(EventDispatcher dispatcher) throws Exception {


        int command = ProtocolConstant.COMMAND_RESPONSE_NOTIFY;
        int reserved = getByte();
        code = getShort();
        int Length = getInt();


        byte[] message = new byte[Length];
        getArray( message, Length );

        msg = new String(message);


        /*
        // “Invalid status code received: 406 Status line: HTTP/1.1 406 Not Acceptable”

        msg = getMessage();

        String tmp = msg.substring(msg.indexOf( "HTTP/1.1" ));
        String tmp2 = null;
        if(tmp != null ) {
            tmp2 = tmp.substring( 9, 13 );
            code = Integer.parseInt( tmp2 );
        }

        if( code == ErrorConstant.SERVER_WEBSOCKET_ERROR && tmp2 != null )
        {
            // Remove unnecessary sentences and deliver them
            msg = msg.substring(msg.indexOf(tmp2));
        }
*/
        if(apexLogger.DEBUG) {
            apexLogger.out(" NotifyParser.parser  Code : " + code + ", msg : " + msg );
            //apexLogger.out(" NotifyParser.parser  Message : " + msg);
        }
    }

    protected void cleanup() {
        //To change body of implemented methods use File | Settings | File Templates.
        msg = null;
    }

    public void doBitCloudCommand(ApexCloudEventListener listener) {
        //To change body of implemented methods use File | Settings | File Templates.
        if(listener == null) return;

        switch(code) {
            case ProtocolConstant.NOTIFYING_MESSAGE_CONNECT:
                listener.notifyConnected();
                break;

            case ProtocolConstant.NOTIFYING_MESSAGE_DISCONNECT:
                listener.notifyDisconnected();
                break;
            case ProtocolConstant.NOTIFYING_MESSAGE_PAGE_ERROR:
            default:
                listener.notifyErrorEvent( code, msg );
                break;
        }
    }

}
