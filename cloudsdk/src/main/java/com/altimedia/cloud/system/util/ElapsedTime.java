/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.util;

import java.util.ArrayList;

public class ElapsedTime {
    private static final ElapsedTime ourInstance = new ElapsedTime();

    public static ElapsedTime getInstance() {
        return ourInstance;
    }

    private boolean enabled = false;

    private ElapsedTime() {
    }

    private class SubSession {
        public String title;
        public String baseTitle;
        public int index;
        public long ts;

        public SubSession(String t, String baseTitle, int idx, long time) {
            this.title = t;
            this.baseTitle = baseTitle;
            this.index = idx;
            this.ts = time;
        }
    }

    private ArrayList<SubSession> subSessions = new ArrayList<>();
    private int subSessionIndex = 0;

    public boolean setEabled(boolean enable) {
        this.enabled = enable;
        return this.enabled;
    }

    public void clearSubSession() {
        if (enabled) {
            subSessionIndex = 0;
            subSessions.clear();
        }
    }

    public void addSubSession(String title, String baseTitle) {
        if (enabled) {
            subSessions.add(new SubSession(title, baseTitle, subSessionIndex++, System.currentTimeMillis()));
        }
    }

    public void addSubSession(String title) {
        if (enabled) {
            subSessions.add(new SubSession(title, null, subSessionIndex++, System.currentTimeMillis()));
        }
    }

    private long getBaseTime(String baseTitle) {
        long baseTime = 0;

        if (baseTitle != null) {
            for (SubSession ss : subSessions) {
                if (ss.title != null && baseTitle.equals(ss.title)) {
                    baseTime = ss.ts;
                    break;
                }
            }
        }

        return baseTime;
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("\r\n");
        if (enabled) {
            long initBase = subSessions.get(0).ts;

            for (SubSession ss : subSessions) {
                stringBuffer.append("[" + ss.title + "] index:" + ss.index);
                stringBuffer.append(", total:" + (ss.ts - initBase) + " msec");
                if (ss.baseTitle != null) {
                    long base = getBaseTime(ss.baseTitle);
                    stringBuffer.append(", (" + ss.baseTitle + ":" + (ss.ts - base) + " msec)");
                }
                stringBuffer.append("\n");
            }
            clearSubSession();
        }
        return stringBuffer.toString();
    }
}