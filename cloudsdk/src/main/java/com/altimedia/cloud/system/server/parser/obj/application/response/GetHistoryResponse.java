/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.server.parser.obj.application.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.altimedia.cloud.system.server.parser.obj.BaseResponse;
import com.altimedia.cloud.system.server.parser.obj.ProtocolType;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mc.kim on 14,05,2020
 */
public class GetHistoryResponse extends BaseResponse {

    @Expose
    @SerializedName("data")
    private GetHistoryData data;

    protected GetHistoryResponse(Parcel in) {
        super(in);
        data = (GetHistoryData) in.readValue(GetHistoryData.class.getClassLoader());
    }

    public GetHistoryResponse(int requestId, ProtocolType protocolType) {
        super(requestId, protocolType);
    }

    @Override
    public void setResult(boolean success) {
        super.setResult(success);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeValue(data);
    }

    public void setHistoryData(String history){
        data = new GetHistoryData(history);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<GetHistoryResponse> CREATOR = new Creator<GetHistoryResponse>() {
        @Override
        public GetHistoryResponse createFromParcel(Parcel in) {
            return new GetHistoryResponse(in);
        }

        @Override
        public GetHistoryResponse[] newArray(int size) {
            return new GetHistoryResponse[size];
        }
    };

    public GetHistoryData getData() {
        return data;
    }

    @Override
    public String toString() {
        return "GetHistoryRequest{" +
                "data=" + data +
                '}';
    }

    private static class GetHistoryData implements Parcelable {
        @Expose
        @SerializedName("value")
        private String value;


        public void setValue(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return "GetHistoryData{" +
                    "value='" + value + '\'' +
                    '}';
        }

        protected GetHistoryData(Parcel in) {
            value = in.readString();
        }

        public GetHistoryData(String value) {
            this.value = value;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(value);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<GetHistoryData> CREATOR = new Creator<GetHistoryData>() {
            @Override
            public GetHistoryData createFromParcel(Parcel in) {
                return new GetHistoryData(in);
            }

            @Override
            public GetHistoryData[] newArray(int size) {
                return new GetHistoryData[size];
            }
        };
    }
}
