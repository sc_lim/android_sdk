
/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.server.parser.obj.animation.request.component.visible;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ActionItem implements Parcelable {

    @SerializedName("objectId")
    @Expose
    private int objectId;

    // (0/1)
    @SerializedName("visible")
    @Expose
    private int visible;
    public final static Creator<ActionItem> CREATOR = new Creator<ActionItem>() {


        @SuppressWarnings({
                "unchecked"
        })
        public ActionItem createFromParcel(Parcel in) {
            return new ActionItem(in);
        }

        public ActionItem[] newArray(int size) {
            return (new ActionItem[size]);
        }

    };

    protected ActionItem(Parcel in) {
        this.objectId = in.readInt();
        this.visible = in.readInt();
    }

    public ActionItem() {
    }

    public int getObjectid() {
        return objectId;
    }

    public boolean isVisible() {
        return visible == 1;
    }

    public void setVisible(int visible) {
        this.visible = visible;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(objectId);
        dest.writeInt(visible);
    }

    public int describeContents() {
        return 0;
    }

}
