/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.server.parser;



import com.altimedia.cloud.constant.ProtocolConstant;
import com.altimedia.cloud.system.Parser;
import com.altimedia.cloud.system.ParserImpl;
import com.altimedia.cloud.system.server.ServerConnector;
import com.altimedia.cloud.system.util.apexLogger;


/**
 * Created by IntelliJ IDEA.
 * User: gh.baek
 * Date: 2018. 5. 30
 * To change this template use File | Settings | File Templates.
 */
public class ParserFactory {

    private final int FRAME_PARSER = 0;
    private final int PING_PARSER = FRAME_PARSER + 1;
    private final int NOTIFY_PARSER = PING_PARSER + 1;
    private final int WEB_NOTIFY_PARSER = NOTIFY_PARSER + 1;

    private Parser[] parser;

    public ParserFactory(ServerConnector serverConnector) {
        create(serverConnector);
    }

    public void create(ServerConnector serverConnector) {
        parser = new ParserImpl[WEB_NOTIFY_PARSER + 1];

        FrameParser frameParser = new FrameParser(serverConnector);
        frameParser.initialize();
        parser[FRAME_PARSER] = frameParser;

        parser[NOTIFY_PARSER] = new NotifyParser();
        parser[NOTIFY_PARSER].initialize();

        WebResponseParser textParser = new WebResponseParser(serverConnector);
        textParser.setAppID(serverConnector.getAppId());

        parser[WEB_NOTIFY_PARSER] = textParser;
        parser[WEB_NOTIFY_PARSER].initialize();
        //parser[WEB_NOTIFY_PARSER] = new WebResponseParser();
        //parser[WEB_NOTIFY_PARSER].initialize();
    }

    public Parser getParser(int protocolID) {
        if (apexLogger.INFO)
            apexLogger.out(" ServerParserFactory.getParser ---------- command ID 0x0" + Integer.toHexString(protocolID));

        Parser parser = null;

        switch (protocolID) {

            case ProtocolConstant.COMMAND_UPDATE_FRAME:
                parser = this.parser[FRAME_PARSER];
                break;

            case ProtocolConstant.COMMAND_RESPONSE_NOTIFY:
                parser = this.parser[NOTIFY_PARSER];
                break;

            case ProtocolConstant.COMMAND_RESPONSE_WEB_MESSAGE:
                parser = this.parser[WEB_NOTIFY_PARSER];
                break;

            default:
                if (apexLogger.ERROR)
                    apexLogger.out(" ServerParserFactory.getParser >>>>>>>>>>>>>>>>>>>>>>>>>>  no defined protocol !!!");
                break;
        }

        return parser;
    }

    public void destroy() {
        if (parser == null) return;

        for (int i = 0; i < parser.length; i++) {

            if (parser[i] != null) {
                parser[i].destroy();
                parser[i] = null;
            }
        }

        parser = null;

        if (apexLogger.INFO) apexLogger.out("[7] ParserFactory is STOP !!!");
    }
}
