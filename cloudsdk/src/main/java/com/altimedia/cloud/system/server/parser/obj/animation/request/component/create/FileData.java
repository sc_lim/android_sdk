/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.server.parser.obj.animation.request.component.create;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mc.kim on 09,04,2020
 */
public class FileData implements Parcelable {
    @Expose
    @SerializedName("imageType")
    private String imageType;

    @Expose
    @SerializedName("byteArray")
    private String byteArray;

    protected FileData(Parcel in) {
        imageType = in.readString();
        byteArray = in.readString();
    }

    public String getImageType() {
        return imageType;
    }

    public String getByteArray() {
        return byteArray;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(imageType);
        dest.writeString(byteArray);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<FileData> CREATOR = new Creator<FileData>() {
        @Override
        public FileData createFromParcel(Parcel in) {
            return new FileData(in);
        }

        @Override
        public FileData[] newArray(int size) {
            return new FileData[size];
        }
    };
}
