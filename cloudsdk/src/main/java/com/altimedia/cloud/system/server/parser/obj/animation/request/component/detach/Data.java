
/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.server.parser.obj.animation.request.component.detach;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data implements Parcelable
{

    @SerializedName("srcComponent")
    @Expose
    private SrcComponent srcComponent;
    public final static Creator<Data> CREATOR = new Creator<Data>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Data createFromParcel(Parcel in) {
            return new Data(in);
        }

        public Data[] newArray(int size) {
            return (new Data[size]);
        }

    }
    ;

    protected Data(Parcel in) {
        this.srcComponent = ((SrcComponent) in.readValue((SrcComponent.class.getClassLoader())));
    }

    public Data() {
    }

    public SrcComponent getSrcComponent() {
        return srcComponent;
    }

    public void setSrcComponent(SrcComponent srcComponent) {
        this.srcComponent = srcComponent;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(srcComponent);
    }

    public int describeContents() {
        return  0;
    }

}
