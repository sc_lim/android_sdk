/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.server.parser.obj.animation.request.component.detach;

import android.os.Parcel;

import com.altimedia.cloud.system.server.parser.obj.BaseRequest;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by mc.kim on 13,05,2020
 */
public class ComponentDetachData  extends BaseRequest {
    @SerializedName("data")
    @Expose
    private ArrayList<com.altimedia.cloud.system.server.parser.obj.animation.request.component.detach.Data> data = new ArrayList<>();

    protected ComponentDetachData(Parcel in) {
        super(in);
        in.readList(this.data,
                com.altimedia.cloud.system.server.parser.obj.animation.request.component.detach.Data.class.getClassLoader());
    }

    public ArrayList<com.altimedia.cloud.system.server.parser.obj.animation.request.component.detach.Data> getData() {
        return data;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeList(data);
    }

    @Override
    public int describeContents() {
        return super.describeContents();
    }
}
