/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.server.parser.obj;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mc.kim on 13,05,2020
 */
public class BaseRequest implements Parcelable {
    @Expose
    @SerializedName("requestId")
    private int requestId;

    @Expose
    @SerializedName("protocolType")
    private ProtocolType protocolType;

    private ProtocolPackageType protocolPackageType;

    protected BaseRequest(Parcel in) {
        requestId = in.readInt();
        protocolType = (ProtocolType) in.readValue(ProtocolType.class.getClassLoader());
    }

    public void setProtocolPackageType(String protocolPackage) {
        this.protocolPackageType = ProtocolPackageType.findByName(protocolPackage);
    }

    public ProtocolPackageType getProtocolPackageType() {
        return protocolPackageType;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(requestId);
        dest.writeValue(protocolType);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BaseRequest> CREATOR = new Creator<BaseRequest>() {
        @Override
        public BaseRequest createFromParcel(Parcel in) {
            return new BaseRequest(in);
        }

        @Override
        public BaseRequest[] newArray(int size) {
            return new BaseRequest[size];
        }
    };

    public int getRequestId() {
        return requestId;
    }

    public ProtocolType getProtocolType() {
        return protocolType;
    }

    @Override
    public String toString() {
        return "BaseRequest{" +
                "requestId=" + requestId +
                ", protocolType=" + protocolType +
                ", protocolPackageType=" + protocolPackageType +
//                ", protocolMethod='" + protocolMethod + '\'' +
                '}';
    }
}
