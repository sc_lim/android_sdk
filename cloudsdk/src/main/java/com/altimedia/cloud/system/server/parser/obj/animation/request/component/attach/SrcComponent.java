
/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.server.parser.obj.animation.request.component.attach;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SrcComponent implements Parcelable {

    @SerializedName("objectId")
    @Expose
    private int objectId;
    @SerializedName("zorder")
    @Expose
    private int zorder = 0;
    public final static Creator<SrcComponent> CREATOR = new Creator<SrcComponent>() {


        @SuppressWarnings({
                "unchecked"
        })
        public SrcComponent createFromParcel(Parcel in) {
            return new SrcComponent(in);
        }

        public SrcComponent[] newArray(int size) {
            return (new SrcComponent[size]);
        }

    };

    protected SrcComponent(Parcel in) {
        this.objectId = in.readInt();
        this.zorder = in.readInt();
    }

    public SrcComponent() {
    }

    public int getObjectid() {
        return objectId;
    }

    public int getZorder() {
        return zorder;
    }


    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(objectId);
        dest.writeValue(zorder);
    }

    public int describeContents() {
        return 0;
    }

    @Override
    public String toString() {
        return "SrcComponent{" +
                "objectId='" + objectId + '\'' +
                ", zorder='" + zorder + '\'' +
                '}';
    }
}
