/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.server.dispatch;

import com.altimedia.cloud.ApexCloudEventListener;
import com.altimedia.cloud.system.util.apexLogger;

import java.util.Vector;

/**
 * Created by IntelliJ IDEA.
 * User: gh.baek
 * Date: 2018. 5. 24
 * To change this template use File | Settings | File Templates.
 */
public final class EventDispatcher {

    private static final byte[] LOCK = new byte[0];

    private Vector queue = new Vector();
    private EventDispatcherThread dispatcherThread;

    public EventDispatcher() {

        dispatcherThread = new EventDispatcherThread();
    }


    public void startDispatcher() {
        dispatcherThread.startDispatchThread(this);
    }

    public void addApexCloudEventListener(ApexCloudEventListener listener) {
        if (dispatcherThread != null) {
            dispatcherThread.addApexCloudEventListener(listener);
        }
    }

    public void removeApexCloudEventListener() {
        if (dispatcherThread != null) {
            dispatcherThread.removeAnyrootListener();
        }
    }

    public ApexCloudEventListener getApexCloudEventListener() {
        return dispatcherThread.getApexCloudEventListener();
    }

    public void putEvent(EventCommand command) {

        synchronized (LOCK) {
            if(command != null) {

                queue.add(command);
                LOCK.notifyAll();
            }
        }
    }

    public EventCommand getEvent() throws Exception {

        if(queue == null) return null;

        if (queue.size() == 0) {

            synchronized (LOCK) {
                if (queue.size() == 0) {
                    try {
                        LOCK.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }

            }
        }

        return ((EventCommand) queue.remove(0));
    }

    public void notifyErrorEvent(int code, String msg) {

        if(dispatcherThread != null) {
            dispatcherThread.notifyErrorEvent(code, msg);
        }
    }


//    private void destroy() {
//        if (dispatcherThread != null) {
//            dispatcherThread.stopDispatchThread();
//            dispatcherThread = null;
//        }
//
//        if (queue != null) {
//            queue.clear();
//            queue = null;
//        }
//    }

    public void destroy() {
        if (queue != null) {
            queue.clear();
        }

        putEvent(new StopEventCommand());
    }

    private class StopEventCommand implements EventCommand {

        public void doBitCloudCommand(ApexCloudEventListener listener) {
            if (dispatcherThread != null) {
                dispatcherThread.stopDispatchThread();
                dispatcherThread = null;
            }

            if (queue != null) {
                queue.clear();
                queue = null;
            }

            if(apexLogger.INFO) apexLogger.out("[2] EventDispatcher.StopEventCommand is STOP !!!");
        }
    }
}
