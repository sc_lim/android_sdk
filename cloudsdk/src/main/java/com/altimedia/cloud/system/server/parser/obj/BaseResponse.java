/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.server.parser.obj;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mc.kim on 13,05,2020
 */
public class BaseResponse implements Parcelable {
    @Expose
    @SerializedName("requestId")
    private int requestId;

    @Expose
    @SerializedName("protocolType")
    private String protocolType;

    @Expose
    @SerializedName("result")
    private int result;

    public BaseResponse(int requestId, ProtocolType protocolType) {
        this.requestId = requestId;
        this.protocolType = protocolType.getName();
    }

    public void setResult(boolean success) {
        this.result = success ? 1 : 0;
    }

    protected BaseResponse(Parcel in) {
        requestId = in.readInt();
        protocolType = in.readString();
        result = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(requestId);
        dest.writeValue(protocolType);
        dest.writeInt(result);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BaseResponse> CREATOR = new Creator<BaseResponse>() {
        @Override
        public BaseResponse createFromParcel(Parcel in) {
            return new BaseResponse(in);
        }

        @Override
        public BaseResponse[] newArray(int size) {
            return new BaseResponse[size];
        }
    };

    public int getRequestId() {
        return requestId;
    }

    public String getProtocolType() {
        return protocolType;
    }

    public int getResult() {
        return result;
    }

    @Override
    public String toString() {
        return "BaseResponse{" +
                "requestId=" + requestId +
                ", protocolType=" + protocolType +
                ", result=" + result +
                '}';
    }
}
