/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.server.parser.obj;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

import com.altimedia.cloud.constant.SourceTypeConstant;

import java.util.Arrays;


/**
 * The type Cs frame.
 */
public class CsFrame {
    private final int imageType;
    private byte[] source;
    private final Rect bounds;
    private final int hashcode;
    private Bitmap bitmap;
    private boolean isUpdate;

    /**
     * Instantiates a new Cs frame.
     *
     * @param imageType Image encoding type (SourceTypeConstant value)
     * @param source image source
     * @param x x coordinate of image
     * @param y y coordinate of image
     * @param w width of image
     * @param h height of image
     */
    public CsFrame(int imageType, byte[] source, int x, int y, int w, int h, boolean isUpdate) {
        this.imageType = imageType;
        this.source = source;
        this.bounds = new Rect(x, y, x + w, y + h);
        this.hashcode = Arrays.hashCode(source);
        this.isUpdate = isUpdate;
    }

    public CsFrame(int x, int y, int w, int h) {
        this(SourceTypeConstant.FRAME_ENCODING_NONE, null, x, y, w, h, true);
    }

    public int getImageType() {
        return imageType;
    }

    public int getHashcode() {
        return hashcode;
    }

    public Rect getBounds() {
        return bounds;
    }

    public int getX() {
        return bounds.left;
    }

    public int getY() {
        return bounds.top;
    }

    public int getWidth() {
        return bounds.right - bounds.left;
    }

    public int getHeight() {
        return bounds.bottom - bounds.top;
    }

    public byte[] getSource() {
        return source;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
        source = null;
    }

    public Bitmap getBitmap() {
        if (bitmap == null) {
            decodeImage();
        }
        return bitmap;
    }

    private void decodeImage() {
        if (source != null) {
            bitmap = BitmapFactory.decodeByteArray(source, 0, source.length);
            source = null;

            bitmap.prepareToDraw(); // BitmapShader를 사용할때만 활성화.
        }
    }

    public boolean isUpdate() {
        return isUpdate;
    }

    @Override
    public String toString() {
        return "CsFrame{" +
                "imageType=" + imageType +
                ", bounds=" + bounds +
                ", hashcode=" + hashcode +
                ", isUpdate=" + isUpdate +
                ", bitmap=" + bitmap +
//                ", source=" + Arrays.toString(source) +
                ", source=" + source +
                '}';
    }
}
