/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.server.parser.obj.application;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mc.kim on 14,05,2020
 */
public class LaunchInfo implements Parcelable {

    @Expose
    @SerializedName("url")
    private String url;
    @Expose
    @SerializedName("appName")
    private String appName;

//    @Expose
//    @SerializedName("cloudPort")
//    private int cloudPort;

    protected LaunchInfo(Parcel in) {
        url = in.readString();
//        cloudIp = in.readString();
//        cloudPort = in.readInt();
    }

    public LaunchInfo(String url, String appName) {
        this.url = url;
        this.appName = appName;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(url);
        dest.writeString(appName);
//        dest.writeInt(cloudPort);
    }


    @Override
    public String toString() {
        return "LaunchInfo{" +
                "url='" + url + '\'' +
                ", appName='" + appName + '\'' +
                '}';
    }

    public String getUrl() {
        return url;
    }

    public String getAppName() {
        return appName;
    }
//
//    public int getCloudPort() {
//        return cloudPort;
//    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<LaunchInfo> CREATOR = new Creator<LaunchInfo>() {
        @Override
        public LaunchInfo createFromParcel(Parcel in) {
            return new LaunchInfo(in);
        }

        @Override
        public LaunchInfo[] newArray(int size) {
            return new LaunchInfo[size];
        }
    };
}

