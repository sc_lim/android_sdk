
/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.server.parser.obj.animation.request.component.rect;

import android.graphics.Color;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data implements Parcelable {

    @SerializedName("objectId")
    @Expose
    private int objectId;
    @SerializedName("argb")
    @Expose
    private String argb;
    @SerializedName("x1")
    @Expose
    private int x1;
    @SerializedName("y1")
    @Expose
    private int y1;
    @SerializedName("x2")
    @Expose
    private int x2;
    @SerializedName("y2")
    @Expose
    private int y2;
    public final static Creator<Data> CREATOR = new Creator<Data>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Data createFromParcel(Parcel in) {
            return new Data(in);
        }

        public Data[] newArray(int size) {
            return (new Data[size]);
        }

    };

    protected Data(Parcel in) {
        this.objectId = in.readInt();
        this.argb = ((String) in.readValue((String.class.getClassLoader())));
        this.x1 = in.readInt();
        this.y1 = in.readInt();
        this.x2 = in.readInt();
        this.y2 = in.readInt();
    }

    public Data() {
    }

    public int getObjectid() {
        return objectId;
    }

    public String getArgb() {
        return argb;
    }

    public int getLeft() {
        return x1;
    }

    public int getTop() {
        return y1;
    }

    public int getRight() {
        return x2;
    }

    public int getBottom() {
        return y2;
    }

    public int getWidth() {
        return getRight() - getLeft();
    }

    public int getHeight() {
        return getBottom() - getTop();
    }

    public int getColorInt() {
        int color = Color.parseColor("#" + argb);
        return color;
    }

    public void setArgb(String argb) {
        this.argb = argb;
    }

    public int getX1() {
        return x1;
    }

    public void setX1(int x1) {
        this.x1 = x1;
    }

    public int getY1() {
        return y1;
    }

    public void setY1(int y1) {
        this.y1 = y1;
    }

    public int getX2() {
        return x2;
    }

    public void setX2(int x2) {
        this.x2 = x2;
    }

    public int getY2() {
        return y2;
    }

    public void setY2(int y2) {
        this.y2 = y2;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(objectId);
        dest.writeValue(argb);
        dest.writeValue(x1);
        dest.writeValue(y1);
        dest.writeValue(x2);
        dest.writeValue(y2);
    }

    public int describeContents() {
        return 0;
    }

}
