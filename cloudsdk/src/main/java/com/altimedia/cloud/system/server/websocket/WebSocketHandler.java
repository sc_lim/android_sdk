/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.server.websocket;

import com.altimedia.cloud.ApexCloudDoc;
import com.altimedia.cloud.constant.ErrorConstant;
import com.altimedia.cloud.constant.ProtocolConstant;
import com.altimedia.cloud.system.Parser;
import com.altimedia.cloud.system.server.ServerConnector;
import com.altimedia.cloud.system.server.dispatch.EventDispatcher;
import com.altimedia.cloud.system.server.dispatch.command.ErrorEventCommand;
import com.altimedia.cloud.system.server.dispatch.command.FailEventCommand;
import com.altimedia.cloud.system.server.parser.ParserFactory;
import com.altimedia.cloud.system.util.CheckResponse;
import com.altimedia.cloud.system.util.ElapsedTime;
import com.altimedia.cloud.system.util.StatusConnect;
import com.altimedia.cloud.system.util.apexLogger;

import org.java_websocket.WebSocket;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.drafts.Draft;
import org.java_websocket.drafts.Draft_6455;
import org.java_websocket.framing.Framedata;
import org.java_websocket.handshake.ServerHandshake;

import java.net.URI;
import java.nio.ByteBuffer;

public class WebSocketHandler extends WebSocketClient {

    private ParserFactory parserFactory;
    private EventDispatcher dispatcher;
    private ApexCloudDoc mApexCloudDoc;

    public WebSocketHandler(EventDispatcher dispatcher, ServerConnector serverConnector, URI serverUrl, ApexCloudDoc cloudDoc, int timeout) {
        super(serverUrl, (Draft) (new Draft_6455()), null, timeout);
        this.mApexCloudDoc = cloudDoc;
        this.dispatcher = dispatcher;

        parserFactory = new ParserFactory(serverConnector);
    }


    @Override
    public void onOpen(ServerHandshake handshakedata) {
        if (apexLogger.INFO) apexLogger.out("ApexCloud WebSocket Connected");

        ErrorEventCommand errorEventCommand = new ErrorEventCommand();
        errorEventCommand.setError(ProtocolConstant.NOTIFYING_MESSAGE_CONNECT, null);

        if (dispatcher != null) {
            dispatcher.putEvent(errorEventCommand);
        }
        CheckResponse.getInstance().setPingCheck(true);

        /*
        Parser parser = parserFactory.getParser( ProtocolConstant.COMMAND_RESPONSE_NOTIFY);

        try {
            parser.parseData( String.valueOf( ProtocolConstant.NOTIFYING_MESSAGE_CONNECT ) );
        } catch (Exception e) {
            if (apexLogger.INFO) apexLogger.out(" ApexCloud got data ------------>  data parser is not supported !!");
        }

        if(dispatcher != null) {
            dispatcher.putEvent(parser);
        }
        */
    }

    @Override
    public void onWebsocketMessageFragment(WebSocket conn, Framedata frame) {
        super.onWebsocketMessageFragment(conn, frame);
        if (apexLogger.INFO) apexLogger.out("ApexCloud call onWebsocketMessageFragment");
    }


    @Override
    public void onMessage(String message) {
        if (apexLogger.INFO) apexLogger.out("ApexCloud got msg: " + message);

        CheckResponse.getInstance().setPingCheck(true);
        Parser parser = parserFactory.getParser(ProtocolConstant.COMMAND_RESPONSE_WEB_MESSAGE);
        try {
            parser.parseData(message);
        } catch (Exception e) {
            if (apexLogger.INFO)
                apexLogger.out(" ApexCloud got data ------------>  data parser is not supported !!");
        }


        if (dispatcher != null) {
            dispatcher.putEvent(parser);
        }

    }

    @Override
    public void onMessage(ByteBuffer buffer) {
        if (apexLogger.DEBUG)
            apexLogger.out("ApexCloud got data --------------------- " + buffer + ", cap:" + buffer.capacity());


        //buffer.order( ByteOrder.LITTLE_ENDIAN );
        CheckResponse.getInstance().setPingCheck(true);

        int msgType = buffer.get();
        Parser parser = parserFactory.getParser(msgType);

//        ElapsedTime.getInstance().addSubSession("onMsg : " + parser.getClass().getSimpleName());

        try {
            parser.parseData(buffer, dispatcher);
        } catch (Exception e) {
            if (apexLogger.INFO)
                apexLogger.out(" ApexCloud got exception ------------>  " + e);
                apexLogger.out(" ApexCloud got data ------------>  data parser is not supported !! type= 0x" + Integer.toHexString(msgType));

            ErrorEventCommand errorEventCommand = new ErrorEventCommand();
            errorEventCommand.setError(ErrorConstant.ERR_IMAGE_DECODE, "FrameParser : Failed Image decode. Please check image size or format.");
            if (dispatcher != null) {
                dispatcher.putEvent(errorEventCommand);
            }
        }


        if (dispatcher != null && msgType == ProtocolConstant.COMMAND_RESPONSE_NOTIFY) {
            dispatcher.putEvent(parser);
        }
    }

    @Override
    public void onClose(int code, String reason, boolean remote) {
        if (apexLogger.INFO)
            apexLogger.out("OnClose : ApexCloud Disconnected code= " + code + ", reason= " + reason);

        CheckResponse.getInstance().setPingCheck(false);
        if (code != 1000) {

            // reconnect try after first connect is failed. It doesn't send the error alarm.
            if (code == -1 && StatusConnect.getInstance().getNoErrorAlarm()) {
                FailEventCommand failEventCommand = new FailEventCommand();
                failEventCommand.setSource(mApexCloudDoc);
                if (dispatcher != null) {
                    dispatcher.putEvent(failEventCommand);
                }
                return;
            }


            if ((code == 1002 || code == -1) && StatusConnect.getInstance().getNoErrorAlarm()) {

                // HTTP Status error 여부 판단
                //reason = "Invalid status code received: 406 Status line: HTTP/1.1 406 Not Acceptable";
                String tmp = reason.substring(reason.indexOf("HTTP/1.1"));
                String tmp2 = null;
                if (tmp != null) {
                    tmp2 = tmp.substring(9, 12);
                    int status = Integer.parseInt(tmp2);
                    if (status == 406)
                        code = ErrorConstant.ERR_SERVER_SESSION_FULL;
                    else if (status == 400)
                        code = ErrorConstant.ERR_SERVER_BAD_REQUEST;
                } else {
                    if (apexLogger.INFO)
                        apexLogger.out(" ApexCloud close ------------>  no alarm. code= " + code + ", reason= " + reason);
                    StatusConnect.getInstance().setTryReconnect(true);
                    return;
                }

            } else if (code == 1006) {
                code = ErrorConstant.ERR_DECTECTED_DISCONNECTION_RECV;
                reason = "the connection was closed abnormally";
            }

            ErrorEventCommand errorEventCommand = new ErrorEventCommand();
            errorEventCommand.setError(code, reason);

            if (dispatcher != null) {
                dispatcher.putEvent(errorEventCommand);
            }
        }

    }

    @Override
    public void onError(Exception ex) {
        if (apexLogger.INFO) apexLogger.out("onError : ApexCloud Error code " + ex.getMessage());
        ex.printStackTrace();
        /*
        String msg = String.format("HTTP/1.1 %d %s", ErrorConstant.SERVER_WEBSOCKET_ERROR, ex.getMessage() );

        Parser parser = parserFactory.getParser( ProtocolConstant.COMMAND_RESPONSE_NOTIFY);
        try {
            parser.parseData( msg );
        } catch (Exception e) {
            if (apexLogger.INFO) apexLogger.out(" ApexCloud got data ------------>  data parser is not supported !!");
        }
        */
    }


    @Override
    public void onWebsocketPong(WebSocket conn, Framedata frame) {
        //super.onWebsocketPong(conn, frame);

        CheckResponse.getInstance().setResponse(true);
        CheckResponse.getInstance().setPingCheck(true);
        if (apexLogger.DEBUG) apexLogger.out("onWebsocketPong : receive pong");
    }

}
