/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.server.parser.obj;

public enum ComponentType {
    root("root"), container("container"), raw("raw"), url("url"), file("file"),
    binary("binary"), sprite("sprite"),
    invalid("invalid");
    final String name;

    ComponentType(String name) {
        this.name = name;
    }

    public static ComponentType findByName(String name) {
        ComponentType[] types = values();
        for (ComponentType type : types) {
            if (type.name.equalsIgnoreCase(name)) {
                return type;
            }
        }
        return invalid;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "ComponentType{" +
                "name='" + name + '\'' +
                '}';
    }
}
