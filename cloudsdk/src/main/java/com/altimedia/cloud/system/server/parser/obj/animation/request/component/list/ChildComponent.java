/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.server.parser.obj.animation.request.component.list;

import android.os.Parcel;
import android.os.Parcelable;

import com.altimedia.cloud.system.server.parser.obj.animation.request.SourceType;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mc.kim on 17,04,2020
 */
public class ChildComponent implements Parcelable {
    @Expose
    @SerializedName("targetId")
    private int targetId;
    @Expose
    @SerializedName("type")
    private SourceType type;
    @Expose
    @SerializedName("source")
    private String source;
    @Expose
    @SerializedName("visible")
    private String visible;
    @Expose
    @SerializedName("parent")
    private TargetComponent parent;


    protected ChildComponent(Parcel in) {
        targetId = in.readInt();
        type = (SourceType) in.readValue(SourceType.class.getClassLoader());
        source = in.readString();
        visible = in.readString();
        parent = (TargetComponent)in.readValue(TargetComponent.class.getClassLoader());
    }

    public static final Creator<ChildComponent> CREATOR = new Creator<ChildComponent>() {
        @Override
        public ChildComponent createFromParcel(Parcel in) {
            return new ChildComponent(in);
        }

        @Override
        public ChildComponent[] newArray(int size) {
            return new ChildComponent[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(targetId);
        dest.writeValue(type);
        dest.writeValue(source);
        dest.writeString(visible);
        dest.writeValue(parent);
    }

    public int getTargetId() {
        return targetId;
    }

    public SourceType getType() {
        return type;
    }

    public String getSource() {
        return source;
    }

    public String getVisible() {
        return visible;
    }

    public boolean isVisible(){
        if(visible == null){
            return true;
        }
        return visible.equalsIgnoreCase("true");
    }

    public TargetComponent getParent() {
        return parent;
    }

    @Override
    public String toString() {
        return "ChildComponent{" +
                "targetId='" + targetId + '\'' +
                ", type=" + type +
                ", source=" + source +
                ", visible='" + visible + '\'' +
                ", parent=" + parent +
                '}';
    }
}
