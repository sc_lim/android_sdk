/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.server.parser.obj.animation.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.altimedia.cloud.system.server.parser.obj.BaseResponse;
import com.altimedia.cloud.system.server.parser.obj.ProtocolType;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mc.kim on 15,04,2020
 */
public class CreateAnimationResponse extends BaseResponse {

    @Expose
    @SerializedName("data")
    private Data data;

    public CreateAnimationResponse(int requestId, ProtocolType protocolType) {
        super(requestId, protocolType);
    }

    protected CreateAnimationResponse(Parcel in) {
        super(in);
        data = (Data) in.readValue(Data.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeValue(data);
    }

    @Override
    public int describeContents() {
        return super.describeContents();
    }

    public void setResultData(int state) {
        data = new Data(state);
    }

    @Override
    public String toString() {
        return "CreateAnimationResponse{" +
                "data=" + data +
                '}';
    }

    public static class Data implements Parcelable {
        @Expose
        @SerializedName("animationState")
        private String animationState;

        public Data(int animationState) {
            this.animationState = String.valueOf(animationState);
        }

        protected Data(Parcel in) {
            animationState = in.readString();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(animationState);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public String toString() {
            return "Data{" +
                    "animationState='" + animationState + '\'' +
                    '}';
        }

        public static final Creator<Data> CREATOR = new Creator<Data>() {
            @Override
            public Data createFromParcel(Parcel in) {
                return new Data(in);
            }

            @Override
            public Data[] newArray(int size) {
                return new Data[size];
            }
        };
    }
}
