/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.util;

public class StatusConnect {
    private static final StatusConnect ourInstance = new StatusConnect();

    public static StatusConnect getInstance() {
        return ourInstance;
    }

    private boolean noErrorAlram = false; //Error Alarm을 App에 전송하지 않는다.
    private boolean tryReconnect = false; // 재접속 시도 여부

    private StatusConnect(){

    }

    public boolean setNoErrorAlarm(boolean noalarm) {
        this.noErrorAlram = noalarm;
        return this.noErrorAlram;
    }

    public boolean getNoErrorAlarm() {
        return this.noErrorAlram;
    }

    public boolean setTryReconnect(boolean reconnect) {
        this.tryReconnect = reconnect;
        return this.tryReconnect;
    }

    public boolean getTryReconnect() {
        return this.tryReconnect;
    }
}
