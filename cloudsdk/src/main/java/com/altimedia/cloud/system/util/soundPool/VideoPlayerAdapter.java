/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.util.soundPool;

public abstract class VideoPlayerAdapter {

    public static class Callback {

        public void onPlayStateChanged(VideoPlayerAdapter adapter, VideoState state) {
        }

        public void onPreparedStateChanged(VideoPlayerAdapter adapter) {
        }

        public void onPlayCompleted(VideoPlayerAdapter adapter) {
        }

        public void onCurrentPositionChanged(VideoPlayerAdapter adapter) {
        }

        public void onBufferedPositionChanged(VideoPlayerAdapter adapter) {
        }

        public void onDurationChanged(VideoPlayerAdapter adapter) {
        }

        public void onVideoSizeChanged(VideoPlayerAdapter adapter, int width, int height) {
        }

        public void onError(VideoPlayerAdapter adapter, int errorCode, String errorMessage) {
        }

        public void onBufferingStateChanged(VideoPlayerAdapter adapter, boolean start) {
        }

        public void onMetadataChanged(VideoPlayerAdapter adapter) {
        }
    }

    Callback mCallback;

    public final void setCallback(Callback callback) {
        mCallback = callback;
    }

    public final Callback getCallback() {
        return mCallback;
    }


    public abstract void play();

    public abstract void pause();

    public void next() {
    }

    public void previous() {
    }

    public void fastForward() {
    }

    public void rewind() {
    }

    public void seekTo(long positionInMs) {
    }

    public void setProgressUpdatingEnabled(boolean enable) {
    }

    public void setShuffleAction(int shuffleActionIndex) {
    }

    public void setRepeatAction(int repeatActionIndex) {
    }

    public boolean isPlaying() {
        return false;
    }

    public long getDuration() {
        return 0;
    }

    public long getCurrentPosition() {
        return 0;
    }

    public long getBufferedPosition() {
        return 0;
    }

}
