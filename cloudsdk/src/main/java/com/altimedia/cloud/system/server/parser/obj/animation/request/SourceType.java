/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.server.parser.obj.animation.request;

/**
 * Created by mc.kim on 17,04,2020
 */
public enum SourceType {

    URL("url"), Binary("binary"), FillRect("fillRect"), Invalid("invalid");

    final String name;

    SourceType(String name) {
        this.name = name;
    }

    public static SourceType findByName(String name) {
        SourceType[] types = values();
        for (SourceType type : types) {
            if (type.name.equalsIgnoreCase(name)) {
                return type;
            }
        }
        return Invalid;
    }

    public String getName() {
        return name;
    }
}
