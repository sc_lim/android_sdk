/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.server.parser.obj.animation.request.animation.create;

import android.os.Parcel;

import com.altimedia.cloud.system.server.parser.obj.BaseRequest;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mc.kim on 13,05,2020
 */
public class AnimationCreateData extends BaseRequest {
    @SerializedName("data")
    @Expose
    private com.altimedia.cloud.system.server.parser.obj.animation.request.animation.create.Data data;


    protected AnimationCreateData(Parcel in) {
        super(in);
        this.data = ((com.altimedia.cloud.system.server.parser.obj.animation.request.animation.create.Data)
                in.readValue((com.altimedia.cloud.system.server.parser.obj.animation.request.animation.create.Data.class.getClassLoader())));
    }

    public com.altimedia.cloud.system.server.parser.obj.animation.request.animation.create.Data getData() {
        return data;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeValue(data);
    }

    @Override
    public int describeContents() {
        return super.describeContents();
    }
}