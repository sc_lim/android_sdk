/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.server.parser.obj;

import com.altimedia.cloud.ApexMessage;

public class CsFrameApexMessage extends ApexMessage  {
    String protocolPackage;
    CsFrame csFrame;

    public CsFrameApexMessage(String protocolPackage, CsFrame csFrame) {
        this.protocolPackage = protocolPackage;
        this.csFrame = csFrame;
    }

    public CsFrame getCsFrame() {
        return csFrame;
    }

    @Override
    public String getData() {
        return null;
    }

    @Override
    public String getProtocolMethod() {
        return null;
    }

    @Override
    public String getProtocolPackage() {
        return protocolPackage;
    }

    @Override
    public String getAppID() {
        return null;
    }

    @Override
    public String toString() {
        return "CsFrameApexMessage{" +
                "protocolPackage='" + protocolPackage + '\'' +
                ", csFrame=" + csFrame +
                '}';
    }
}
