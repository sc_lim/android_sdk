/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.server.parser.obj;

/**
 * Created by mc.kim on 13,05,2020
 */
public enum ProtocolPackageType {
    ApexCsFrame("ApexCsFrame"),
    AnimationManager("IAnimation"),
    ApplicationManager("ApplicationManager"),
    Other("Other");

    final String name;

    ProtocolPackageType(String name) {
        this.name = name;
    }

    public String getName(){
        return name;
    }

    public static ProtocolPackageType findByName(String name) {
        ProtocolPackageType[] protocolPackageTypes = values();
        for (ProtocolPackageType type : protocolPackageTypes) {
            if (type.name.equalsIgnoreCase(name)) {
                return type;
            }
        }
        return Other;
    }

    @Override
    public String toString() {
        return "ProtocolPackageType{" +
                "name='" + name + '\'' +
                '}';
    }
}
