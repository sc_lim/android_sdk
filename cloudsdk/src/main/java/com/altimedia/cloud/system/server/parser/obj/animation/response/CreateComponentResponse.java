/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.server.parser.obj.animation.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.altimedia.cloud.system.server.parser.obj.BaseResponse;
import com.altimedia.cloud.system.server.parser.obj.ProtocolType;
import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mc.kim on 10,04,2020
 */
public class CreateComponentResponse extends BaseResponse {

    @Expose
    @SerializedName("data")
    private Data data;

    public CreateComponentResponse(int requestId, ProtocolType protocolType) {
        super(requestId, protocolType);
    }

    protected CreateComponentResponse(Parcel in) {
        super(in);
        data = (Data) in.readValue(Data.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeValue(data);
    }

    @Override
    public int describeContents() {
        return super.describeContents();
    }

    public void setResultData(int objectId, int width, int height) {
        data = new Data(objectId,
                String.valueOf(width), String.valueOf(height));
    }

    public static class Data implements Parcelable {
        @Expose
        @SerializedName("objectId")
        private int objectId;

        @Expose
        @SerializedName("width")
        private String width;

        @Expose
        @SerializedName("height")
        private String height;

        public Data(int objectid, String width, String height) {
            this.objectId = objectid;
            this.width = width;
            this.height = height;
        }

        protected Data(Parcel in) {
            objectId = in.readInt();
            width = in.readString();
            height = in.readString();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(objectId);
            dest.writeString(width);
            dest.writeString(height);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<Data> CREATOR = new Creator<Data>() {
            @Override
            public Data createFromParcel(Parcel in) {
                return new Data(in);
            }

            @Override
            public Data[] newArray(int size) {
                return new Data[size];
            }
        };
    }

    public String getJsonString() {
        return new Gson().toJson(this);
    }
}
