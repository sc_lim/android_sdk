/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.server.parser.obj.application.request;

import android.os.Parcel;
import android.os.Parcelable;

import com.altimedia.cloud.system.server.parser.obj.BaseRequest;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mc.kim on 14,05,2020
 */
public class GetHistoryRequest extends BaseRequest {
    @Expose
    @SerializedName("data")
    private GetHistoryData data;

    protected GetHistoryRequest(Parcel in) {
        super(in);
        data = (GetHistoryData)in.readValue(GetHistoryData.class.getClassLoader());
    }


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest,flags);
        dest.writeValue(data);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<GetHistoryRequest> CREATOR = new Creator<GetHistoryRequest>() {
        @Override
        public GetHistoryRequest createFromParcel(Parcel in) {
            return new GetHistoryRequest(in);
        }

        @Override
        public GetHistoryRequest[] newArray(int size) {
            return new GetHistoryRequest[size];
        }
    };

    public GetHistoryData getData() {
        return data;
    }

    @Override
    public String toString() {
        return "GetHistoryRequest{" +
                "data=" + data +
                '}';
    }

    public String getHistoryKey(){
        if(data == null){
            return "";
        }
        return data.getKey();
    }

    private static class GetHistoryData implements Parcelable {
        @Expose
        @SerializedName("key")
        private String key;

        public String getKey() {
            return key;
        }

        @Override
        public String toString() {
            return "GetHistoryData{" +
                    "key='" + key + '\'' +
                    '}';
        }

        protected GetHistoryData(Parcel in) {
            key = in.readString();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(key);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<GetHistoryData> CREATOR = new Creator<GetHistoryData>() {
            @Override
            public GetHistoryData createFromParcel(Parcel in) {
                return new GetHistoryData(in);
            }

            @Override
            public GetHistoryData[] newArray(int size) {
                return new GetHistoryData[size];
            }
        };
    }
}
