/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.server.dispatch.command;

import com.altimedia.cloud.ApexCloudDoc;
import com.altimedia.cloud.ApexCloudEventListener;
import com.altimedia.cloud.system.server.dispatch.EventCommand;

/**
 * Created by mc.kim on 22,05,2020
 */
public class FailEventCommand implements EventCommand {
    private ApexCloudDoc mApexCloudDoc;

    public void setSource(ApexCloudDoc apexCloudDoc) {
        this.mApexCloudDoc = apexCloudDoc;
    }

    public void doBitCloudCommand(ApexCloudEventListener listener) {
        //To change body of implemented methods use File | Settings | File Templates.
        if (listener != null) {
            //listener.notifyConnectFailEvent(mApexCloudDoc);
        }
    }
}