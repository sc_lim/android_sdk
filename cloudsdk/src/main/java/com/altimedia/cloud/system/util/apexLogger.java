/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.util;

import android.util.Log;

/**
 * Created by IntelliJ IDEA.
 * User: gh.baek
 * Date: 2018. 5. 14
 * To change this template use File | Settings | File Templates.
 */
public class apexLogger {
    public static boolean DEBUG = true;
    public static boolean INFO = true;
    public static boolean ERROR = true;
    public static boolean CACHE = false;
    public static boolean REPAINT = false;
    public static boolean Memory = true;

    public static void out(String s) {
        Log.e("## atl[apexCloud] - ", s);
    }
}
