/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.server.parser.obj.application.request;

import android.os.Parcel;
import android.os.Parcelable;

import com.altimedia.cloud.system.server.parser.obj.BaseRequest;
import com.altimedia.cloud.system.server.parser.obj.application.def.KeyCode;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mc.kim on 13,05,2020
 */
public class SetKeyRequest extends BaseRequest {

    @Expose
    @SerializedName("data")
    private SetKeyData data;


    public SetKeyRequest(Parcel in) {
        super(in);
        data = (SetKeyData) in.readValue(SetKeyData.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeValue(data);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SetKeyRequest> CREATOR = new Creator<SetKeyRequest>() {
        @Override
        public SetKeyRequest createFromParcel(Parcel in) {
            return new SetKeyRequest(in);
        }

        @Override
        public SetKeyRequest[] newArray(int size) {
            return new SetKeyRequest[size];
        }
    };

    private static class SetKeyData implements Parcelable {
        @Expose
        @SerializedName("keySet")
        private KeyCode keySet;

        protected SetKeyData(Parcel in) {
            keySet = (KeyCode) in.readValue(KeyCode.class.getClassLoader());
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeValue(keySet);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<SetKeyData> CREATOR = new Creator<SetKeyData>() {
            @Override
            public SetKeyData createFromParcel(Parcel in) {
                return new SetKeyData(in);
            }

            @Override
            public SetKeyData[] newArray(int size) {
                return new SetKeyData[size];
            }
        };
    }

}
