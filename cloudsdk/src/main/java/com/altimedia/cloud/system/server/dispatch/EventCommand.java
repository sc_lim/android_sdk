/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.server.dispatch;


import com.altimedia.cloud.ApexCloudEventListener;

/**
 * Created by IntelliJ IDEA.
 * User: sungwoo
 * Date: 2014. 12. 3
 * To change this template use File | Settings | File Templates.
 */
public interface EventCommand {
    void doBitCloudCommand(ApexCloudEventListener listener);
}
