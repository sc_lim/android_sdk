/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.server.parser.obj.application.request;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.core.util.Pair;

import com.altimedia.cloud.system.server.parser.obj.BaseRequest;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 * Created by mc.kim on 14,05,2020
 */
public class SetHistoryRequest extends BaseRequest {
    @Expose
    @SerializedName("data")
    private SetHistoryData data;

    protected SetHistoryRequest(Parcel in) {
        super(in);
        data = (SetHistoryData) in.readValue(SetHistoryData.class.getClassLoader());
    }


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeValue(data);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SetHistoryRequest> CREATOR = new Creator<SetHistoryRequest>() {
        @Override
        public SetHistoryRequest createFromParcel(Parcel in) {
            return new SetHistoryRequest(in);
        }

        @Override
        public SetHistoryRequest[] newArray(int size) {
            return new SetHistoryRequest[size];
        }
    };

    public SetHistoryData getData() {
        return data;
    }

    @Override
    public String toString() {
        return "SetHistoryRequest{" +
                "data=" + data +
                '}';
    }

    public Pair<String, String> getHistory() {
        if (data == null) {
            return null;
        }
        return new Pair<>(data.key, data.value);
    }

    private static class SetHistoryData implements Parcelable {
        @Expose
        @SerializedName("key")
        private String key;
        @Expose
        @SerializedName("value")
        private String value;

        public String getKey() {
            return key;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return "SetHistoryData{" +
                    "key='" + key + '\'' +
                    ", value='" + value + '\'' +
                    '}';
        }

        protected SetHistoryData(Parcel in) {
            key = in.readString();
            value = in.readString();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(key);
            dest.writeString(value);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<SetHistoryData> CREATOR = new Creator<SetHistoryData>() {
            @Override
            public SetHistoryData createFromParcel(Parcel in) {
                return new SetHistoryData(in);
            }

            @Override
            public SetHistoryData[] newArray(int size) {
                return new SetHistoryData[size];
            }
        };
    }
}
