
/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.server.parser.obj.animation.request.component.attach;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DestComponent implements Parcelable {

    @SerializedName("objectId")
    @Expose
    private int objectId;

    @SerializedName("dx1")
    @Expose
    private int left;
    @SerializedName("dy1")
    @Expose
    private int top;
    @SerializedName("dx2")
    @Expose
    private int right;
    @SerializedName("dy2")
    @Expose
    private int bottom;

    @SerializedName("dz")
    @Expose
    private int zOrder;

    public final static Creator<DestComponent> CREATOR = new Creator<DestComponent>() {


        @SuppressWarnings({
                "unchecked"
        })
        public DestComponent createFromParcel(Parcel in) {
            return new DestComponent(in);
        }

        public DestComponent[] newArray(int size) {
            return (new DestComponent[size]);
        }

    };

    protected DestComponent(Parcel in) {
        this.objectId = in.readInt();
        this.left = in.readInt();
        this.top = in.readInt();
        this.right = in.readInt();
        this.bottom = in.readInt();
        this.zOrder = in.readInt();
    }

    public DestComponent() {
    }

    public int getRight() {
        return right;
    }

    public int getBottom() {
        return bottom;
    }

    public int getWidth() {
        return right - left;
    }

    public int getHeight() {
        return bottom - top;
    }

    public int getObjectid() {
        return objectId;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(objectId);
        dest.writeValue(left);
        dest.writeValue(top);
        dest.writeValue(right);
        dest.writeValue(bottom);
        dest.writeValue(zOrder);
    }

    public int describeContents() {
        return 0;
    }

    public int getLeft() {
        return left;
    }

    public int getTop() {
        return top;
    }

    public int getzOrder() {
        return zOrder;
    }

    @Override
    public String toString() {
        return "DestComponent{" +
                "objectId='" + objectId + '\'' +
                ", left=" + left +
                ", top=" + top +
                ", width=" + getWidth() +
                ", height=" + getHeight() +
                ", zOrder=" + zOrder +
                '}';
    }
}
