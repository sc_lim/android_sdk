/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.util;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by IntelliJ IDEA.
 * User: sungwoo
 * Date: 2014. 12. 1
 * To change this template use File | Settings | File Templates.
 */
public class SocketReader {
    private InputStream is = null;

    public byte[] read(byte[] b, int offset, int len) throws Exception {

        int readNum = 0;
        int remainSize = len - offset;

        while (remainSize > 0) {

            if ((readNum = is.read(b, len - remainSize, remainSize)) < 0) {
                throw new IOException();
            }

            remainSize -= readNum;
        }

        return b;
    }

    public byte[] copyBytes(int len) throws Exception {

        byte[] buffer = new byte[len];

        read(buffer, 0, len);

        byte[] result = new byte[len];
        System.arraycopy(buffer, 0, result, 0, len);

        return result;
    }

    public int readBytes(int len) throws Exception {

        int value = 0;
        byte[] buffer = new byte[len];

        read(buffer, 0, len);

        for (int i = 0; i < len; i++) {

            int shiftCnt = (len - i) - 1;
            value += ((buffer[i] & 0xFF) << (8 * shiftCnt));
        }

        return value;
    }

    public void setInputStream(InputStream is) {
        this.is = is;
    }

    public void disconnect() {
        if (is != null) {

            try {
                is.close();
            } catch (IOException e) {
                if (apexLogger.ERROR) apexLogger.out(" SocketReader.disconnect -----------> " + e.getMessage());
            }

            is = null;
        } else {
            if (apexLogger.ERROR) apexLogger.out(" SocketReader.disconnect -----------> stream is null !!!");
        }

        if(apexLogger.INFO) apexLogger.out("[5] SocketReader.disconnect is STOP !!!");
    }

    public void destroy() {
        disconnect();
    }
}
