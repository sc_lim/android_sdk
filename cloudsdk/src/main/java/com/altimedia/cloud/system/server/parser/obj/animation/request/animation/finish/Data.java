
/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.server.parser.obj.animation.request.animation.finish;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data implements Parcelable {

    @SerializedName("animateId")
    @Expose
    private String animateId;
    public final static Creator<Data> CREATOR = new Creator<Data>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Data createFromParcel(Parcel in) {
            return new Data(in);
        }

        public Data[] newArray(int size) {
            return (new Data[size]);
        }

    };

    protected Data(Parcel in) {
        this.animateId = ((String) in.readValue((String.class.getClassLoader())));
    }

    public Data() {
    }

    public int getAnimateId() {
        if(animateId == null || animateId.isEmpty()){
            return -1;
        }
        return Integer.parseInt(animateId);
    }

    public void setAnimateId(String animateId) {
        this.animateId = animateId;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(animateId);
    }

    public int describeContents() {
        return 0;
    }

}
