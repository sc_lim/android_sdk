/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.server.parser.obj;

import com.altimedia.cloud.system.server.parser.obj.animation.request.animation.close.AnimationCloseData;
import com.altimedia.cloud.system.server.parser.obj.animation.request.animation.create.AnimationCreateData;
import com.altimedia.cloud.system.server.parser.obj.animation.request.animation.finish.AnimationFinishData;
import com.altimedia.cloud.system.server.parser.obj.animation.request.component.attach.ComponentAttachData;
import com.altimedia.cloud.system.server.parser.obj.animation.request.component.create.CreateComponentData;
import com.altimedia.cloud.system.server.parser.obj.animation.request.component.delete.ComponentDeleteData;
import com.altimedia.cloud.system.server.parser.obj.animation.request.component.detach.ComponentDetachData;
import com.altimedia.cloud.system.server.parser.obj.animation.request.component.list.ComponentCreates;
import com.altimedia.cloud.system.server.parser.obj.animation.request.component.rect.FillRectData;
import com.altimedia.cloud.system.server.parser.obj.animation.request.component.visible.ComponentVisibleData;
import com.altimedia.cloud.system.server.parser.obj.application.request.GetHistoryRequest;
import com.altimedia.cloud.system.server.parser.obj.application.request.SetHistoryRequest;

/**
 * Created by mc.kim on 09,04,2020
 */
public enum ProtocolType {
    createComponent("IAnimation.createComponent", CreateComponentData.class),
    attachComponent("IAnimation.attachComponent", ComponentAttachData.class),
    detachComponent("IAnimation.detachComponent", ComponentDetachData.class),
    deleteComponent("IAnimation.deleteComponent", ComponentDeleteData.class),
    setText("IAnimation.setText", BaseRequest.class),
    fillRect("IAnimation.fillRect", FillRectData.class),
    animate("IAnimation.animate", AnimationCreateData.class),
    setVisible("IAnimation.setVisible", ComponentVisibleData.class),
    finishAnimation("IAnimation.finishAnimation", AnimationFinishData.class),
    closeAnimation("IAnimation.closeAnimation", AnimationCloseData.class),
    createComponents("IAnimation.createComponents", ComponentCreates.class),

    deviceManagerGetDeviceInfo("DeviceManager.getDeviceInfo", BaseRequest.class),
    deviceManagerGetDeviceStatus("DeviceManager.getDeviceStatus", BaseRequest.class),

    //TODO: history 관련 protocol 은 IHistory 로 package 변경 예정
    applicationManagerSetHistory("ApplicationManager.setHistory", SetHistoryRequest.class),
    applicationManagerGetHistory("ApplicationManager.getHistory", GetHistoryRequest.class),

    other("other", BaseRequest.class);

    private final String name;
    private final Class aClass;

    ProtocolType(String name, Class aClass) {
        this.name = name;
        this.aClass = aClass;
    }

    public static ProtocolType findByName(String name) {
        ProtocolType[] types = values();
        for (ProtocolType type : types) {
            if (type.name.equalsIgnoreCase(name)) {
                return type;
            }
        }
        return other;
    }

    public Class getaClass() {
        return aClass;
    }

    public String getName() {
        return name;
    }
}
