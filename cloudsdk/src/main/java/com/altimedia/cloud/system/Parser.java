/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system;

import com.altimedia.cloud.system.server.dispatch.EventCommand;
import com.altimedia.cloud.system.server.dispatch.EventDispatcher;

import java.nio.ByteBuffer;

/**
 * Created by IntelliJ IDEA.
 * User: gh.baek
 * Date: 2018. 5. 30
 * To change this template use File | Settings | File Templates.
 */
public interface Parser extends EventCommand {

    public void initialize();

    public void destroy();

    public void parseData(byte[] data) throws Exception;

    public void parseData(String data) throws Exception;

    public void parseData(ByteBuffer data, EventDispatcher dispatcher) throws Exception;
}
