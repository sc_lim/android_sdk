/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.util.soundPool;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;

public class SoundPoolUtil {

    private final int DEFAULT_VOLUM = 1;
    private final float DEFAULT_RATE = 1;
    private final int NO_LOOP = 0;
    private final int LOOP = -1;
    private final int PRIORITY_SOUND_POOL = 1;
    SoundPool mSoundPool = new SoundPool(1, AudioManager.STREAM_NOTIFICATION, 1);
    private Context mContext = null;
    private int soundId = 0;

    public SoundPoolUtil(Context context, int rawId) {
        this.mContext = context;
        soundId = mSoundPool.load(context, rawId, PRIORITY_SOUND_POOL);
    }

    public void playSoundPool(int rawId) {
        this.playSoundPool(rawId, DEFAULT_VOLUM);
    }

    public void playSoundPool(int rawId, int volume) {
        this.playSoundPool(rawId, volume, DEFAULT_RATE);

    }

    public void playSoundPool(int rawId, int volume, float rate) {
//        final int soundID = mSoundPool.load(mContext, rawId, PRIORITY_SOUND_POOL);
        mSoundPool.play(soundId, volume, volume, PRIORITY_SOUND_POOL, NO_LOOP, rate);
    }

    public void release() {
        if (mSoundPool != null) {
            mSoundPool.release();
            mSoundPool = null;
        }
    }

//    private SoundPlayerAdapter soundPlayerAdapter = null;
//
//    public void playMusic(final Uri uri) {
//        if (soundPlayerAdapter == null) {
//            soundPlayerAdapter = new SoundPlayerAdapter(mContext);
//            soundPlayerAdapter.setCallback(new VideoPlayerAdapter.Callback());
//        }
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                soundPlayerAdapter.setDataSource(uri);
//            }
//        }).start();
//    }
//
//    public void releaseMusic() {
//        if (soundPlayerAdapter == null) {
//            return;
//        }
//        soundPlayerAdapter.release();
//        soundPlayerAdapter = null;
//    }

}
