/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.server.parser.obj.animation.request.component.create;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mc.kim on 09,04,2020
 */
public class RectData implements Parcelable {
    @Expose
    @SerializedName("width")
    private int width;
    @Expose
    @SerializedName("height")
    private int height;

    protected RectData(Parcel in) {
        width = in.readInt();
        height = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(width);
        dest.writeInt(height);
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<RectData> CREATOR = new Creator<RectData>() {
        @Override
        public RectData createFromParcel(Parcel in) {
            return new RectData(in);
        }

        @Override
        public RectData[] newArray(int size) {
            return new RectData[size];
        }
    };

    @Override
    public String toString() {
        return "RectData{" +
                "width=" + width +
                ", height=" + height +
                '}';
    }
}
