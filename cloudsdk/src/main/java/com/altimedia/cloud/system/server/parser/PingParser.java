/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.server.parser;

import com.altimedia.cloud.ApexCloudEventListener;
import com.altimedia.cloud.system.ParserImpl;
import com.altimedia.cloud.system.server.dispatch.EventDispatcher;

/**
 * Created by IntelliJ IDEA.
 * User: sungwoo
 * Date: 2014. 12. 7
 * To change this template use File | Settings | File Templates.
 */
class PingParser extends ParserImpl {
    protected void cleanup() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void initialize() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void parse(EventDispatcher dispatcher) throws Exception {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void doBitCloudCommand(ApexCloudEventListener listener) {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
