/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.server;


import android.util.Log;

import com.altimedia.cloud.ApexCloudDoc;
import com.altimedia.cloud.system.server.dispatch.EventDispatcher;
import com.altimedia.cloud.system.server.websocket.WebSocketHandler;
import com.altimedia.cloud.system.util.StatusConnect;
import com.altimedia.cloud.system.util.apexLogger;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;

/**
 * Created by IntelliJ IDEA.
 * User: gh.baek
 * Date: 2018. 5. 14
 * To change this template use File | Settings | File Templates.
 */
public class ServerConnector {

    private WebSocketHandler wSocket;

    private Writer writer;
    private PingThread pingThread;

    private ApexCloudDoc apexCloudDoc;

    private String appId;

    public ServerConnector(String appId) {
        this.appId = appId;
    }

    public void setApexCloudDoc(ApexCloudDoc apexCloudDoc) {
        this.apexCloudDoc = null;
        this.apexCloudDoc = apexCloudDoc;
    }

    public ApexCloudDoc getApexCloudDoc() {
        return apexCloudDoc;
    }

    public String getAppId() {
        return appId;
    }

    public synchronized boolean connect(EventDispatcher dispatcher) throws URISyntaxException {
        //if (apexLogger.DEBUG) apexLogger.out("ServerConnector.connect      ip " + ip + "   port " + port);


        int timeout = apexCloudDoc.getServerConnectTimeOut();
        String web_url = apexCloudDoc.getURL();
        if (apexLogger.DEBUG) {
            Log.d("TAG", "web_url : " + web_url);
        }
        int imageFormat = apexCloudDoc.getEncodingType();
        String mac_addr = apexCloudDoc.getDeviceID();
        int width = apexCloudDoc.getWidth();
        int height = apexCloudDoc.getHeight();
        String Option = apexCloudDoc.getOption();

        String ip = apexCloudDoc.getCloudIP();
        int port = apexCloudDoc.getCloudPort();

        /**
         * update url
         * ws://host:port/v1/req?sender=stb&url=http://www.naver.com&encode=png
         * ws://host:port/v1/req?sender=stb&imageSize=960x540&url=http://www.naver.com&imageFormat=5&mac=%s
         */

        String enc_url = null;
        String enc_id = null;

        if (apexLogger.DEBUG) {
            Log.d("TAG", "connect web_url: " + web_url);
        }

        try {
            if (web_url != null)
                enc_url = URLEncoder.encode(web_url, "utf-8");
            if (mac_addr != null)
                enc_id = URLEncoder.encode(mac_addr, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String uri = String.format("ws://%s:%d/v1/req?sender=stb&imageSize=%dx%d&url=%s&imageFormat=%d&mac=%s", ip, port, width, height, enc_url == null? "" : enc_url, imageFormat, enc_id);
        try {
            String temp = null;
            int i = 0;
            if (Option != null) {
                String[] Params = Option.split("&");
                for (String param : Params) {
                    String name = param.split("=")[0];
                    String value = URLEncoder.encode(param.split("=")[1], "utf-8");
                    if (i > 0)
                        temp += String.format("&%s=%s", name, value);
                    else
                        temp = String.format("&%s=%s", name, value);
                    i++;
                }

                uri += temp;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (apexLogger.INFO) apexLogger.out("URI : " + uri);
        //wSocket = new WebSocketHandler(dispatcher, new URI("ws://demos.kaazing.com/echo") );
        StatusConnect.getInstance().setNoErrorAlarm(true);
        wSocket = new WebSocketHandler(dispatcher, this, new URI(uri), apexCloudDoc, timeout);

        try {
            if (!wSocket.connectBlocking()) {
                disconnect();
                //Thread.sleep(1000);

                if (wSocket == null && StatusConnect.getInstance().getTryReconnect()) {
                    StatusConnect.getInstance().setNoErrorAlarm(false);
                    if (apexLogger.ERROR) {
                        apexLogger.out("Connect is failed. client try to reconnect...");
                    }
                    wSocket = new WebSocketHandler(dispatcher, this, new URI(uri), apexCloudDoc, timeout);
                    if (!wSocket.connectBlocking()) {
                        if (apexLogger.ERROR) {
                            apexLogger.out("The second connection also failed. please check the status of the cloud server.");
                        }
                        return false;
                    }
                    StatusConnect.getInstance().setTryReconnect(false);
                } else
                    return false;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        StatusConnect.getInstance().setNoErrorAlarm(false);

        if (apexLogger.INFO) apexLogger.out("INFO : URI return = " + wSocket.getURI());
        if (apexLogger.INFO) apexLogger.out("INFO : URI length = " + uri.length());

        writer = new Writer(dispatcher);
        writer.setOutputSocket(wSocket.getConnection());

        return true;
    }

    public void startPing(EventDispatcher eventDispatcher, int pingPeriod, int connectTimeout) {
        pingThread = new PingThread(eventDispatcher);
        pingThread.startPing(this, pingPeriod, connectTimeout);
    }


    public boolean isAlive() {
        if (writer == null) {
            return false;
        }
        return writer.isAlive();
        //return wSocket != null;
    }

    public void requestURL(String url) throws Exception {
        if (url == null) {
            throw new NullPointerException(" URL is NULL !!!!");
        }

        writer.sendRequestURL(url);
    }

    public void requestFrame(int incremental) throws Exception {
        int x = apexCloudDoc.getXPos();
        int y = apexCloudDoc.getYPos();
        int width = apexCloudDoc.getWidth();
        int height = apexCloudDoc.getHeight();

        writer.requestFrame(incremental, x, y, width, height);
    }


    public void sendWeb(String text) throws Exception {
        writer.sendWeb(text);
    }

    public void sendBinary(byte[] buffer) throws Exception {
        writer.sendBinary(buffer);
    }

    public void sendPing() throws Exception {
        writer.sendPing();
    }

    public void sendDisconnect() throws Exception {
        writer.sendQuit();
    }

    public void sendMouseEvent(int type, int button, int x, int y) throws Exception {
        writer.sendMouseEvent(type, button, x, y);
    }

    public void sendKeyEvent(short key, boolean type) throws Exception {
        writer.sendKeyEvent(key, type);
    }

    /*
        public void sendReleaseKeyEvent(int key, char c) throws Exception {
            writer.sendReleaseKeyEvent(key, c);
        }
    */
    public synchronized void disconnect() throws IOException {
        if (apexLogger.INFO) apexLogger.out("[1] ServerConnector.disconnect ----------- START !!!");

        if (pingThread != null) {
            pingThread.stopPing();
            pingThread = null;
        }
/*
        if(reader != null) {
            reader.close();
            reader = null;
        }
*/
        if (writer != null) {
            writer.destroy();
            writer = null;
        }

        if (wSocket != null) {
            wSocket.close();
            //wSocket.closeConnection( 1000, null );
            //wSocket.onWebsocketClose(wSocket.getConnection(), 1000, null, false );
            wSocket = null;
        }

        if (apexLogger.INFO) apexLogger.out("[1] ServerConnector.disconnect ----------- END !!!");
    }

    public void close() throws IOException {
        disconnect();

        this.apexCloudDoc = null;

        if (apexLogger.INFO) apexLogger.out("[6] ServerConnector.close ----------- END !!!");
    }
}
