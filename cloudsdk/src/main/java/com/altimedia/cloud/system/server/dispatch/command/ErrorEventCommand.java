/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.server.dispatch.command;

import com.altimedia.cloud.ApexCloudEventListener;
import com.altimedia.cloud.constant.ProtocolConstant;
import com.altimedia.cloud.system.server.dispatch.EventCommand;

/**
 * Created by IntelliJ IDEA.
 * User: sungwoo
 * Date: 2014. 12. 9
 * To change this template use File | Settings | File Templates.
 */
public class ErrorEventCommand implements EventCommand {
    private int errorCode;
    private String errorMsg = null;

    public void setError(int code, String msg) {
        errorCode = code;
        errorMsg = msg;
    }

    public void doBitCloudCommand(ApexCloudEventListener listener) {
        //To change body of implemented methods use File | Settings | File Templates.
        if(listener != null) {

            if( errorCode == ProtocolConstant.NOTIFYING_MESSAGE_CONNECT )
                listener.notifyConnected();
            else
                listener.notifyErrorEvent(errorCode, errorMsg);
        }
    }
}
