/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.server.parser.util;

import com.altimedia.cloud.ApexMessage;
import com.altimedia.cloud.system.server.parser.obj.ComponentType;
import com.altimedia.cloud.system.server.parser.obj.ComponentTypeDeserializer;
import com.altimedia.cloud.system.server.parser.obj.ProtocolType;
import com.altimedia.cloud.system.server.parser.obj.ProtocolTypeDeserializer;
import com.altimedia.cloud.system.server.parser.obj.WebMessageDeserializer;
import com.altimedia.cloud.system.server.parser.obj.animation.request.ActionDeserializer;
import com.altimedia.cloud.system.server.parser.obj.animation.request.InterpolatorTypeDeserializer;
import com.altimedia.cloud.system.server.parser.obj.animation.request.SourceType;
import com.altimedia.cloud.system.server.parser.obj.animation.request.SourceTypeDeserializer;
import com.altimedia.cloud.system.server.parser.obj.animation.request.animation.Action;
import com.altimedia.cloud.system.server.parser.obj.animation.request.animation.InterpolatorType;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by mc.kim on 10,04,2020
 */
public class JsonUtil {

    public static Gson newGson() {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(ProtocolType.class, new ProtocolTypeDeserializer());
        builder.registerTypeAdapter(SourceType.class, new SourceTypeDeserializer());
        builder.registerTypeAdapter(Action.class, new ActionDeserializer());
        builder.registerTypeAdapter(ApexMessage.class, new WebMessageDeserializer());
        builder.registerTypeAdapter(InterpolatorType.class, new InterpolatorTypeDeserializer());
        builder.registerTypeAdapter(ComponentType.class, new ComponentTypeDeserializer());
        return builder.create();
    }

    public static Gson baseGson() {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(ProtocolType.class, new ProtocolTypeDeserializer());
        builder.registerTypeAdapter(SourceType.class, new SourceTypeDeserializer());
        builder.registerTypeAdapter(Action.class, new ActionDeserializer());
        builder.registerTypeAdapter(InterpolatorType.class, new InterpolatorTypeDeserializer());
        builder.registerTypeAdapter(ComponentType.class, new ComponentTypeDeserializer());
        return builder.create();
    }
}
