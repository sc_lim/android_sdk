/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.server.parser.obj.animation.request.component.delete;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mc.kim on 18,04,2020
 */
public class RemoveData implements Parcelable {

    @SerializedName("targetId")
    @Expose
    private String targetId;
    @SerializedName("targetVisible")
    @Expose
    private String targetVisible;
    public final static Creator<Data> CREATOR = new Creator<Data>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Data createFromParcel(Parcel in) {
            return new Data(in);
        }

        public Data[] newArray(int size) {
            return (new Data[size]);
        }

    };

    protected RemoveData(Parcel in) {
        this.targetId = ((String) in.readValue((String.class.getClassLoader())));
        this.targetVisible = ((String) in.readValue((String.class.getClassLoader())));
    }

    public RemoveData() {
    }

    public int getObjectid() {
        try {
            return Integer.parseInt(targetId);
        }catch (Exception e){
            e.printStackTrace();
            return -1;
        }
    }
    public boolean isTargetVisible() {
        if (this.targetVisible == null) {
            return true;
        }
        return this.targetVisible.equalsIgnoreCase("true");
    }


    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(targetId);
    }

    public int describeContents() {
        return 0;
    }

}
