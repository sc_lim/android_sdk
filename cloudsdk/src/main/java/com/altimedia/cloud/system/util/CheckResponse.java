/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.util;

public class CheckResponse {
    private static final CheckResponse ourInstance = new CheckResponse();

    public static CheckResponse getInstance() {
        return ourInstance;
    }

    private boolean response = true;
    private boolean isConnecting = false;

    private CheckResponse(){

    }

    public boolean setResponse(boolean enable) {
        this.response = enable;
        return this.response;
    }

    public boolean getResponse() {
        return this.response;
    }

    public boolean setPingCheck(boolean connecting) {
        this.isConnecting = connecting;
        return this.isConnecting;
    }

    public boolean getPingCheck() {
        return this.isConnecting;
    }
}
