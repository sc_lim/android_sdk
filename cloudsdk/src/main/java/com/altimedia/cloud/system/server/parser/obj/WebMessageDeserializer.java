/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.server.parser.obj;

import com.altimedia.cloud.ApexMessage;
import com.altimedia.cloud.impl.ApexMessageImpl;
import com.altimedia.cloud.system.server.parser.util.JsonUtil;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

/**
 * Created by mc.kim on 09,04,2020
 */
public class WebMessageDeserializer implements JsonDeserializer<ApexMessage> {
    private final String TAG = WebMessageDeserializer.class.getSimpleName();
    private final String REQEUST_ID = "requestId";
    private final String TYPE = "protocolType";
    private final String DATA = "data";

    @Override
    public ApexMessage deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        String jsonString = json.toString();

        String protocolType = json.getAsJsonObject().get(TYPE).getAsString();
        int requestId = json.getAsJsonObject().get(REQEUST_ID).getAsInt();
        ProtocolType type = ProtocolType.findByName(protocolType);

        ApexMessage apexMessage = null;
        if (type == ProtocolType.other) {
            final String data = json.getAsJsonObject().get(DATA).toString();
            apexMessage = new ApexMessageImpl(requestId,protocolType,data);
        } else {
            BaseRequest baseRequest = (BaseRequest) JsonUtil.baseGson().fromJson(jsonString, type.getaClass());
            apexMessage = new InnerApexMessage(baseRequest);
        }
        return apexMessage;
    }
}