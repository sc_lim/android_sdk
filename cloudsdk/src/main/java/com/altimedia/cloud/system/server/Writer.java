/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.server;


import com.altimedia.cloud.constant.ErrorConstant;
import com.altimedia.cloud.constant.ProtocolConstant;
import com.altimedia.cloud.system.server.dispatch.EventDispatcher;
import com.altimedia.cloud.system.util.ByteSwapper;
import com.altimedia.cloud.system.util.ByteWriter;
import com.altimedia.cloud.system.util.CheckResponse;
import com.altimedia.cloud.system.util.apexLogger;

import org.java_websocket.WebSocket;

/**
 * Created by IntelliJ IDEA.
 * User: gh.baek
 * Date: 2018. 5. 15
 * To change this template use File | Settings | File Templates.
 */
class Writer {
    private WebSocket conn;
    private EventDispatcher dispatcher;

    private ByteWriter bw = new ByteWriter();
    private ByteSwapper swapper;

    //private byte[] buffer = new byte[1024];
    private byte[] buffer;

    public Writer(EventDispatcher dispatcher) {
        this.dispatcher = dispatcher;
    }

    public void setOutputSocket(WebSocket conn) {
        this.conn = conn;
    }


    private void writeData(String b) throws Exception {
        try {
            if (conn.isOpen()) {
                conn.send(b);
                if (apexLogger.ERROR) {
                    apexLogger.out("Writer. send data!!!");
                }

                if (!CheckResponse.getInstance().getPingCheck()) {
                    if (dispatcher != null)
                        dispatcher.notifyErrorEvent(ErrorConstant.ERR_DECTECTED_DISCONNECTION_RECV, "[Warning] client didn't get a response from the cloud server.");
                }
            }

        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.

            dispatcher.notifyErrorEvent(ErrorConstant.ERR_DECTECTED_DISCONNECTION_SEND, "client SDK : Data transmission failure.");
        }

        //if (apexLogger.INFO) apexLogger.out( "Writer.data  b : " + byteArrayToHex(b) );
    }
    private void writeData(byte[] b) throws Exception {
        try {
            if (conn.isOpen()) {
                conn.send(b);
                if(apexLogger.ERROR){
                    apexLogger.out("Writer. send data!!!");
                }

                if (!CheckResponse.getInstance().getPingCheck()) {
//                    if (dispatcher != null)
//                        dispatcher.notifyErrorEvent(ErrorConstant.ERR_DECTECTED_DISCONNECTION_RECV, "[Warning] client didn't get a response from the cloud server.");
                }
            }

        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.

            dispatcher.notifyErrorEvent(ErrorConstant.ERR_DECTECTED_DISCONNECTION_SEND, "client SDK : Data transmission failure.");
        }

        //if (apexLogger.INFO) apexLogger.out( "Writer.data  b : " + byteArrayToHex(b) );
    }

    String byteArrayToHex(byte[] a) {
        StringBuilder sb = new StringBuilder();
        for (final byte b : a)
            sb.append(String.format("%02x ", b & 0xff));
        return sb.toString();
    }

    private void writeTag(int command) {
        bw.setBuffer(buffer);
        bw.writeValue(1, command);
    }

    /**
     * data 사이즈를 구하기 - 전체
     */
    private void writeDataLength() {
        bw.writeValueAtPosition(4, 4, bw.getPos() - 8);
    }


    public void sendRequestURL(String url) throws Exception {

        byte[] urlBytes = url.getBytes();

        writeTag(ProtocolConstant.COMMAND_REQUEST_URL);

        bw.writeValue(2, ProtocolConstant.REQUEST_URL);
        bw.writeValue(2, urlBytes.length);
        bw.writeBytes(urlBytes);

        writeDataLength();
        //writeData(buffer, 0, bw.getPos());

        if (apexLogger.DEBUG) apexLogger.out("Writer.sendRequestURL  --------- url " + url);
        if (apexLogger.INFO) apexLogger.out("Writer.sendRequestURL  done !!!!");
    }

    public void sendKeyEvent(short keycode, boolean keydown) throws Exception {

        buffer = new byte[8];
        writeTag(ProtocolConstant.MESSAGE_TYPE_KEY); // messageType

        //bw.writeValue( 1, 0x21 );
        bw.writeValue(1, 0x02);  // deviceType
        bw.writeValue(1, 0x00);  // devideId;

        byte eventType = 0x01;
        if (!keydown) eventType = 0x02;

        bw.writeValue(1, eventType);    //eventType key Down/up
        //bw.writeValue( 2, swapper.swap(keycode) );
        bw.writeValue(2, keycode);
        bw.writeValue(2, 0);      // fieldLength

        writeData(buffer);

        if (apexLogger.DEBUG)
            apexLogger.out("Writer.sendPressKeyEvent  --------- keycode " + keycode+", keyDown : "+keydown);
        //if(apexLogger.INFO) apexLogger.out("Writer.sendPressKeyEvent  done !!!!");
    }

    public void sendMouseEvent(int type, int button, int x, int y) throws Exception {

        buffer = new byte[20];
        writeTag(ProtocolConstant.MESSAGE_TYPE_KEY);

        bw.writeValue(1, 0x03);  // deviceType - MOU
        bw.writeValue(1, 0x00);  // devideId;
        bw.writeValue(1, type);        //eventType key Down/up
/*
        bw.writeValue( 2, swapper.swap((short)button) );        // eventCode   LEFT=0x01, RIGHT=0x02, MIDDLE=0x03 / VERT=0x01, HORZ=0x02
        bw.writeValue( 2, swapper.swap((short)12) );      // fieldLength
        bw.writeValue( 4, swapper.swap(x) );
        bw.writeValue( 4, swapper.swap(y) );
*/
        bw.writeValue(2, (short) button);        // eventCode   LEFT=0x01, RIGHT=0x02, MIDDLE=0x03 / VERT=0x01, HORZ=0x02
        bw.writeValue(2, (short) 12);      // fieldLength
        bw.writeValue(4, x);
        bw.writeValue(4, y);
        bw.writeValue(4, (int) 0.0f);    // delta


        writeData(buffer);

        if (apexLogger.DEBUG) apexLogger.out("Writer.sendMouseEvent  done !!!!");
    }

    public void requestFrame(int incremental, int x, int y, int w, int h) throws Exception {

        bw.setBuffer(buffer);

        writeTag(ProtocolConstant.COMMAND_REQUEST_FRAME);

        bw.writeValue(2, ProtocolConstant.INCREMENTAL_UPDATE_FLAG);
        bw.writeValue(2, 2);
        bw.writeValue(2, incremental);

        bw.writeValue(2, ProtocolConstant.UPDATE_RECT);
        bw.writeValue(2, 8);
        bw.writeValue(2, x);
        bw.writeValue(2, y);
        bw.writeValue(2, w);
        bw.writeValue(2, h);

        writeDataLength();
        //writeData(buffer, 0, bw.getPos());

        if (apexLogger.INFO) apexLogger.out("Writer.requestFrame  done !!!!");
    }

    public boolean isAlive() {
        return conn.isOpen();
    }






    public void sendWeb(String text) throws Exception {

//        conn.send(text);

        writeData(text);
//        byte[] msgBytes = text.getBytes();
//        int pkSize = 6 + msgBytes.length;
//
//        buffer = new byte[pkSize];
//
//        writeTag(ProtocolConstant.MESSAGE_TYPE_MSG); // messageType
//
//        bw.writeValue(1, 0x00);         // Reserved field
//        //bw.writeValue( 4, swapper.swap(msgBytes.length) );    // message length
//        bw.writeValue(4, msgBytes.length);    // message length
//        bw.writeBytes(msgBytes);
//
//        writeData(buffer);
//
//        if (apexLogger.DEBUG)
//            apexLogger.out("Writer.sendWeb  --------- length : " + text.length() + ", text [" + text + "]");
//        if (apexLogger.INFO) apexLogger.out("Writer.sendWeb  done !!!!");
    }

    public void sendBinary(byte[] data) throws Exception {

        int pkSize = 6 + data.length;
        buffer = new byte[pkSize];

        writeTag(ProtocolConstant.MESSAGE_TYPE_DATA); // messageType

        bw.writeValue(1, 0x00);         // Reserved field
        //bw.writeValue( 4, swapper.swap(data.length) );    // message length
        bw.writeValue(4, data.length);    // message length
        bw.writeBytes(data);

        writeData(buffer);

        if (apexLogger.DEBUG)
            apexLogger.out("Writer.sendBinary  --------- length : " + data.length);
        if (apexLogger.INFO) apexLogger.out("Writer.sendBinary  done !!!!");
    }


    public void sendPing() throws Exception {

        if (conn.isOpen()) {
            conn.sendPing();

            if (apexLogger.INFO) apexLogger.out("Writer.sendPing  done !!!!");
        }
    }

    public void sendQuit() throws Exception {

        writeTag(ProtocolConstant.COMMAND_QUIT);

        writeDataLength();
        //writeData(buffer, 0, bw.getPos());

        if (apexLogger.INFO) apexLogger.out("Writer.sendQuit  done !!!!");
    }

    public void destroy() {

        //disconnect();

        if (bw != null) {
            bw.destroy();
            bw = null;
        }

        buffer = null;

        if (apexLogger.INFO) apexLogger.out("[4] Writer.destroy is STOP !!!");

    }
}
