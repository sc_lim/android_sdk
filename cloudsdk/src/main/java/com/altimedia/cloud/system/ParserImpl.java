/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system;

import com.altimedia.cloud.system.server.ServerConnector;
import com.altimedia.cloud.system.server.dispatch.EventDispatcher;
import com.altimedia.cloud.system.util.apexLogger;

import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * Created by IntelliJ IDEA.
 * User: gh.baek
 * Date: 2018. 5. 30
 * To change this template use File | Settings | File Templates.
 */
public abstract class ParserImpl implements Parser {
    protected ServerConnector serverConnector;

    protected int position;
    protected int dataLength;
    protected byte[] dataBytes;
    protected ByteBuffer databuffer;
    protected String message;

    public void setServerConnector(ServerConnector serverConnector) {
        this.serverConnector = serverConnector;
    }

    public void parseData(byte[] data) throws Exception {
        dataBytes = data;
        dataLength = data == null ? 0 : data.length;
        position = 0;

        parse(null);
    }

    public void parseData(ByteBuffer data, EventDispatcher dispatcher) throws Exception {
        databuffer = data;
        position = 0;

        parse(dispatcher);
    }

    public void parseData(String data) throws Exception {
        message = data;
        parse(null);
    }


    protected int getCapacity() throws Exception {
        return databuffer.capacity();
    }

    protected int getPosition() throws Exception {
        return databuffer.position();
    }

    protected int getByte() throws Exception {
        return databuffer.get();
    }

    protected int getShort() throws Exception {
        return databuffer.getShort();
    }

    protected int getInt() throws Exception {
        return databuffer.getInt();
    }

    protected String getMessage() throws Exception {
        return message;
    }

    protected int getArray(byte[] image, int len) throws Exception {
        //if (apexLogger.INFO) apexLogger.out("ApexCloud got data --------------------- " + databuffer + ", len : " + len);

        //image = databuffer.array();
        databuffer.get(image, 0, len);

        //if(databuffer.hasArray())
        {
            //System.arraycopy(databuffer.array(), 0, image, 0, len);
        }
        //if (apexLogger.INFO) apexLogger.out("ApexCloud got data --------------------- " + databuffer);

        return len;
    }

    protected boolean hasElement() {

        return position < dataLength;
    }

    protected int nextElementID() throws Exception {
        int id = readBytes(2);

        if (apexLogger.DEBUG)
            apexLogger.out("ParserImpl.nextElementID -----------------  id 0x0" + Integer.toHexString(id));
        return id;
    }

    protected int nextElementValue() throws Exception {
        int len = readBytes(2);
        return readBytes(len);
    }


    protected byte[] nextElementByte(int len) throws Exception {
        readBytes(2);

        byte[] bytes = new byte[len];

        System.arraycopy(dataBytes, position, bytes, 0, len);

        position += len;

        return bytes;
    }

    protected int[] nextElementSizeValue() throws Exception {
        int len = readBytes(2);
        return readSizeBytes(len);
    }

    protected int[] nextElementRectangleValue() throws Exception {
        int len = readBytes(2);
        return readRectangleBytes(len);
    }

    protected String nextElementStringValue() throws Exception {
        return readStringType(2);
    }

    private int readBytes(int len) throws Exception {

        int value = 0;

        for (int i = 0; i < len; i++) {

            int shiftCnt = (len - i) - 1;
            value += ((dataBytes[i + position] & 0xFF) << (8 * shiftCnt));
        }

        position += len;

        return value;
    }

    private int[] readSizeBytes(int len) throws Exception {

        if (len != 4) {
            if (apexLogger.ERROR) {
                apexLogger.out("ParserImpl.readSizeBytes ---------------------- read size is not 4 :::: size : " + len);
            }

            throw new IOException();
        }

        int value = 0;
        int[] size = new int[2];

        for (int i = 0; i < 2; i++) {

            int shiftCnt = (2 - i) - 1;
            value += ((dataBytes[i + position] & 0xFF) << (8 * shiftCnt));
        }

        size[0] = value;

        position += 2;

        value = 0;

        for (int i = 0; i < 2; i++) {

            int shiftCnt = (2 - i) - 1;
            value += ((dataBytes[i + position] & 0xFF) << (8 * shiftCnt));
        }

        size[1] = value;

        position += 2;

        return size;
    }

    private int[] readRectangleBytes(int len) throws Exception {

        if (len != 8) {
            if (apexLogger.ERROR) {
                apexLogger.out("ParserImpl.readRectangleBytes ---------------------- read size is not 8 :::: size : " + len);
            }

            throw new IOException();
        }

        int value = 0;
        int[] size = new int[4];

        for (int i = 0; i < 2; i++) {

            int shiftCnt = (2 - i) - 1;
            value += ((dataBytes[i + position] & 0xFF) << (8 * shiftCnt));
        }

        size[0] = value;

        position += 2;

        value = 0;

        for (int i = 0; i < 2; i++) {

            int shiftCnt = (2 - i) - 1;
            value += ((dataBytes[i + position] & 0xFF) << (8 * shiftCnt));
        }

        size[1] = value;

        position += 2;

        value = 0;

        for (int i = 0; i < 2; i++) {

            int shiftCnt = (2 - i) - 1;
            value += ((dataBytes[i + position] & 0xFF) << (8 * shiftCnt));
        }

        size[2] = value;

        position += 2;

        value = 0;

        for (int i = 0; i < 2; i++) {

            int shiftCnt = (2 - i) - 1;
            value += ((dataBytes[i + position] & 0xFF) << (8 * shiftCnt));
        }

        size[3] = value;

        position += 2;

        return size;
    }

    private String readStringType(int headerLen) throws Exception {

        int len = readBytes(headerLen);

        String value = new String(dataBytes, position, len);

        position += len;

        return value;
    }

    public void destroy() {
        cleanup();

        dataBytes = null;
    }

    protected abstract void cleanup();

    protected abstract void parse(EventDispatcher dispatcher) throws Exception;

}
