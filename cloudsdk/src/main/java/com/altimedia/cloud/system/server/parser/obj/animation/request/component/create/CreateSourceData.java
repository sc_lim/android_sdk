/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.server.parser.obj.animation.request.component.create;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CreateSourceData implements Parcelable {
    @Expose
    @SerializedName("width")
    private int width;
    @Expose
    @SerializedName("height")
    private int height;
    @Expose
    @SerializedName("color")
    private String color;
    @Expose
    @SerializedName("path")
    private String path;
    @Expose
    @SerializedName("imageType")
    private String imageType;
    @Expose
    @SerializedName("byteArray")
    private String byteArray;
    @Expose
    @SerializedName("spriteWidth")
    private int spriteWidth;
    @Expose
    @SerializedName("spriteHeight")
    private int spriteHeight;
    @Expose
    @SerializedName("spriteCount")
    private int spriteCount;
    @Expose
    @SerializedName("firstVisibleIndex")
    private int firstVisibleIndex;

    protected CreateSourceData(Parcel in) {
        width = in.readInt();
        height = in.readInt();
        color = in.readString();
        path = in.readString();
        imageType = in.readString();
        byteArray = in.readString();
        spriteWidth = in.readInt();
        spriteHeight = in.readInt();
        spriteCount = in.readInt();
        firstVisibleIndex = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(width);
        dest.writeInt(height);
        dest.writeString(color);
        dest.writeString(path);
        dest.writeString(imageType);
        dest.writeString(byteArray);
        dest.writeInt(spriteWidth);
        dest.writeInt(spriteHeight);
        dest.writeInt(spriteCount);
        dest.writeInt(firstVisibleIndex);
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getSpriteWidth() {
        return spriteWidth;
    }

    public int getSpriteHeight() {
        return spriteHeight;
    }

    public int getSpriteCount() {
        return spriteCount;
    }

    public int getFirstVisibleIndex() {
        return firstVisibleIndex;
    }

    public String getColor() {
        return color;
    }

    public String getPath() {
        return path;
    }

    public String getByteArray() {
        return byteArray;
    }

    public String getImageType() {
        return imageType;
    }

    public static final Creator<CreateSourceData> CREATOR = new Creator<CreateSourceData>() {
        @Override
        public CreateSourceData createFromParcel(Parcel in) {
            return new CreateSourceData(in);
        }

        @Override
        public CreateSourceData[] newArray(int size) {
            return new CreateSourceData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public String toString() {
        return "CreateSourceData{" +
                "width=" + width +
                ", height=" + height +
                ", color='" + color + '\'' +
                ", path='" + path + '\'' +
                ", imageType='" + imageType + '\'' +
                ", byteArray='" + byteArray + '\'' +
                ", spriteWidth=" + spriteWidth +
                ", spriteHeight=" + spriteHeight +
                ", firstVisibleIndex=" + firstVisibleIndex +
                '}';
    }
}
