
/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.server.parser.obj.animation.request.component.visible;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Data implements Parcelable
{

    @SerializedName("actionItem")
    @Expose
    private List<ActionItem> actionItem = null;
    public final static Creator<Data> CREATOR = new Creator<Data>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Data createFromParcel(Parcel in) {
            return new Data(in);
        }

        public Data[] newArray(int size) {
            return (new Data[size]);
        }

    }
    ;

    protected Data(Parcel in) {
        in.readList(this.actionItem, (ActionItem.class.getClassLoader()));
    }

    public Data() {
    }

    public List<ActionItem> getActionItem() {
        return actionItem;
    }

    public void setActionItem(List<ActionItem> actionItem) {
        this.actionItem = actionItem;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(actionItem);
    }

    public int describeContents() {
        return  0;
    }

}
