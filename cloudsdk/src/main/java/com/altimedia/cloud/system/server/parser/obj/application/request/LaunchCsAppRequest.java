/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.server.parser.obj.application.request;

import android.os.Parcel;
import android.os.Parcelable;

import com.altimedia.cloud.system.server.parser.obj.BaseRequest;
import com.altimedia.cloud.system.server.parser.obj.application.LaunchInfo;
import com.altimedia.cloud.system.server.parser.obj.application.Parameter;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mc.kim on 14,05,2020
 */
public class LaunchCsAppRequest extends BaseRequest {
    public enum LaunchType {
        CloudService("CS"),
        Native("Native"),
        Invalid("Invalid");
        private final String name;

        LaunchType(String name) {
            this.name = name;
        }

        public static LaunchType findByName(String name) {
            LaunchType[] types = values();
            for (LaunchType type : types) {
                if (type.name.equalsIgnoreCase(name)) {
                    return type;
                }
            }
            return Invalid;
        }
    }

    @Expose
    @SerializedName("data")
    private LaunchCspAppData launchCspAppData;

    protected LaunchCsAppRequest(Parcel in) {
        super(in);
        launchCspAppData = (LaunchCspAppData) in.readValue(LaunchCspAppData.class.getClassLoader());
    }

    public LaunchCspAppData getLaunchCspAppData() {
        return launchCspAppData;
    }

    public LaunchInfo getLaunchInfo() {
        return launchCspAppData.getLaunchInfo();
    }

    public LaunchType getLaunchType() {
        return LaunchType.findByName(launchCspAppData.appType);
    }

    @Override
    public String toString() {
        return "LaunchCsAppRequest{" +
                "launchCspAppData=" + launchCspAppData +
                '}';
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeValue(launchCspAppData);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<LaunchCsAppRequest> CREATOR = new Creator<LaunchCsAppRequest>() {
        @Override
        public LaunchCsAppRequest createFromParcel(Parcel in) {
            return new LaunchCsAppRequest(in);
        }

        @Override
        public LaunchCsAppRequest[] newArray(int size) {
            return new LaunchCsAppRequest[size];
        }
    };


    private static class LaunchCspAppData implements Parcelable {
        @Expose
        @SerializedName("appType")
        private String appType;
        @Expose
        @SerializedName("launchInfo")
        private LaunchInfo launchInfo;

        @Expose
        @SerializedName("parameter")
        private List<Parameter> parameters;


        public String getAppType() {
            return appType;
        }

        public LaunchInfo getLaunchInfo() {
            return launchInfo;
        }

        public List<Parameter> getParameters() {
            return parameters;
        }

        protected LaunchCspAppData(Parcel in) {
            appType = in.readString();
            launchInfo = in.readParcelable(LaunchInfo.class.getClassLoader());
            parameters = new ArrayList<>();
            in.readList(parameters, Parameter.class.getClassLoader());
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeValue(appType);
            dest.writeParcelable(launchInfo, flags);
            dest.writeList(parameters);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Parcelable.Creator<LaunchCsAppRequest.LaunchCspAppData> CREATOR = new Parcelable.Creator<LaunchCsAppRequest.LaunchCspAppData>() {
            @Override
            public LaunchCsAppRequest.LaunchCspAppData createFromParcel(Parcel in) {
                return new LaunchCsAppRequest.LaunchCspAppData(in);
            }

            @Override
            public LaunchCsAppRequest.LaunchCspAppData[] newArray(int size) {
                return new LaunchCsAppRequest.LaunchCspAppData[size];
            }
        };

    }
}
