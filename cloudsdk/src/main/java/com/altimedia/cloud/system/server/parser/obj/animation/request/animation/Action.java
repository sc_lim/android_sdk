/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.server.parser.obj.animation.request.animation;

/**
 * Created by mc.kim on 10,04,2020
 */
public enum Action {
    fade("fade"), scale("scale"), bounds("bounds"), sprite("sprite"), invalid("invalid");
    private final String name;

    Action(String name) {
        this.name = name;
    }

    public static Action findByName(String name) {
        Action[] actions = values();
        for (Action action : actions) {
            if (action.name.equalsIgnoreCase(name)) {
                return action;
            }
        }
        return invalid;
    }
}
