/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.server.parser.obj.animation.request.component.create;

import android.os.Parcel;
import android.os.Parcelable;

import com.altimedia.cloud.system.server.parser.obj.ComponentType;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ComponentData implements Parcelable {
    @Expose
    @SerializedName("type")
    private ComponentType type;

    @Expose
    @SerializedName("source")
    private CreateSourceData source;

    // (0/1)
    @Expose
    @SerializedName("visible")
    private int visible;

    // "(0/1 default 0)"
    @Expose
    @SerializedName("cache_mem")
    private int cache_mem;

    // (0/1/2 default 0)  0:disable 1:only 2.loadAtBoot."
    @Expose
    @SerializedName("cache_file")
    private int cache_file;

    protected ComponentData(Parcel in) {
        type = (ComponentType) in.readValue(ComponentType.class.getClassLoader());
        source = (CreateSourceData) in.readValue(CreateSourceData.class.getClassLoader());
        visible = in.readInt();
        cache_mem = in.readInt();
        cache_file = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(type);
        dest.writeValue(source);
        dest.writeInt(visible);
        dest.writeInt(cache_mem);
        dest.writeInt(cache_file);
    }

    public ComponentType getType() {
        return type;
    }

    public CreateSourceData getSource() {
        return source;
    }

    public boolean isVisible() {
        return (visible == 1);
    }

    public boolean isCache_mem() {
        return (cache_mem == 1);
    }

    public boolean isCache_file() {
        return (cache_file == 1);
    }

    public boolean isPreload_file() {
        return (cache_file == 2);
    }

    public static final Creator<ComponentData> CREATOR = new Creator<ComponentData>() {
        @Override
        public ComponentData createFromParcel(Parcel in) {
            return new ComponentData(in);
        }

        @Override
        public ComponentData[] newArray(int size) {
            return new ComponentData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public String toString() {
        return "ComponentData{" +
                "type=" + type +
                ", source=" + source +
                ", visible=" + visible +
                ", cache_mem=" + cache_mem +
                ", cache_file=" + cache_file +
                '}';
    }
}
