/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.server.parser.obj.animation.request.component.create;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mc.kim on 19,05,2020
 */
public class SpriteData implements Parcelable {
    @Expose
    @SerializedName("source")
    private String source;
    @Expose
    @SerializedName("spriteWidth")
    private int spriteWidth;
    @Expose
    @SerializedName("spriteHeight")
    private int spriteHeight;
    @Expose
    @SerializedName("firstVisibleIndex")
    private int firstVisibleIndex;


    protected SpriteData(Parcel in) {
        source = in.readString();
        spriteWidth = in.readInt();
        spriteHeight = in.readInt();
        firstVisibleIndex = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(source);
        dest.writeInt(spriteWidth);
        dest.writeInt(spriteHeight);
        dest.writeInt(firstVisibleIndex);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SpriteData> CREATOR = new Creator<SpriteData>() {
        @Override
        public SpriteData createFromParcel(Parcel in) {
            return new SpriteData(in);
        }

        @Override
        public SpriteData[] newArray(int size) {
            return new SpriteData[size];
        }
    };

    public String getSource() {
        return source;
    }

    public int getSpriteWidth() {
        return spriteWidth;
    }

    public int getSpriteHeight() {
        return spriteHeight;
    }

    public int getFirstVisibleIndex() {
        return firstVisibleIndex;
    }

    @Override
    public String toString() {
        return "SpriteData{" +
                "source='" + source + '\'' +
                ", spriteWidth=" + spriteWidth +
                ", spriteHeight=" + spriteHeight +
                ", firstVisibleIndex=" + firstVisibleIndex +
                '}';
    }
}
