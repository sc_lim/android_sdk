/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.server.parser.obj.application.def;

/**
 * Created by mc.kim on 13,05,2020
 */
public enum KeyCode {
    RED(0x1, "0x1"),
    GREEN(0x2, "0x2"),
    YELLOW(0x4, "0x4"),
    BLUE(0x8, "0x8"),
    NAVICATION(0x10, "0x10"),
    VCR(0x40, "0x40"),
    CHANNEL(0x80, "0x80"),
    INFO(0x100, "0x100"),
    MENU(0x200, "0x200"),
    NUMBERIC(0x400, "0x400"),
    CHAR(0x800, "0x800"),
    OTHER(0x1000, "0x1000"),
    BACK(0x1000, "0x1000"),
    INPUT(0x2000, "0x2000");

    final int CODE;
    final String CODE_VALUE;

    KeyCode(int CODE, String CODE_VALUE) {
        this.CODE = CODE;
        this.CODE_VALUE = CODE_VALUE;
    }

}
