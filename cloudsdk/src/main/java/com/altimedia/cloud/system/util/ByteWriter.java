/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.util;

/**
 * Created by IntelliJ IDEA.
 * User: gh.baek
 * Date: 2018. 5. 24
 * To change this template use File | Settings | File Templates.
 */
public class ByteWriter {
    private byte[] buffer;
    private int curPos;

    public void setBuffer(byte[] buffer) {
        this.buffer = buffer;
        curPos = 0;
    }

    public void writeValueAtPosition(int byteLen, int position, int value) {

        int shiftCnt = 0;

        for (int i = 0; i < byteLen; i++) {

            shiftCnt = (byteLen - i) - 1;
            shiftCnt = shiftCnt * 8;

            buffer[position++] = (byte) ((value >> shiftCnt) & 0xFF);
        }
    }

    public void writeValue(int byteLen, int value) {

        int shiftCnt = 0;

        for (int i = 0; i < byteLen; i++) {

            shiftCnt = (byteLen - i) - 1;
            shiftCnt = shiftCnt * 8;

            buffer[curPos++] = (byte) ((value >> shiftCnt) & 0xFF);
        }
    }

    public void writeBytes(byte[] values) {

        System.arraycopy(values, 0, buffer, curPos, values.length);

        curPos += values.length;
    }

    public int getPos() {
        return curPos;
    }

    public void destroy() {
        buffer = null;
    }
}

