
/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.server.parser.obj.animation.request.component.create;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SourceData implements Parcelable
{
    @SerializedName("source")
    @Expose
    private String source;
    public final static Creator<SourceData> CREATOR = new Creator<SourceData>() {


        @SuppressWarnings({
            "unchecked"
        })
        public SourceData createFromParcel(Parcel in) {
            return new SourceData(in);
        }

        public SourceData[] newArray(int size) {
            return (new SourceData[size]);
        }

    };

    protected SourceData(Parcel in) {
        this.source = ((String) in.readValue((String.class.getClassLoader())));
    }

    public SourceData() {
    }

    public String getSource() {
        return source;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(source);
    }

    public int describeContents() {
        return  0;
    }

}
