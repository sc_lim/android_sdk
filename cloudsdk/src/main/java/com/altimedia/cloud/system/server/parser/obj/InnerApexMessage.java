/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.system.server.parser.obj;

import com.altimedia.cloud.ApexMessage;

public class InnerApexMessage extends ApexMessage {
    BaseRequest baseRequest;

    public InnerApexMessage(BaseRequest baseRequest) {
        this.baseRequest = baseRequest;
        this.baseRequest.setProtocolPackageType(getProtocolPackage());
    }

    @Override
    public String getData() {
        return null;
    }

    @Override
    public String getProtocolMethod() {
        String protocolMethod = null;
        String protocolType = baseRequest.getProtocolType().getName();
        if (protocolType != null) {
            int index = protocolType.lastIndexOf(".");
            if (index > -1) {
                protocolMethod = protocolType.substring(index + 1);
            }
        }
        return protocolMethod;
    }

    @Override
    public String getProtocolPackage() {
        String protocolPackage = null;
        String protocolType = baseRequest.getProtocolType().getName();
        if (protocolType != null) {
            int index = protocolType.lastIndexOf(".");
            if (index > -1) {
                protocolPackage = protocolType.substring(0, index);
            }
        }
        return protocolPackage;
    }

    @Override
    public String getAppID() {
        return null;
    }

    public BaseRequest getBaseRequest() {
        return baseRequest;
    }

    @Override
    public String toString() {
        return "InnerApexMessage{" +
                " baseRequest=" + baseRequest +
                '}';
    }
}
