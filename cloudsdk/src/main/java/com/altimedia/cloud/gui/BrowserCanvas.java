/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.gui;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.Xfermode;

import com.altimedia.cloud.ApexCloudDoc;
import com.altimedia.cloud.constant.SourceTypeConstant;
import com.altimedia.cloud.system.util.apexLogger;


/**
 * Created by IntelliJ IDEA.
 * User: gh.baek
 * Date: 2018. 5. 28
 * To change this template use File | Settings | File Templates.
 */
final class BrowserCanvas {

    private Bitmap clearImage;

    private Bitmap memImage;
    private Canvas memCanvas;
    private Paint clearPaint, imgPaint;

    private int x, y, width, height;

    void create(ApexCloudDoc anyDoc) {

        x = anyDoc.getXPos();
        y = anyDoc.getYPos();

        width = anyDoc.getWidth();
        height = anyDoc.getHeight();

        memImage = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        memCanvas = new Canvas(memImage);

        clearPaint = new Paint();
        Xfermode xmode = new PorterDuffXfermode(PorterDuff.Mode.CLEAR);
        clearPaint.setXfermode(xmode);
        this.clearPaint.setXfermode(xmode);
        this.clearImage = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888);

        imgPaint = new Paint();
        Xfermode xmode2 = new PorterDuffXfermode(PorterDuff.Mode.SRC);
        imgPaint.setXfermode(xmode2);

        if (apexLogger.DEBUG) apexLogger.out(" BrowserCanvas.create   DONE !!!");
    }

    void updateImage(int sourceType, Bitmap image, int x, int y, int w, int h) {
        //if (apexLogger.DEBUG)
        //    apexLogger.out(" BrowserCanvas.updateImage ---- x: " + x + "   y: " + y + "   width: " + w + "   height: " + h);

        Rect rect = new Rect(x, y, x + w, y + h);


        switch (sourceType) {
            case SourceTypeConstant.FRAME_ENCODING_PNG:
            case SourceTypeConstant.FRAME_ENCODING_WEBP:
                //memCanvas.drawBitmap(this.clearImage, new Rect(0, 0, 1, 1), new Rect(x, y, x + w, y + h), this.clearPaint);
                break;
        }

        if (image != null) {
            memCanvas.drawBitmap(image, new Rect(0, 0, w, h), rect, imgPaint);
        }

    }

    void onDraw(Canvas canvas) {
        canvas.drawBitmap(memImage, 0.0f, 0.0f, null);
    }

    void flush() {
        if (memCanvas != null) {
            memCanvas.drawRect(x, y, x + width, y + height, clearPaint);
        }
    }

    void dispose() {
        //if (apexLogger.DEBUG) apexLogger.out(" BrowserCanvas.dispose !!!");

        memImage = null;
        memCanvas = null;
        clearImage = null;
        clearPaint = null;

        if (apexLogger.INFO) apexLogger.out("[7] BrowserCanvas.dispose ----------- END !!!");
    }

}
