/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.cloud.gui;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;

import com.altimedia.cloud.ApexCloudDoc;
import com.altimedia.cloud.system.util.ElapsedTime;
import com.altimedia.cloud.system.util.apexLogger;


/**
 * Created by IntelliJ IDEA.
 * User: gh.baek
 * Date: 2018. 5. 28
 * To change this template use File | Settings | File Templates.
 */
public final class GUIManager {
    private static GUIManager instance = new GUIManager();

    private BrowserCanvas browserCanvas;

    private static Bitmap rectImage;

    private GUIManager() {

    }
    public static GUIManager getInstance() {
        return instance;
    }

    public void initialize(ApexCloudDoc anyDoc) {
        browserCanvas = new BrowserCanvas();
        browserCanvas.create(anyDoc);
    }

    public boolean fireImageSource(int sourceType, byte[] source, int offset, int length, int x, int y, int w, int h) {

        //if (apexLogger.INFO)            apexLogger.out("[1] GUIManager.fireImageSource ----------- ING !!! " + Integer.toHexString(source[0]) +", "  + Integer.toHexString(source[1]) +", "+ Integer.toHexString(source[2]) +", "+ Integer.toHexString(source[3]) );

        ElapsedTime.getInstance().addSubSession("dec_s");
        try {
            rectImage = BitmapFactory.decodeByteArray(source, offset, length);
        } catch (Exception e) {
            if(apexLogger.ERROR){
                apexLogger.out("[Error] GUIManager.fireImageSource decodeByteArray failed.");
            }
            return false;
        }

        /*
        final int lnth= rectImage.getByteCount();
        ByteBuffer dst= ByteBuffer.allocate(lnth);
        rectImage.copyPixelsToBuffer( dst);
        byte[] barray=dst.array();

        updateImage = createFromColorChannels(barray, w, h);
        */

        /*
        switch (sourceType)
        {
            case SourceTypeConstant.FRAME_ENCODING_PNG:
                rectImage = BitmapFactory.decodeByteArray(source, offset, length);
                //Bitmap.CompressFormat.WEBP
                break;
            case SourceTypeConstant.FRAME_ENCODING_WEBP:
                byte[] decoded = libwebp.WebPDecodeARGB(source, length, w, h);
                int[] pixels = new int[decoded.length / 4];
                ByteBuffer.wrap(decoded).asIntBuffer().get(pixels);
                rectImage = Bitmap.createBitmap(pixels, w,h, Bitmap.Config.ARGB_8888);
                break;
        }
        */

        if (browserCanvas != null && rectImage != null) {
            //if (apexLogger.INFO) apexLogger.out("[2] GUIManager.fireImageSource ----------- ING !!! byte count :" + rectImage.getByteCount());
            browserCanvas.updateImage(sourceType, rectImage, x, y, w, h);
        } else {
            if(apexLogger.ERROR){
                apexLogger.out("[Error] GUIManager.fireImageSource ----------- ING !!! browserCanvas:" + browserCanvas + "rectImage: " + rectImage);
            }
            return false;
        }

        ElapsedTime.getInstance().addSubSession("dec_e");
        return true;
    }

    public void onDraw(Canvas canvas) {

        if (browserCanvas != null) {
            browserCanvas.onDraw(canvas);
        }
    }

    public void flush() {
        browserCanvas.flush();
    }

    public static void destroyInstance() {
        if (instance != null) {
            instance.destroy();
            instance = null;
        }
    }

    public void destroy() {
        if (browserCanvas != null) {
            browserCanvas.dispose();
            browserCanvas = null;
        }

        rectImage = null;

        if (apexLogger.INFO) apexLogger.out("[8] GUIManager.destroy ----------- END !!!");
    }
}
