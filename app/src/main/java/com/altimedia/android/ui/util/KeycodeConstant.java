/*
 *
 *  * Copyright (C) $year. Altimedia Corporation. All Rights Reserved.
 *  *
 *  * This software is the confidential and proprietary information
 *  * of Altimedia Corporation. You may not use or distribute
 *  * this software except in compliance with the terms and conditions
 *  * of any applicable license agreement in writing between Altimedia
 *  * Corporation and you.
 *
 */

package com.altimedia.android.ui.util;

/**
 * Created by IntelliJ IDEA.
 * User: gh.baek
 * Date: 2018. 05. 29
 * To change this template use File | Settings | File Templates.
 */
public interface KeycodeConstant {

    /**
     * key code type : android key code -> virtual key code
     */
    public static final short
            VK_RED = 0x01,
            VK_GREEN = 0x02,
            VK_YELLOW = 0x04,
            VK_CANCEL = 0x03,
            VK_BACK = 0x08,
            VK_CLEAR = 0x0C,
            VK_RETURN = 0x0A, //0x0D,
            VK_ESCAPE = 0x1B,
            VK_SPACE = 0x20,
            VK_HOME = 0x24,
            VK_LEFT = 0x25,
            VK_UP = 0x26,
            VK_RIGHT = 0x27,
            VK_DOWN = 0x28,
            VK_DELETE = 0x2E,
            VK_TAB = 0x09,
            VK_0 = 0x30,
            VK_1 = 0x31,
            VK_2 = 0x32,
            VK_3 = 0x33,
            VK_4 = 0x34,
            VK_5 = 0x35,
            VK_6 = 0x36,
            VK_7 = 0x37,
            VK_8 = 0x38,
            VK_9 = 0x39;

    /**
     * key code type : android key code
     */
    public static final short
            KEYCODE_BACK = 0x25F,
            KEYCODE_ESCAPE = 0x259;


}
