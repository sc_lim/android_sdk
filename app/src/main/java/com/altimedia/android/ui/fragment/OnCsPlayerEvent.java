/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.android.ui.fragment;

import com.altimedia.cloud.system.server.parser.obj.application.LaunchInfo;

/**
 * Created by mc.kim on 17,05,2020
 */
public interface OnCsPlayerEvent {
    boolean requestStart(LaunchInfo info);

    boolean requestPlayback(MenuWindowView menuWindowView, String fileName);

    void requestStopPlayback();

    void needToReload();

    void onKeyPressed(int keyCode);

    void onStartCloudService(MenuWindowView url);

    void onDestroyedCloudService(MenuWindowView url);

    void notifySuccess(MenuWindowView menuWindowView);

    void notifyNetworkError(MenuWindowView menuWindowView, int errorCode, String errorMessage);

    void notifyError(MenuWindowView menuWindowView, int errorCode, String errorMessage);

    void notifyFrameUpdate(MenuWindowView menuWindowView, int x, int y, int w, int h);
}
