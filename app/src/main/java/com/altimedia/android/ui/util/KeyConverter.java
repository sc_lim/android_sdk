/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.android.ui.util;

import android.view.KeyEvent;

public class KeyConverter {

    public static short MapVirtualKey(int code)
    {
        short vk_code = 0x00;

        switch(code)
        {
            case KeyEvent.KEYCODE_DPAD_LEFT:
                vk_code = KeycodeConstant.VK_LEFT;
                break;
            case KeyEvent.KEYCODE_DPAD_RIGHT:
                vk_code = KeycodeConstant.VK_RIGHT;
                break;
            case KeyEvent.KEYCODE_DPAD_UP:
                vk_code = KeycodeConstant.VK_UP;
                break;
            case KeyEvent.KEYCODE_DPAD_DOWN:
                vk_code = KeycodeConstant.VK_DOWN;
                break;
            case KeyEvent.KEYCODE_DPAD_CENTER:
            case KeyEvent.KEYCODE_ENTER:
                vk_code = KeycodeConstant.VK_RETURN;
                break;
            case KeyEvent.KEYCODE_HOME:
                vk_code = KeycodeConstant.VK_HOME;
                break;
            case KeyEvent.KEYCODE_ESCAPE:
            case KeycodeConstant.KEYCODE_ESCAPE:
                vk_code = KeycodeConstant.VK_ESCAPE;
                break;
            case KeyEvent.KEYCODE_BACK:
            case KeycodeConstant.KEYCODE_BACK:
                vk_code = KeycodeConstant.VK_BACK;
                break;
            case KeyEvent.KEYCODE_TAB:
                vk_code = KeycodeConstant.VK_TAB;
                break;
            case KeyEvent.KEYCODE_0:
                vk_code = KeycodeConstant.VK_0;
                break;
            case KeyEvent.KEYCODE_1:
                vk_code = KeycodeConstant.VK_1;
                break;
            case KeyEvent.KEYCODE_2:
                vk_code = KeycodeConstant.VK_2;
                break;
            case KeyEvent.KEYCODE_3:
                vk_code = KeycodeConstant.VK_3;
                break;
            case KeyEvent.KEYCODE_4:
                vk_code = KeycodeConstant.VK_4;
                break;
            case KeyEvent.KEYCODE_5:
                vk_code = KeycodeConstant.VK_5;
                break;
            case KeyEvent.KEYCODE_6:
                vk_code = KeycodeConstant.VK_6;
                break;
            case KeyEvent.KEYCODE_7:
                vk_code = KeycodeConstant.VK_7;
                break;
            case KeyEvent.KEYCODE_8:
                vk_code = KeycodeConstant.VK_8;
                break;
            case KeyEvent.KEYCODE_9:
                vk_code = KeycodeConstant.VK_9;
                break;
        }

        return vk_code;
    }

}
