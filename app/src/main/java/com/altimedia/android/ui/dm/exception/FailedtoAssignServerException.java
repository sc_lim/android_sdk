/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.android.ui.dm.exception;

/**
 * Created by IntelliJ IDEA.
 * User: sungwoo
 * Date: 2011. 8. 11
 * To change this template use File | Settings | File Templates.
 */
public class FailedtoAssignServerException extends Exception{
}
