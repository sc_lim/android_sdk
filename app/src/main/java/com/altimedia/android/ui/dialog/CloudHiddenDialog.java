/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.android.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.altimedia.android.R;
import com.altimedia.android.ui.util.UserPreferenceManagerImp;
import com.altimedia.cloud.system.util.apexLogger;

/**
 * Created by mc.kim on 13,05,2020
 */
public class CloudHiddenDialog extends Dialog
        implements UserPreferenceManagerImp.UserPreferenceChangedListener {
    public final static String CLASS_NAME = CloudHiddenDialog.class.getName();
    private final String TAG = CloudHiddenDialog.class.getSimpleName();
    private UserPreferenceManagerImp managerImp = null;
    private TextView cloudIpAddress = null;
    private TextView cloudPort = null;
    private TextView appUrl = null;
    private TextView serverAccessValeTrue = null;
    private TextView serverAccessValeFalse = null;

    private CloudHiddenDialog(Context context) {
        super(context,R.style.transparent_dialog_borderless);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setStyle(STYLE_NO_FRAME, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);

        setContentView(R.layout.fragment_hidden);
        managerImp = new UserPreferenceManagerImp(getContext());
        managerImp.registUserPreferenceChangedListener(this);

        cloudIpAddress = findViewById(R.id.cloudIpAddress);
        cloudPort = findViewById(R.id.cloudPort);
        appUrl = findViewById(R.id.appUrl);

        serverAccessValeTrue = findViewById(R.id.serverAccessValeTrue);
        serverAccessValeFalse = findViewById(R.id.serverAccessValeFalse);
        initCloudInfo(cloudIpAddress, cloudPort, appUrl);
        initServerAccessValue(serverAccessValeTrue, serverAccessValeFalse);
    }


    @Override
    public void dismiss() {
        super.dismiss();
        managerImp.unRegistUserPreferenceChangedListener(this);
    }


    public static CloudHiddenDialog newInstance(Context context) {
        CloudHiddenDialog dialogFragment = new CloudHiddenDialog(context);
        return dialogFragment;
    }

    @Override
    public boolean onKeyDown(int keyCode, @NonNull KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_ESCAPE || keyCode == KeyEvent.KEYCODE_TV || keyCode == KeyEvent.KEYCODE_BACK) {
            dismiss();
            return true;
        }
        if(apexLogger.DEBUG){
            Log.d(TAG, "onDialogKey : " + keyCode);
        }
        return onDialogKey(keyCode, event);
    }


    private boolean onDialogKey(int keyCode, KeyEvent event) {
        if (event.getAction() != KeyEvent.ACTION_DOWN) {
            return false;
        }

        switch (keyCode) {
            case KeyEvent.KEYCODE_3:
                managerImp.writeBoolean(UserPreferenceManagerImp.KEY_CLOUD_SERVICE_ALLOW, !managerImp.readBoolean(UserPreferenceManagerImp.KEY_CLOUD_SERVICE_ALLOW));
                return true;
        }
        return false;
    }


    private void initCloudInfo(TextView cloudIpAddress, TextView cloudPort, TextView appUrl) {

        cloudIpAddress.setText(managerImp.readString(UserPreferenceManagerImp.KEY_CLOUD_IP));
        cloudPort.setText(managerImp.readString(UserPreferenceManagerImp.KEY_CLOUD_PORT));
        appUrl.setText(managerImp.readString(UserPreferenceManagerImp.KEY_CLOUD_WEB_APP));
    }


    private void initServerAccessValue(TextView serverAccessValeTrue, TextView serverAccessValeFalse) {
        boolean isCSEnabled = managerImp.readBoolean(UserPreferenceManagerImp.KEY_CLOUD_SERVICE_ALLOW);
        initServerAccessValue(serverAccessValeTrue, serverAccessValeFalse, isCSEnabled);
    }

    private void initServerAccessValue(TextView serverAccessValeTrue, TextView serverAccessValeFalse, boolean isCSEnabled) {
        serverAccessValeTrue.setEnabled(isCSEnabled);
        serverAccessValeFalse.setEnabled(!isCSEnabled);
    }

    @Override
    public void notifyUpChanged(String key, Object value) {
        switch (key) {
            case UserPreferenceManagerImp.KEY_CLOUD_SERVICE_ALLOW:
                initServerAccessValue(serverAccessValeTrue, serverAccessValeFalse, (Boolean) value);
                break;
            case UserPreferenceManagerImp.KEY_CLOUD_IP:
                cloudIpAddress.setText(String.valueOf(value));
                break;
            case UserPreferenceManagerImp.KEY_CLOUD_PORT:
                cloudPort.setText(String.valueOf(value));
                break;
            case UserPreferenceManagerImp.KEY_CLOUD_WEB_APP:
                appUrl.setText(String.valueOf(value));
                break;
        }
    }
}
