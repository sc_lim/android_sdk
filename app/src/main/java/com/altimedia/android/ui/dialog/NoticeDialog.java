/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.android.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.altimedia.android.R;
import com.altimedia.cloud.system.util.apexLogger;

/**
 * Created by mc.kim on 18,05,2020
 */
public class NoticeDialog extends Dialog {
    public final static String CLASS_NAME = NoticeDialog.class.getName();
    private final String TAG = NoticeDialog.class.getSimpleName();
    private static final String KEY_CODE = "code";
    private static final String KEY_TYPE = "type";
    private static final String KEY_MSG = "msg";

    private NoticeDialog(Context context) {
        super(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.fragment_notice);
        ColorDrawable drawable = new ColorDrawable(Color.BLACK);
        drawable.setAlpha(120);
        getWindow().setBackgroundDrawable(drawable);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialogInterface, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ESCAPE || keyCode == KeyEvent.KEYCODE_TV || keyCode == KeyEvent.KEYCODE_BACK) {
                    if (event.getAction() == KeyEvent.ACTION_UP) {
                        dismiss();
                    }
                    return true;
                }
                if (apexLogger.DEBUG) {
                    Log.d(TAG, "onDialogKey : " + keyCode);
                }
                return true;
            }
        });
        TextView titleView = findViewById(R.id.titleView);
        TextView msgView = findViewById(R.id.msgView);

        Bundle bundle = getArguments();
        int errorCode = bundle.getInt(KEY_CODE);
        String type = bundle.getString(KEY_TYPE);
        String errorMsg = bundle.getString(KEY_MSG);

        titleView.setText(type + " : " + errorCode);
        msgView.setText(errorMsg);
    }

    public static NoticeDialog newInstance(Context context, String type, int code, String msg) {
        NoticeDialog dialogFragment = new NoticeDialog(context);
        Bundle bundle = new Bundle();
        bundle.putString(KEY_TYPE, type);
        bundle.putInt(KEY_CODE, code);
        bundle.putString(KEY_MSG, msg);
        dialogFragment.setArguments(bundle);
        return dialogFragment;
    }


    private Bundle mBundle = new Bundle();

    public void setArguments(Bundle bundle) {
        mBundle = bundle;
    }

    private Bundle getArguments() {
        return mBundle;
    }

}
