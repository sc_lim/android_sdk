/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.android.ui.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by mc.kim on 12,02,2020
 */
public class UserPreferenceManagerImp extends SystemUserPreferenceManager {
    private final String TAG = UserPreferenceManagerImp.class.getSimpleName();

    public UserPreferenceManagerImp(Context context) {
        super(context);
    }


    @Override
    public String readString(String key) {
        String defaultValue = getDefaultString(key);
        return getString(PREF_USER, key, defaultValue);
    }

    @Override
    public boolean readBoolean(String key) {
        boolean defaultValue = getDefaultBoolean(key);
        Log.d(TAG,"readBoolean : "+key+", defaultValue : "+defaultValue);
        return getBoolean(PREF_USER, key, defaultValue);
    }

    @Override
    public int readInt(String key) {
        int defaultValue = getDefaultInt(key);
        return getInt(PREF_USER, key, defaultValue);
    }

    public String defaultWeb() {
        return DEFAULT_WEB;
    }

    public String defaultColorWeb(){
        return DEFAULT_COLOR_WEB;
    }

    public String defaultColorAppname(){
        return DEFAULT_COLOR_APPNAME;
    }

    @Override
    public long readLong(String key) {
        long defaultValue = getDefaultLong(key);
        return getLong(PREF_USER, key, defaultValue);
    }

    @Override
    public boolean writeString(String key, String value) {
        return putString(PREF_USER, key, value);
    }

    @Override
    public boolean writeBoolean(String key, boolean value) {
        return putBoolean(PREF_USER, key, value);
    }

    @Override
    public boolean writeInt(String key, int value) {
        return putInt(PREF_USER, key, value);
    }

    @Override
    public boolean writeLong(String key, long value) {
        return putLong(PREF_USER, key, value);
    }

    private ArrayList<UserPreferenceChangedListener> mCallbackList = new ArrayList<>();

    public void registUserPreferenceChangedListener(UserPreferenceChangedListener userPreferenceChangedListener) {
        if (mCallbackList.contains(userPreferenceChangedListener)) {
            return;
        }
        mCallbackList.add(userPreferenceChangedListener);
    }

    public void unRegistUserPreferenceChangedListener(UserPreferenceChangedListener userPreferenceChangedListener) {
        if (!mCallbackList.contains(userPreferenceChangedListener)) {
            return;
        }
        mCallbackList.remove(userPreferenceChangedListener);
    }

    private void fireUpChangedEvent(String key, Object value) {
        if (mCallbackList.isEmpty()) {
            return;
        }
        for (UserPreferenceChangedListener userPreferenceChangedListener : mCallbackList) {
            userPreferenceChangedListener.notifyUpChanged(key, value);
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Map<String, ?> map = sharedPreferences.getAll();
        Object o = map.get(key);
        fireUpChangedEvent(key, o);
    }


    public interface UserPreferenceChangedListener {
        void notifyUpChanged(String key, Object value);
    }
}
