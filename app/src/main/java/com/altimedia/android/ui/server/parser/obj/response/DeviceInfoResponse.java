/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.android.ui.server.parser.obj.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.altimedia.cloud.system.server.parser.obj.BaseResponse;
import com.altimedia.cloud.system.server.parser.obj.ProtocolType;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mc.kim on 13,05,2020
 */
public class DeviceInfoResponse extends BaseResponse {
    @Expose
    @SerializedName("data")
    private DeviceData data;

    protected DeviceInfoResponse(Parcel in) {
        super(in);
        data = (DeviceData) in.readValue(DeviceData.class.getClassLoader());
    }

    public DeviceInfoResponse(int requestId, ProtocolType protocolType) {
        super(requestId, protocolType);
    }

    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeValue(data);
    }

    public int describeContents() {
        return 0;
    }



    public static final Creator<DeviceInfoResponse> CREATOR = new Creator<DeviceInfoResponse>() {
        @Override
        public DeviceInfoResponse createFromParcel(Parcel in) {
            return new DeviceInfoResponse(in);
        }

        @Override
        public DeviceInfoResponse[] newArray(int size) {
            return new DeviceInfoResponse[size];
        }
    };

    public void setResponseData(String broadcaster,
                                String manufacturer,
                                String model,
                                String verHW,
                                String verSW,
                                String verPlatform,
                                String releaseDate) {

        DeviceData.DeviceInfo mDeviceInfo = new DeviceData.DeviceInfo()
                .withBroadcaster(broadcaster)
                .withManufacturer(manufacturer)
                .withModel(model)
                .withVerHW(verHW)
                .withVerSW(verSW)
                .withVerPlatform(verPlatform)
                .withReleaseDate(releaseDate);
        data = new DeviceData(mDeviceInfo);
    }


    private static class DeviceData implements Parcelable {
        @Expose
        @SerializedName("info")
        private DeviceInfo info;

        private static class DeviceInfo implements Parcelable {
            @SerializedName("broadcaster")
            @Expose
            public String broadcaster;
            @SerializedName("manufacturer")
            @Expose
            public String manufacturer;
            @SerializedName("model")
            @Expose
            public String model;
            @SerializedName("verHW")
            @Expose
            public String verHW;
            @SerializedName("verSW")
            @Expose
            public String verSW;
            @SerializedName("verPlatform")
            @Expose
            public String verPlatform;
            @SerializedName("releaseDate")
            @Expose
            public String releaseDate;
            public final static Parcelable.Creator<DeviceInfo> CREATOR = new Creator<DeviceInfo>() {


                @SuppressWarnings({
                        "unchecked"
                })
                public DeviceInfo createFromParcel(Parcel in) {
                    return new DeviceInfo(in);
                }

                public DeviceInfo[] newArray(int size) {
                    return (new DeviceInfo[size]);
                }

            };

            protected DeviceInfo(Parcel in) {
                this.broadcaster = ((String) in.readValue((String.class.getClassLoader())));
                this.manufacturer = ((String) in.readValue((String.class.getClassLoader())));
                this.model = ((String) in.readValue((String.class.getClassLoader())));
                this.verHW = ((String) in.readValue((String.class.getClassLoader())));
                this.verSW = ((String) in.readValue((String.class.getClassLoader())));
                this.verPlatform = ((String) in.readValue((String.class.getClassLoader())));
                this.releaseDate = ((String) in.readValue((String.class.getClassLoader())));
            }

            public DeviceInfo() {
            }

            public DeviceInfo withBroadcaster(String broadcaster) {
                this.broadcaster = broadcaster;
                return this;
            }

            public DeviceInfo withManufacturer(String manufacturer) {
                this.manufacturer = manufacturer;
                return this;
            }

            public DeviceInfo withModel(String model) {
                this.model = model;
                return this;
            }

            public DeviceInfo withVerHW(String verHW) {
                this.verHW = verHW;
                return this;
            }

            public DeviceInfo withVerSW(String verSW) {
                this.verSW = verSW;
                return this;
            }

            public DeviceInfo withVerPlatform(String verPlatform) {
                this.verPlatform = verPlatform;
                return this;
            }

            public DeviceInfo withReleaseDate(String releaseDate) {
                this.releaseDate = releaseDate;
                return this;
            }

            public void writeToParcel(Parcel dest, int flags) {
                dest.writeValue(broadcaster);
                dest.writeValue(manufacturer);
                dest.writeValue(model);
                dest.writeValue(verHW);
                dest.writeValue(verSW);
                dest.writeValue(verPlatform);
                dest.writeValue(releaseDate);
            }

            public int describeContents() {
                return 0;
            }
        }


        protected DeviceData(Parcel in) {
            info = in.readParcelable(DeviceInfo.class.getClassLoader());
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeParcelable(info, flags);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<DeviceData> CREATOR = new Creator<DeviceData>() {
            @Override
            public DeviceData createFromParcel(Parcel in) {
                return new DeviceData(in);
            }

            @Override
            public DeviceData[] newArray(int size) {
                return new DeviceData[size];
            }
        };

        public DeviceData(DeviceInfo info) {
            this.info = info;
        }
    }

}
