
/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.android.ui.dm.obj;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResolverInfo implements Parcelable
{

    @SerializedName("ipPort")
    @Expose
    public String ipPort;
    @SerializedName("data")
    @Expose
    public Data data;
    public final static Creator<ResolverInfo> CREATOR = new Creator<ResolverInfo>() {


        @SuppressWarnings({
            "unchecked"
        })
        public ResolverInfo createFromParcel(Parcel in) {
            return new ResolverInfo(in);
        }

        public ResolverInfo[] newArray(int size) {
            return (new ResolverInfo[size]);
        }

    }
    ;

    protected ResolverInfo(Parcel in) {
        this.ipPort = ((String) in.readValue((String.class.getClassLoader())));
        this.data = ((Data) in.readValue((Data.class.getClassLoader())));
    }

    public ResolverInfo() {
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(ipPort);
        dest.writeValue(data);
    }

    public int describeContents() {
        return  0;
    }

    @Override
    public String toString() {
        return "ResolverInfo{" +
                "ipPort='" + ipPort + '\'' +
                ", data=" + data +
                '}';
    }

    public String getIpPort() {
        return ipPort;
    }

    public void setIpPort(String ipPort) {
        this.ipPort = ipPort;
    }

    public Data getData() {
        return data;
    }
}
