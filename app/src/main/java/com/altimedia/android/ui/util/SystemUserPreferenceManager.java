/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.android.ui.util;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.DisplayMetrics;
import android.util.Log;

import java.util.HashMap;

import javax.crypto.BadPaddingException;

/**
 * Created by mc.kim on 11,02,2020
 */
abstract class SystemUserPreferenceManager implements SharedPreferences.OnSharedPreferenceChangeListener {
    protected final String PREF_USER = "USER_PREF";
    protected final String PREF_USER_PRIVATE = "USER_PREF_PRIVATE";

    public static final String KEY_CLOUD_SERVICE_ALLOW = "sf.cloud_service_allow";
    public static final String KEY_RESOLVER_DISABLED = "sf.resolver_disabled";
    public static final String KEY_CLOUD_SET_MANUAL = "sf.cs_manual";
    public static final String KEY_CLOUD_IP = "sf.cloud_ip";
    public static final String KEY_CLOUD_PORT = "sf.cloud_port";
    public static final String KEY_CLOUD_WEB_APP = "sf.cloud_web_app";

    //    public static final String DEFAULT_IP = "10.9.51.21";
    public static final String DEFAULT_IP = "10.70.0.25";
    //     public static final String DEFAULT_IP = "10.20.1.181";
//    private static final String DEFAULT_WEB_720 = "http://10.9.51.24:6080/launcher/720/";
//    private static final String DEFAULT_WEB_1080 = "http://10.9.51.24:6080/launcher/1080/";
//    private static final String DEFAULT_WEB_720 = "http://211.49.97.218:6080/launcher/720/apps/doctor_noani/";
//    private static final String DEFAULT_WEB_1080 = "http://211.49.97.218:6080/launcher/1080/apps/doctor/";
    private static final String DEFAULT_WEB_720 = "http://10.20.1.146:6080/LGHV/doctor_random_720/index.html";
    private static final String DEFAULT_WEB_1080 = "http://10.20.1.146:6080/LGHV/doctor_random_1080/index.html";
    //private static final String DEFAULT_WEB_1080 = "http://10.20.0.174:80/lghv/launcher/1080/";

    private static final String DEFAULT_COLOR_720 = "http://10.9.51.24:6080/launcher/720/apps/doctor/";
    private static final String DEFAULT_COLOR_1080 = "http://10.9.51.24:6080/launcher/1080/apps/doctor/";
    private static final String DEFAULT_COLOR_720_APPNAME = "good_doctor_720_random";
    private static final String DEFAULT_COLOR_1080_APPNAME = "good_doctor_1080_random";

    protected final String DEFAULT_WEB;
    protected final String DEFAULT_COLOR_WEB;
    protected final String DEFAULT_COLOR_APPNAME;
    protected final String DEFAULT_PORT_720 = "20001";
    protected final String DEFAULT_PORT_1080 = "20001";
    protected final String DEFAULT_PORT;

    protected HashMap<String, String> defaultValueMap = new HashMap<>();
    protected HashMap<String, String> defaultPortMap = new HashMap<>();


    private SharedPreferences mUserPreferences = null;
    private SharedPreferences mUserPrivatePreferences = null;

    private boolean is720View(Context context) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return (metrics.heightPixels == 720);
        //return true;
    }

    protected SystemUserPreferenceManager(Context context) {
        DEFAULT_PORT = is720View(context) ? DEFAULT_PORT_720 : DEFAULT_PORT_1080;
        DEFAULT_WEB = is720View(context) ? DEFAULT_WEB_720 : DEFAULT_WEB_1080;
        DEFAULT_COLOR_WEB = is720View(context) ? DEFAULT_COLOR_720 : DEFAULT_COLOR_1080;
        DEFAULT_COLOR_APPNAME= is720View(context) ? DEFAULT_COLOR_720_APPNAME : DEFAULT_COLOR_1080_APPNAME;
        mUserPreferences = context.getSharedPreferences(PREF_USER, Activity.MODE_PRIVATE);
        mUserPrivatePreferences = context.getSharedPreferences(PREF_USER_PRIVATE, Activity.MODE_PRIVATE);
        mUserPreferences.registerOnSharedPreferenceChangeListener(this);
        mUserPrivatePreferences.registerOnSharedPreferenceChangeListener(this);
        initDefaultValue();

//        defaultPortMap.put("http://10.9.51.24:6080/launcher/720/", "20001");  //메뉴 720
//        defaultPortMap.put("http://10.9.51.24:6080/launcher/1080/", "20002"); // 메뉴 1080
//        defaultPortMap.put("http://10.9.51.24:6080/launcher/720/apps/doctor_noani/", "20003"); //명의 720
//        defaultPortMap.put("http://10.9.51.24:6080/launcher/1080/apps/doctor/", "20004"); //명의 1080
    }

    public String getDefaultPort(String url){
        if(defaultPortMap.containsKey(url)){
            return defaultPortMap.get(url);
        }
        return DEFAULT_PORT;
    }

    private void initDefaultValue() {
        defaultValueMap.put(KEY_CLOUD_SERVICE_ALLOW, Boolean.toString(true));
        defaultValueMap.put(KEY_RESOLVER_DISABLED, Boolean.toString(true));
        defaultValueMap.put(KEY_CLOUD_IP, DEFAULT_IP);
        defaultValueMap.put(KEY_CLOUD_PORT, DEFAULT_PORT);
        defaultValueMap.put(KEY_CLOUD_WEB_APP, DEFAULT_WEB);
        defaultValueMap.put(KEY_CLOUD_SET_MANUAL, Boolean.toString(false));
    }

    public void setDefaultResolverSet(String value){
        Log.d("TAG","setDefaultResolverSet  : "+value);
        defaultValueMap.put(KEY_RESOLVER_DISABLED, value);
    }


    protected boolean checkDefaultValue(String key) {
        if (!defaultValueMap.containsKey(key)) {
            return false;
        }
        return true;
    }

    protected String getDefaultString(String key) {
        if (!defaultValueMap.containsKey(key)) {
            return "";
        } else {
            return defaultValueMap.get(key);
        }
    }

    protected boolean getDefaultBoolean(String key) {
        if (!defaultValueMap.containsKey(key)) {
            return false;
        } else {
            return defaultValueMap.get(key).equalsIgnoreCase("true");
        }
    }

    protected int getDefaultInt(String key) {
        if (!defaultValueMap.containsKey(key)) {
            return -1;
        } else {
            return Integer.parseInt(defaultValueMap.get(key));
        }
    }

    protected long getDefaultLong(String key) {
        if (!defaultValueMap.containsKey(key)) {
            return -1;
        } else {
            return Long.parseLong(defaultValueMap.get(key));
        }
    }

    private SharedPreferences getUserPreferences(String upType) {
        switch (upType) {
            case PREF_USER_PRIVATE:
                return mUserPrivatePreferences;
            default:
                return mUserPreferences;
        }
    }

    protected String getString(String upType, String key, String defaultValue) {
        SharedPreferences preferences = getUserPreferences(upType);

        return preferences.getString(key, defaultValue);
    }

    protected int getInt(String upType, String key, int defaultValue) {
        SharedPreferences preferences = getUserPreferences(upType);
        return preferences.getInt(key, defaultValue);
    }

    protected long getLong(String upType, String key, long defaultValue) {
        SharedPreferences preferences = getUserPreferences(upType);
        return preferences.getLong(key, defaultValue);
    }

    protected boolean getBoolean(String upType, String key, boolean defaultValue) {
        SharedPreferences preferences = getUserPreferences(upType);
        return preferences.getBoolean(key, defaultValue);
    }


    protected boolean putString(String upType, String key, String value) {
        SharedPreferences preferences = getUserPreferences(upType);
        SharedPreferences.Editor editor = preferences.edit().putString(key, value);
        return editor.commit();
    }

    protected boolean putInt(String upType, String key, int value) {
        SharedPreferences preferences = getUserPreferences(upType);
        SharedPreferences.Editor editor = preferences.edit().putInt(key, value);
        return editor.commit();
    }

    protected boolean putLong(String upType, String key, long value) {
        SharedPreferences preferences = getUserPreferences(upType);
        SharedPreferences.Editor editor = preferences.edit().putLong(key, value);
        return editor.commit();
    }

    protected boolean putBoolean(String upType, String key, boolean value) {
        SharedPreferences preferences = getUserPreferences(upType);
        SharedPreferences.Editor editor = preferences.edit().putBoolean(key, value);
        return editor.commit();
    }

    protected void registUpChangedListener() {

    }

    abstract public String readString(String key);

    abstract public boolean readBoolean(String key);

    abstract public int readInt(String key);

    abstract public long readLong(String key);

    abstract public boolean writeString(String key, String value);

    abstract public boolean writeBoolean(String key, boolean value);

    abstract public boolean writeInt(String key, int value);

    abstract public boolean writeLong(String key, long value);

    @Override
    abstract public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key);

}