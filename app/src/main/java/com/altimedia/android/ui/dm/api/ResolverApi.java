/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.android.ui.dm.api;

import com.altimedia.android.ui.dm.obj.ResolverInfo;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

/**
 * Created by mc.kim on 13,05,2020
 */
public interface ResolverApi {
    @Headers("Content-Type: application/json")
    @GET
    Call<ResolverInfo> getServerInfo(@Url String url, @QueryMap Map<String, String> options);
}
