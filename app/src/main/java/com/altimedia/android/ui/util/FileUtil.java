/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.android.ui.util;



import com.altimedia.cloud.system.util.apexLogger;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Properties;

/**
 * Created by ZioN on 2017-12-07.
 */
public class FileUtil {
    private static final String TAG = "FileUtil";

    public static final String EUCKR = "EUC-KR";
    public static final String ISO8859_1 = "ISO8859-1";
    public static final String UTF8 = "UTF-8";

    private static final String EMPTY = "";

    private static final int BUFFER_SIZE = 1024 * 2;


    // -----------------------------------------------------------------------------------------
    // File Properties
    // - readData, getValue

    public static Properties readProperties(File file) {

        if (file != null && file.exists() && file.isFile()) {
            Properties properties = new Properties();

            BufferedInputStream bis = null;
            try {
                bis = new BufferedInputStream(new FileInputStream(file));
                properties.load(bis);
            } catch (Exception e) {
                properties = null;
            } finally {
                if (bis != null) {
                    try {
                        bis.close();
                    } catch (IOException ioe) {
                        ioe.printStackTrace();
                    }
                }
            }
            return properties;
        }
        return null;

    }

    public static boolean writeProperties(File file, Properties properties) {
        if (file == null) {
            return false;
        }


        if (file.exists()) {
            file.delete();
        }

        FileOutputStream out = null;
        try {
            out = new FileOutputStream(file);
            properties.store(out, "EPG SETTING");
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException ignore) {
                }
            }
        }
        return false;
    }

    public static boolean getBooleanValue(Properties p, String key, boolean defaultValue) {
        boolean value = defaultValue;
        try {
            value = "true".equalsIgnoreCase(p.getProperty(key));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    public static boolean getBooleanValue(Properties p, String key, boolean defaultValue, String checkValue) {
        boolean value = defaultValue;
        try {
            value = p.getProperty(key).equals(checkValue);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }


    public static int getIntValue(Properties p, String key) {
        return getIntValue(p, key, -1);
    }


    public static int getIntValue(Properties p, String key, int defaultValue) {
        int value = defaultValue;
        try {
            value = Integer.parseInt(p.getProperty(key));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    public static long getLongValue(Properties p, String key) {
        return getLongValue(p, key, -1);
    }

    public static long getLongValue(Properties p, String key, long defaultValue) {
        long value = defaultValue;
        try {
            value = Long.parseLong(p.getProperty(key));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    public static String getStringValue(Properties p, String key) {
        return getStringValue(p, key, EMPTY);
    }

    public static String getStringValue(Properties p, String key, String defaultValue) {
        String value = defaultValue;
        try {
            value = p.getProperty(key);
        } catch (Exception e) {

            e.printStackTrace();
        }
        return value;
    }

    // -----------------------------------------------------------------------------------------
    // File Handling

    public static void deleteFile(File file) {
        if (file == null || !file.exists()) {
            return;
        }

        if (file.isDirectory()) {
            File[] f = file.listFiles();
            for (int i = 0; i < f.length; i++) {
                deleteFile(f[i]);
            }
        }
        boolean result = file.delete();
    }

    /**
     *
     * @param source
     * @param destination
     */
    public static void copy(File source, File destination) {
        if (source == null || source.length() == 0) {
            return;
        }

        byte[] buffer = new byte[BUFFER_SIZE];

        FileInputStream fis = null;
        FileOutputStream fos = null;

        int read;
        try {
            fis = new FileInputStream(source);

            File temp = new File(destination.getAbsolutePath() + ".temp");
            fos = new FileOutputStream(temp);

            while ((read = fis.read(buffer)) != -1) {
                fos.write(buffer, 0, read);
            }
            temp.renameTo(destination);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }
            }
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }
            }
        }


    }

    //------------------------
    // read/write file


    public static boolean writeFile(String filename, byte[] data) {
        if (filename == null || filename.length() == 0) {
            return false;
        }
        return writeFile(new File(filename), data);
    }


    public static boolean writeFile(File file, byte[] data) {
        if (file == null) {
            return false;
        }

        File temp = new File(file.getAbsolutePath() + ".temp");

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(temp);
            fos.write(data);

            if (file.exists()) {
                file.delete();
            }
            temp.renameTo(file);
            return true;

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }
            }

            if (temp.exists()) {
                temp.delete();
            }
        }
        return false;
    }


    public static boolean writeFile(String filename, String data) {
        if (filename == null || filename.length() == 0) {
            return false;
        }
        return writeFile(new File(filename), data);
    }


    public static boolean writeFile(File file, String data) {
        if (data == null) {
            return false;
        }

        try {
            byte[] bData = data.getBytes("UTF8");
            return writeFile(file, bData);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    ///===========================================================================================================
    public static String[] readStringArray(File file) {
        return readStringArray(file, UTF8);
    }

    public static String[] readStringArray(File file, String encoding) {
        ArrayList l = new ArrayList();

        BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(new FileInputStream(file), encoding));

            String s;
            while ((s = br.readLine()) != null) {
                l.add(s);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }
            }
        }

        String[] data = new String[l.size()];
        data = (String[]) l.toArray(data);
        return data;
    }
    ///===========================================================================================================

    public static String getFileData(String filename) {
        if (filename == null || filename.length() == 0) {
            return null;
        }
        return getFileData(new File(filename));
    }

    public static String getFileData(File file) {
        byte[] data = readFileData(file);
        if (data != null) {
            try {
                return new String(data, "UTF8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static byte[] readFileData(String filename) {
        if (filename == null || filename.length() == 0) {
            return null;
        }
        return readFileData(new File(filename));
    }


    public static byte[] readFileData(File file) {
        if (file == null) {
            return null;
        }


        byte[] data = null;

        FileInputStream fis = null;
        try {
            data = readData(new FileInputStream(file));
            return data;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }
            }
        }
        return data;
    }

    public static byte[] readData(InputStream stream) { // rename readData
        byte[] data = null;
        BufferedInputStream bis = null;
        try {
            bis = new BufferedInputStream(stream);

            ByteArrayOutputStream baos = new ByteArrayOutputStream(1024 * 4);

            byte[] buffer = new byte[BUFFER_SIZE];

            int read;
            while ((read = bis.read(buffer)) != -1) {
                baos.write(buffer, 0, read);
            }

            data = baos.toByteArray();
            return data;

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (bis != null) {
                try {
                    bis.close();
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }
            }
        }
        return data;
    }



    //========================================================================================

    public static void copyDirectory(File sourceLocation, File targetLocation) throws IOException {

        if (sourceLocation.isDirectory()) {



            if (!targetLocation.isDirectory()) {
                targetLocation.mkdir();
            } // if


            String[] children = sourceLocation.list();


            for (int i = 0; i < children.length; i++) {
                copyDirectory(new File(sourceLocation, children[i]), new File(targetLocation, children[i]));
            } // for
        } else {
            InputStream in = new FileInputStream(sourceLocation);
            OutputStream out = new FileOutputStream(targetLocation);

            byte[] buf = new byte[1024];
            int len = 0;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            } // while
            out.flush();
            in.close();
            out.close();
        } // else
    }// copyDirectory



    public static void main(String[] args) {

        File sourceLocation = new File("c:\\data");
        File targetLocation = new File("c:\\_data");

        try {
            copyDirectory(sourceLocation, targetLocation);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if(apexLogger.DEBUG){
            System.out.println("end");
        }

    }
}
