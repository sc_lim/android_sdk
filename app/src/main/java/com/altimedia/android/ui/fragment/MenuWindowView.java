/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.android.ui.fragment;

import android.content.Context;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;

import com.altimedia.cloud.ApexCloudEventListener;
import com.altimedia.cloud.ApexCloudManager;
import com.altimedia.cloud.impl.ApexCloudManagerImpl;
import com.altimedia.cloud.ApexMessage;
import com.altimedia.cloud.ApexMessageController;
import com.altimedia.cloud.ApexMessageHandler;
import com.altimedia.android.R;
import com.altimedia.android.ui.util.CloudConfiguration;
import com.altimedia.android.ui.util.CloudConnectionInfoManager;
import com.altimedia.android.ui.util.DeviceUtil;
import com.altimedia.android.ui.util.KeyConverter;
import com.altimedia.android.ui.util.ProtocolType;
import com.altimedia.android.ui.util.UserPreferenceManagerImp;
import com.altimedia.cloud.ApexCloudDoc;
import com.altimedia.cloud.ApexCloudSession;
import com.altimedia.cloud.constant.SourceTypeConstant;
import com.altimedia.cloud.system.server.parser.obj.application.LaunchInfo;
import com.altimedia.cloud.system.util.apexLogger;

import org.json.JSONObject;

import java.io.IOException;

import static android.content.Context.WINDOW_SERVICE;
import static com.altimedia.android.ui.util.ProtocolType.applicationManagerStart;

/**
 * Created by mc.kim on 17,05,2020
 */
public class MenuWindowView implements View.OnKeyListener {
    public static final String TAG = MenuWindowView.class.getSimpleName();
    public static final String CLASS_NAME = MenuWindowView.class.getName();
    private WindowManager.LayoutParams params = null;
    private WindowManager mWm = null;

    private final Context mContext;
    private ApexCloudSession mApexCloudSession = null;
    private UserPreferenceManagerImp mUserPreferenceManagerImp = null;
    private boolean needToCallReload = false;
    private boolean mIsRequestedPlay = false;
    private OnCsPlayerEvent mCspEvent;
    private View mParentView = null;
    private boolean requestedPause = false;
    private boolean isKeyBlocked = false;

    private ApexCloudEventListener mApexCloudEventListener = new ApexCloudEventListener() {
        @Override
        public void notifyConnected() {
            if (apexLogger.DEBUG) {
                Log.d(TAG, "notifyConnected");
            }
            mCspEvent.notifySuccess(MenuWindowView.this);
        }

        @Override
        public void notifyDisconnected() {
            if (apexLogger.DEBUG) {
                Log.d(TAG, "notifyDisconnected");
            }
        }

        @Override
        public void notifyFrameUpdate(int x, int y, int w, int h) {
            if (apexLogger.DEBUG) {
                Log.d(TAG, "notifyFrameUpdate : " + new Rect(x, y, w, h));

                mCspEvent.notifyFrameUpdate(MenuWindowView.this, x, y ,w, h);
            }
        }

        @Override
        public void notifyErrorEvent(final int code,final String msg) {
            if (mCspEvent != null) {
                final String errorMessage = "서버와의 접속이 종료되었습니다. \n셋탑 전원을 껏다가 켠 후에도 문제가 지속되면 \n고객센터로 연락 주시기 바랍니다.\n전화번호 : 1234 - 5678";
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        mCspEvent.notifyNetworkError(MenuWindowView.this, code, errorMessage);
                    }
                });
            }
        }
    };

    private ApexMessageHandler apexMessageHandler = new ApexMessageHandler() {
        String protocolPackage;
        String protocolMethod;
        String data;
        @Override
        public boolean notifyWebAppMessage(ApexMessage message) {
            protocolPackage = message.getProtocolPackage();
            protocolMethod = message.getProtocolMethod();
            data = message.getData();
            ProtocolType type = ProtocolType.findByPackageAndMethod(protocolPackage, protocolMethod);
            switch (type){
                case applicationManagerStart:
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            //mCspEvent.requestStart(info);
                        }
                    });
                    break;
                case applicationManagerStop:
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            dismiss(false);
                        }
                    });
                    break;
                case applicationManagerPause:
                    if (apexLogger.DEBUG) {
                        Log.d(TAG, "call requestPause");
                    }
                    requestedPause = true;
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                mApexCloudSession.pause();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    break;
                default:
                    return false;
            }
            return true;
        }
    };

    public MenuWindowView(Context context, LayoutInflater inflater) {
        mContext = context;
        if (mContext instanceof OnCsPlayerEvent) {
            mCspEvent = (OnCsPlayerEvent) mContext;
        }
        mUserPreferenceManagerImp = new UserPreferenceManagerImp(context);
        if (apexLogger.ERROR) {
            Log.d(TAG, "MenuWindowView: ");
        }
        readyFrame(mContext, inflater);
    }


    private void readyFrame(Context context, LayoutInflater inflater) {
        params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.TYPE_APPLICATION_ATTACHED_DIALOG,
                WindowManager.LayoutParams.FLAG_FULLSCREEN, PixelFormat.TRANSLUCENT);

        params.gravity = Gravity.RIGHT | Gravity.TOP;
        mWm = (WindowManager) context.getSystemService(WINDOW_SERVICE);
        mParentView = inflater.inflate(R.layout.dialog_main_csp, null);
        mParentView.setOnKeyListener(this);
        mWm.addView(mParentView, params);

        readyPlayer(mParentView);
    }

    public void start() {
        this.start(null);
//        CloudConfiguration coanfiguration = CloudConfiguration.getInstance(mUserPreferenceManagerImp, DeviceUtil.getModel());
//        configuration.init(null);
//        if (apexLogger.DEBUG) {
//            Log.d(TAG, "configuration.getWebAppUrl() : " + configuration.getWebAppUrl());
//        }
//        mICsPlayer.start(generateCloudDoc(mContext, configuration.getWebAppUrl(), configuration.getParam(), configuration.getCloudIp(), configuration.getCloudPort(), CloudConfiguration.DEFAULT_APP_NAME + getWindowHeight()));
//        mCspEvent.onStartCloudService(this);
    }

    private int getWindowHeight() {
        DisplayMetrics metrics = mContext.getResources().getDisplayMetrics();
        return metrics.heightPixels;
//        return 720;
    }

    public String getUrl() {
        return mApexCloudSession.getApexCloudDoc().getURL();
    }


    public void start(final LaunchInfo launchInfo) {
        final CloudConfiguration configuration = CloudConfiguration.getInstance(mUserPreferenceManagerImp, DeviceUtil.getModel());
        configuration.init(launchInfo);

        ApexMessageController.getInstance().addApexMessageHandler(apexMessageHandler, applicationManagerStart.getProtocolPackage());
        ApexCloudDoc apexCloudDoc = generateCloudDoc(mContext, configuration.getWebAppUrl(), configuration.getParam(),
                configuration.getCloudIp(), configuration.getCloudPort()
        );
        Log.d(TAG, "start apexCloudDoc before resolving : " + apexCloudDoc);
        String appName = launchInfo == null ? CloudConfiguration.DEFAULT_APP_NAME + getWindowHeight() : launchInfo.getAppName();
        CloudConnectionInfoManager cloudConnectionInfoManager = new CloudConnectionInfoManager(apexCloudDoc);
        cloudConnectionInfoManager.setUsingResolverFunction(
                !mUserPreferenceManagerImp.readBoolean(UserPreferenceManagerImp.KEY_RESOLVER_DISABLED));
        cloudConnectionInfoManager.setResolverAppName(appName);

        final String addtionalParam = configuration.getParam().toString();

        Log.d(TAG, "start : " + cloudConnectionInfoManager.getResolverAppName() + " use resolver : " + cloudConnectionInfoManager.isUsingResolverFunction());

        cloudConnectionInfoManager.initializeConnectionInfo(new CloudConnectionInfoManager.OnInfoCallback() {
            @Override
            public void onInitialized(ApexCloudDoc mAnyDoc) {

                if (mAnyDoc.getURL() != null) {
                    mAnyDoc.setURL(mAnyDoc.getURL() + "?param=" + addtionalParam);
                }
                Log.d(TAG, "start apexCloudDoc resolved : " + mAnyDoc);

                mApexCloudSession.setApexCloudDoc(mAnyDoc);
                mApexCloudSession.start();
            }

            @Override
            public void onFail(int resultCode) {

            }
        });
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                mApexCloudSession.start();
//            }
//        }).start();
        mCspEvent.onStartCloudService(MenuWindowView.this);
    }

    private void readyPlayer(View parentsView) {
        mApexCloudSession = initView(parentsView);
        mApexCloudSession.addApexCloudListener(mApexCloudEventListener);
    }

    public void setKeyBlocked(boolean keyBlocked) {
        isKeyBlocked = keyBlocked;
    }


    public void forcePause(boolean needToReconnect) {

        if (mApexCloudSession.isConnecting()) {
            return;
        }
        // history 확인.
        try {
            mApexCloudSession.pause();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (needToReconnect) {
            mApexCloudSession.resume(CloudConfiguration.getInstance().getResumeParam());
        }
    }

    public void dismiss(boolean forceStop) {
        isKeyBlocked = true;
        onStop(forceStop);
    }


    private ApexCloudDoc generateCloudDoc(Context context, String webApp, JSONObject param, String ip, int port) {
        Log.d(TAG, "generateCloudDoc");
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        ApexCloudDoc apexCloudDoc = new ApexCloudDoc();
//        needToCallReload = webApp.contains("senior");
        apexCloudDoc.setWidth(metrics.widthPixels);
        apexCloudDoc.setHeight(metrics.heightPixels);
        if (webApp != null) {
            Uri uri = Uri.parse(webApp);
            apexCloudDoc.setURL(uri.toString());
        }
        //apexCloudDoc.setParam(param);
        apexCloudDoc.setDeviceID(DeviceUtil.getMacAddress());
        apexCloudDoc.setCloudIP(ip);
        apexCloudDoc.setCloudPort(port);
        apexCloudDoc.setEncodingType(SourceTypeConstant.FRAME_ENCODING_WEBP);
        return apexCloudDoc;
    }

    private String generateAppId(){
        return System.currentTimeMillis() + "";
    }

    private String appId;

    private ApexCloudSession initView(View parentView) {
        ApexCloudSession mCsManager = ApexCloudManager.getInstance().create(generateAppId());
        appId = mCsManager.getAppID();
        mCsManager.setCloudView(parentView);
        return mCsManager;
    }

    private void onStop(final boolean forceStop) {
        if (mApexCloudSession != null) {
            ApexCloudManagerImpl.getInstance().destroy(appId);
            mCspEvent.onDestroyedCloudService(MenuWindowView.this);
            mApexCloudSession = null;
        }
        if (mIsRequestedPlay) {
            mCspEvent.requestStopPlayback();
        }
        if (needToCallReload) {
            mCspEvent.needToReload();
            if (forceStop) {
                mCspEvent.requestStart(null);
            }
        }
        mCspEvent = null;
        if (mWm != null)
            mWm.removeViewImmediate(mParentView);

        ApexMessageController.getInstance().removeApexMessageHandler(apexMessageHandler);
    }

    private boolean isNeedKeyConvert = true;
    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        if (isKeyBlocked) {
            return true;
        }
        if (event.getAction() == KeyEvent.ACTION_UP) {
            return true;
        }

        if (keyCode == KeyEvent.KEYCODE_BACK || keyCode == KeyEvent.KEYCODE_ESCAPE || keyCode == KeyEvent.KEYCODE_TV) {
            dismiss(true);
            return true;
        }

        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            mCspEvent.onKeyPressed(keyCode);
        }

        if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN ||
                keyCode == KeyEvent.KEYCODE_VOLUME_UP ||
                keyCode == KeyEvent.KEYCODE_VOLUME_MUTE ||
                keyCode == KeyEvent.KEYCODE_MUTE) {
            return false;
        }


        if (mApexCloudSession == null) {
            return false;
        }
        if (mApexCloudSession.isPaused()) {
            if (requestedPause) {
                requestedPause = false;
                return true;
            }
            mApexCloudSession.resume(CloudConfiguration.getInstance().getResumeParam());
            return true;
        } else {
            if (isNeedKeyConvert) {
                keyCode = KeyConverter.MapVirtualKey(keyCode);
            }
            mApexCloudSession.sendKeyEvent(event.getAction(), (short) keyCode);
            return true;
        }
    }

}
