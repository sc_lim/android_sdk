/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.android.ui.server.parser.obj.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.altimedia.cloud.system.server.parser.obj.BaseResponse;
import com.altimedia.cloud.system.server.parser.obj.ProtocolType;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mc.kim on 13,05,2020
 */
public class DeviceStatusResponse extends BaseResponse {
    @Expose
    @SerializedName("data")
    private StatusData data;

    protected DeviceStatusResponse(Parcel in) {
        super(in);
        data = (StatusData) in.readValue(StatusData.class.getClassLoader());
    }

    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeValue(data);
    }

    public DeviceStatusResponse(int requestId, ProtocolType protocolType) {
        super(requestId, protocolType);
    }

    public void setResponseData(String netType,
                                String isNetAvailable,
                                String ipAddress,
                                String macAddress,
                                String dnsAddress,
                                String gateway,
                                String subnetMask,
                                String apnName, String xmlCapabilities) {

        StatusData.Status.NetInfo netInfo = new StatusData.Status.NetInfo()
                .withNetType(0L)
                .withIsNetAvailable(true)
                .withIpAddress(ipAddress)
                .withMacAddress(macAddress)
                .withDnsAddress(dnsAddress)
                .withGateway(gateway)
                .withSubnetMask(subnetMask)
                .withApnName(apnName);

        StatusData.Status status = new StatusData.Status();
        status.withNetInfo(netInfo)
                .withXmlCapabilities("xmlCapabilities");


        data = new StatusData();
        data.withStatus(status);
    }

    public final static Parcelable.Creator<DeviceStatusResponse> CREATOR = new Creator<DeviceStatusResponse>() {

        @SuppressWarnings({
                "unchecked"
        })
        public DeviceStatusResponse createFromParcel(Parcel in) {
            return new DeviceStatusResponse(in);
        }

        public DeviceStatusResponse[] newArray(int size) {
            return (new DeviceStatusResponse[size]);
        }

    };


    public int describeContents() {
        return 0;
    }

    private static class StatusData implements Parcelable {

        @SerializedName("status")
        @Expose
        public Status status;
        public final static Parcelable.Creator<StatusData> CREATOR = new Creator<StatusData>() {


            @SuppressWarnings({
                    "unchecked"
            })
            public StatusData createFromParcel(Parcel in) {
                return new StatusData(in);
            }

            public StatusData[] newArray(int size) {
                return (new StatusData[size]);
            }

        };

        protected StatusData(Parcel in) {
            this.status = ((Status) in.readValue((Status.class.getClassLoader())));
        }

        public StatusData() {
        }

        public StatusData withStatus(Status status) {
            this.status = status;
            return this;
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeValue(status);
        }

        public int describeContents() {
            return 0;
        }


        private static class Status implements Parcelable {

            @SerializedName("netInfo")
            @Expose
            public NetInfo netInfo;
            @SerializedName("xmlCapabilities")
            @Expose
            public String xmlCapabilities;
            public final static Parcelable.Creator<Status> CREATOR = new Creator<Status>() {


                @SuppressWarnings({
                        "unchecked"
                })
                public Status createFromParcel(Parcel in) {
                    return new Status(in);
                }

                public Status[] newArray(int size) {
                    return (new Status[size]);
                }

            };

            protected Status(Parcel in) {
                this.netInfo = ((NetInfo) in.readValue((NetInfo.class.getClassLoader())));
                this.xmlCapabilities = ((String) in.readValue((String.class.getClassLoader())));
            }

            public Status() {
            }

            public Status withNetInfo(NetInfo netInfo) {
                this.netInfo = netInfo;
                return this;
            }

            public Status withXmlCapabilities(String xmlCapabilities) {
                this.xmlCapabilities = xmlCapabilities;
                return this;
            }

            public void writeToParcel(Parcel dest, int flags) {
                dest.writeValue(netInfo);
                dest.writeValue(xmlCapabilities);
            }

            public int describeContents() {
                return 0;
            }


            private static class NetInfo implements Parcelable {

                @SerializedName("netType")
                @Expose
                public Long netType;
                @SerializedName("isNetAvailable")
                @Expose
                public Boolean isNetAvailable;
                @SerializedName("ipAddress")
                @Expose
                public String ipAddress;
                @SerializedName("macAddress")
                @Expose
                public String macAddress;
                @SerializedName("dnsAddress")
                @Expose
                public String dnsAddress;
                @SerializedName("gateway")
                @Expose
                public String gateway;
                @SerializedName("subnetMask")
                @Expose
                public String subnetMask;
                @SerializedName("apnName")
                @Expose
                public String apnName;
                public final static Parcelable.Creator<NetInfo> CREATOR = new Creator<NetInfo>() {


                    @SuppressWarnings({
                            "unchecked"
                    })
                    public NetInfo createFromParcel(Parcel in) {
                        return new NetInfo(in);
                    }

                    public NetInfo[] newArray(int size) {
                        return (new NetInfo[size]);
                    }

                };

                protected NetInfo(Parcel in) {
                    this.netType = ((Long) in.readValue((Long.class.getClassLoader())));
                    this.isNetAvailable = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
                    this.ipAddress = ((String) in.readValue((String.class.getClassLoader())));
                    this.macAddress = ((String) in.readValue((String.class.getClassLoader())));
                    this.dnsAddress = ((String) in.readValue((String.class.getClassLoader())));
                    this.gateway = ((String) in.readValue((String.class.getClassLoader())));
                    this.subnetMask = ((String) in.readValue((String.class.getClassLoader())));
                    this.apnName = ((String) in.readValue((String.class.getClassLoader())));
                }

                public NetInfo() {
                }

                public NetInfo withNetType(Long netType) {
                    this.netType = netType;
                    return this;
                }

                public NetInfo withIsNetAvailable(Boolean isNetAvailable) {
                    this.isNetAvailable = isNetAvailable;
                    return this;
                }

                public NetInfo withIpAddress(String ipAddress) {
                    this.ipAddress = ipAddress;
                    return this;
                }

                public NetInfo withMacAddress(String macAddress) {
                    this.macAddress = macAddress;
                    return this;
                }

                public NetInfo withDnsAddress(String dnsAddress) {
                    this.dnsAddress = dnsAddress;
                    return this;
                }

                public NetInfo withGateway(String gateway) {
                    this.gateway = gateway;
                    return this;
                }

                public NetInfo withSubnetMask(String subnetMask) {
                    this.subnetMask = subnetMask;
                    return this;
                }

                public NetInfo withApnName(String apnName) {
                    this.apnName = apnName;
                    return this;
                }

                public void writeToParcel(Parcel dest, int flags) {
                    dest.writeValue(netType);
                    dest.writeValue(isNetAvailable);
                    dest.writeValue(ipAddress);
                    dest.writeValue(macAddress);
                    dest.writeValue(dnsAddress);
                    dest.writeValue(gateway);
                    dest.writeValue(subnetMask);
                    dest.writeValue(apnName);
                }

                public int describeContents() {
                    return 0;
                }

            }


        }


    }

}
