/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.android.ui.activity;

import android.app.ActivityManager;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Rect;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.altimedia.android.R;
import com.altimedia.android.ui.dialog.CloudHiddenDialogFragment;
import com.altimedia.android.ui.dialog.NoticeDialog;
import com.altimedia.android.ui.dialog.NoticeDialogFragment;
import com.altimedia.android.ui.fragment.MenuDialogFragment;
import com.altimedia.android.ui.fragment.MenuWindowView;
import com.altimedia.android.ui.fragment.OnCsPlayerEvent;
import com.altimedia.android.ui.util.CloudManagementHTTPD;
import com.altimedia.android.ui.util.UserPreferenceManagerImp;
import com.altimedia.cloud.comp.CacheUtil;
import com.altimedia.cloud.system.server.parser.obj.application.LaunchInfo;
import com.altimedia.cloud.system.util.ElapsedTime;
import com.altimedia.cloud.system.util.apexLogger;

import java.io.IOException;
import java.util.ArrayList;


public class LauncherActivity extends FragmentActivity
        implements OnCsPlayerEvent {
    private final String TAG = LauncherActivity.class.getSimpleName();
    private UserPreferenceManagerImp managerImp;
    private CloudManagementHTTPD mCloudManagerHTTPD = null;
    private MediaPlayer mMediaPlayer = null;
    private SurfaceCallback mSurfaceCallback = new SurfaceCallback();
    private Context mContext = null;
    private boolean isSubViewUse = true;
    private View mBg = null;
    private final int KICKED_ERROR_CODE = 1011;
    private final int PAUSED_ERROR_CODE = 1003;
    private boolean mIsRetryError = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        super.onCreate(savedInstanceState);
        mContext = this;
        managerImp = new UserPreferenceManagerImp(this);
        if (is720View(getApplicationContext())) {
            setContentView(R.layout.activity_launcher_playback);
            SurfaceView mPlayView = findViewById(R.id.playView);
            mPlayView.getHolder().addCallback(mSurfaceCallback);
        } else {
            setContentView(R.layout.activity_launcher);
        }
        mBg = findViewById(R.id.bg);

        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
        final int cacheSize = maxMemory / 8;
        int diskCacheSizeBytes = 1024 * 1024 * 100; // 100 MB

        CacheUtil.getInstance().init(cacheSize, diskCacheSizeBytes);

        memoryUsage();
    }


    private static class SurfaceCallback implements SurfaceHolder.Callback {
        private Surface mSurface = null;

        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            this.mSurface = holder.getSurface();
        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
            this.mSurface = null;
        }

        public Surface getSurface() {
            return mSurface;
        }
    }

    private void runServer() throws IOException {
        if (mCloudManagerHTTPD != null) {
            return;
        }
        mCloudManagerHTTPD = new CloudManagementHTTPD(managerImp);
        mCloudManagerHTTPD.start();
    }

    private void stopServer() {
        if (mCloudManagerHTTPD == null) {
            return;
        }
        mCloudManagerHTTPD.stop();
        mCloudManagerHTTPD = null;
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private boolean is720View(Context context) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return (metrics.heightPixels == 720);
    }

    @Nullable
    @Override
    public View onCreateView(@Nullable View parent, @NonNull String name, @NonNull Context context, @NonNull AttributeSet attrs) {


        return super.onCreateView(parent, name, context, attrs);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        new Thread(new Runnable() {
            @Override
            public void run() {
//                Glide.get(mContext).clearMemory();
//                CacheUtil.clearPreloadImageSource(mContext);
                CacheUtil.loadPreloadImageSource(mContext, null);

            }
        }).start();

        try {
            runServer();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopServer();
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }

    private void showMenu() {
        ElapsedTime.getInstance().addSubSession("showMenu isSubViewUse " + isSubViewUse);

        if (isSubViewUse) {

//            if (!managerImp.readBoolean(UserPreferenceManagerImp.KEY_CLOUD_SERVICE_ALLOW)) {
////                NoticeDialog noticeDialogFragment = NoticeDialog.newInstance(this, 999, "Cloud Service Application can not access");
////                noticeDialogFragment.show();
//                return;
//            }

            MenuWindowView windowView = new MenuWindowView(mContext, LayoutInflater.from(mContext));
            windowView.start();
        } else {


            if (!managerImp.readBoolean(UserPreferenceManagerImp.KEY_CLOUD_SERVICE_ALLOW)) {
                NoticeDialogFragment noticeDialogFragment = NoticeDialogFragment.newInstance(999, "Cloud Service Application can not access");
                noticeDialogFragment.show(getSupportFragmentManager(), NoticeDialogFragment.CLASS_NAME);
                return;
            }

            MenuDialogFragment dialogFragment = MenuDialogFragment.newInstance();
            dialogFragment.show(getSupportFragmentManager(), MenuDialogFragment.CLASS_NAME);
        }

    }

    private boolean isShowingMenu() {
        Fragment f = getSupportFragmentManager().findFragmentByTag(MenuDialogFragment.CLASS_NAME);
        if (f == null) {
            return false;
        }
        MenuDialogFragment mMenuDialogFragment = (MenuDialogFragment) f;
        return mMenuDialogFragment.getDialog().isShowing();
    }

    private Handler mKeyCodeHandler = new Handler(Looper.getMainLooper()) {
        private ArrayList<Integer> keyCodeList = new ArrayList<>();
        private final int HIDDEN_PATTERN_1 =
                KeyEvent.KEYCODE_0;
        private final int HIDDEN_PATTERN_2 =
                KeyEvent.KEYCODE_9;
        private final int[] HIDDEN_TRIGGER = new int[]{
                KeyEvent.KEYCODE_PROG_RED,
                KeyEvent.KEYCODE_PROG_RED,
                KeyEvent.KEYCODE_PROG_RED,
                KeyEvent.KEYCODE_PROG_RED,
                KeyEvent.KEYCODE_PROG_RED
        };

        private final int[] HIDDEN_TRIGGER_2 = new int[]{
                KeyEvent.KEYCODE_MEDIA_REWIND,
                KeyEvent.KEYCODE_MEDIA_REWIND,
                KeyEvent.KEYCODE_MEDIA_REWIND,
                KeyEvent.KEYCODE_MEDIA_REWIND,
                KeyEvent.KEYCODE_MEDIA_REWIND
        };

        private boolean mNeedToCheckLastTrigger = false;

        private void checkHiddenTrigger(int keyCode) {
            if (mNeedToCheckLastTrigger) {
                if (keyCode == HIDDEN_PATTERN_1 || keyCode == HIDDEN_PATTERN_2) {
                    showHiddenMenu(keyCode);
                }
                keyCodeList.clear();
                return;
            }
            keyCodeList.add(keyCode);
            boolean isMatched = true;
            for (int i = 0; i < keyCodeList.size(); i++) {
                if (HIDDEN_TRIGGER[i] != keyCodeList.get(i) && HIDDEN_TRIGGER_2[i] != keyCodeList.get(i)) {
                    isMatched = false;
                }
            }
            if (!isMatched) {
                keyCodeList.clear();
            } else {
                boolean needToCheckLastTrigger = keyCodeList.size() == HIDDEN_TRIGGER.length;
                mNeedToCheckLastTrigger = needToCheckLastTrigger;
            }
        }

        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            int what = msg.what;
            checkHiddenTrigger(what);
        }
    };

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (apexLogger.DEBUG) {
            Log.d(TAG, "onKeyDown | " + keyCode);
        }
        if (keyCode == KeyEvent.KEYCODE_TV || keyCode == KeyEvent.KEYCODE_BACK || keyCode == KeyEvent.KEYCODE_ESCAPE) {
            return true;
        }
        mKeyCodeHandler.sendEmptyMessage(keyCode);

        if (keyCode == KeyEvent.KEYCODE_DPAD_CENTER || keyCode == KeyEvent.KEYCODE_ENTER) {
            if (!isShowingMenu()) {
                showMenu();
                return true;
            }
        }
//        else if (keyCode == KeyEvent.KEYCODE_5) {
//            LaunchInfo info = new LaunchInfo(managerImp.defaultColorWeb(), managerImp.defaultColorAppname());
//            if (!isShowingMenu()) {
//                MenuWindowView windowView = new MenuWindowView(mContext, LayoutInflater.from(mContext));
//                windowView.startEmptyLauncher(info);
//                return true;
//            }
//        }


        return super.onKeyDown(keyCode, event);
    }

    private void showHiddenMenu(int keyCode) {
        if (keyCode == KeyEvent.KEYCODE_0 || keyCode == KeyEvent.KEYCODE_9) {
            CloudHiddenDialogFragment cloudHiddenDialogFragment = CloudHiddenDialogFragment.newInstance(keyCode);
            cloudHiddenDialogFragment.show(getSupportFragmentManager(), CloudHiddenDialogFragment.CLASS_NAME);
        }
    }

    public static Uri generateMovie(Context context, String resName) {
        String uri = "android.resource://" + context.getPackageName() + "/" + getRawResIdByName(context, resName);
        return Uri.parse(uri);
    }

    private static int getRawResIdByName(Context context, String resName) {
        String pkgName = context.getPackageName();
        // Return 0 if not found.
        int index = resName.indexOf(".");
        int resID = context.getResources().getIdentifier(resName.substring(0, index), "raw", pkgName);
        Log.i("AndroidVideoView", "Res Name: " + resName + "==> Res ID = " + resID);
        return resID;
    }

    @Override
    public boolean requestStart(final LaunchInfo info) {
        if (apexLogger.DEBUG) {
            Log.d(TAG, "call : requestStart");
        }

        if (isSubViewUse) {
            MenuWindowView menuWindowView = new MenuWindowView(mContext, LayoutInflater.from(mContext));
            menuWindowView.start(info);

        } else {
            MenuDialogFragment dialogFragment = MenuDialogFragment.newInstance(info);
            dialogFragment.show(getSupportFragmentManager(), MenuDialogFragment.CLASS_NAME);
        }
        return true;
    }

    @Override
    public boolean requestPlayback(final MenuWindowView menuWindowView, String fileName) {
        if (!is720View(getApplicationContext())) {
            return false;
        }
        if (mMediaPlayer != null && mMediaPlayer.isPlaying()) {
            return false;
        }

        menuWindowView.setKeyBlocked(true);
        mMediaPlayer = new MediaPlayer();
        mMediaPlayer.setSurface(mSurfaceCallback.getSurface());

        mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {

                menuWindowView.setKeyBlocked(false);
                mp.start();
            }
        });
        try {
            mMediaPlayer.setDataSource(this, generateMovie(this, fileName));
            mMediaPlayer.setLooping(true);
            mMediaPlayer.prepare();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void requestStopPlayback() {
        if (mMediaPlayer == null) {
            return;
        }
        mMediaPlayer.release();
        mMediaPlayer = null;
    }

    @Override
    public void needToReload() {
//        CacheUtil.getInstance().loadImageView(this);
    }

    public void memoryUsage() {
        if (apexLogger.Memory) {
            ActivityManager activityManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
            ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
            activityManager.getMemoryInfo(memoryInfo);

            final int byteUnit = 1024;

            Log.d(TAG, "memoryUsage memoryInfo.availMem " + (memoryInfo.availMem / (byteUnit * byteUnit)) + " MB");
            Log.d(TAG, "memoryUsage memoryInfo.totalMem " + (memoryInfo.totalMem / (byteUnit * byteUnit)) + " MB");
            Log.d(TAG, "memoryUsage memoryInfo.threshold " + (memoryInfo.threshold / (byteUnit * byteUnit)) + " MB");
            Log.d(TAG, "===============================memoryUsage=========================================");
        }
    }

    @Override
    public void onKeyPressed(int keyCode) {
        mKeyCodeHandler.sendEmptyMessage(keyCode);

//        ElapsedTime.getInstance().addSubSession("onKeyPressed : " + keyCode);

//        if (apexLogger.Memory) {
//            memoryUsage();
//        }
    }

    private ArrayList<String> urlQueue = new ArrayList<>();

    @Override
    public void onStartCloudService(MenuWindowView menuWindowView) {
        urlQueue.add(menuWindowView.getClass().getSimpleName());
        boolean isMenuApp = urlQueue.isEmpty();
        mBg.setVisibility(isMenuApp ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onDestroyedCloudService(MenuWindowView menuWindowView) {
        urlQueue.remove(menuWindowView.getClass().getSimpleName());
        boolean isMenuApp = urlQueue.isEmpty();
        mBg.setVisibility(isMenuApp ? View.VISIBLE : View.GONE);
    }

    @Override
    public void notifyNetworkError(final MenuWindowView menuWindowView, int errorCode, String errorMessage) {
        NoticeDialog noticeDialog = NoticeDialog.newInstance(this, "RSV", errorCode, errorMessage);
        noticeDialog.show();
        noticeDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                menuWindowView.dismiss(true);
            }
        });
    }

    @Override
    public void notifySuccess(MenuWindowView menuWindowView) {
        mIsRetryError = false;
    }

    @Override
    public void notifyError(final MenuWindowView menuWindowView, int errorCode, String errorMessage) {
        if (apexLogger.ERROR) {
            Log.d(TAG, "call notifyErrorEvent | code : " + errorCode + ", msg : " + errorMessage);
        }
        if (errorCode == PAUSED_ERROR_CODE
                && !mIsRetryError) {
            mIsRetryError = true;
            menuWindowView.forcePause(true);
            return;
        }
        if (mIsRetryError) {
            mIsRetryError = false;
        }
        if (isSubViewUse) {
            NoticeDialog noticeDialog = NoticeDialog.newInstance(this, "CLD", errorCode, errorMessage);
            noticeDialog.show();
            noticeDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    if (menuWindowView != null) {
                        menuWindowView.dismiss(true);
                    }
                }
            });
        } else {
            NoticeDialogFragment dialogFragment = NoticeDialogFragment.newInstance(errorCode, errorMessage);
            dialogFragment.show(getSupportFragmentManager(), NoticeDialogFragment.CLASS_NAME);
        }
    }

    @Override
    public void notifyFrameUpdate(MenuWindowView menuWindowView, int x, int y, int w, int h) {
        if (apexLogger.DEBUG) {
            Log.d(TAG, "call : notifyFrameUpdate : " + new Rect(x, y, w, h));
        }
        memoryUsage();

    }
}
