/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.android.ui.util;
/**
 * Created by mc.kim on 09,04,2020
 */
public enum ProtocolType {


    applicationManagerStart("ApplicationManager", "start"),
    applicationManagerStop("ApplicationManager", "stop"),
    applicationManagerPause("ApplicationManager", "pause"),

    other("other", "none");

    private final String protocolPackage;
    private final String protocolMethod;

    ProtocolType(String pkg, String method) {
        this.protocolPackage = pkg;
        this.protocolMethod = method;
    }

    public static ProtocolType findByPackageAndMethod(String pkg, String method) {
        ProtocolType[] types = values();
        for (ProtocolType type : types) {
            if (type.protocolPackage.equalsIgnoreCase(pkg) &&
            type.protocolMethod.equalsIgnoreCase(method)) {
                return type;
            }
        }
        return other;
    }

    public String getProtocolPackage() {
        return protocolPackage;
    }
    public String getProtocolMethod() {
        return protocolMethod;
    }
}
