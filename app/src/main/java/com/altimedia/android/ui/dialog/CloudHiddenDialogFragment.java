/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.android.ui.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.altimedia.android.R;
import com.altimedia.android.ui.util.CloudConfiguration;
import com.altimedia.android.ui.util.UserPreferenceManagerImp;
import com.altimedia.cloud.system.util.apexLogger;

import java.util.ArrayList;

/**
 * Created by mc.kim on 13,05,2020
 */
public class CloudHiddenDialogFragment extends DialogFragment
        implements UserPreferenceManagerImp.UserPreferenceChangedListener {
    public final static String CLASS_NAME = CloudHiddenDialogFragment.class.getName();
    private final String TAG = CloudHiddenDialogFragment.class.getSimpleName();
    private static final String KEY_PRIVATE_MENU = "keyPrivate";
    private UserPreferenceManagerImp managerImp = null;
    private boolean isManual = false;

    private CloudHiddenDialogFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        managerImp = new UserPreferenceManagerImp(inflater.getContext());
        managerImp.registUserPreferenceChangedListener(this);
        return inflater.inflate(R.layout.fragment_hidden, container, false);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        managerImp.unRegistUserPreferenceChangedListener(this);
    }

    public static CloudHiddenDialogFragment newInstance(int keyCode) {
        CloudHiddenDialogFragment dialogFragment = new CloudHiddenDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean(KEY_PRIVATE_MENU, keyCode == KeyEvent.KEYCODE_9);
        dialogFragment.setArguments(bundle);
        return dialogFragment;
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        final RelativeLayout root = new RelativeLayout(getActivity());
        root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(root);
        ColorDrawable drawable = new ColorDrawable(Color.BLACK);
        drawable.setAlpha(120);
        dialog.getWindow().setBackgroundDrawable(drawable);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialogInterface, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ESCAPE || keyCode == KeyEvent.KEYCODE_TV || keyCode == KeyEvent.KEYCODE_BACK) {
                    if(event.getAction() == KeyEvent.ACTION_UP){
                        dismiss();
                    }
                    return true;
                }
                if(apexLogger.DEBUG){
                    Log.d(TAG, "onDialogKey : " + keyCode);
                }
                return onDialogKey(keyCode, event);
            }
        });
        return dialog;
    }


    private boolean onDialogKey(int keyCode, KeyEvent event) {
        if (event.getAction() != KeyEvent.ACTION_DOWN) {
            return false;
        }
        mKeyCodeHandler.sendEmptyMessage(keyCode);
        switch (keyCode) {
            case KeyEvent.KEYCODE_3:
                managerImp.writeBoolean(UserPreferenceManagerImp.KEY_CLOUD_SERVICE_ALLOW, !managerImp.readBoolean(UserPreferenceManagerImp.KEY_CLOUD_SERVICE_ALLOW));
                return true;
            case KeyEvent.KEYCODE_4:
                managerImp.writeBoolean(UserPreferenceManagerImp.KEY_RESOLVER_DISABLED, !managerImp.readBoolean(UserPreferenceManagerImp.KEY_RESOLVER_DISABLED));
                return true;
        }
        return false;
    }

    private TextView cloudIpAddress = null;
    private TextView cloudPort = null;
    private TextView appUrl = null;
    private TextView serverAccessValeTrue = null;
    private TextView serverAccessValeFalse = null;


    private TextView resolverIpAddress = null;
    private TextView resolverPort = null;
    private TextView appID = null;
    private TextView version = null;
    private TextView resolverEnableTrue = null;
    private TextView resolverEnableFalse = null;
    private View resolverLayer = null;
    private View resolverInfoLayer = null;
    private TextView exitText = null;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        isManual = managerImp.readBoolean(UserPreferenceManagerImp.KEY_CLOUD_SET_MANUAL);
        cloudIpAddress = view.findViewById(R.id.cloudIpAddress);
        cloudPort = view.findViewById(R.id.cloudPort);
        appUrl = view.findViewById(R.id.appUrl);

        serverAccessValeTrue = view.findViewById(R.id.serverAccessValeTrue);
        serverAccessValeFalse = view.findViewById(R.id.serverAccessValeFalse);

        initCloudInfo(cloudIpAddress, cloudPort, appUrl);
        initServerAccessValue(serverAccessValeTrue, serverAccessValeFalse);

        exitText = view.findViewById(R.id.exitText);

        resolverLayer = view.findViewById(R.id.resolverLayer);
        resolverInfoLayer = view.findViewById(R.id.resolverInfo);
        resolverIpAddress = view.findViewById(R.id.resolverIpAddress);
        resolverPort = view.findViewById(R.id.resolverPort);
        appID = view.findViewById(R.id.appID);
        version = view.findViewById(R.id.version);

        resolverEnableTrue = view.findViewById(R.id.resolverEnableTrue);
        resolverEnableFalse = view.findViewById(R.id.resolverEnableFalse);


        boolean resolingMenuVisible = getArguments().getBoolean(KEY_PRIVATE_MENU, false);
        resolverLayer.setVisibility(resolingMenuVisible ? View.VISIBLE : View.INVISIBLE);

        initResolverInfo(resolverIpAddress, resolverPort, appID, version);
        initResolvingServerAccessValue(resolverEnableTrue, resolverEnableFalse);

        initExitText(exitText);

    }


    private void initExitText(TextView exitText) {

        boolean isResolving = !managerImp.readBoolean(UserPreferenceManagerImp.KEY_RESOLVER_DISABLED);
        initExitText(exitText, isResolving);
    }

    private void initExitText(TextView exitText, boolean isResolving) {
        resolverInfoLayer.setVisibility(isResolving ? View.VISIBLE : View.INVISIBLE);
        if (isResolving) {
            exitText.setText("나가기 : EXIT.");
        } else {
            exitText.setText("나가기 : EXIT");
        }
    }

    private void initCloudInfo(TextView cloudIpAddress, TextView cloudPort, TextView appUrl) {
        if (isManual) {
            cloudIpAddress.setText(managerImp.readString(UserPreferenceManagerImp.KEY_CLOUD_IP));
            cloudPort.setText(managerImp.readString(UserPreferenceManagerImp.KEY_CLOUD_PORT));
            appUrl.setText(managerImp.readString(UserPreferenceManagerImp.KEY_CLOUD_WEB_APP));
        } else {
            cloudIpAddress.setText("-1.-1.-1.-1");
            cloudPort.setText("-1");
            appUrl.setText("-1");
        }
    }

    private void initResolverInfo(TextView cloudIpAddress, TextView cloudPort, TextView appUr, TextView version) {
        Uri uri = Uri.parse(CloudConfiguration.DEFAULT_RESOLVER_URL);
        cloudIpAddress.setText(uri.getHost());
        cloudPort.setText(String.valueOf(uri.getPort()));
        appUr.setText(uri.getQueryParameter("app"));
        version.setText("1001");
    }


    private void initResolvingServerAccessValue(TextView serverAccessValeTrue, TextView serverAccessValeFalse) {
        boolean isResolvingDisable = managerImp.readBoolean(UserPreferenceManagerImp.KEY_RESOLVER_DISABLED);
        resolverInfoLayer.setVisibility(isResolvingDisable ? View.INVISIBLE : View.INVISIBLE);
        initResolvingServerAccessValue(serverAccessValeTrue, serverAccessValeFalse, isResolvingDisable);
    }

    private void initResolvingServerAccessValue(TextView serverAccessValeTrue, TextView serverAccessValeFalse, boolean isDisable) {
        serverAccessValeTrue.setEnabled(!isDisable);
        serverAccessValeFalse.setEnabled(isDisable);
    }


    private void initServerAccessValue(TextView serverAccessValeTrue, TextView serverAccessValeFalse) {
        boolean isCSEnabled = managerImp.readBoolean(UserPreferenceManagerImp.KEY_CLOUD_SERVICE_ALLOW);
        initServerAccessValue(serverAccessValeTrue, serverAccessValeFalse, isCSEnabled);
    }

    private void initServerAccessValue(TextView serverAccessValeTrue, TextView serverAccessValeFalse, boolean isCSEnabled) {
        serverAccessValeTrue.setEnabled(isCSEnabled);
        serverAccessValeFalse.setEnabled(!isCSEnabled);
    }

    @Override
    public void notifyUpChanged(String key, Object value) {
        switch (key) {
            case UserPreferenceManagerImp.KEY_CLOUD_SERVICE_ALLOW:
                initServerAccessValue(serverAccessValeTrue, serverAccessValeFalse, (Boolean) value);
                break;
            case UserPreferenceManagerImp.KEY_CLOUD_IP:
                cloudIpAddress.setText(String.valueOf(value));
                break;
            case UserPreferenceManagerImp.KEY_CLOUD_PORT:
                cloudPort.setText(String.valueOf(value));
                break;
            case UserPreferenceManagerImp.KEY_CLOUD_WEB_APP:
                appUrl.setText(String.valueOf(value));
                break;
            case UserPreferenceManagerImp.KEY_RESOLVER_DISABLED:
                initResolvingServerAccessValue(resolverEnableTrue, resolverEnableFalse, (Boolean) value);
                initExitText(exitText, !(Boolean) value);
                break;

            case UserPreferenceManagerImp.KEY_CLOUD_SET_MANUAL:
                isManual = (Boolean) value;
                initCloudInfo(cloudIpAddress, cloudPort, appUrl);
                break;
        }
    }


    private Handler mKeyCodeHandler = new Handler(Looper.getMainLooper()) {
        private ArrayList<Integer> keyCodeList = new ArrayList<>();
        private final int HIDDEN_PATTERN_1 =
                KeyEvent.KEYCODE_1;
        private final int[] HIDDEN_TRIGGER = new int[]{
                KeyEvent.KEYCODE_MUTE,
                KeyEvent.KEYCODE_MUTE
        };

        private final int[] HIDDEN_TRIGGER_2 = new int[]{
                KeyEvent.KEYCODE_VOLUME_MUTE,
                KeyEvent.KEYCODE_VOLUME_MUTE
        };

        private boolean mNeedToCheckLastTrigger = false;

        private void checkHiddenTrigger(int keyCode) {
            if (mNeedToCheckLastTrigger) {
                if (keyCode == HIDDEN_PATTERN_1) {
                    managerImp.writeBoolean(UserPreferenceManagerImp.KEY_RESOLVER_DISABLED, !managerImp.readBoolean(UserPreferenceManagerImp.KEY_RESOLVER_DISABLED));
                }
                keyCodeList.clear();
                return;
            }
            keyCodeList.add(keyCode);
            boolean isMatched = true;
            for (int i = 0; i < keyCodeList.size(); i++) {
                if (HIDDEN_TRIGGER[i] != keyCodeList.get(i) && HIDDEN_TRIGGER_2[i] != keyCodeList.get(i)) {
                    isMatched = false;
                }
            }
            if (!isMatched) {
                keyCodeList.clear();
            } else {
                boolean needToCheckLastTrigger = keyCodeList.size() == HIDDEN_TRIGGER.length;
                mNeedToCheckLastTrigger = needToCheckLastTrigger;
            }
        }

        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            int what = msg.what;
            checkHiddenTrigger(what);
        }
    };
}
