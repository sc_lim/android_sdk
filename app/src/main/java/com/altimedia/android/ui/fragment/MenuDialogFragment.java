/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.android.ui.fragment;


import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.altimedia.cloud.ApexCloudEventListener;
import com.altimedia.cloud.ApexCloudSession;
import com.altimedia.android.R;
import com.altimedia.android.ui.dialog.NoticeDialogFragment;
import com.altimedia.android.ui.util.CloudConfiguration;
import com.altimedia.android.ui.util.DeviceUtil;
import com.altimedia.android.ui.util.UserPreferenceManagerImp;
import com.altimedia.cloud.ApexCloudDoc;
import com.altimedia.cloud.constant.SourceTypeConstant;
import com.altimedia.cloud.system.server.parser.obj.application.LaunchInfo;
import com.altimedia.cloud.system.util.apexLogger;

import org.json.JSONObject;

/**
 * Created by mc.kim on 18,04,2020
 */
public class MenuDialogFragment extends DialogFragment implements ApexCloudEventListener {
    public static String CLASS_NAME = MenuDialogFragment.class.getName();
    private final String TAG = MenuDialogFragment.class.getSimpleName();
    private static final String KEY_LAUNCH_INFO = "launchInfo";
    private OnCsPlayerEvent mCspEvent = null;
    private boolean mIsRequestedPlay = false;
    private boolean needToCallReload = false;

    public static MenuDialogFragment newInstance() {
        MenuDialogFragment menuDialogFragment = new MenuDialogFragment();
        Bundle bundle = new Bundle();
        menuDialogFragment.setArguments(bundle);
        return menuDialogFragment;
    }

    public static MenuDialogFragment newInstance(LaunchInfo info) {
        MenuDialogFragment menuDialogFragment = new MenuDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(KEY_LAUNCH_INFO, info);
        menuDialogFragment.setArguments(bundle);
        return menuDialogFragment;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnCsPlayerEvent) {
            mCspEvent = (OnCsPlayerEvent) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (mIsRequestedPlay) {
            mCspEvent.requestStopPlayback();
        }
        if (needToCallReload) {
            mCspEvent.needToReload();
        }
        mCspEvent = null;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mIsRequestedPlay = false;
        needToCallReload = false;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final RelativeLayout root = new RelativeLayout(getActivity());
        root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        root.setBackground(new ColorDrawable(Color.TRANSPARENT));
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(root);
        dialog.getWindow().setDimAmount(0f);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);


        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ESCAPE || keyCode == KeyEvent.KEYCODE_TV) {
                    if (needToCallReload) {
                        mCspEvent.requestStart(null);
                    }
                    dismiss();
                    return true;
                }

                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    mCspEvent.onKeyPressed(keyCode);
                }

                if (apexCloudSession.isPaused()) {
                    //return apexCloudSession.resume(CloudConfiguration.getInstance().getResumeParam());
                    return true;
                } else {
                    apexCloudSession.sendKeyEvent(event.getAction(), (short) keyCode);
                    return true;
                }

            }
        });
        return dialog;
    }

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_launcher, container, false);
    }

    private ApexCloudSession apexCloudSession = null;
    private View parentsView = null;


    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);
    }

    private UserPreferenceManagerImp mUserPreferenceManagerImp = null;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        parentsView = view;
        mUserPreferenceManagerImp = new UserPreferenceManagerImp(getContext());
    }


    @Override
    public void onStart() {
        super.onStart();
        Bundle bundle = getArguments();
        LaunchInfo launchInfo = bundle.getParcelable(KEY_LAUNCH_INFO);
        CloudConfiguration configuration = CloudConfiguration.getInstance(mUserPreferenceManagerImp, DeviceUtil.getModel());
        configuration.init(launchInfo);
        //apexCloudSession = new ApexCloudSession(parentsView);
        if (apexLogger.DEBUG) {
            Log.d(TAG, "configuration.getWebAppUrl() : " + configuration.getWebAppUrl());
        }
        ApexCloudDoc apexCloudDoc = generateCloudDoc(parentsView.getContext(),
                configuration.getWebAppUrl(), configuration.getParam(),
                configuration.getCloudIp(), configuration.getCloudPort(),
                launchInfo.getAppName());

        apexCloudSession.setApexCloudDoc(apexCloudDoc);

        apexCloudSession.addApexCloudListener(this);

    }

    @Override
    public void onStop() {
        super.onStop();
        if (apexCloudSession != null) {
//            try {
//                apexCloudSession.destroy();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
            apexCloudSession = null;
        }
    }


    private ApexCloudDoc generateCloudDoc(Context context, String webApp, JSONObject param, String ip, int port, String appName) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        ApexCloudDoc apexCloudDoc = new ApexCloudDoc();
//        needToCallReload = webApp.contains("senior");
        apexCloudDoc.setWidth(metrics.widthPixels);
        apexCloudDoc.setHeight(metrics.heightPixels);
        Uri uri = Uri.parse(webApp);
        //apexCloudDoc.setParam(param);
        apexCloudDoc.setDeviceID(DeviceUtil.getMacAddress());
        apexCloudDoc.setURL(uri.toString());
        apexCloudDoc.setCloudIP(ip);
        apexCloudDoc.setCloudPort(port);
        apexCloudDoc.setEncodingType(SourceTypeConstant.FRAME_ENCODING_WEBP);
        return apexCloudDoc;
    }


    @Override
    public void onResume() {
        super.onResume();


    }

    @Override
    public void onPause() {
        super.onPause();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void notifyConnected() {
        if (apexLogger.DEBUG) {
            Log.d(TAG, "notifyConnected");
        }
    }

    @Override
    public void notifyDisconnected() {
        if (apexLogger.DEBUG) {
            Log.d(TAG, "notifyDisconnected");
        }
    }

    @Override
    public void notifyErrorEvent(int code, String msg) {
        if (apexLogger.ERROR) {
            Log.d(TAG, "call notifyErrorEvent | code : " + code + ", msg : " + msg);
        }
        if (code == 1003) {
            return;
        }
        NoticeDialogFragment dialogFragment = NoticeDialogFragment.newInstance(code, msg);
        dialogFragment.show(getFragmentManager(), NoticeDialogFragment.CLASS_NAME);
    }

    @Override
    public void notifyFrameUpdate(int x, int y, int w, int h) {

    }
}
