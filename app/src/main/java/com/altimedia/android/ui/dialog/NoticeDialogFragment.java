/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.android.ui.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.altimedia.android.R;
import com.altimedia.cloud.system.util.apexLogger;

/**
 * Created by mc.kim on 13,05,2020
 */
public class NoticeDialogFragment extends DialogFragment {
    public final static String CLASS_NAME = NoticeDialogFragment.class.getName();
    private final String TAG = NoticeDialogFragment.class.getSimpleName();
    private static final String KEY_CODE = "code";
    private static final String KEY_MSG = "msg";

    private NoticeDialogFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        final RelativeLayout root = new RelativeLayout(getActivity());
        root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(root);
        ColorDrawable drawable = new ColorDrawable(Color.BLACK);
        drawable.setAlpha(120);
        dialog.getWindow().setBackgroundDrawable(drawable);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialogInterface, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ESCAPE || keyCode == KeyEvent.KEYCODE_TV || keyCode == KeyEvent.KEYCODE_BACK) {
                    if(event.getAction() == KeyEvent.ACTION_UP){
                        dismiss();
                    }
                    return true;
                }
                if(apexLogger.DEBUG){
                    Log.d(TAG, "onDialogKey : " + keyCode);
                }
                return true;
            }
        });
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_notice, container, false);
    }

    public static NoticeDialogFragment newInstance(int code, String msg) {
        NoticeDialogFragment dialogFragment = new NoticeDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(KEY_CODE, code);
        bundle.putString(KEY_MSG, msg);
        dialogFragment.setArguments(bundle);
        return dialogFragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        TextView titleView = view.findViewById(R.id.titleView);
        TextView msgView = view.findViewById(R.id.msgView);

        Bundle bundle = getArguments();
        int errorCode = bundle.getInt(KEY_CODE);
        String errorMsg = bundle.getString(KEY_MSG);

        titleView.setText("ERROR : " + errorCode);
        msgView.setText(errorMsg);
    }
}
