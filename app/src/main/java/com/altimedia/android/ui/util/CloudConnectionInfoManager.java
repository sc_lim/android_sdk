/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.android.ui.util;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.altimedia.cloud.ApexCloudDoc;
import com.altimedia.cloud.system.util.ElapsedTime;
import com.altimedia.cloud.system.util.apexLogger;
import com.altimedia.android.ui.dm.OkHttpGenerator;
import com.altimedia.android.ui.dm.module.ResolverModule;
import com.altimedia.android.ui.dm.obj.ResolverInfo;
import com.altimedia.android.ui.dm.remote.ResolverRemote;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

/**
 * Created by mc.kim on 13,05,2020
 */
public class CloudConnectionInfoManager {
    private static final String TAG = CloudConnectionInfoManager.class.getSimpleName();

    public CloudConnectionInfoManager(ApexCloudDoc doc) {
        this.mAnyDoc = doc;
    }
    private boolean usingResolverFunction = false;

    private final ApexCloudDoc mAnyDoc;

    private String resolverAppName;

    public void setResolverAppName(String resolverAppName) {
        this.resolverAppName = resolverAppName;
    }

    public void setUsingResolverFunction(boolean useResolver) {
        this.usingResolverFunction = useResolver;
    }

    public boolean isUsingResolverFunction() {
        return usingResolverFunction;
    }

    public String getResolverAppName() {
        return resolverAppName;
    }

    public void initializeConnectionInfo(final OnInfoCallback onInfoCallback) {
        if (usingResolverFunction) {
            getServerInfoFromResolver(onInfoCallback);
        } else {
            onInfoCallback.onInitialized(mAnyDoc);
        }
    }

    private void getServerInfoFromResolver(final OnInfoCallback onInfoCallback) {
        OkHttpClient mOkHttpClient = OkHttpGenerator.getUnsafeOkHttpClient();
        ResolverModule resolverModule = new ResolverModule();
        Retrofit retrofit = resolverModule.provideResolverModule(mOkHttpClient,
                CloudConfiguration.DEFAULT_RESOLVER_URL + resolverAppName);
        ResolverRemote mResolverRemote = new ResolverRemote(resolverModule.provideResolverApi(retrofit));
        CloudInfoTask mCloudInfoTask = new CloudInfoTask(mResolverRemote, onInfoCallback,
                CloudConfiguration.DEFAULT_RESOLVER_URL + resolverAppName);
        mCloudInfoTask.execute(mAnyDoc);
    }

    public interface OnInfoCallback {
        void onInitialized(ApexCloudDoc mAnyDoc);

        void onFail(int resultCode);
    }

    private static class CloudInfoTask extends AsyncTask<ApexCloudDoc, Void, Object> {
        private final ResolverRemote mResolverRemote;
        private final OnInfoCallback mOnInfoCallback;
        private final String resolverUrl;
        public CloudInfoTask(ResolverRemote mResolverRemote, OnInfoCallback mOnInfoCallback, String resolverUrl) {
            this.mResolverRemote = mResolverRemote;
            this.mOnInfoCallback = mOnInfoCallback;
            this.resolverUrl = resolverUrl;
        }

        @Override
        protected Object doInBackground(ApexCloudDoc... apexCloudDocs) {
            ApexCloudDoc requestInfo = apexCloudDocs[0];
            Uri uri = Uri.parse(resolverUrl);
            final String appName = uri.getQueryParameter("app");
            Log.d(TAG, "last AppName : " + appName);

            ElapsedTime.getInstance().addSubSession("Proxy request start");

            //TODO : hasCacheResolveInfo true 경우의 코드를 Async 밖으로 빼기..
//            if (CacheUtil.getInstance().hasCacheResolveInfo(appName)) {
//                ResolverInfo mResolverInfo = CacheUtil.getInstance().getPreviousInfo(appName);
//                String ipPort = mResolverInfo.getIpPort();
//                String[] ipPortArray = ipPort.split(":");
//                if (apexLogger.DEBUG) {
//                    Log.d(TAG, "cached ipPort : " + ipPort);
//                    Log.d(TAG, "cached ip : " + ipPortArray[0]);
//                    Log.d(TAG, "cached port : " + ipPortArray[1]);
//                }
//                String url = mResolverInfo.getData().getUrl();
//                requestInfo.setCloudIP(ipPortArray[0]);
//                requestInfo.setCloudPort(Integer.parseInt(ipPortArray[1]));
//                requestInfo.setURL(url);
//                return requestInfo;
//            }

            try {
                Object info = mResolverRemote.getResolverInfo(resolverUrl);
                ElapsedTime.getInstance().addSubSession("Proxy received");

                if (info instanceof ResolverInfo) {
                    ResolverInfo mResolverInfo = (ResolverInfo) info;
                    String ipPort = mResolverInfo.getIpPort();
                    String[] ipPortArray = ipPort.split(":");
                    if (apexLogger.DEBUG) {
                        Log.d(TAG, "ipPort : " + ipPort);
                        Log.d(TAG, "ip : " + ipPortArray[0]);
                        Log.d(TAG, "port : " + ipPortArray[1]);
                    }
                    String url = mResolverInfo.getData().getUrl();
                    requestInfo.setCloudIP(ipPortArray[0]);
                    requestInfo.setCloudPort(Integer.parseInt(ipPortArray[1]));
                    requestInfo.setURL(url);
                    return requestInfo;
                } else if (info instanceof Integer) {
                    return (Integer) info;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }


        @Override
        protected void onPostExecute(Object resolverInfo) {
            super.onPostExecute(resolverInfo);

            ElapsedTime.getInstance().addSubSession("Proxy request end", "Proxy request start");

            if (resolverInfo == null) {
                mOnInfoCallback.onFail(404);
            } else {
                if (resolverInfo instanceof ApexCloudDoc) {
                    mOnInfoCallback.onInitialized((ApexCloudDoc) resolverInfo);
                } else {
                    mOnInfoCallback.onFail((Integer) resolverInfo);
                }
            }
        }
    }
}
