/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.android.ui.dm.remote;

import android.net.Uri;
import android.util.Log;

import com.altimedia.cloud.system.util.apexLogger;
import com.altimedia.android.ui.dm.api.ResolverApi;
import com.altimedia.android.ui.dm.obj.ResolverInfo;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by mc.kim on 13,05,2020
 */
public class ResolverRemote {

    private static final String TAG = ResolverRemote.class.getSimpleName();
    private ResolverApi mResolverApi;

    public ResolverRemote(ResolverApi resolverApi) {
        mResolverApi = resolverApi;
    }

    private HashMap<String, String> makeQueryMap(Uri uri) {
        HashMap<String, String> queryMap = new HashMap<>();
        Iterator<String> keys = uri.getQueryParameterNames().iterator();
        while (keys.hasNext()) {
            String queryKey = keys.next();
            queryMap.put(queryKey, uri.getQueryParameter(queryKey));
        }
        return queryMap;
    }

    public Object getResolverInfo(String url) throws IOException {
        Uri uri = Uri.parse(url);
        Call<ResolverInfo> call = mResolverApi.getServerInfo(uri.getPath(), makeQueryMap(uri));
        if (apexLogger.DEBUG) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<ResolverInfo> response = call.execute();
        if (apexLogger.DEBUG) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        if (response.isSuccessful()) {
            ResolverInfo mResolverInfo = response.body();
            return mResolverInfo;
        } else {
            return response.code();
        }
    }

}
