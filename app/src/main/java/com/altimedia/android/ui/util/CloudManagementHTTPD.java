/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.android.ui.util;

import fi.iki.elonen.NanoHTTPD;

/**
 * Created by mc.kim on 14,05,2020
 */
public class CloudManagementHTTPD extends NanoHTTPD {
    private final String TAG = CloudManagementHTTPD.class.getSimpleName();
    public static final int PORT = 8765;

    public static final String KEY_MANUAL = "manual";
    public static final String KEY_SERVER = "server";
    public static final String KEY_PORT = "port";
    public static final String KEY_APP_URL = "appurl";
    public static final String KEY_RIGHT = "right";
    private final UserPreferenceManagerImp mUserPreferenceManagerImp;

    public CloudManagementHTTPD(UserPreferenceManagerImp managerImp) {
        super(PORT);
        mUserPreferenceManagerImp = managerImp;
    }

    @Override
    public Response serve(IHTTPSession session) {
//        String uri = session.getParms();
//        Log.d(TAG,"uri : "+uri);
//        Uri responseUri = Uri.parse(uri);
        String manual = session.getParms().get(KEY_MANUAL);
        String server = session.getParms().get(KEY_SERVER);
        String port = session.getParms().get(KEY_PORT);
        String appurl = session.getParms().get(KEY_APP_URL);
        String right = session.getParms().get(KEY_RIGHT);

        setManualValue(manual);
        setServerValue(server);
        setPortValue(port);
        setAppUrlValue(appurl);
        setRightValue(right);

        String response = "HelloWorld";
        return newFixedLengthResponse(response);
    }

    private void setManualValue(String value) {
        if(value == null){
            return;
        }
        boolean prevValue = mUserPreferenceManagerImp.readBoolean(UserPreferenceManagerImp.KEY_CLOUD_SET_MANUAL);
        boolean currentValue = value.equalsIgnoreCase("1");
        if (prevValue != currentValue) {
            mUserPreferenceManagerImp.writeBoolean(UserPreferenceManagerImp.KEY_CLOUD_SET_MANUAL, currentValue);
        }
    }

    private void setServerValue(String value) {
        if (value == null) {
            return;
        }

        String prevIp = mUserPreferenceManagerImp.readString(UserPreferenceManagerImp.KEY_CLOUD_IP);
        if (!prevIp.equalsIgnoreCase(value)) {
            mUserPreferenceManagerImp.writeString(UserPreferenceManagerImp.KEY_CLOUD_IP, value);
        }
    }

    private void setPortValue(String value) {
        if (value == null) {
            return;
        }

        String prevPort = mUserPreferenceManagerImp.readString(UserPreferenceManagerImp.KEY_CLOUD_PORT);
        if (!prevPort.equalsIgnoreCase(value)) {
            mUserPreferenceManagerImp.writeString(UserPreferenceManagerImp.KEY_CLOUD_PORT, value);
        }
    }

    private void setAppUrlValue(String value) {
        if (value == null) {
            return;
        }

        String prevApp = mUserPreferenceManagerImp.readString(UserPreferenceManagerImp.KEY_CLOUD_WEB_APP);
        if (!prevApp.equalsIgnoreCase(value)) {
            mUserPreferenceManagerImp.writeString(UserPreferenceManagerImp.KEY_CLOUD_WEB_APP, value);
        }
    }

    private void setRightValue(String value) {
        if (value == null) {
            return;
        }

        boolean prevValue = mUserPreferenceManagerImp.readBoolean(UserPreferenceManagerImp.KEY_CLOUD_SERVICE_ALLOW);
        boolean currentValue = value.equalsIgnoreCase("1");
        if (prevValue != currentValue) {
            mUserPreferenceManagerImp.writeBoolean(UserPreferenceManagerImp.KEY_CLOUD_SERVICE_ALLOW, currentValue);
        }
    }


    @Override
    public void stop() {
        super.stop();
    }

}
