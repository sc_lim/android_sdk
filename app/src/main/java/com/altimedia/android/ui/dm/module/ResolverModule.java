/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.android.ui.dm.module;

import android.net.Uri;
import android.util.Log;

import com.altimedia.cloud.system.util.apexLogger;
import com.altimedia.android.ui.dm.api.ResolverApi;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by mc.kim on 13,05,2020
 */
public class ResolverModule {
    private final String TAG = ResolverModule.class.getSimpleName();
    public Retrofit provideResolverModule(OkHttpClient okHttpClient, String baseUrl) {
        Uri uri = Uri.parse(baseUrl);
        if(apexLogger.DEBUG){
            Log.d(TAG,"path"+uri.getPath());
            Log.d(TAG,"host : "+uri.getHost());
            Log.d(TAG,"getPort : "+uri.getPort());
            Log.d(TAG,"getEncodedPath : "+uri.getEncodedPath());
            Log.d(TAG,"getFragment : "+uri.getFragment());
            Log.d(TAG,"getScheme : "+uri.getScheme());
        }

        return new Retrofit.Builder()
                .baseUrl(uri.getScheme()+"://"+uri.getHost()+":"+uri.getPort())
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(getGson()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    private Gson getGson() {
        GsonBuilder builder = new GsonBuilder();
        return builder.create();
    }

    public ResolverApi provideResolverApi(Retrofit retrofit) {
        return retrofit.create(ResolverApi.class);
    }
}
