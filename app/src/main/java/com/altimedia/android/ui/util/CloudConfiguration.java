/*
 * Copyright (C) 2020. Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */
package com.altimedia.android.ui.util;


import android.util.Log;

import com.altimedia.cloud.system.server.parser.obj.application.LaunchInfo;
import com.altimedia.cloud.system.util.apexLogger;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class CloudConfiguration implements UserPreferenceManagerImp.UserPreferenceChangedListener {

    private static String TAG = CloudConfiguration.class.getSimpleName();
    private static CloudConfiguration ourInstance = null;

    public static final String DEFAULT_RESOLVER_URL = "http://10.9.51.24:10000/resolving/icui_testi?app=";
    public static  String DEFAULT_APP_NAME = "menu_app_";
    boolean isManual;
    private JSONObject jsonObject = new JSONObject();

    private String mediaPath = "/mnt/media/";
    private String configFile = "/config/cloudConfig.txt";
    private String mWebAppUrl;
    private String mCloudIp;
    private int mCloudPort;
    private final UserPreferenceManagerImp mUserPreferenceManagerImp;


    public static CloudConfiguration getInstance() {
        if (ourInstance == null) {
            throw new NullPointerException("instance null");
        }
        return ourInstance;
    }

    public static CloudConfiguration getInstance(UserPreferenceManagerImp userPreferenceManagerImp, String stbModel) {
        if (ourInstance == null) {
            ourInstance = new CloudConfiguration(userPreferenceManagerImp, stbModel);
        }
        return ourInstance;
    }

    private CloudConfiguration(UserPreferenceManagerImp userPreferenceManagerImp, String modelName) {
        mUserPreferenceManagerImp = userPreferenceManagerImp;
        mUserPreferenceManagerImp.registUserPreferenceChangedListener(this);
        isManual = userPreferenceManagerImp.readBoolean(UserPreferenceManagerImp.KEY_CLOUD_SET_MANUAL);
        try {
            jsonObject.put("stb", modelName);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getResumeParam() {
        String param = jsonObject.toString();
        return param + "&state=resume";
    }


    public void init(LaunchInfo launchInfo) {
        if (launchInfo != null) {
            mWebAppUrl = launchInfo.getUrl();
            mCloudIp = SystemUserPreferenceManager.DEFAULT_IP;
            mCloudPort = Integer.parseInt(mUserPreferenceManagerImp.getDefaultPort(mWebAppUrl));
        } else {
            if (isManual) {
                mWebAppUrl = mUserPreferenceManagerImp.readString(SystemUserPreferenceManager.KEY_CLOUD_WEB_APP);
                mCloudIp = mUserPreferenceManagerImp.readString(SystemUserPreferenceManager.KEY_CLOUD_IP);
                mCloudPort = Integer.parseInt(mUserPreferenceManagerImp.readString(SystemUserPreferenceManager.KEY_CLOUD_PORT));
            } else {
                String path = getUsbPath();
                if (!path.isEmpty()) {
                    Properties dmConfig = createDmConfig(path);
                    loadProperties(dmConfig);
                } else {
                    mWebAppUrl = mUserPreferenceManagerImp.defaultWeb();
                    mCloudIp = SystemUserPreferenceManager.DEFAULT_IP;
                    mCloudPort = Integer.parseInt(mUserPreferenceManagerImp.getDefaultPort(mWebAppUrl));
                }
            }
        }
        try {
            jsonObject.put("server", mCloudIp + ":" + mCloudPort);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setDefaultFromUsb(){
        String path = getUsbPath();
        if (!path.isEmpty()) {
            Properties dmConfig = createDmConfig(path);
            loadProperties(dmConfig);
        }
    }


    private String getUsbPath() {
        File f = new File(mediaPath);
        File[] files = f.listFiles();
        if (files == null) {
            return "";
        }
        for (File file : files) {
            String usbPath = file.getPath() + configFile;
            File configFile = new File(usbPath);
            if (configFile.exists()) {
                return usbPath;
            }
        }
        return "";
    }

    private Properties createDmConfig(String filePath) {
        File f = new File(filePath);

        Properties configProperties = new Properties();
        InputStream is = null;
        try {
            is = new FileInputStream(f);
            configProperties.load(is);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return configProperties;
    }

    private void loadProperties(Properties cloudConfig) {
        mWebAppUrl = FileUtil.getStringValue(cloudConfig, "WebAppUrl");
        mCloudIp = FileUtil.getStringValue(cloudConfig, "CloudIP");
        mCloudPort = FileUtil.getIntValue(cloudConfig, "CloudPort");
        String value = FileUtil.getStringValue(cloudConfig, "ResolverDisable","false");
        if(apexLogger.DEBUG){
            Log.d(TAG,"resolverProperty : "+value);
        }
        mUserPreferenceManagerImp.setDefaultResolverSet(value);
        String appName = FileUtil.getStringValue(cloudConfig, "AppName","menu_app_");
        if(apexLogger.DEBUG){
            Log.d(TAG,"appName property : "+appName);
        }
        DEFAULT_APP_NAME = appName;
    }

    public String getWebAppUrl() {
//        return mWebAppUrl;
        return mWebAppUrl;
    }

    public JSONObject getParam() {
        return jsonObject;
    }

    public String getCloudIp() {
        return mCloudIp;
    }

    public int getCloudPort() {
        return mCloudPort;
    }

    @Override
    public void notifyUpChanged(String key, Object value) {
        if (apexLogger.ERROR) {
            Log.d(TAG, "notifyUpChanged : " + key + ", value : " + String.valueOf(value));
        }
        switch (key) {
            case UserPreferenceManagerImp.KEY_CLOUD_SET_MANUAL:
                isManual = (Boolean) value;
                break;
        }
    }
}
